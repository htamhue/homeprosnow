<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ServiceController extends AppController {

    var $helpers = array('Text', 'HtmlFixer');
    var $components = array('Cookie');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index', 'web_service', 'graphic_design', 'email_marketing', 'seo', 'social_media_marketing');
//        if (!$this->Session->read('Auth.User.id')) {
//            $this->redirect(array('controller' => 'landing'));
//        }
    }

    function web_service() {
        $this->layout = 'service';

        $data['header_heading'] = 'Get a fully managed custom website for your business ';
        $data['header_sub_heading'] = 'We build, host and manage your website so you can focus on what matters most - growing your business!';
        $data['header_img'] = 'top_banner_right.png';
        $data['header_button_text'] = 'Free Sample Website';
        $this->set('header_data', $data);

        $this->loadModel('WebsiteTemplate');
        $template_lists = $this->WebsiteTemplate->find('all', array('order' => array('id ASC')));
        $this->set('template_lists', $template_lists);

        $this->set('view_header', 'landing');
    }

    function graphic_design() {
        $this->layout = 'service';

        $data['header_heading'] = 'Give your brand a unique identity with our creative Graphic Design services';
        $data['header_sub_heading'] = 'Choose our fully managed website package and get every design requirement for your business fulfilled in pixel perfect quality.';
        $data['header_img'] = 'graphic_page_top_banner.png';

        $this->set('header_data', $data);

        $this->set('view_header', 'landing');
    }

    function seo() {
        $this->layout = 'service';

        $data['header_heading'] = "Grow your visibility online with Homeprosnow's SEO Services";
        $data['header_sub_heading'] = "Increase your website's rank organically by pairing our SEO services with our fully managed website package.";
        $data['header_img'] = 'seo_top_banner.png';

        $this->set('header_data', $data);

        $this->set('view_header', 'landing');
    }

    function social_media_marketing() {
        $this->layout = 'service';

        $data['header_heading'] = "Grow your social presence to influence your customers' purchasing behaviour";
        $data['header_sub_heading'] = "Combine our winning social media marketing strategies with our fully managed website package to give your brand the edge it deserves.";
        $data['header_img'] = 'social_marketing.png';

        $this->set('header_data', $data);

        $this->set('view_header', 'landing');
    }

    function email_marketing() {
        $this->layout = 'service';

        $data['header_heading'] = "Boost your leads with Homeprosnow's Email Marketing Service";
        $data['header_sub_heading'] = "Getting a website from Homeprosnow also allows you to engage subscribers, nurture leads and send compelling emails to your customers.";
        $data['header_img'] = 'email_marketing_top_banner.png';

        $this->set('header_data', $data);

        $this->set('view_header', 'landing');
    }

}
