<?php

class AdminController extends AppController {

    var $name = 'Admin';
    var $uses = array("Admin");
    var $components = array('Auth', 'Session', 'Cookie', "Email");
    var $helpers = array('Form', 'Html', 'Flash');

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('admin_index');
        $this->loadModel('Admin');
    }

    function isAuthorized() {
        if ($this->Auth->user("role") == "admin") {
            return true;
        } else {
            return false;
        }
    }

    function admin_index() {
        $this->layout = "admin_login";
        $this->set("pageTitle", "Admin Login");

        if (!empty($this->data)) {
            $password = AuthComponent::password(trim($this->data['passwrd']));
            $user = $this->Admin->find('first', array('conditions' => array('Admin.email' => $this->data['email'], 'Admin.password' => $password)));
            //pr($this->data); exit;
            $user['Admin']['login_as'] = 'admin';
            $this->Auth->login($user['Admin']);
            $userId = $this->Auth->user('id');
            if (!empty($userId)) {
                if (!empty($this->data['remember']) && $this->data['remember'] == 'on') {
                    $this->Cookie->write('Email', $this->data['email'], false, '+2 weeks');
                    $this->Cookie->write('Password', $password, false, '+2 weeks');
                }
                $this->redirect(array("controller" => "admin", "action" => "dashboard"));
            } else {
                $this->Session->setFlash("Invalid Username / Password");
                $this->redirect(array("controller" => "admin", "action" => "index"));
            }
        }
    }

    function admin_dashboard() {
        $this->layout = "admin_pages";
        $this->set("pageTitle", "Dashboard");
    }

    function admin_change() {
        $this->layout = "admin_pages";
        $this->set("pageTitle", "Change Password");

        $id = $this->Auth->user("id");
        $username = $this->Auth->user("username");
        $user_email = $this->Auth->user("email");

        $this->set("username", $username);
        $this->set("user_email", $user_email);
        $this->set("admin_id", $id);

        if (!empty($this->data)) {
            $admin_user = $this->Admin->findById($id);
            $db_pass = $admin_user['Admin']['password'];

            if ($db_pass == md5($this->data['Admin']['current_password'])) {
                $this->data['Admin']['password'] = md5($this->data['Admin']['passwrd']);
                $this->Admin->save($this->data);
                $this->Session->setFlash('<div class="notification success png_bg"><a href="#" class="close"></a><div>Details updated succesfully</div></div>');
                $this->redirect(BASE_PATH_ADMIN . "/admin/change");
            } else {
                $this->Session->setFlash('<div class="notification error png_bg"><a href="#" class="close"></a><div>Current Password is incorrect</div></div>');
            }
        }
    }

    function admin_logout() {
        $url = $this->Auth->logout();
        $this->redirect(array('controller' => 'admin', 'action' => 'index'));
    }

    function admin_homepros_videos() {
        $this->layout = "admin_pages";
        $this->loadModel('HomeprosVideo');
        $homepros_videos = $this->HomeprosVideo->find('all');
        $this->set('homepros_videos', $homepros_videos);
    }

    function admin_edit_homepros_video() {
        $this->layout = "admin_pages";
        $this->loadModel('HomeprosVideo');
        $homepros_video = $this->HomeprosVideo->findById($this->request->params['named']['id']);
        $this->set('homepros_video', $homepros_video);

        if (!empty($this->data)) {
            $id = $this->data['id'];
            $homepros_video['video_info'] = $this->data['video_info'];
            $homepros_video['update_date'] = date('Y-m-d H:i:s');
            if ($this->data['video_code']) {
                $homepros_video['video_code'] = $this->data['video_code'];
            }

            $this->HomeprosVideo->id = $id;
            if ($this->HomeprosVideo->save($homepros_video)) {
                $this->redirect(array('controller' => 'admin', 'action' => 'admin_homepros_videos'));
            }
        }
    }

    function admin_add_homepros_file() {
        $this->layout = "admin_pages";
        $this->loadModel('HomeprosFile');
        if (!empty($_FILES['homepro_file']['name'])) {
            $pathinfo = pathinfo($_FILES['homepro_file']['name']);
            $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
            $path = 'uploaded_files/homepros_files/' . $file_name;
            if (file_exists($path)) {
                $file_name = time() . $file_name;
                $path = 'uploaded_files/homepros_files/' . $file_name;
            }
            if (move_uploaded_file($_FILES['homepro_file']['tmp_name'], $path)) {
                $HomeprosFile['HomeprosFile']['homepros_file'] = $file_name;
                $HomeprosFile['HomeprosFile']['file_info'] = $this->data['file_info'];
                $HomeprosFile['HomeprosFile']['create_date'] = date('Y-m-d H:i:s');
                if ($this->HomeprosFile->save($HomeprosFile)) {
                    $this->redirect(array('controller' => 'admin', 'action' => 'admin_homepros_files'));
                }
            } else {
//                $response['type'] = 'error';
            }
        }
    }

    function admin_delete_files() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('HomeprosFile');
        $id = $this->data['id'];
        $homepros_file = $this->HomeprosFile->findById($id);
        if ($this->HomeprosFile->delete($id)) {
            $path = 'uploaded_files/homepros_files/' . $homepros_file['HomeprosFile']['homepros_file'];
            if (file_exists($path)) {
                unlink($path);
            }
            echo 'success';
        }
    }

    function admin_category_list() {
        $this->layout = "admin_pages";
        $this->loadModel('Categorie');
        $categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('categories', $categories);
        $this->set('menu_select', 'category');
        $this->set('sub_menu_select', 'category_list');
    }

    function admin_add_category() {

        $this->layout = "admin_pages";
        $this->loadModel('Categorie');
        $this->set('menu_select', 'category');
        $this->set('sub_menu_select', 'category_list');
        if (!empty($this->data['category'])) {
            $category['Categorie']['category'] = $this->data['category'];
            if (!empty($_FILES['category_file']['name'])) {
                $pathinfo = pathinfo($_FILES['category_file']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $path = 'uploaded_files/category_img/' . $file_name;
                if (file_exists($path)) {
                    $file_name = time() . $file_name;
                    $path = 'uploaded_files/category_img/' . $file_name;
                }
                if (move_uploaded_file($_FILES['category_file']['tmp_name'], $path)) {
                    $category['Categorie']['category_image'] = $file_name;
                    $category['Categorie']['create_date'] = date('Y-m-d H:i:s');
                    if ($this->Categorie->save($category)) {
                        $this->redirect(array('controller' => 'admin', 'action' => 'admin_category_list'));
                    }
                } else {
//                $response['type'] = 'error';
                }
            }
        }
    }

    function admin_edit_category() {

        $this->layout = "admin_pages";
        $this->loadModel('Categorie');
        $this->set('menu_select', 'category');
        $this->set('sub_menu_select', 'category_list');

        if (!empty($this->data['category'])) {
            $category_id = $this->data['category_id'];
            $category = $this->Categorie->findById($category_id);
            $category['Categorie']['category'] = $this->data['category'];
            if (!empty($_FILES['category_file']['name'])) {
                $pathinfo = pathinfo($_FILES['category_file']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $path = 'uploaded_files/category_img/' . $file_name;
                $old_path = 'uploaded_files/category_img/' . $category['Categorie']['category_image'];
                if (file_exists($old_path)) {
                    unlink($old_path);
                }
                if (move_uploaded_file($_FILES['category_file']['tmp_name'], $path)) {
                    $category['Categorie']['category_image'] = $file_name;
                }
            }
            $category['Categorie']['update_date'] = date('Y-m-d H:i:s');
            $this->Categorie->id = $category_id;
            if ($this->Categorie->save($category)) {
                $this->redirect(array('controller' => 'admin', 'action' => 'admin_category_list'));
            }
        } else {
            $category_id = $this->request->params['named']['category_id'];
            $category = $this->Categorie->findById($category_id);
            $this->set('category', $category);
        }
    }

    function admin_delete_category() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('Categorie');
        $id = $this->data['id'];
        $category = $this->Categorie->findById($id);
        if ($this->Categorie->delete($id)) {
            $path = 'uploaded_files/category_img/' . $category['Categorie']['category_image'];
            if (file_exists($path)) {
                unlink($path);
            }
            echo 'success';
        }
    }

    function admin_website_template_list() {
        $this->layout = "admin_pages";
        $this->loadModel('WebsiteTemplate');
        $template_lists = $this->WebsiteTemplate->find('all', array('order' => array('id ASC')));
        $this->set('template_lists', $template_lists);
        $this->set('menu_select', 'website_template');
    }

    function admin_add_website_template() {

        $this->layout = "admin_pages";
        $this->loadModel('WebsiteTemplate');
        $this->set('menu_select', 'website_template');
        if (!empty($this->data['title'])) {
            $website_template['WebsiteTemplate']['title'] = $this->data['title'];
            if (!empty($_FILES['image']['name'])) {
                $pathinfo = pathinfo($_FILES['image']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $path = 'uploaded_files/website_templates/' . $file_name;
                if (file_exists($path)) {
                    $file_name = time() . $file_name;
                    $path = 'uploaded_files/website_templates/' . $file_name;
                }
                if (move_uploaded_file($_FILES['image']['tmp_name'], $path)) {
                    $website_template['WebsiteTemplate']['image'] = $file_name;
                    $website_template['WebsiteTemplate']['create_date'] = date('Y-m-d H:i:s');
                    if ($this->WebsiteTemplate->save($website_template)) {
                        $this->redirect(array('controller' => 'admin', 'action' => 'admin_website_template_list'));
                    }
                } else {
//                $response['type'] = 'error';
                }
            }
        }
    }

    function admin_edit_website_template() {

        $this->layout = "admin_pages";
        $this->loadModel('WebsiteTemplate');
        $this->set('menu_select', 'website_template');

        if (!empty($this->data['title'])) {
            $website_template_id = $this->data['website_template_id'];
            $website_template = $this->WebsiteTemplate->findById($website_template_id);
            $website_template['WebsiteTemplate']['title'] = $this->data['title'];
            if (!empty($_FILES['image']['name'])) {
                $pathinfo = pathinfo($_FILES['image']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $path = 'uploaded_files/website_templates/' . $file_name;
                $old_path = 'uploaded_files/website_templates/' . $website_template['WebsiteTemplate']['image'];
                if (file_exists($old_path)) {
                    unlink($old_path);
                }
                if (move_uploaded_file($_FILES['image']['tmp_name'], $path)) {
                    $website_template['WebsiteTemplate']['image'] = $file_name;
                }
            }
            $website_template['WebsiteTemplate']['update_date'] = date('Y-m-d H:i:s');
            $this->WebsiteTemplate->id = $website_template_id;
            if ($this->WebsiteTemplate->save($website_template)) {
                $this->redirect(array('controller' => 'admin', 'action' => 'admin_website_template_list'));
            }
        } else {
            $website_template_id = $this->request->params['named']['website_template_id'];
            $website_template = $this->WebsiteTemplate->findById($website_template_id);
            $this->set('website_template', $website_template);
        }
    }

    function admin_delete_website_template() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('WebsiteTemplate');
        $id = $this->data['id'];
        $website_template = $this->WebsiteTemplate->findById($id);
        if ($this->WebsiteTemplate->delete($id)) {
            $path = 'uploaded_files/website_templates/' . $website_template['WebsiteTemplate']['image'];
            if (file_exists($path)) {
                unlink($path);
            }
            echo 'success';
        }
    }

    function admin_sub_category_list() {
        $this->layout = "admin_pages";
        $this->loadModel('SubCategorie');
        $this->loadModel('Categorie');
        $this->set('menu_select', 'category');
        $this->set('sub_menu_select', 'sub_category_list');
        $sub_categories = $this->SubCategorie->find('all');
        $this->Categorie->recursive = -1;
        foreach ($sub_categories as $key => &$sub_categorie) {
            $sub_categories[$key]['category'] = $this->Categorie->findById($sub_categorie['SubCategorie']['category_id']);
        }
        $this->set('sub_categories', $sub_categories);
    }

    function admin_add_sub_category() {
        $this->layout = "admin_pages";
        $this->loadModel('Categorie');
        $this->loadModel('SubCategorie');
        $this->set('menu_select', 'category');
        $this->set('sub_menu_select', 'sub_category_list');

        $category_id = $this->data['category_id'];
        $sub_category = $this->data['sub_category'];

        if (!empty($this->data['sub_category'])) {
            $subCategory = $this->SubCategorie->find('first', array('conditions' => array('SubCategorie.category_id' => $category_id, 'SubCategorie.sub_category' => $sub_category)));
            if (!$subCategory) {
                $sub_category_data['SubCategorie']['sub_category'] = $this->data['sub_category'];
                $sub_category_data['SubCategorie']['category_id'] = $this->data['category_id'];
                $sub_category_data['SubCategorie']['create_date'] = date('Y-m-d H:i:s');
                if ($this->SubCategorie->save($sub_category_data)) {
                    $this->redirect(array('controller' => 'admin', 'action' => 'admin_sub_category_list'));
                }
            } else {
                $this->redirect(array('controller' => 'admin', 'action' => 'admin_sub_category_list'));
            }
        } else {
            $this->Categorie->recursive = -1;
            $categories = $this->Categorie->find('all', array(
                'order' => array('category ASC')
            ));
            $this->set('categories', $categories);
        }
    }

    function admin_edit_sub_category() {
        $this->layout = "admin_pages";
        $this->loadModel('Categorie');
        $this->loadModel('SubCategorie');
        $this->set('menu_select', 'category');
        $this->set('sub_menu_select', 'sub_category_list');
        $sub_category_id = $this->request->params['named']['sub_category_id'];
        $category_id = $this->data['category_id'];
        $sub_category = $this->data['sub_category'];
        if (!empty($sub_category)) {
            $subCategory = $this->SubCategorie->find('first', array('conditions' => array('SubCategorie.category_id' => $category_id, 'SubCategorie.sub_category' => $sub_category)));
            if (!$subCategory) {
                $subCategory = array();
                $subCategory['SubCategorie'] = array();
                $subCategory['SubCategorie']['sub_category'] = $sub_category;
                $subCategory['SubCategorie']['category_id'] = $category_id;
                $subCategory['SubCategorie']['create_date'] = date('Y-m-d H:i:s');
                $this->SubCategorie->id = $this->data['id'];
                if ($this->SubCategorie->save($subCategory)) {
                    $this->redirect(array('controller' => 'admin', 'action' => 'admin_sub_category_list'));
                }
            } else {
                $this->redirect(array('controller' => 'admin', 'action' => 'admin_sub_category_list'));
            }
        } else {
            $this->Categorie->recursive = -1;
            $categories = $this->Categorie->find('all', array(
                'order' => array('category ASC')
            ));
            $this->set('categories', $categories);
            $sub_category = $this->SubCategorie->findById($sub_category_id);
            $this->set('sub_category', $sub_category);
        }
    }

    function admin_delete_sub_category() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('SubCategorie');
        $id = $this->data['id'];
        $category = $this->SubCategorie->findById($id);
        if ($this->SubCategorie->delete($id)) {
            echo 'success';
        }
    }

    function admin_get_sub_category_by_id() {
        $this->layout = 'ajax';
        $this->loadModel('SubCategorie');
        $category_id = $this->data['category_id'];
        $this->SubCategorie->recursive = -1;
        $sub_category = $this->SubCategorie->find('all', array('conditions' => array('SubCategorie.category_id' => $category_id)));
        $this->set('sub_categories', $sub_category);
    }

    function admin_featured_category() {
        $this->layout = "admin_pages";
        $this->loadModel('FeaturedCategorie');
        $this->set('menu_select', 'featured_category');
        $FeaturedCategories = $this->FeaturedCategorie->find('all');
        $this->set('FeaturedCategories', $FeaturedCategories);
    }

    function admin_edit_featured_category() {
        $this->layout = "admin_pages";
        $this->set('menu_select', 'featured_category');
        $this->loadModel('FeaturedCategorie');
        $this->loadModel('Categorie');
        $id = $this->request->params['named']['featured_category_id'];
        $FeaturedCategorie = $this->FeaturedCategorie->findById($id);
        $this->set('FeaturedCategorie', $FeaturedCategorie);
        $Categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('Categories', $Categories);
    }

    function admin_temp_featured_img_upload() {
        $this->layout = "admin_pages";
        $this->set('menu_select', 'featured_category');
        $this->loadModel('FeaturedCategorie');
        $this->loadModel('Categorie');
        $id = $this->data['id'];
        $category_id = $this->data['category_id'];
        $category = $this->Categorie->findById($category_id);
        if (!empty($_FILES['featured_iamge']['name'])) {
            $pathinfo = pathinfo($_FILES['featured_iamge']['name']);
            $file_name = Inflector::slug($category['Categorie']['category']) . '.' . $pathinfo['extension'];
            $path = 'uploaded_files/temp_file/' . $file_name;

            if (move_uploaded_file($_FILES['featured_iamge']['tmp_name'], $path)) {
                $this->set('featured_iamge', $file_name);
                $this->set('FeaturedCategorieId', $id);
                $this->FeaturedCategorie->id = $id;
                $this->FeaturedCategorie->saveField('category_id', $this->data['category_id']);
            }
        } else {
            $this->set('FeaturedCategorieId', $id);
            $this->FeaturedCategorie->id = $id;
            if ($this->FeaturedCategorie->saveField('category_id', $this->data['category_id'])) {
                $this->redirect(array('controller' => 'admin', 'action' => 'admin_featured_category'));
            }
        }
    }

    function admin_featured_img_upload() {
        $this->layout = "admin_pages";
        $this->set('menu_select', 'featured_category');

        $id = $this->data['id'];
        $x1 = $this->data['x1'];
        $y1 = $this->data['y1'];
        $x2 = $this->data['x2'];
        $y2 = $this->data['y2'];
        $width = $this->data['width'];
        $image_name = $this->data['image_name'];

        $tmp_image_path = 'uploaded_files/temp_file/';
        $final_image_path = 'uploaded_files/featured_category_image/';
        $tmp_img_fullpath = $tmp_image_path . $image_name;
        $final_image_fullpath = $final_image_path . $image_name;

//        $dst_width = 332;
//        $dst_height = 222;
//
//        if (!is_file($tmp_image_path . $image_name)) {
//            exit('Image not found');
//        } else {
//            $tmp_img_fullpath = $tmp_image_path . $image_name;
//            $final_image_fullpath = $final_image_path . $image_name;
//        }
//
//
//        $ext = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));
//        if ($ext == 'png') {
//            $src_img = imagecreatefrompng($tmp_img_fullpath);
//        } elseif ($ext == 'gif') {
//            $src_img = imagecreatefromgif($tmp_img_fullpath);
//        } elseif ($ext == 'jpg' or $ext == 'jpeg') {
//            $src_img = imagecreatefromjpeg($tmp_img_fullpath);
//        } else {
//            exit('Invalid file type');
//        }
//        $img_size = getimagesize($tmp_img_fullpath);
//        $original_width = $img_size[0];
//
//        $scale = $original_width / $width;
//        $src_crop_width = ($x2 - $x1) * $scale;
//        $src_crop_height = ($y2 - $y1) * $scale;
//        $src_crop_x1 = $x1 * $scale;
//        $src_crop_y1 = $y1 * $scale;
//
//        $dst_img = imagecreatetruecolor($dst_width, $dst_height);
//        imagecopyresampled($dst_img, $src_img, 0, 0, $src_crop_x1, $src_crop_y1, $dst_width, $dst_height, $src_crop_width, $src_crop_height);
//        if ($ext == 'png') {
//            imagepng($dst_img, $final_image_fullpath);
//        } else if ($ext == 'gif') {
//            imagegif($dst_img, $final_image_fullpath);
//        } else {
//            imagejpeg($dst_img, $final_image_fullpath);
//        }
//        $image_bytes = filesize($final_image_fullpath);


        
        if (rename($tmp_img_fullpath, $final_image_fullpath)) {
            $this->loadModel('FeaturedCategorie');
            $this->FeaturedCategorie->id = $id;
            if ($this->FeaturedCategorie->saveField('image', $image_name)) {
                $this->redirect(array('controller' => 'admin', 'action' => 'admin_featured_category'));
            }
        }
    }

    function admin_recommendation() {
        $this->layout = "admin_pages";
        $this->loadModel('Recommendation');
        $this->set('menu_select', 'recommendation');
        $recommendations = $this->Recommendation->find('all');
        $this->set('recommendations', $recommendations);
    }

    function admin_add_recommendation() {

        $this->layout = "admin_pages";
        $this->loadModel('Recommendation');
        $this->set('menu_select', 'recommendation');


        if (!empty($this->data)) {
            $recommendation['Recommendation']['client_name'] = $this->data['client_name'];
            $recommendation['Recommendation']['contractor_name'] = $this->data['contractor_name'];
            $recommendation['Recommendation']['rating'] = $this->data['rating'];
            $recommendation['Recommendation']['recommendation'] = $this->data['recommendation'];
            $recommendation['Recommendation']['create_date'] = date('Y-m-d H:i:s');
            if (!empty($_FILES['cover_photo']['name'])) {
                $pathinfo = pathinfo($_FILES['cover_photo']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $path = 'uploaded_files/recommendation_img/cover_photo/' . $file_name;
                if (file_exists($path)) {
                    $file_name = time() . $file_name;
                    $path = 'uploaded_files/recommendation_img/cover_photo/' . $file_name;
                }
                if (move_uploaded_file($_FILES['cover_photo']['tmp_name'], $path)) {
                    $recommendation['Recommendation']['cover_photo'] = $file_name;
                }
            }
            if (!empty($_FILES['profile_photo']['name'])) {
                $pathinfo = pathinfo($_FILES['profile_photo']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $path = 'uploaded_files/recommendation_img/profile_photo/' . $file_name;
                if (file_exists($path)) {
                    $file_name = time() . $file_name;
                    $path = 'uploaded_files/recommendation_img/profile_photo/' . $file_name;
                }
                if (move_uploaded_file($_FILES['profile_photo']['tmp_name'], $path)) {
                    $recommendation['Recommendation']['profile_photo'] = $file_name;
                }
            }
            if ($this->Recommendation->save($recommendation)) {
                $this->redirect(array('controller' => 'admin', 'action' => 'admin_recommendation'));
            }
        }
    }

    function admin_edit_recommendation() {

        $this->layout = "admin_pages";
        $this->loadModel('Recommendation');
        $this->set('menu_select', 'recommendation');
        if (!empty($this->data)) {
            $recommendation_id = $this->data['id'];
            $recommendation = $this->Recommendation->findById($recommendation_id);
            $recommendation['Recommendation']['client_name'] = $this->data['client_name'];
            $recommendation['Recommendation']['contractor_name'] = $this->data['contractor_name'];
            $recommendation['Recommendation']['rating'] = $this->data['rating'];
            $recommendation['Recommendation']['recommendation'] = $this->data['recommendation'];
            $recommendation['Recommendation']['update_date'] = date('Y-m-d H:i:s');
            if (!empty($_FILES['cover_photo']['name'])) {
                $pathinfo = pathinfo($_FILES['cover_photo']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $path = 'uploaded_files/recommendation_img/cover_photo/' . $file_name;
                $old_cover_path = 'uploaded_files/recommendation_img/cover_photo/' . $recommendation['Recommendation']['cover_photo'];
                if (file_exists($old_cover_path)) {
                    unlink($old_cover_path);
                }
                if (move_uploaded_file($_FILES['cover_photo']['tmp_name'], $path)) {
                    $recommendation['Recommendation']['cover_photo'] = $file_name;
                }
            }
            if (!empty($_FILES['profile_photo']['name'])) {
                $pathinfo = pathinfo($_FILES['profile_photo']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $path = 'uploaded_files/recommendation_img/profile_photo/' . $file_name;
                $old_profile_path = 'uploaded_files/recommendation_img/profile_photo/' . $recommendation['Recommendation']['profile_photo'];
                if (file_exists($old_profile_path)) {
                    unlink($old_profile_path);
                }
                if (move_uploaded_file($_FILES['profile_photo']['tmp_name'], $path)) {
                    $recommendation['Recommendation']['profile_photo'] = $file_name;
                }
            }
            if ($this->Recommendation->save($recommendation)) {
                $this->redirect(array('controller' => 'admin', 'action' => 'admin_recommendation'));
            }
        } else {
            $recommendation_id = $this->request->params['named']['recommendation_id'];
            $recommendation = $this->Recommendation->findById($recommendation_id);
            $this->set('recommendation', $recommendation);
        }
    }

    function admin_delete_recommendation() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('Recommendation');
        $id = $this->data['id'];
        $recommendation = $this->Recommendation->findById($id);
        if ($this->Recommendation->delete($id)) {
            $cover_photo_path = 'uploaded_files/recommendation_img/cover_photo/' . $recommendation['Recommendation']['cover_photo'];
            if (file_exists($cover_photo_path)) {
                unlink($cover_photo_path);
            }
            $profile_photo_path = 'uploaded_files/recommendation_img/profile_photo/' . $recommendation['Recommendation']['profile_photo'];
            if (file_exists($profile_photo_path)) {
                unlink($profile_photo_path);
            }
            echo 'success';
        }
    }

    function admin_bulk_category_uploadPrev() {
        $this->layout = "admin_pages";
        if (!empty($this->data)) {
//            print_r($this->data);
            $file = $this->data['Admin']['csv_file'];
            $name_parts = pathinfo($file['name']);
            $dst_folder = 'uploaded_files/temp_file/';
            $csv_filename = time() . $this->Auth->user('id') . '.csv';
            $csv_filename = $dst_folder . $csv_filename;
            if (strtolower($name_parts['extension']) == 'csv' and $file['error'] == 0) {
                if (is_writable($dst_folder)) {
                    move_uploaded_file($file['tmp_name'], $csv_filename);
                    $q_arr = $this->csv_to_array($csv_filename);
                    @unlink($csv_filename);
                } else {
                    exit('CSV destination folder not writable.');
                }
            }
            if (!is_array($q_arr)) {
                exit('Invalid format of CSV file 1');
            } else {
                $headings = array_keys($q_arr[0]);
                if (!($headings[0] == 'Category')) {
                    exit('Invalid format of CSV file 2');
                } elseif (!($headings[1] == 'Sub-Category 3')) {
                    exit('Invalid format of CSV file');
                }
            }

            $this->loadModel('Categorie');
            $this->loadModel('SubCategorie');

            $prev_category = '';
            foreach ($q_arr as $q) {
                $category = trim($q['Category']);
                if (strlen($category) == 0) {
                    $category = $prev_category;
                } else {
                    $prev_category = $category;
                }
                $subcategory = trim($q['Sub-Category']);

                $cat = $this->Categorie->findByCategory($category);
                if (!$cat) {
                    continue;
                } else {
                    $cat_id = $cat['Categorie']['id'];
                    $subcat = $this->SubCategorie->findBySubCategory($subcategory);
                    if (!$subcat) {
                        $subcatdata = array();
                        $subcatdata['SubCategorie']['category_id'] = $cat_id;
                        $subcatdata['SubCategorie']['sub_category'] = $subcategory;
                        $subcatdata['SubCategorie']['create_date'] = date('Y-m-d H:i:s');
                        $this->SubCategorie->create();
                        $this->SubCategorie->save($subcatdata);
                        $subcat_id = $this->SubCategorie->getInsertID();
                    } else {
                        $subcat_id = $subcat['SubCategorie']['id'];
                    }
                }
            }
            $this->redirect(array('controller' => 'admin', 'action' => 'bulk_category_upload', 'admin' => true));
        }
    }

    function admin_bulk_category_upload() {
        $this->layout = "admin_pages";
        if (!empty($this->data)) {
//            print_r($this->data);
            $file = $this->data['Admin']['csv_file'];
            $name_parts = pathinfo($file['name']);
            $dst_folder = 'uploaded_files/temp_file/';
            $csv_filename = time() . $this->Auth->user('id') . '.csv';
            $csv_filename = $dst_folder . $csv_filename;
            if (strtolower($name_parts['extension']) == 'csv' and $file['error'] == 0) {
                if (is_writable($dst_folder)) {
                    move_uploaded_file($file['tmp_name'], $csv_filename);
                    $q_arr = $this->csv_to_array($csv_filename);
                    @unlink($csv_filename);
                } else {
                    exit('CSV destination folder not writable.');
                }
            }
            if (!is_array($q_arr)) {
                exit('Invalid format of CSV file 1');
            }

            $this->loadModel('Categorie');
            $this->loadModel('SubCategorie');

            foreach ($q_arr as $row) {

                foreach ($row as $category => $subcategory) {
                    $category = trim($category);
                    $subcat = trim($subcategory);
                    if (strlen($category) == 0 or strlen($subcategory) == 0) {
                        continue;
                    }
                    $cat = $this->Categorie->findByCategory($category);

                    if (!$cat) {
                        $catData = array('Categorie' => array());
                        $catData['Categorie']['category'] = $category;
                        $catData['Categorie']['category_image'] = '';
                        $catData['Categorie']['create_date'] = date('Y-m-d H:i:s');
                        $this->Categorie->create();
                        $this->Categorie->save($catData);
                        $cat_id = $this->Categorie->getInsertID();
                    } else {
                        $cat_id = $cat['Categorie']['id'];
                    }
                    $subcat = $this->SubCategorie->find('first', array('conditions' => array('SubCategorie.category_id' => $cat_id, 'SubCategorie.sub_category' => $subcategory)));
//                    $subcat = $this->SubCategorie->findBySubCategory($subcategory);
                    if (!$subcat) {
                        $subcatdata = array();
                        $subcatdata['SubCategorie']['category_id'] = $cat_id;
                        $subcatdata['SubCategorie']['sub_category'] = $subcategory;
                        $subcatdata['SubCategorie']['create_date'] = date('Y-m-d H:i:s');
                        $this->SubCategorie->create();
                        $this->SubCategorie->save($subcatdata);
                        $subcat_id = $this->SubCategorie->getInsertID();
                    } else {
                        $subcat_id = $subcat['SubCategorie']['id'];
                    }
                }
            }
            $this->redirect(array('controller' => 'admin', 'action' => 'bulk_category_upload', 'admin' => true));
        }
    }

    private function csv_to_array($filename = '') {
        if (!file_exists($filename) || !is_readable($filename)) {
            echo "CSV destinatiion not readable.";
            return FALSE;
        }

        $delimiter = ';';
        $handle = fopen($filename, 'r');

        if (!$handle) {
            return FALSE;
        }

        $row = fgetcsv($handle, 0, $delimiter);
        if (!$row or count($row) == 1) {
            $delimiter = ',';
        }
        fclose($handle);
        $handle = fopen($filename, 'r');
        $header = NULL;
        $data = array();
        while (($row = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
            if (!$header)
                $header = $row;
            else {
                foreach ($row as $k => $r) {
                    $row[$k] = utf8_encode($r);
                }
                $data[] = array_combine($header, $row);
            }
        }
        fclose($handle);
        return $data;
    }

    function admin_download_contractor() {
        $this->autoRender = false;
        $this->layout = "admin_pages";
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="contractor_list.csv"');

// do not cache the file
        header('Pragma: no-cache');
        header('Expires: 0');

// create a file pointer connected to the output stream
        $file = fopen('php://output', 'w');

// send the column headers

        $this->loadModel('Categorie');
        $this->loadModel('SubCategorie');
        $this->loadModel('ProUser');

        $this->Categorie->unbindModel(array('hasMany' => array('SubCategorie')), true);
        $Categories = $this->Categorie->find('list', array(
            'order' => array('category ASC')
        ));


        $joins = array();
        $joins[] = array(
            'table' => 'categories',
            'conditions' => array(
                'Categorie.id = ProUser.category_id',
            ),
            'type' => 'INNER',
            'alias' => 'Categorie'
        );

        $pro_user_lists = $this->ProUser->find('all', array('joins' => $joins, 'order' => 'Categorie.category ASC'));
        fputcsv($file, array('Category', 'Sub-Category', 'Company Name', 'Company Description', 'Username', 'Email', 'Phone', 'Address', 'State', 'City', 'Zip Code', 'Website', 'Profile Link'));
        foreach ($pro_user_lists as $key => $val) {
            $SubCategorieList = '';
            foreach ($val['ProUserCategorie'] as $key2 => $val2) {
                $SubCategorie = $this->SubCategorie->findById($val2['sub_category_id']);
                $SubCategorieList .= $SubCategorie['SubCategorie']['sub_category'] . ", ";
            }
            if (strlen($val['ProUser']['zip_code']) == 4) {
                $zip_code = '0' . $val['ProUser']['zip_code'];
            } else {
                $zip_code = $val['ProUser']['zip_code'];
            }
            $profile_link = Router::url('/', true) . 'profile/' . $val['ProUser']['user_id'];
            $csv_data = array($val['Category']['category'], $SubCategorieList, $val['ProUser']['company_name'], $val['ProUser']['company_description'], $val['User']['name'], $val['User']['email'], $val['User']['phone'], $val['ProUser']['address'], $val['ProUser']['state'], $val['ProUser']['city'], $zip_code, $val['ProUser']['website_url'], $profile_link);
            fputcsv($file, $csv_data);
        }
        exit();
    }

}
