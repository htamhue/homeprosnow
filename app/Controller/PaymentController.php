<?php

App::uses('CakeTime', 'Utility');

class PaymentController extends AppController {

    var $helpers = array('Text', 'HtmlFixer');
    var $components = array('Cookie');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->loadModel('PaymentAccount');
        App::uses('PaymentProcessor', 'Lib');
    }

    function add_payment() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $response = array();
        $error = array();
        $account_data = array();
        if ($this->data['account_type'] == 'credit_card') {

            if (strlen($this->data['name_on_the_card']) == 0) {
                $error['cardNameError'] = 'Card name is required';
            } else {
                $account_data['PaymentAccount']['name_on_the_card'] = $this->data['name_on_the_card'];
            }
            if (strlen($this->data['card_number']) == 0) {
                $error['cardNumberError'] = 'Card number is required';
            } else {
                $account_data['PaymentAccount']['card_number'] = $this->data['card_number'];
            }
            if (strlen($this->data['expiry_month']) == 0 && strlen($this->data['expiry_year']) == 0) {
                $error['expiryDateError'] = 'Expiry month and year is required';
            } else {
                $account_data['PaymentAccount']['expiry_month'] = $this->data['expiry_month'];
                $account_data['PaymentAccount']['expiry_year'] = $this->data['expiry_year'];
            }
            if (strlen($this->data['ccv']) == 0) {
                $error['ccvError'] = 'CCV is required';
            } else {
                $account_data['PaymentAccount']['ccv'] = $this->data['ccv'];
            }
            $account_data['PaymentAccount']['payment_account_type'] = 'credit_card';
        } else {
            if (strlen($this->data['paypal_email']) == 0) {
                $error['paypalEmailError'] = 'Your email is required';
            } else if (filter_var($this->data['paypal_email'], FILTER_VALIDATE_EMAIL) == FALSE) {
                $error['paypalEmailError'] = 'Your email is not valid';
            } else {
                $account_data['PaymentAccount']['paypal_email'] = $this->data['paypal_email'];
            }
            $account_data['PaymentAccount']['payment_account_type'] = 'paypal';
        }
        $account_data['PaymentAccount']['method'] = $this->data['method'];
        $account_data['PaymentAccount']['user_id'] = $this->Auth->user('id');
        $account_data['PaymentAccount']['create_date'] = date('Y-m-d H:i:s');


        if (!$error) {
            if ($this->PaymentAccount->save($account_data)) {
                $id = $this->PaymentAccount->getLastInsertId();
                $response['type'] = "success";
                $response['id'] = $id;
            } else {
                $response['type'] = "error";
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }
        return json_encode($response);
    }

    function single_payment_method() {
        $this->layout = 'ajax';
        $id = $this->data['id'];
        $single_payment_method = $this->PaymentAccount->findById($id);
        $this->set('single_payment_method', $single_payment_method);
    }

    function remove_payment_method() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $id = $this->data['id'];

        if ($this->PaymentAccount->delete($id)) {
            echo 'success';
        }
    }

    function set_default_payment() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $this->loadModel('PaymentAccount');
        $user_id = $this->Auth->user('id');
        $id = $this->data['id'];
        $method = $this->data['method'];
        $status = $this->data['status'];

        $this->PaymentAccount->updateAll(
                array('PaymentAccount.default' => 0), array('PaymentAccount.user_id' => $user_id, 'PaymentAccount.method' => $method)
        );
        $this->PaymentAccount->id = $id;
        if ($status == 'checked') {
            if ($this->PaymentAccount->saveField('default', 1)) {
                echo 'selected';
            }
        } else {
            if ($this->ProUser->saveField('default', 0)) {
                echo 'unselected';
            }
        }
    }

    function initial_payment_of_project_view() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $Payment = new PaymentProcessor();
        $result = $Payment->project_view_payment();
        $this->loadModel('Notification');
        $project_id = $this->data['project_id'];
        if ($result) {

            $notification = array();
            $this->Notification->create();
            $notification['Notification']['user_id'] = $this->Auth->user('id');
            $notification['Notification']['user_type'] = 'pro';
            $notification['Notification']['notification_type'] = 'pro_project_fee_applied';
            $notification['Notification']['related_table_id'] = $project_id;
            $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
            if ($this->Notification->save($notification)) {
                echo 'success';
            }
        } else {
            echo 'error';
        }
    }

    function payment_request() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $hire_id = $this->data['hire_id'];
        $this->loadModel('Hire');
        $this->loadModel('Notification');
        $hire = $this->Hire->findById($hire_id);
        $this->Hire->id = $hire_id;
        if ($this->Hire->saveField('payment_status', 'requested')) {
            $notification = array();
            $this->Notification->create();
            $notification['Notification']['user_id'] = $hire['Hire']['user_id'];
            $notification['Notification']['user_type'] = 'user';
            $notification['Notification']['notification_type'] = 'user_payment_request_received';
            $notification['Notification']['related_table_id'] = $hire_id;
            $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
            if ($this->Notification->save($notification)) {
                echo 'success';
            }
        }
    }

    function pre_payment() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $this->loadModel('Hire');
        $this->loadModel('ProjectQuote');
        $response = array();
        $hire_id = $this->data['hire_id'];
        $this->Hire->recursive = -1;
        $hire = $this->Hire->findById($hire_id);
        $project_quote = $this->ProjectQuote->findById($hire['Hire']['project_quote_id']);
        $response['project_name'] = $project_quote['Project']['project_title'];
        $response['contractor_name'] = $project_quote['User']['name'];
        $response['quote_amount'] = $project_quote['ProjectQuote']['amount'];
        $response['hire_id'] = $hire_id;
        echo json_encode($response);
    }

    function payment_confirm() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $hire_id = $this->data['hire_id'];
        $this->loadModel('Hire');
        $this->loadModel('Notification');
        $hire = $this->Hire->findById($hire_id);
        $Payment = new PaymentProcessor();
        $result = $Payment->quote_payment();
        if ($result) {
            $this->Hire->id = $hire_id;
            $hireData ['payment_status'] = 'paid';
            $hireData ['hire_status'] = 'completed';
            if ($this->Hire->save($hireData)) {

                $notification = array();
                $this->Notification->create();
                $notification['Notification']['user_id'] = $hire['Hire']['user_id'];
                $notification['Notification']['user_type'] = 'user';
                $notification['Notification']['notification_type'] = 'user_paid_a_pro';
                $notification['Notification']['related_table_id'] = $hire['Hire']['id'];
                $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
                $this->Notification->save($notification);

                $notification = array();
                $this->Notification->create();
                $notification['Notification']['user_id'] = $hire['Hire']['pro_user_id'];
                $notification['Notification']['user_type'] = 'pro';
                $notification['Notification']['notification_type'] = 'pro_received_payment';
                $notification['Notification']['related_table_id'] = $hire['Hire']['id'];
                $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
                $this->Notification->save($notification);

                echo 'success';
            }
        }
    }

}
