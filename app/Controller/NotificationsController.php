<?php

App::uses('CakeTime', 'Utility');

class NotificationsController extends AppController {

    var $helpers = array('Text', 'HtmlFixer');
    var $components = array('Cookie');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->loadModel('Notification');
    }

    function get_notifications() {
        $this->layout = 'ajax';
        $this->loadModel('Hire');
        $this->loadModel('User');
        $this->loadModel('ProUser');
        $this->loadModel('ProjectQuote');
        $this->loadModel('Message');
        $this->loadModel('Project');
        $user_id = $this->Auth->user('id');
        $user_type = $this->Auth->user('user_type');
        $notifications = $this->Notification->find('all', array('conditions' => array('Notification.user_id' => $user_id, 'Notification.user_type' => $user_type), 'order' => array('Notification.create_date' => 'desc')));
        foreach ($notifications as $key => &$notification) {
            if ($notification['Notification']['notification_type'] == 'user_hired_pro') {
                $hire = $this->Hire->findById($notification['Notification']['related_table_id']);
                if (empty($hire)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['hire'] = $hire;
                }
            }
//            else if ($notification['Notification']['notification_type'] == 'received_message') {
//                $this->Message->recursive = -1;
//                $Message = $this->Message->findById($notification['Notification']['related_table_id']);
//                if (empty($Message)) {
//                    unset($notifications[$key]);
//                }
//            } 
            else if ($notification['Notification']['notification_type'] == 'pro_quote_accepted') {
//                $this->ProjectQuote->recursive = -1;
                $hire = $this->Hire->findById($notification['Notification']['related_table_id']);
                if (empty($hire)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['hire'] = $hire;
                }
            } else if ($notification['Notification']['notification_type'] == 'pro_quote_declined') {
                $this->ProjectQuote->recursive = -1;
                $project_quote = $this->ProjectQuote->findById($notification['Notification']['related_table_id']);
                if (empty($project_quote)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['project_quote'] = $project_quote;
                }
            } else if ($notification['Notification']['notification_type'] == 'pro_project_submitted_near_you') {
//                $this->Project->recursive = -1;
                $project = $this->Project->findById($notification['Notification']['related_table_id']);
//                $Payment = new PaymentProcessor(); // create object for html dom
//                $var = $Payment->test();
                if (empty($project) || !$project['Project']['is_verified']) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['project'] = $project;
                }
            } else if ($notification['Notification']['notification_type'] == 'user_received_quote') {
//                 $this->ProjectQuote->recursive = -1;
                $project_quote = $this->ProjectQuote->findById($notification['Notification']['related_table_id']);

                if (empty($project_quote)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['project_quote'] = $project_quote;
                }
            } else if ($notification['Notification']['notification_type'] == 'user_received_review') {
                $hire = $this->Hire->findById($notification['Notification']['related_table_id']);
                if (empty($hire)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['hire'] = $hire;
                    $notifications[$key]['pro_user'] = $this->User->findById($hire['Hire']['pro_user_id']);
                }
            } else if ($notification['Notification']['notification_type'] == 'pro_received_review') {
                $hire = $this->Hire->findById($notification['Notification']['related_table_id']);
                if (empty($hire)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['hire'] = $hire;
                }
            } else if ($notification['Notification']['notification_type'] == 'user_project_approved') {
                $project = $this->Project->findById($notification['Notification']['related_table_id']);
                if (empty($project)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['project'] = $project;
                }
            } else if ($notification['Notification']['notification_type'] == 'pro_account_approved') {
                $ProUser = $this->ProUser->findById($notification['Notification']['related_table_id']);
                if (empty($ProUser)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['ProUser'] = $ProUser;
                }
            } else if ($notification['Notification']['notification_type'] == 'user_payment_request_received') {
                $hire = $this->Hire->findById($notification['Notification']['related_table_id']);
                if (empty($hire)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['hire'] = $hire;
                    $this->User->recursive = -1;
                    $notifications[$key]['pro_user'] = $this->User->findById($hire['Hire']['pro_user_id']);
                }
            } else if ($notification['Notification']['notification_type'] == 'user_paid_a_pro') {
                $hire = $this->Hire->findById($notification['Notification']['related_table_id']);
                if (empty($hire)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['hire'] = $hire;
                    $notifications[$key]['project_quote'] = $this->ProjectQuote->findById($hire['Hire']['project_quote_id']);
                }
            } else if ($notification['Notification']['notification_type'] == 'pro_received_payment') {
                $hire = $this->Hire->findById($notification['Notification']['related_table_id']);
                if (empty($hire)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['hire'] = $hire;
                    $notifications[$key]['project_quote'] = $this->ProjectQuote->findById($hire['Hire']['project_quote_id']);
                }
            } else if ($notification['Notification']['notification_type'] == 'pro_project_fee_applied') {
                $project = $this->Project->findById($notification['Notification']['related_table_id']);
                if (empty($project)) {
                    unset($notifications[$key]);
                } else {
                    $notifications[$key]['project'] = $project;
                }
            }
        }

        $this->set('notifications', $notifications);
    }

    function read_notifications() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $id = $this->data['id'];
        $this->Notification->id = $id;
        if ($this->Notification->saveField("read_on", date('Y-m-d H:i:s'))) {
            echo 'success';
        }
    }

    function delete_notifications() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $id = $this->data['id'];
        if ($this->Notification->delete($id)) {
            echo 'success';
        }
    }

    function get_notification_count() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $user_id = $this->Auth->user('id');
        $user_type = $this->Auth->user('user_type');
        $notification_count = $this->Notification->find('count', array('conditions' => array('Notification.user_id' => $user_id, 'Notification.user_type' => $user_type, 'Notification.read_on' => null)));
        echo $notification_count;
    }

}
