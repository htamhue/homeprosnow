<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class HomeController extends AppController {

    var $helpers = array('Text', 'HtmlFixer');
    var $components = array('Cookie');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index', 'contact', 'contact_submit', 'test', 'about_us', 'email_marketing', 'lead_generation', 'rewarding_partnerships', 'seo', 'social_media_marketing', 'web_development', 'update_zipcode', 'terms_of_service', 'copyright', 'privacy_policy');
//        if (!$this->Session->read('Auth.User.id')) {
//            $this->redirect(array('controller' => 'landing'));
//        }
    }

    function index() {
        $this->layout = 'home';
        $this->set('view_header', 'landing');
        $this->loadModel('SecretQuestion');
        $this->loadModel('FeaturedCategorie');
        $this->loadModel('Categorie');
        $this->loadModel('ProUser');

        $categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));

        $secret_questions = $this->SecretQuestion->find('all');
        $this->set('categories', $categories);
        $this->set('secret_questions', $secret_questions);

        $this->loadModel('HomeprosVideo');
        $landing_video = $this->HomeprosVideo->findById(1);
        $this->set('landing_video', $landing_video);

        //$this->loadModel('Recommendation');
        //$recommendations = $this->Recommendation->find('all');
        //$this->set('recommendations', $recommendations);
        
        $this->loadModel('ProUserPackage');
        
        $user_zipcode = $this->Session->read('user_zipcode');
        $user_zipcode = '01701';

        $joins = array();
        $joins[] = array(
            'table' => 'pro_user_packages',
            'conditions' => array(
                'ProUserPackage.pro_user_id = ProUser.id',
            ),
            'type' => 'INNER',
            'alias' => 'ProUserPackage'
        );
        
        
        

        $condition = array();
        if ($user_zipcode) {
            $condition['ProUser.zip_code'] = $user_zipcode;
        }

         $pro_users = $this->ProUser->find('all', array('joins' => $joins, 'conditions' => $condition));
//         $u = $this->ProUser->getLastQuery();
         
         foreach ($pro_users as $key => &$val) {
            //$val['user_details'] = $this->User->findById($val['id']);
            $this->ProUserPackage->create();
            $ProUserPackage['ProUserPackage']['view_count'] = $val['view_count'] - 1;
            $ProUserPackage['ProUserPackage']['update_date'] = date('Y-m-d H:i:s');
            $this->ProUserPackage->pro_user_id= $val['pro_user_id'];
            $this->ProUserPackage->save($ProUserPackage);
        }

         
         
    }

    function contact() {
        $this->set('view_header', 'dashboard');
        $this->layout = 'home';
    }

    function contact_submit() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $response = array();
        $error = array();
        if (strlen($this->data['name']) == 0) {
            $error['nameError'] = __('Your name is required');
        }
        if (strlen($this->data['email']) == 0) {
            $error['emailError'] = __('Your email is required');
        } elseif (filter_var($this->data['email'], FILTER_VALIDATE_EMAIL) == FALSE) {
            $error['emailError'] = __('Your email is not valid');
        }
        if (strlen(trim($this->data['message'])) == 0) {
            $error['msgError'] = __('Pleae write a message');
        }

        if (!$error) {
            $this->loadModel(EmailTemplate);
            $emailTemplateArr = $this->EmailTemplate->findByTemplateName('contact_form');

            $variables['[{NAME}]'] = $this->data['name'];
            $variables['[{EMAIL}]'] = $this->data['email'];
            $variables['[{MESSAGE}]'] = $this->data['message'];
            $variables['[{baselink}]'] = Configure::read('settings.config.baselink');

            $this->sendEmail($emailTemplateArr, $variables, 'info@homeprosnow.com');
            $response['type'] = "success";
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }
        return json_encode($response);
    }

    function test() {
        $this->loadModel('EmailTemplate');

        $emailTemplateArr = $this->EmailTemplate->findByTemplateName('new_proposal_received_email_to_client');

        $variables['[{company_name}]'] = 'Infinacy';
        $variables['[{view_quote_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'view_quote', 'quote_id' => 4), true);
        $variables['[{cancel_project}]'] = Router::url(array('controller' => 'pro', 'action' => 'cancel_project', 'project_id' => 3), true);

        $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
        $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';

        $this->sendEmail($emailTemplateArr, $variables, 'nazmul@infinacy.com');
        echo 'sent';
        exit;
    }

    function about_us() {
//        $this->set('view_header', 'dashboard');
        $this->set('view_header', 'landing');
        $this->layout = 'home';
    }

    function email_marketing() {
        $this->set('view_header', 'landing');
        $this->layout = 'home';
    }

    function lead_generation() {
        $this->set('view_header', 'landing');
        $this->layout = 'home';
    }

    function rewarding_partnerships() {
        $this->set('view_header', 'landing');
        $this->layout = 'home';
    }

    function seo() {
        $this->set('view_header', 'landing');
        $this->layout = 'home';
    }

    function social_media_marketing() {
        $this->set('view_header', 'landing');
        $this->layout = 'home';
    }

    function web_development() {
        $this->set('view_header', 'landing');
        $this->layout = 'home';
    }

    function terms_of_service() {
        $this->set('view_header', 'landing');
        $this->layout = 'home';
    }

    function copyright() {
        $this->set('view_header', 'landing');
        $this->layout = 'home';
    }

    function privacy_policy() {
        $this->set('view_header', 'landing');
        $this->layout = 'home';
    }

    function update_zipcode() {

        $this->autoRender = false;
        $this->loadModel('ProUser');
        $pro_users = $this->ProUser->find('all');
        foreach ($pro_users as $key => $val) {
            if (strlen($val['ProUser']['zip_code']) == 4) {
                $this->ProUser->id = $val['ProUser']['id'];
                $update_data['ProUser']['zip_code'] = '0' . $val['ProUser']['zip_code'];
                $this->ProUser->save($update_data);
            }
        }
    }

    function photoResize($pro_user_id, $file_name, $w, $h) {
        
//        $this->loadModel('User');
//        $this->loadModel('ProUser');
//        
//        $first_image = PostImage::where('post_id', $post_id)->first();
//        if ($first_image) {
//            $first_image = $first_image->toArray();
//
////            $uplinPath = 'uploads/posts/' . $post_id . '/' . $PostImageData['image'];
//            $source_file = 'uploads/posts/' . $post_id . '/' . $first_image['image'];
//        }
//        
        $source_file = 'uploaded_files/pro_user_files/' . $pro_user_id . '/' . $file_name;
        
        if (!file_exists($source_file)) {
            return FALSE;
        }
        $p = pathinfo($source_file);
        $extension = strtolower($p['extension']);
        switch ($extension) {
            case "jpg":
            case "jpeg":
                $image = imagecreatefromjpeg($source_file);
                break;
            case "gif":
                $image = imagecreatefromgif($source_file);
                break;
            case "png":
                $image = imagecreatefrompng($source_file);
                break;
            case "bmp":
                $image = imagecreatefrombmp($source_file);
                break;
            default:
                return FALSE;
        }

        // Get new dimensions
        list ( $width, $height ) = getimagesize($source_file);

        //if the original images width is less than the expected cropped width
        if ($width < $w) {
            //determine the scale factor that will make the original images width equal to the expected width
            $scaleFactor = $w / $width;
        } else {
            $scaleFactor = 1;
        }
        //after scaling, if the height or the image is less than the expected height
        if ($height * $scaleFactor < $h) {
            $scaleFactor = $h / $height; //it'll be larger than the previos scale factor, cause after calculating with the width based one, the height was still smaller than expected
        }
        $scaledOriginalWidth = $width * $scaleFactor;
        $scaledOriginalHeight = $height * $scaleFactor;

        //calculate how much we can scale our crop dimension width wise
        $cropScaleFactor = $scaledOriginalWidth / $w;
        //if the scaling makes target height bigger than source height
        if ($h * $cropScaleFactor > $scaledOriginalHeight) {
            //calculate the crop scaling height wise
            //it'll be less than the width wise scale. Cause using the width wise scale made the height bigger than expected
            $cropScaleFactor = $scaledOriginalHeight / $h;
        }
        $scaledTargetWidth = $w * $cropScaleFactor;
        $scaledTargetHeight = $h * $cropScaleFactor;

        //now we calculate the actual positions 
        //difference between source and target width divided by 2 will give us the source start x position for a centered base
        //then divide with ScaleFactor to restore original dimentions
        $src_x = (($scaledOriginalWidth - $scaledTargetWidth) / 2) / $scaleFactor;
        $src_y = (($scaledOriginalHeight - $scaledTargetHeight) / 2) / $scaleFactor;
        //we divide them with the scalefactor too, cause they were calculated based on the scaled dimentions of the original image
        $src_width = $scaledTargetWidth / $scaleFactor;
        $src_height = $scaledTargetHeight / $scaleFactor;


        // Resample
        $image_p = imagecreatetruecolor($w, $h);
        //imagecrop($image, $rect)
        imagecopyresampled($image_p, $image, 0, 0, $src_x, $src_y, $w, $h, $src_width, $src_height);

        // Content type
        header('Content-type: image/jpeg');
        // Output
//        imagejpeg($image_p, $destination_file, 100);
        imagejpeg($image_p, null, 100);
//        chmod($destination_file, 0777);
//        imagepng($image_p, null, 100);
        imagedestroy($image_p);
        imagedestroy($image);
        exit;
    }

}
