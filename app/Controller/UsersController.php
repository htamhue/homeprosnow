<?php

App::uses('CakeTime', 'Utility');

class UsersController extends AppController {

    var $helpers = array('Text', 'HtmlFixer');
    var $components = array('Cookie');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('sign_up_action', 'sign_in_action', 'logout', 'forgot_password', 'reset_password', 'reset_password_action', 'confirm_account');
        $this->loadModel('User');
    }

    function sign_in_action() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $response = array();
        $error = array();
        if (!$this->request->is('ajax')) {
            $this->redirect('/');
        } elseif ($this->Auth->user('id')) {
            //if user is already logged in
            $response['type'] = "success";
        } elseif (!empty($this->data)) {

            if (strlen($this->data['email']) == 0) {
                $error['emailError'] = __('Your email is required');
            } elseif (filter_var($this->data['email'], FILTER_VALIDATE_EMAIL) == FALSE) {
                $error['emailError'] = __('Your email is not valid');
            }

            if (strlen($this->data['password']) == 0) {
                $error['passwordError'] = __('Password is required');
            }

            if (!$error) {
                $this->User->recursive = 0;
                $result = $this->User->find('first', array('conditions' => array('User.email' => $this->data['email'], 'User.password' => AuthComponent::password($this->data['password']), 'User.confirmed' => 1)));

                if ($result) {
                    $this->Auth->login($result['User']);
                    $this->Session->write('RealUserType', $result['User']['user_type']);
                } else {
                    $response['type'] = "error";
                    $response['errorMsg']['loginError'] = __('Wrong email or password !!');
                }
            } else {
                $response['type'] = "error";
                $response['errorMsg'] = $error;
            }
            return json_encode($response);
        }
    }

    function sign_up_action() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $response = array();
        $this->loadModel('User');
        $this->User->create();
        $userData = $this->data;

        $user = $this->User->find('first', array('conditions' => array('User.email' => $userData['User']['email'])));

        $userData["User"]['create_date'] = date('Y-m-d H:i:s');
        $userData["User"]['plan'] = 1;
        if ($userData['User']['password']) {
            $userData['User']['password'] = AuthComponent::password($userData['User']['password']);
        }
        unset($userData['User']['id']); //to prevent primary key injection from the registration form
        $error = false;

        if (strlen($userData['User']['name']) == 0) {
            $error['nameError'] = 'Your name is required';
        }

        if (strlen($userData['User']['email']) == 0) {
            $error['emailError'] = 'Your email is required';
        } elseif (filter_var($userData['User']['email'], FILTER_VALIDATE_EMAIL) == FALSE) {
            $error['emailError'] = 'Your email is not valid';
        } else if ($user) {
            $error['emailError'] = 'Your email is already exist';
        }

        if (strlen($userData['User']['password']) == 0) {
            $error['passwordError'] = 'Password is required';
        } else if ($userData['User']['password'] != AuthComponent::password($userData['User']['confirm_password'])) {
            $error['passwordError'] = 'Password does not match';
        }
        $userData['User']['user_type'] = 'user';

//        exit;
        if (!$error) {
            if ($this->User->save($userData)) {
                $id = $this->User->getLastInsertId();
                $this->sendConfirmationEmail($userData, $id);
                $userdata = $this->User->read();
                $this->Auth->login($userdata['User']);
                $this->Session->write('RealUserType', $userdata['User']['user_type']);
                $this->Session->setFlash(__('CONTROLLER__USERS_CONTROLLER__REGISTRATION_SUCCESSFULLY'), 'front_flash_success');
                $response['type'] = "success";
            } else {
                $response['type'] = "error";
                $this->Session->setFlash(__('CONTROLLER__USERS_CONTROLLER__REGISTRATION_FAILED'));
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }
        return json_encode($response);
    }

    public function logout() {
        $url = $this->Auth->logout();
        $this->Session->delete('RealUserType');
        $this->Session->delete('user_zipcode');
        $this->redirect(array('controller' => 'home'));
    }

    function confirm_account($id, $confirmLink) {

        $user = $this->User->find('first', array('conditions' => array('User.id' => $id)));
        if ($user) {
            $link = md5("CONFIRM" . $user['User']['email'] . "USER" . $id . "CONFIRM");
            if ($link == $confirmLink) {
                $this->loadModel('User');
                $this->User->id = $id;
                if ($this->User->saveField('confirmed', 1)) {

                    $this->loadModel('EmailTemplate');
                    if ($user['User']['user_type'] == 'user') {
                        $emailTemplateArr = $this->EmailTemplate->findByTemplateName('client_registration_email_to_client');
                        $variables['[{find_pro_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'find_more_pro'), true);
                    } else {
                        $emailTemplateArr = $this->EmailTemplate->findByTemplateName('contractor_registration_email_to_contractor');
                        $variables['[{update_profile_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'profile', $user['User']['id']), true);
                    }

                    $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                    $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';

                    $welcome_mail = $this->EmailTemplate->findByTemplateName('welcome_mail');

                    $this->sendEmail($welcome_mail, $variables, $user['User']['email']);
                    $this->sendEmail($emailTemplateArr, $variables, $user['User']['email']);
                    $this->Session->setFlash('<div class="notification success">Your account is confirmed. Please login using your email and password</div>');
                    $this->redirect(array('controller' => 'home', 'action' => 'index'));
                }
//                echo $sql = $this->User->getLastQuery();
            } else {
                $this->Session->setFlash('<div class="notification error">The link is invalid</div>');
                $this->redirect(array('controller' => 'home', 'action' => 'index'));
            }
        } else {
            $this->Session->setFlash('<div class="notification error">The link is invalid</div>');
            $this->redirect(array('controller' => 'home', 'action' => 'index'));
        }
    }

    function update_password() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $response = array();
        $this->loadModel('User');
        $user_id = $this->Auth->user('id');
        $user = $this->User->findById($user_id);

        $old_password = AuthComponent::password($this->data['oldPassword']);



        if (!empty($user['User']['password'])) {
            if (strlen($user['User']['password']) == 0) {
                $error['passwordError'] = 'Old password is required';
            } else if ($user['User']['password'] != $old_password) {
                $error['passwordError'] = 'Wrong password';
            } else if (strlen($this->data['newPassword']) == 0) {
                $error['passwordError'] = 'New Password is required';
            } else if (strlen($this->data['confirmNewPassword']) == 0) {
                $error['passwordError'] = 'Confirm New Password is required';
            } else if ($this->data['newPassword'] != $this->data['confirmNewPassword']) {
                $error['passwordError'] = 'Password does not match';
            } else {
                $userData['User']['password'] = AuthComponent::password($this->data['confirmNewPassword']);
            }
        } else {
            if (strlen($this->data['newPassword']) == 0) {
                $error['passwordError'] = 'New Password is required';
            } else if (strlen($this->data['confirmNewPassword']) == 0) {
                $error['passwordError'] = 'Confirm New Password is required';
            } else if ($this->data['newPassword'] != $this->data['confirmNewPassword']) {
                $error['passwordError'] = 'Password does not match';
            } else {
                $userData['User']['password'] = AuthComponent::password($this->data['confirmNewPassword']);
            }
        }

        if (!$error) {
            $this->User->id = $user['User']['id'];
            if ($this->User->saveField("password", $userData['User']['password'])) {
                $this->loadModel('EmailTemplate');
                $emailTemplateArr = $this->EmailTemplate->findByTemplateName('updated_password');

                $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';
                if ($this->sendEmail($emailTemplateArr, $variables, $user['User']['email'])) {
                    $error = 0;
                }

                $response['type'] = "success";
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }

        return json_encode($response);
    }

    function update_secret_question() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $user_id = $this->Auth->user('id');
        $update_data['User']['secret_question_id'] = $this->data['question'];
        $update_data['User']['secret_answer'] = $this->data['answer'];

        $this->User->id = $user_id;
        if ($this->User->save($update_data)) {
            echo 'success';
        } else {
            echo 'error';
        }
//        $user = $this->User->findById($user_id);
    }

    function update_profile() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $user_id = $this->Auth->user('id');
        $name = $this->data['name'];
        $email = $this->data['email'];
        $phone = $this->data['phone'];
        $address = $this->data['address'];
        $city = $this->data['city'];
        $state = $this->data['state'];
        $country = $this->data['country'];
        $zip_code = $this->data['zip_code'];

        $this->User->recursive = -1;
        $user = $this->User->findById($user_id);
        $any_user = $this->User->find('first', array('conditions' => array('User.email' => $email)));
        $response = array();
        if (strlen($email) == 0) {
            $error['emailError'] = 'Email is required';
        } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
            $error['emailError'] = 'Email is not valid';
        } else if ($any_user['User']['id'] != $user['User']['id']) {
            $error['emailError'] = 'Email is already exist';
        }

        if (!$error) {
            $user_data = array();
            if (!empty($_FILES['profile_pic']['name'])) {
                $path = 'uploaded_files/profile_pic/' . $user_id;
                if (!file_exists($path)) {
                    $folder = mkdir($path, 0777);
                    chmod($path, 0777);
                }
                $accepted_type = array('jpg', 'png', 'gif');
                $pathinfo = pathinfo($_FILES['profile_pic']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $finalpath = $path . '/' . $file_name;
                if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                    if (move_uploaded_file($_FILES['profile_pic']['tmp_name'], $finalpath)) {
                        unlink($path . '/' . $user['User']['profile_pic']);
                        $user_data['User']['profile_pic'] = $file_name;
                    } else {
                        $response['type'] = 'error';
                    }
                } else {
                    $error['imageError'] = 'You can upload jpg, png and gif image';
                }
            }
            if (!$error) {
                $user_data['User']['name'] = $name;
                $user_data['User']['phone'] = $phone;
                $user_data['User']['email'] = $email;
                $user_data['User']['address'] = $address;
                $user_data['User']['city'] = $city;
                $user_data['User']['state'] = $state;
                $user_data['User']['country'] = $country;
                $user_data['User']['zip_code'] = $zip_code;
                $this->User->id = $user_id;
                $this->User->save($user_data);
                $user = $this->User->findById($user_id);

                if ($user['User']['address'] == '') {
                    $user['User']['address'] = 'Please set your address';
                }
                if ($user['User']['city'] == '') {
                    $user['User']['city'] = 'Please set your city';
                }
                if ($user['User']['state'] == '') {
                    $user['User']['state'] = 'Please set your state';
                }
                if ($user['User']['country'] == '') {
                    $user['User']['country'] = 'Please set your country';
                }
                if ($user['User']['zip_code'] == '') {
                    $user['User']['zip_code'] = 'Please set your zip_code';
                }

                $this->reload_user_data();
                $response['type'] = 'success';
                $response['user'] = $user;
            } else {
                $response['type'] = 'error';
                $response['error'] = $error;
            }
        } else {
            $response['type'] = 'error';
            $response['error'] = $error;
        }


        return json_encode($response);
    }

    function forgot_password() {

        $this->autoRender = false;
        $this->layout = 'ajax';

        if (!empty($this->data['email'])) {
            $email = $this->data['email'];
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error = 1;
                $msg = __('Your email is not valid');
            } else {
                $this->User->recursive = -1;
                $user = $this->User->find('first', array('conditions' => array('User.email' => $this->data['email'], 'User.secret_answer' => $this->data['answer'], 'User.secret_question_id' => $this->data['secret_question'])));
                if (count($user) == 1) {
                    $key = md5(rand(1000000, 9999999999)) . md5(rand(100, 99999));
                    $link = Router::url(array('controller' => 'users', 'action' => 'reset_password', 'user_id' => $user['User']['id'], 'key' => $key), true);
                    $data = array();
                    $data['ForgotpassRequest']['user_id'] = $user['User']['id'];
                    $data['ForgotpassRequest']['user_email'] = $user['User']['email'];
                    $data['ForgotpassRequest']['key'] = $key;
                    $data['ForgotpassRequest']['issued'] = date('Y-m-d H:i:s');
                    $data['ForgotpassRequest']['expires'] = date('Y-m-d H:i:s', time() + (3600));
                    $data['ForgotpassRequest']['used'] = 0;
                    $this->loadModel('ForgotpassRequest');
                    if ($this->ForgotpassRequest->save($data)) {
                        $this->loadModel('EmailTemplate');

                        $emailTemplateArr = $this->EmailTemplate->findByTemplateName('forgot_password');


                        $variables['[{forgot_password_link}]'] = $link;

                        $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                        $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';
                        if ($this->sendEmail($emailTemplateArr, $variables, $user['User']['email'])) {
                            $error = 0;
                        }
                    }
                } else {
                    $error = 1;
                    $msg = __('No user found by this email');
                }
            }
        } else {
            $error = 1;
            $msg = __('Your email is not valid');
        }
        if ($error) {
            $return = array('status' => 'error', 'msg' => $msg);
        } else {
            $return = array('status' => 'success');
        }
        return json_encode($return);
        exit;
    }

    function reset_password() {
        $this->layout = 'home';
        $this->set('view_header', 'landing');


        $user_id = $this->params->named['user_id'];
        $key = $this->params->named['key'];

        $this->set('user_id', $user_id);
        $this->set('key', $key);
    }

    function reset_password_action() {
        $this->autoRender = false;
        $this->layout = 'ajax';

        $user_id = $this->data['user_id'];
        $key = $this->data['key'];
        $newPassword = $this->data['newPassword'];
        $confirmNewPassword = $this->data['confirmNewPassword'];
        $response = array();
        $error = array();
        $this->loadModel('ForgotpassRequest');
        $forgotpass_request = $this->ForgotpassRequest->find('first', array('conditions' => array('ForgotpassRequest.user_id' => $user_id, 'ForgotpassRequest.key' => $key, 'ForgotpassRequest.status' => 'issued')));
        if (!$forgotpass_request) {
            $error['passwordError'] = 'Forgot password link expired';
        } elseif ($forgotpass_request['ForgotpassRequest']['expires'] < date('Y-m-d H:i:s')) {
            $this->ForgotpassRequest->id = $forgotpass_request['ForgotpassRequest']['id'];
            $this->ForgotpassRequest->saveField('status', 'expired');
        } else {
            if (strlen($this->data['newPassword']) == 0) {
                $error['passwordError'] = 'New Password is required';
            } else if (strlen($this->data['confirmNewPassword']) == 0) {
                $error['passwordError'] = 'Confirm New Password is required';
            } else if ($this->data['newPassword'] != $this->data['confirmNewPassword']) {
                $error['passwordError'] = 'Password does not match';
            } else {
                $userData['User']['password'] = AuthComponent::password($this->data['confirmNewPassword']);
            }
        }

        if (!$error) {
            $this->User->id = $user_id;
            if ($this->User->saveField('password', $userData['User']['password'])) {
                $this->ForgotpassRequest->id = $forgotpass_request['ForgotpassRequest']['id'];
                $forgotpass_update['status'] = 'used';
                $forgotpass_update['used'] = date('Y-m-d H:i:s');
                if ($this->ForgotpassRequest->save($forgotpass_update)) {
                    $response['type'] = "success";
                }
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }

        return json_encode($response);
        exit;
    }

    function login_as_user() {
        $user_id = $this->Auth->User('id');
        if ($user_id) {
            $this->Session->write('Auth.User.user_type', 'user');
        }
        $this->redirect('/');
    }

    function login_as_pro() {
        $user_id = $this->Auth->User('id');
        $this->loadModel('ProUser');
        if ($user_id and $this->ProUser->hasProAccount($user_id)) {
            $this->Session->write('Auth.User.user_type', 'pro');
        }
        $this->redirect('/');
    }

    function admin_get_user_list() {
        $condition = array();
        if ($_GET['name']) {
            $condition['User.name LIKE'] = '%' . $_GET['name'] . '%';
        }

        if ($_GET['email']) {
            $condition['User.email'] =  $_GET['email'] ;
        }

        $this->layout = "admin_pages";
        $this->loadModel('User');

        $per_page = 5;
        $options = array(
            //'conditions' => array('ProUser.is_verified' => 1),
            'conditions' => $condition,
            'limit' => $per_page,
//            'group' => 'Question.id',
//            'order' => 'Question.date DESC'
        );

        $this->paginate = $options;
        $users = $this->paginate('User');
        $this->set("users", $users);
    }

    function admin_edit_user_by_admin($user_id) {
        $this->layout = 'admin_pages';
        $this->loadModel('User');
        $user = $this->User->findById($user_id);
        $this->set('user', $user);

        $state_lists = array('AL' => "Alabama",
            'AK' => "Alaska",
            'AZ' => "Arizona",
            'AR' => "Arkansas",
            'CA' => "California",
            'CO' => "Colorado",
            'CT' => "Connecticut",
            'DE' => "Delaware",
            'DC' => "District Of Columbia",
            'FL' => "Florida",
            'GA' => "Georgia",
            'HI' => "Hawaii",
            'ID' => "Idaho",
            'IL' => "Illinois",
            'IN' => "Indiana",
            'IA' => "Iowa",
            'KS' => "Kansas",
            'KY' => "Kentucky",
            'LA' => "Louisiana",
            'ME' => "Maine",
            'MD' => "Maryland",
            'MA' => "Massachusetts",
            'MI' => "Michigan",
            'MN' => "Minnesota",
            'MS' => "Mississippi",
            'MO' => "Missouri",
            'MT' => "Montana",
            'NE' => "Nebraska",
            'NV' => "Nevada",
            'NH' => "New Hampshire",
            'NJ' => "New Jersey",
            'NM' => "New Mexico",
            'NY' => "New York",
            'NC' => "North Carolina",
            'ND' => "North Dakota",
            'OH' => "Ohio",
            'OK' => "Oklahoma",
            'OR' => "Oregon",
            'PA' => "Pennsylvania",
            'RI' => "Rhode Island",
            'SC' => "South Carolina",
            'SD' => "South Dakota",
            'TN' => "Tennessee",
            'TX' => "Texas",
            'UT' => "Utah",
            'VT' => "Vermont",
            'VA' => "Virginia",
            'WA' => "Washington",
            'WV' => "West Virginia",
            'WI' => "Wisconsin",
            'WY' => "Wyoming");
        $this->loadModel('User');
        $this->set('state_lists', $state_lists);

        $this->loadModel('Country');
        $country = $this->Country->find('all');
        $this->set('countries', $country);
    }

    function admin_update_user_profile_by_admin() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $user_id = $this->data['user_id'];
        $name = $this->data['name'];
        $email = $this->data['email'];
        $phone = $this->data['phone'];
        $password = $this->data['password'];
        $address = $this->data['address'];
        $country = $this->data['country'];
        $state = $this->data['state'];
        $city = $this->data['city'];
        $zip_code = $this->data['zip_code'];



        $this->loadModel('User');
        $this->User->recursive = -1;
        $user = $this->User->findById($user_id);
        $any_user = $this->User->find('first', array(
            'conditions' => array('User.email' => $email, 'User.id !=' => $user_id)
        ));
        $response = array();
        if (strlen($email) == 0) {
            $error['emailError'] = 'Email is required';
        } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
            $error['emailError'] = 'Email is not valid';
        } else if ($any_user['User']['email'] == $email) {
            $error['emailError'] = 'Email is already exist';
        }

        if (strlen($name) == 0) {
            $error['nameError'] = 'Name is required';
        }

        if (!$error) {
            $user_data = array();
            if (!empty($_FILES['profile_pic']['name'])) {
                $path = 'uploaded_files/profile_pic/' . $user_id;
                if (!file_exists($path)) {
                    $folder = mkdir($path, 0777);
                    chmod($path, 0777);
                }
                $accepted_type = array('jpg', 'png', 'gif');
                $pathinfo = pathinfo($_FILES['profile_pic']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $finalpath = $path . '/' . $file_name;
                if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                    if (move_uploaded_file($_FILES['profile_pic']['tmp_name'], $finalpath)) {
                        unlink($path . '/' . $user['User']['profile_pic']);
                        $user_data['User']['profile_pic'] = $file_name;
                    } else {
                        $response['type'] = 'error';
                    }
                } else {
                    $error['imageError'] = 'You can upload jpg, png and gif image';
                }
            }
            if (!$error) {
                $user_data['User']['name'] = $name;
                $user_data['User']['phone'] = $phone;
                $user_data['User']['email'] = $email;
                $user_data['User']['address'] = $address;
                $user_data['User']['country'] = $country;
                $user_data['User']['state'] = $state;
                $user_data['User']['city'] = $city;
                $user_data['User']['zip_code'] = $zip_code;
                if ($password) {
                    $user_data['User']['password'] = AuthComponent::password($password);
                }

                $this->User->id = $user_id;
                $this->User->save($user_data);
                $user = $this->User->findById($user_id);
                $response['type'] = 'success';
                $response['user'] = $user;
            } else {
                $response['type'] = 'error';
                $response['error'] = $error;
            }
        } else {
            $response['type'] = 'error';
            $response['error'] = $error;
        }


        return json_encode($response);
    }

}
