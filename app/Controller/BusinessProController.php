<?php
App::uses('CakeTime', 'Utility');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class BusinessProController extends AppController {

    public $connection;

    private $data;

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('become_a_pro', 'register', 'add_pro', 'add_project');
    }

    public function become_a_pro() {
        $this->layout = 'home';
        $this->set('view_header', 'landing');
    }

    public function register(){
        $this->autoRender = false;
        $this->layout = 'ajax';
        //$this->loadModel('BusinessPro');
        $this->loadModel('Categorie');
        $this->loadModel('User');
        $this->loadModel('ProUser');
        $result = array();
        $reg_data = $this->request->data;
        $tbl_prouser = $tbl_user = array();
        $check_account = $this->User->find('first', array('conditions' => array('User.email' => $reg_data['email'])));

        if($check_account){
            $result['status'] = 'error';
            $result['messages'] = 'Your email is already exist';
        }else{
            //$table['BusinessPro']['work'] = $reg_data['works'];
            //$table['BusinessPro']['services'] = implode(',', $reg_data['services']);
            $tbl_user['User']['name'] = $reg_data['fname'] . ' ' . $reg_data['lname'];
            $tbl_user['User']['email'] = $reg_data['email'];
            $password_hasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
            $tbl_user['User']['password'] = $password_hasher->hash($reg_data['confirm']);
            $tbl_user['User']['create_date'] = date('Y-m-d H:i:s');
            $this->User->save($tbl_user);
            $user_id = $this->User->getLastInsertId();

            $tbl_prouser['ProUser']['user_id'] = $user_id;
            $tbl_prouser['ProUser']['company_name'] = $reg_data['bname'];
            $tbl_prouser['ProUser']['business_phone'] = $reg_data['phone'];
            $tbl_prouser['ProUser']['zip_code'] = $reg_data['zip'];
            $tbl_prouser['ProUser']['website_url'] = (isset($reg_data['website'])) ? $reg_data['website'] : '';
            $this->ProUser->save($tbl_prouser);

            $this->sendConfirmationEmail($tbl_user, $user_id);
            //$user_data = $this->User->read();
            //$this->Auth->login($user_data['User']);
            $result['status'] = 'success';
            $result['messages'] = 'Your account has been created';
        }
        return json_encode($result);
    }

    public function add_pro(){
        $this->layout = 'home';
        $this->set('view_header', 'landing');
        $this->loadModel('Country');
        $countries = $this->Country->find('all');
        $this->set('countries', $countries);
        $state_lists = array('AL' => "Alabama",
            'AK' => "Alaska",
            'AZ' => "Arizona",
            'AR' => "Arkansas",
            'CA' => "California",
            'CO' => "Colorado",
            'CT' => "Connecticut",
            'DE' => "Delaware",
            'DC' => "District Of Columbia",
            'FL' => "Florida",
            'GA' => "Georgia",
            'HI' => "Hawaii",
            'ID' => "Idaho",
            'IL' => "Illinois",
            'IN' => "Indiana",
            'IA' => "Iowa",
            'KS' => "Kansas",
            'KY' => "Kentucky",
            'LA' => "Louisiana",
            'ME' => "Maine",
            'MD' => "Maryland",
            'MA' => "Massachusetts",
            'MI' => "Michigan",
            'MN' => "Minnesota",
            'MS' => "Mississippi",
            'MO' => "Missouri",
            'MT' => "Montana",
            'NE' => "Nebraska",
            'NV' => "Nevada",
            'NH' => "New Hampshire",
            'NJ' => "New Jersey",
            'NM' => "New Mexico",
            'NY' => "New York",
            'NC' => "North Carolina",
            'ND' => "North Dakota",
            'OH' => "Ohio",
            'OK' => "Oklahoma",
            'OR' => "Oregon",
            'PA' => "Pennsylvania",
            'RI' => "Rhode Island",
            'SC' => "South Carolina",
            'SD' => "South Dakota",
            'TN' => "Tennessee",
            'TX' => "Texas",
            'UT' => "Utah",
            'VT' => "Vermont",
            'VA' => "Virginia",
            'WA' => "Washington",
            'WV' => "West Virginia",
            'WI' => "Wisconsin",
            'WY' => "Wyoming");
        $this->set('state_lists', $state_lists);
    }
    public function add_project(){
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('Categorie');
        $this->loadModel('User');
        $this->loadModel('Project');
        $result = array();
        $project_data = $this->request->data;
        $tbl_user = $tbl_project = array();
        $check_account = $this->User->find('first', array('conditions' => array('User.email' => $project_data['email'])));
        if($check_account){
            $result['status'] = 'error';
            $result['messages'] = 'Your email is already exist';
        }else{
            $tbl_user['User']['name'] = $project_data['fname'] . ' ' . $project_data['lname'];
            $tbl_user['User']['email'] = $project_data['email'];
            $password_hasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
            $tbl_user['User']['password'] = $password_hasher->hash($project_data['repassword']);
            $tbl_user['User']['phone'] = $project_data['phone'];
            $tbl_user['User']['address'] = $project_data['street'];
            $tbl_user['User']['state'] = $project_data['state'];
            $tbl_user['User']['country'] = $project_data['country'];
            $tbl_user['User']['zip_code'] = $project_data['zipcode'];
            $tbl_user['User']['create_date'] = date('Y-m-d H:i:s');
            $this->User->save($tbl_user);
            $user_id = $this->User->getLastInsertId();
            $this->sendConfirmationEmail($tbl_user, $user_id);

            $tbl_project['Project']['user_id'] = $user_id;
            $tbl_project['Project']['budget'] = $project_data['budget'];
            $tbl_project['Project']['details'] = $project_data['describe'] . ' - Specific contractors: ' . $project_data['question3'];
            $tbl_project['Project']['materials'] = $project_data['question1'];
            $tbl_project['Project']['start_date'] = $project_data['question2'];
            $tbl_project['Project']['create_date'] = date('Y-m-d H:i:s');
            $tbl_project['Project']['zip_code'] = $project_data['zipcode'];
            $tbl_project['Project']['project_title'] = 'The new project #' . time();
            $this->Project->save($tbl_project);
            $project_id = $this->Project->getLastInsertId();

            $this->loadModel('EmailTemplate');
            $project_user = $this->User->findById($user_id);
            $emailTemplateArr = $this->EmailTemplate->findByTemplateName('project_verify_email_to_admin');

            $variables = [];
            $variables['[{client_name}]'] = $project_user['User']['name'];
            $variables['[{view_project_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $project_id), true);

            $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
            $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';
            if ($this->sendEmail($emailTemplateArr, $variables, Configure::read('settings.config.admin_mail'))) {
                $result['status'] = 'success';
                $result['messages'] = 'Success! Please check your email to know more detail. Your project has been created';
            }else{
                $result['status'] = 'error';
                $result['messages'] = 'Failed! The email can not send';
            }
        }
        return json_encode($result);
    }
}