<?php

App::uses('CakeTime', 'Utility');

class MessagesController extends AppController {

    var $helpers = array('Text', 'HtmlFixer');
    var $components = array('Cookie');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->loadModel('Message');
        $this->loadModel('Notification');
    }

    function index() {
        $this->set('main_menu', 'message');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $this->loadModel('User');
        $user_id = $this->Auth->user('id');
        $user_type = $this->Auth->user('user_type');
        $messages = $this->Message->get_message_threads($user_id, $user_type);
        if ($this->request->params['named']['project_quote_id']) {
            $project_quote_id = $this->request->params['named']['project_quote_id'];
            $this->loadModel('ProjectQuote');
            $project_quote = $this->ProjectQuote->findById($project_quote_id);
            if ($user_type == 'pro') {
                $conditions = array('Message.sender' => 'user', 'Message.pro_user_id' => $user_id);
            } else {
                $conditions = array('Message.sender' => 'pro', 'Message.user_id' => $user_id);
            }
            $conditions = array_merge($conditions, array('Message.project_quote_id' => $project_quote_id, 'Message.read_at' => null));
            $project_quote['unReadCount'] = $this->Message->find('count', array('conditions' => $conditions));
            $project_quote['Project']['user'] = $this->User->findById($project_quote['Project']['user_id']);
            $this->set('project_quote', $project_quote);
        }

        $this->set('messages', $messages);
    }

    function open_message() {
        $user_id = $this->Auth->user('id');
        $user_type = $this->Auth->user('user_type');

        $this->layout = 'ajax';
        $project_id = $this->data['project_id'];
        $project_quote_id = $this->data['project_quote_id'];
        $last_message_id = $this->data['last_message_id'];
        $this->set('project_id', $project_id);
        $this->set('project_quote_id', $project_quote_id);
        $conditions = array('Message.project_id' => $project_id, 'Message.project_quote_id' => $project_quote_id);
        if ($last_message_id) {
            $conditions['Message.id >'] = $last_message_id;
        }
        $messages = $this->Message->find('all', array('conditions' => $conditions, 'order' => array('Message.create_date' => 'asc')));
        $this->loadModel('User');
        $this->User->recursive = -1;
        foreach ($messages as $key => &$message) {
            if ($user_type == 'pro') {
                if ($message['Message']['sender'] == 'user') {
                    $this->Message->id = $message['Message']['id'];
                    $read_at = date('Y-m-d H:i:s');
                    $this->Message->saveField('read_at', $read_at);
                }
            } else {
                if ($message['Message']['sender'] == 'pro') {
                    $this->Message->id = $message['Message']['id'];
                    $read_at = date('Y-m-d H:i:s');
                    $this->Message->saveField('read_at', $read_at);
                }
            }

            $messages[$key]['Message']['read_at'] = $read_at;
            if ($message['Message']['sender'] == 'user') {
                $messages[$key]['User'] = $this->User->findById($message['Message']['user_id']);
            } else {
                $messages[$key]['User'] = $this->User->findById($message['Message']['pro_user_id']);
            }
        }

        $this->set('messages', $messages);
    }

    function send_message() {
        $this->layout = 'ajax';
        $this->loadModel('User');
        $message = array();
        $user_type = $this->Auth->user('user_type');
        $user_id = $this->Auth->user('id');
        $project_quote_id = $this->data['project_quote_id'];
        $attached_file = $this->data['attached_file'];

        $message['Message']['sender'] = $user_type;
        $message['Message']['project_id'] = $this->data['project_id'];
        $message['Message']['project_quote_id'] = $project_quote_id;
        $message['Message']['message'] = $this->data['message'];
        $message['Message']['create_date'] = date('Y-m-d H:i:s');
        $this->loadModel('ProjectQuote');
        $project_quote = $this->ProjectQuote->findById($project_quote_id);
        if ($user_type == 'user') {
            $message['Message']['pro_user_id'] = $project_quote['ProjectQuote']['user_id'];
            $message['Message']['user_id'] = $user_id;
        } else {
            $message['Message']['pro_user_id'] = $user_id;
            $message['Message']['user_id'] = $project_quote['Project']['user_id'];
        }

        if ($this->Message->save($message)) {
            $id = $this->Message->getInsertID();
            if ($attached_file) {
                $move_path = 'uploaded_files/message_attachment/' . $id;
                $folder = mkdir($move_path, 0777);
                $move_path = $move_path . '/' . $attached_file;
                $path = 'uploaded_files/temp_file/' . $attached_file;
                if (rename($path, $move_path)) {
                    $message['Message']['attachment'] = $attached_file;
                    $this->Message->id = $id;
                    $this->Message->saveField("attachment", $attached_file);
                }
            }
            $this->Message->recursive = -1;
            $message = $this->Message->findById($id);
            $message['user_name'] = $this->Auth->user('name');
            $message['pro_pic'] = $this->Auth->user('profile_pic');
            $message['user_id'] = $this->Auth->user('id');
            $this->set('message', $message);

            if ($message['Message']['sender'] == 'pro') {
                $user = $this->User->findById($message['Message']['user_id']);
                $notification_for = 'user';
            } else {
                $user = $this->User->findById($message['Message']['pro_user_id']);
                $notification_for = 'pro';
            }

//            $notification = array();
//            $this->Notification->create();
//            $notification['Notification']['user_id'] = $user['User']['id'];
//            $notification['Notification']['user_type'] = $notification_for;
//            $notification['Notification']['notification_type'] = 'received_message';
//            $notification['Notification']['related_table_id'] = $id;
//            $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
//            $this->Notification->save($notification);
        }
    }

    function file_attach() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $response = array();

        if (!empty($_FILES['message_attach'])) {
            $pathinfo = pathinfo($_FILES['message_attach']['name']);
            $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
            $path = 'uploaded_files/temp_file/' . $file_name;

            if (move_uploaded_file($_FILES['message_attach']['tmp_name'], $path)) {
                $response['type'] = 'success';
                $response['file_name'] = $file_name;
            } else {
                $response['type'] = 'error';
            }
        }
        return json_encode($response);
    }

    function remove_attachment() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $file_name = $this->data['file_name'];
        $path = 'uploaded_files/temp_file/' . $file_name;
        if (file_exists($path)) {
            if (unlink($path)) {
                echo 'success';
            }
        }
    }

    function read_message() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $id = $this->data['id'];
        $this->Message->id = $id;
        if ($this->Message->saveField("read_at", date('Y-m-d H:i:s'))) {
            echo 'success';
        }
    }

    function get_unread_count() {
        $this->layout = 'ajax';
        $user_type = $this->Auth->user('user_type');
        $user_id = $this->Auth->user('id');
        if ($user_type == 'pro') {
            $conditions = array('Message.sender' => 'user', 'Message.pro_user_id' => $user_id);
        } else {
            $conditions = array('Message.sender' => 'pro', 'Message.user_id' => $user_id);
        }
        $conditions['Message.read_at'] = NULL;
        $messages = $this->Message->find('count', array('conditions' => $conditions));
//        $messages = $this->Message->find('count', array('conditions' => $conditions, 'group_by' => 'Message.project_quote_id'));
        echo $messages;
//        echo '<pre>';
//        print_r($messages);

        exit();
    }

}
