<?php

App::uses('CakeTime', 'Utility');

class DashboardController extends AppController {

    var $helpers = array('Text', 'HtmlFixer');
    var $components = array('Cookie');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
    }

    function index() {
        $this->set('main_menu', 'dashboard');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
    }

    function manage_account() {
        $this->set('main_menu', 'dashboard');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $user_id = $this->Auth->user('id');
        $this->loadModel('User');
        $user = $this->User->findById($user_id);
        $this->loadModel('Country');
        $country = $this->Country->find('all');
        $this->set('countries', $country);
        $state_lists = array('AL' => "Alabama",
            'AK' => "Alaska",
            'AZ' => "Arizona",
            'AR' => "Arkansas",
            'CA' => "California",
            'CO' => "Colorado",
            'CT' => "Connecticut",
            'DE' => "Delaware",
            'DC' => "District Of Columbia",
            'FL' => "Florida",
            'GA' => "Georgia",
            'HI' => "Hawaii",
            'ID' => "Idaho",
            'IL' => "Illinois",
            'IN' => "Indiana",
            'IA' => "Iowa",
            'KS' => "Kansas",
            'KY' => "Kentucky",
            'LA' => "Louisiana",
            'ME' => "Maine",
            'MD' => "Maryland",
            'MA' => "Massachusetts",
            'MI' => "Michigan",
            'MN' => "Minnesota",
            'MS' => "Mississippi",
            'MO' => "Missouri",
            'MT' => "Montana",
            'NE' => "Nebraska",
            'NV' => "Nevada",
            'NH' => "New Hampshire",
            'NJ' => "New Jersey",
            'NM' => "New Mexico",
            'NY' => "New York",
            'NC' => "North Carolina",
            'ND' => "North Dakota",
            'OH' => "Ohio",
            'OK' => "Oklahoma",
            'OR' => "Oregon",
            'PA' => "Pennsylvania",
            'RI' => "Rhode Island",
            'SC' => "South Carolina",
            'SD' => "South Dakota",
            'TN' => "Tennessee",
            'TX' => "Texas",
            'UT' => "Utah",
            'VT' => "Vermont",
            'VA' => "Virginia",
            'WA' => "Washington",
            'WV' => "West Virginia",
            'WI' => "Wisconsin",
            'WY' => "Wyoming");
        $this->set('state_lists', $state_lists);

        
        $this->set('user', $user);
    }

    function transaction_history() {
        $this->set('main_menu', 'dashboard');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
    }

    function payment_method() {
        $this->set('main_menu', 'dashboard');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $this->loadModel('PaymentAccount');
        $user_id = $this->Auth->user('id');
        $payment_accounts = $this->PaymentAccount->find('all', array('conditions' => array('PaymentAccount.user_id' => $user_id, 'PaymentAccount.method' => 'payment')));
        $this->set('payment_accounts', $payment_accounts);
    }

    function payout_method() {
        $this->set('main_menu', 'dashboard');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $this->loadModel('PaymentAccount');
        $user_id = $this->Auth->user('id');
        $payment_accounts = $this->PaymentAccount->find('all', array('conditions' => array('PaymentAccount.user_id' => $user_id, 'PaymentAccount.method' => 'payout')));
        $this->set('payment_accounts', $payment_accounts);
    }

    function security() {
        $this->set('main_menu', 'dashboard');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $this->loadModel('SecretQuestion');
        $secret_questions = $this->SecretQuestion->find('all');
        $this->set('secret_questions', $secret_questions);
    }

    function badges() {
        $this->set('main_menu', 'dashboard');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
    }

    function your_project() {
        $this->set('main_menu', 'dashboard');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $this->loadModel('Project');
        $this->loadModel('Hire');
        $this->loadModel('ProUser');
        $this->loadModel('ProjectQuote');
        $user_id = $this->Auth->user('id');
        $this->Project->recursive = -1;
        $this->Hire->recursive = -1;
        $this->ProUser->recursive = 0;
        $this->ProjectQuote->recursive = -1;
        $projects = $this->Project->find('all', array('conditions' => array('Project.user_id' => $user_id)));
        foreach ($projects as $key => &$project) {
            $hire = $this->Hire->find('first', array('conditions' => array('Hire.project_id' => $project['Project']['id'])));
            $projects[$key]['hire'] = $hire;
            $projects[$key]['project_quote'] = $this->ProjectQuote->find('first', array('conditions' => array('ProjectQuote.project_id' => $project['Project']['id'])));
            $projects[$key]['pro_user'] = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $hire['Hire']['pro_user_id'])));
        }
        $this->set('projects', $projects);
    }

    function your_business() {
        $this->set('main_menu', 'dashboard');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
    }

    function show_review() {
        $this->layout = 'ajax';
        $hire_id = $this->data['id'];
        $this->loadModel('Hire');
        $this->loadModel('User');
        $hire = $this->Hire->findById($hire_id);
        $hire['pro_user'] = $this->User->findById($hire['Hire']['pro_user_id']);
        $this->set('hire', $hire);
    }

}
