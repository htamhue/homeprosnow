<?php

App::uses('CakeTime', 'Utility');

class BusinessController extends AppController {

    var $helpers = array('Text', 'HtmlFixer');
    var $components = array('Cookie');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
    }

    function index() {
        $user_id = $this->Auth->user('id');
        $this->loadModel('ProUser');
        if ($user_id and $this->ProUser->hasProAccount($user_id)) {
            $this->Session->write('Auth.User.user_type', 'pro');
            $this->redirect(array('controller' => 'business', 'action' => 'your_business'));
        }
        $this->set('main_menu', 'business');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $this->loadModel('HomeprosVideo');
        $landing_video = $this->HomeprosVideo->findById(2);
        $this->set('landing_video', $landing_video);
    }

    function your_business() {
        $this->set('main_menu', 'business');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $user_id = $this->Auth->user('id');
        $this->loadModel('ProUser');
        if (!$user_id or ! $this->ProUser->hasProAccount($user_id)) {
            $this->redirect(array('controller' => 'business', 'action' => 'index'));
        }
        $this->loadModel('ProUser');
        $this->loadModel('Hire');

        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $user_id)));

        $pro_user['project_count'] = $this->Hire->find('count', array('conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'])));
        $this->Hire->virtualFields['proRating'] = 'SUM(Hire.pro_review)';
        $pro_user['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.proRating'), 'conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed')));
        $this->Hire->virtualFields['reviewCount'] = 'count(Hire.pro_review)';
        $pro_user['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));


        $this->set('pro_user', $pro_user);
    }

    function your_business_details() {
        $this->set('main_menu', 'business');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $user_id = $this->Auth->user('id');
        $this->loadModel('ProUser');
        if (!$user_id or ! $this->ProUser->hasProAccount($user_id)) {
            $this->redirect(array('controller' => 'business', 'action' => 'index'));
        }
        $this->loadModel('ProUser');
        $this->loadModel('ProUserFile');
        $this->loadModel('User');
        $this->loadModel('Categorie');
        $this->loadModel('ProUserPackage');
        $this->loadModel('SubCategorie');
        $this->set('ownprofile', true);
        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $user_id)));

        if ($pro_user['ProUser']['operational_since'] != '0000-00-00') {
            $date1 = date_create($pro_user['ProUser']['operational_since']);
            $date2 = date_create(date('Y-m-d'));
            $diff = date_diff($date1, $date2);
            $res = $diff->format("%y years experience");
            $pro_user['ProUser']['experience'] = $res;
        } else {
            $pro_user['ProUser']['experience'] = '';
        }

        $pro_user['category'] = $this->Categorie->findById($pro_user['ProUser']['category_id']);

        foreach ($pro_user['ProUserCategorie'] as $key => &$val) {
            $val['sub_category'] = $this->SubCategorie->findById($val['sub_category_id']);
        }


        $this->loadModel('Hire');
        
       

//        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $user_id)));

        $pro_user['project_count'] = $this->Hire->find('count', array('conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'])));
        $this->Hire->virtualFields['proRating'] = 'SUM(Hire.pro_review)';
        $pro_user['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.proRating'), 'conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed')));
        $this->Hire->virtualFields['reviewCount'] = 'count(Hire.pro_review)';
        $pro_user['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));
        unset($this->Hire->virtualFields['proRating']);
        unset($this->Hire->virtualFields['reviewCount']);
        $pro_user['reviews'] = $this->Hire->find('all', array('conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));


        $sql = "SELECT * FROM `pro_users` as ProUser WHERE id in
(SELECT `pro_user_id` FROM `pro_user_packages` WHERE 
(category_id = " . $pro_user['ProUser']['category_id'] . ")"
                . " && (`pro_user_id`> 0) && (`view_count` > 0)) "
                . "&& (user_id !=" . $user_id . ") order by rand() limit 3";
        $pro_users = $this->ProUser->query($sql);
        $package_universal_profile = $this->ProUser->find('all', array('order' => 'rand()', 'limit' => 3, 'conditions' => array('ProUser.package_universal_profile' => 1, 'ProUser.category_id' => $pro_user['ProUser']['category_id'], 'ProUser.user_id !=' => $user_id)));
        
        $new_users = array_unique(array_merge($pro_users, $package_universal_profile), SORT_REGULAR);
        $pro_user['user_packages'] = $new_users;

        //$pro_user['user_packages'] = $this->ProUserPackage->find('all', array('order' => 'rand()', 'conditions' => array('ProUserPackage.category_id' => $pro_user['ProUser']['category_id'], 'ProUserPackage.view_count >' => 0)));

        foreach ($pro_user['user_packages'] as $key => &$val) {
            $val['cover_photo'] = $this->ProUserFile->find('first', array('conditions' => array('ProUserFile.pro_user_id' => $val['ProUser']['id'])));
            $this->Hire->virtualFields['proRating'] = 'SUM(Hire.pro_review)';
            $val['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.proRating'), 'conditions' => array('Hire.pro_user_id' => $val['User']['id'], 'Hire.hire_status' => 'completed')));
            $this->Hire->virtualFields['reviewCount'] = 'count(Hire.pro_review)';
            $val['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.pro_user_id' => $val['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));
            unset($this->Hire->virtualFields['proRating']);
            unset($this->Hire->virtualFields['reviewCount']);


            $val['user_details'] = $this->User->findById($val['ProUser']['user_id']);
            
            $user_package = $this->ProUserPackage->find('first', array('conditions' => array('ProUserPackage.pro_user_id' => $val['ProUser']['id'])));
            
            if ($val['ProUser']['package_universal_profile'] < 1) {
                if ($user_package['ProUserPackage']['view_count'] > 1) {
                    $ProUserPackage['ProUserPackage']['view_count'] = $user_package['ProUserPackage']['view_count'] - 1;
                    $ProUserPackage['ProUserPackage']['update_date'] = date('Y-m-d H:i:s');
                    $this->ProUserPackage->id = $user_package['ProUserPackage']['id'];
                    $this->ProUserPackage->save($ProUserPackage);
                }
            }
        }
        $this->set('pro_user', $pro_user);
    }

    function your_project() {
        $this->set('main_menu', 'business');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $user_id = $this->Auth->user('id');
        $this->loadModel('Hire');
        $this->loadModel('Categorie');
        $this->loadModel('ProjectQuote');
        $projects = array();
        $projects['active_project'] = $this->Hire->find('all', array('conditions' => array('Hire.pro_user_id' => $user_id, 'Hire.hire_status' => 'working')));
        $projects['complete_project'] = $this->Hire->find('all', array('conditions' => array('Hire.pro_user_id' => $user_id, 'Hire.hire_status' => 'completed')));
        $projects['canceled_project'] = $this->Hire->find('all', array('conditions' => array('Hire.pro_user_id' => $user_id, 'Hire.hire_status' => 'closed')));
        $this->Categorie->recursive = -1;
        $this->ProjectQuote->recursive = -1;

        foreach ($projects['active_project'] as $key => &$active_project) {
            $projects['active_project'][$key]['project_category'] = $this->Categorie->findById($active_project['Project']['category_id']);
            $projects['active_project'][$key]['project_quote'] = $this->ProjectQuote->findById($active_project['Hire']['project_quote_id']);
            $projects['active_project'][$key]['project_count'] = $this->Hire->find('count', array('conditions' => array('Hire.user_id' => $active_project['User']['id'])));
            $this->Hire->virtualFields['userRating'] = 'SUM(Hire.user_review)';
            $projects['active_project'][$key]['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.userRating'), 'conditions' => array('Hire.user_id' => $active_project['User']['id'], 'Hire.hire_status' => 'completed')));
            $this->Hire->virtualFields['reviewCount'] = 'count(Hire.user_review)';
            $projects['active_project'][$key]['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.user_id' => $active_project['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.user_review !=' => 0)));
            unset($this->Hire->virtualFields['userRating']);
            unset($this->Hire->virtualFields['reviewCount']);
            $projects['active_project'][$key]['reviews'] = $this->Hire->find('all', array('conditions' => array('Hire.user_id' => $active_project['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.user_review !=' => 0)));
        }
        foreach ($projects['complete_project'] as $key => &$complete_project) {
            $projects['complete_project'][$key]['project_category'] = $this->Categorie->findById($complete_project['Project']['category_id']);
            $projects['complete_project'][$key]['project_quote'] = $this->ProjectQuote->findById($complete_project['Hire']['project_quote_id']);
            $projects['complete_project'][$key]['project_count'] = $this->Hire->find('count', array('conditions' => array('Hire.user_id' => $complete_project['User']['id'])));
            $this->Hire->virtualFields['userRating'] = 'SUM(Hire.user_review)';
            $projects['complete_project'][$key]['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.userRating'), 'conditions' => array('Hire.user_id' => $complete_project['User']['id'], 'Hire.hire_status' => 'completed')));
            $this->Hire->virtualFields['reviewCount'] = 'count(Hire.user_review)';
            $projects['complete_project'][$key]['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.user_id' => $complete_project['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.user_review !=' => 0)));
            unset($this->Hire->virtualFields['userRating']);
            unset($this->Hire->virtualFields['reviewCount']);
            $projects['complete_project'][$key]['reviews'] = $this->Hire->find('all', array('conditions' => array('Hire.user_id' => $complete_project['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.user_review !=' => 0)));
        }
        foreach ($projects['canceled_project'] as $key => &$canceled_project) {
            $projects['canceled_project'][$key]['project_category'] = $this->Categorie->findById($canceled_project['Project']['category_id']);
            $projects['canceled_project'][$key]['project_quote'] = $this->ProjectQuote->findById($canceled_project['Hire']['project_quote_id']);
            $projects['canceled_project'][$key]['project_count'] = $this->Hire->find('count', array('conditions' => array('Hire.user_id' => $canceled_project['User']['id'])));
            $this->Hire->virtualFields['userRating'] = 'SUM(Hire.user_review)';
            $projects['canceled_project'][$key]['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.userRating'), 'conditions' => array('Hire.user_id' => $canceled_project['User']['id'], 'Hire.hire_status' => 'completed')));
            $this->Hire->virtualFields['reviewCount'] = 'count(Hire.user_review)';
            $projects['canceled_project'][$key]['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.user_id' => $canceled_project['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.user_review !=' => 0)));
            unset($this->Hire->virtualFields['userRating']);
            unset($this->Hire->virtualFields['reviewCount']);
            $projects['canceled_project'][$key]['reviews'] = $this->Hire->find('all', array('conditions' => array('Hire.user_id' => $canceled_project['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.user_review !=' => 0)));
        }
        $this->set('projects', $projects);
    }

}
