<?php

App::uses('CakeTime', 'Utility');

class ProController extends AppController {

    var $helpers = array('Text', 'HtmlFixer');
    var $components = array('Cookie');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index', 'add_pro', 'get_sub_category', 'add_pro_action', 'find_more_pro', 'find_pro', 'add_project', 'add_project_action', 'search_pro', 'profile', 'find_contractors', 'profinder', 'pro_finder_lists', 'submit_project_details', 'submit_project_details_action', 'claim_business');
        $this->loadModel('Notification');
        $this->loadModel('ProUser');
        $this->loadModel('ProServiceArea');
//        $this->loadModel('ProServiceOffer');
        $this->loadModel('ProAward');
        $this->loadModel('ProOperatingHour');
    }

    function index() {
        $user_id = $this->Auth->user('id');
        if ($user_id and $this->ProUser->hasProAccount($user_id)) {
            $this->redirect(array('controller' => 'business', 'action' => 'your_business'));
        }
        $this->layout = 'home';
        $this->set('view_header', 'landing');
        $this->loadModel('HomeprosVideo');
        $landing_video = $this->HomeprosVideo->findById(2);
        $this->set('landing_video', $landing_video);
    }

    function admin_pro_user_package() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('ProUserPackage');
        $this->loadModel('ProUserNewPackage');
        $this->loadModel('ProUser');
        $pro_user_id = $this->data['pro_user_id'];
        $category_id = $this->data['category_id'];
        $package = $this->data['package'];
        $package_home = $this->data['package_home'];
        $package_universal = $this->data['package_universal'];
        $package_universal_profile = $this->data['package_universal_profile'];


        if ($package == 1) {
            $price = 50;
            $view_count = 100;
        } else if ($package == 2) {
            $price = 75;
            $view_count = 200;
        } else if ($package == 3) {
            $price = 100;
            $view_count = 300;
        } else {
            $price = 0;
            $view_count = 0;
        }


        if ($package_home == 1) {
            $price_home = 50;
            $view_count_home = 100;
        } else if ($package_home == 2) {
            $price_home = 75;
            $view_count_home = 200;
        } else if ($package_home == 3) {
            $price_home = 100;
            $view_count_home = 300;
        } else {
            $price_home = 0;
            $view_count_home = 0;
        }

        if (($package > 0) || ($package_home > 0) || ($package_universal_profile > 0) || ($package_universal > 0)) { 
            if ($package_universal_profile) {
                $ProUser['ProUser']['package_universal_profile'] = $package_universal_profile;
                $this->ProUser->id = $pro_user_id;
                $this->ProUser->save($ProUser);
            }
            
            if ($package_universal) {
                $ProUser['ProUser']['package_universal'] = $package_universal;
                $this->ProUser->id = $pro_user_id;
                $this->ProUser->save($ProUser);
            }

            if (($package > 0) || ($package_home > 0)) {
                $this->ProUserNewPackage->create();
                $ProUserNewPackage['ProUserNewPackage']['pro_user_id'] = $pro_user_id;
                $ProUserNewPackage['ProUserNewPackage']['package'] = $package;
                $ProUserNewPackage['ProUserNewPackage']['package_home'] = $package_home;
                $ProUserNewPackage['ProUserNewPackage']['pro_user_id'] = $pro_user_id;
                $ProUserNewPackage['ProUserNewPackage']['category_id'] = $category_id;
                $ProUserNewPackage['ProUserNewPackage']['view_count'] = $view_count;
                $ProUserNewPackage['ProUserNewPackage']['view_count_home'] = $view_count_home;
                $ProUserNewPackage['ProUserNewPackage']['create_date'] = date('Y-m-d H:i:s');
                $ProUserNewPackage['ProUserNewPackage']['update_date'] = date('Y-m-d H:i:s');
                $this->ProUserNewPackage->save($ProUserNewPackage);


                $exist = @$this->ProUserPackage->find('first', array('conditions' => array('pro_user_id' => $pro_user_id)));
                if ($exist) {
                    $ProUserPackage['ProUserPackage']['pro_user_id'] = $pro_user_id;
                    $ProUserPackage['ProUserPackage']['category_id'] = $category_id;
                    $ProUserPackage['ProUserPackage']['view_count'] = $exist['ProUserPackage']['view_count'] + $view_count;
                    $ProUserPackage['ProUserPackage']['view_count_home'] = $exist['ProUserPackage']['view_count_home'] + $view_count_home;
                    $ProUserPackage['ProUserPackage']['update_date'] = date('Y-m-d H:i:s');
                    $this->ProUserPackage->id = $exist['ProUserPackage']['id'];
                    $this->ProUserPackage->save($ProUserPackage);
                } else {
                    $this->ProUserPackage->create();
                    $ProUserPackage['ProUserPackage']['pro_user_id'] = $pro_user_id;
                    $ProUserPackage['ProUserPackage']['category_id'] = $category_id;
                    $ProUserPackage['ProUserPackage']['view_count'] = $view_count;
                    $ProUserPackage['ProUserPackage']['view_count_home'] = $view_count_home;
                    $ProUserPackage['ProUserPackage']['create_date'] = date('Y-m-d H:i:s');
                    $ProUserPackage['ProUserPackage']['update_date'] = date('Y-m-d H:i:s');
                    $this->ProUserPackage->save($ProUserPackage);
                }
            }

            echo "Successfuly Inserted Package";
        } else {
            echo "Error";
        }
    }

    function pro_user_award_delete() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('ProAward');


        $aw_id = $this->data['aw_id'];
        $aw = $this->ProAward->findById($aw_id);

        if ($aw) {
            if ($this->ProAward->delete($aw['ProAward']['id'])) {
                $file_path = 'uploaded_files/pro_user_files/awards/' . $aw['ProAward']['pro_user_id'] . "/" . $aw['ProAward']['award_image'];
                if (file_exists($file_path)) {
                    unlink($file_path);
                }
                echo 'success';
            }
        }


        //echo $aw_id;
    }

    function admin_pro_user_award_delete() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('ProAward');

        $aw_id = $this->data['aw_id'];
        $aw = $this->ProAward->findById($aw_id);

        if ($aw) {
            if ($this->ProAward->delete($aw['ProAward']['id'])) {
                $file_path = 'uploaded_files/pro_user_files/awards/' . $aw['ProAward']['pro_user_id'] . "/" . $aw['ProAward']['award_image'];
                if (file_exists($file_path)) {
                    unlink($file_path);
                }
                echo 'success';
            }
        }


        //echo $aw_id;
    }

    function admin_edit_pro_by_admin($user_id) {
        $this->layout = 'admin_pages';
        $this->loadModel('User');
        $user = $this->User->findById($user_id);
        $this->set('user', $user);


        $this->set('Categories', $Categories);

        $this->loadModel('ProUser');
        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $user['User']['id'])));
        $this->set('pro_user', $pro_user);
        $this->loadModel('Categorie');

        $this->loadModel('SubCategorie');
        $SubCategories = $this->SubCategorie->find('all', array('conditions' => array('SubCategorie.category_id' => $pro_user['ProUser']['category_id'])));
        $this->set('sub_categories', $SubCategories);
        
        
        $this->loadModel('ProUserPackage');
        $ProUserPackage = $this->ProUserPackage->find('first', array('conditions' => array('ProUserPackage.pro_user_id' => $pro_user['ProUser']['id'])));
        $this->set('package', $ProUserPackage);

        $this->loadModel('ProUserCategorie');
        $pro_user_sub_categorie = $this->ProUserCategorie->find('list', array('fields' => array('ProUserCategorie.sub_category_id'), 'conditions' => array('ProUserCategorie.pro_user_id' => $pro_user['ProUser']['id'], 'ProUserCategorie.category_id' => $pro_user['ProUser']['category_id'])));
        $pro_user_sub_categorie = array_unique($pro_user_sub_categorie);
        $Categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        
        $this->set('pro_user_sub_categorie', $pro_user_sub_categorie);
        $this->set('Categories', $Categories);
        $this->loadModel('ClaimBusiness');

        $claimCount = $this->ClaimBusiness->find('count', array('conditions' => array('ClaimBusiness.pro_user_id' => $pro_user['ProUser']['id'])));
        $this->set('claimCount', $claimCount);

        $this->loadModel('ProUserNewPackage');
        $ProUserNewPackage = $this->ProUserNewPackage->find('first', array( 'order' => 'ProUserNewPackage.id DESC', 'conditions' => array('ProUserNewPackage.pro_user_id' => $pro_user['ProUser']['id'])));
        $this->set('newpackage', $ProUserNewPackage);
        
        

        $state_lists = array('AL' => "Alabama",
            'AK' => "Alaska",
            'AZ' => "Arizona",
            'AR' => "Arkansas",
            'CA' => "California",
            'CO' => "Colorado",
            'CT' => "Connecticut",
            'DE' => "Delaware",
            'DC' => "District Of Columbia",
            'FL' => "Florida",
            'GA' => "Georgia",
            'HI' => "Hawaii",
            'ID' => "Idaho",
            'IL' => "Illinois",
            'IN' => "Indiana",
            'IA' => "Iowa",
            'KS' => "Kansas",
            'KY' => "Kentucky",
            'LA' => "Louisiana",
            'ME' => "Maine",
            'MD' => "Maryland",
            'MA' => "Massachusetts",
            'MI' => "Michigan",
            'MN' => "Minnesota",
            'MS' => "Mississippi",
            'MO' => "Missouri",
            'MT' => "Montana",
            'NE' => "Nebraska",
            'NV' => "Nevada",
            'NH' => "New Hampshire",
            'NJ' => "New Jersey",
            'NM' => "New Mexico",
            'NY' => "New York",
            'NC' => "North Carolina",
            'ND' => "North Dakota",
            'OH' => "Ohio",
            'OK' => "Oklahoma",
            'OR' => "Oregon",
            'PA' => "Pennsylvania",
            'RI' => "Rhode Island",
            'SC' => "South Carolina",
            'SD' => "South Dakota",
            'TN' => "Tennessee",
            'TX' => "Texas",
            'UT' => "Utah",
            'VT' => "Vermont",
            'VA' => "Virginia",
            'WA' => "Washington",
            'WV' => "West Virginia",
            'WI' => "Wisconsin",
            'WY' => "Wyoming");
        $this->loadModel('User');
        $this->set('state_lists', $state_lists);
    }

    function admin_update_user_profile_by_admin() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $user_id = $this->data['user_id'];
        $name = $this->data['name'];
        $email = $this->data['email'];
        $phone = $this->data['phone'];
        $password = $this->data['password'];



        $this->loadModel('User');
        $this->User->recursive = -1;
        $user = $this->User->findById($user_id);
        $any_user = $this->User->find('first', array(
            'conditions' => array('User.email' => $email, 'User.id !=' => $user_id)
        ));
        $response = array();
        if (strlen($email) == 0) {
            $error['emailError'] = 'Email is required';
        } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
            $error['emailError'] = 'Email is not valid';
        } else if ($any_user['User']['email'] == $email) {
            $error['emailError'] = 'Email is already exist';
        }

//        else if ($any_user['User']['id'] != $user['User']['id']) {
//            $error['emailError'] = 'Email is already exist';
//        }

        if (!$error) {
            $user_data = array();
            if (!empty($_FILES['profile_pic']['name'])) {
                $path = 'uploaded_files/profile_pic/' . $user_id;
                if (!file_exists($path)) {
                    $folder = mkdir($path, 0777);
                    chmod($path, 0777);
                }
                $accepted_type = array('jpg', 'png', 'gif');
                $pathinfo = pathinfo($_FILES['profile_pic']['name']);
                $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
                $finalpath = $path . '/' . $file_name;
                if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                    if (move_uploaded_file($_FILES['profile_pic']['tmp_name'], $finalpath)) {
                        unlink($path . '/' . $user['User']['profile_pic']);
                        $user_data['User']['profile_pic'] = $file_name;
                    } else {
                        $response['type'] = 'error';
                    }
                } else {
                    $error['imageError'] = 'You can upload jpg, png and gif image';
                }
            }
            if (!$error) {
                $user_data['User']['name'] = $name;
                $user_data['User']['phone'] = $phone;
                $user_data['User']['email'] = $email;
                if ($password) {
                    $user_data['User']['password'] = AuthComponent::password($password);
                }

                $this->User->id = $user_id;
                $this->User->save($user_data);
                $user = $this->User->findById($user_id);
                $this->reload_user_data();
                $response['type'] = 'success';
                $response['user'] = $user;
            } else {
                $response['type'] = 'error';
                $response['error'] = $error;
            }
        } else {
            $response['type'] = 'error';
            $response['error'] = $error;
        }


        return json_encode($response);
    }

    function admin_live_profile_pic_upload_by_admin() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $user_id = $this->data['user_id'];

        $this->loadModel('User');
        $this->User->recursive = -1;
        $user = $this->User->findById($user_id);

        $response = array();

        $user_data = array();
        if (!empty($_FILES['profile_pic']['name'])) {
            $path = 'uploaded_files/profile_pic/' . $user_id;
            if (!file_exists($path)) {
                $folder = mkdir($path, 0777);
                chmod($path, 0777);
            }
            $accepted_type = array('jpg', 'png', 'gif');
            $pathinfo = pathinfo($_FILES['profile_pic']['name']);
            $file_name = Inflector::slug($pathinfo['filename']) . '.' . $pathinfo['extension'];
            $finalpath = $path . '/' . $file_name;
            if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                if (move_uploaded_file($_FILES['profile_pic']['tmp_name'], $finalpath)) {
                    unlink($path . '/' . $user['User']['profile_pic']);
                    $user_data['User']['profile_pic'] = $file_name;
                } else {
                    $response['type'] = 'error';
                }
            } else {
                $error['imageError'] = 'You can upload jpg, png and gif image';
            }
        }
        if (!$error) {
            $this->User->id = $user_id;
            $this->User->save($user_data);
            $user = $this->User->findById($user_id);
            $response['type'] = 'success';
            $response['user'] = $user;
        } else {
            $response['type'] = 'error';
            $response['error'] = $error;
        }

        return json_encode($response);
    }

    function admin_edit_pro_by_admin_action() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('ProUser');
        $this->loadModel('User');
        $this->loadModel('CompanyLicense');
        $this->loadModel('ProUserCategorie');
        $this->loadModel('ProUserFile');
        $response = array();
        $error = array();
        $proUser = array();
        $proUserCategoty = array();
        $proUserlicense = array();
        $user_id = $this->data['user_id'];

        if (strlen($this->data['address']) == 0) {
            $error['addressError'] = __('Your address is required');
        } else {
            $proUser['ProUser']['address'] = $this->data['address'];
        }

        if (strlen($this->data['state']) == 0) {
            $error['stateError'] = __('Your state is required');
        } else {
            $proUser['ProUser']['state'] = $this->data['state'];
        }

        if (strlen($this->data['city']) == 0) {
            $error['cityError'] = __('Your city is required');
        } else {
            $proUser['ProUser']['city'] = $this->data['city'];
        }

        if (strlen($this->data['zip_code']) == 0) {
            $error['zipCodeError'] = __('Your zip code is required');
        } elseif (!$this->User->validateUSAZip($this->data['zip_code'])) {
            $error['zipCodeError'] = __('The zip code is not a valid USA zip code');
        } else {
            $proUser['ProUser']['zip_code'] = $this->data['zip_code'];
        }
        if (strlen($this->data['company_name']) == 0) {
            $error['companyNameError'] = __('Your company name is required');
        } else {
            $proUser['ProUser']['company_name'] = $this->data['company_name'];
        }
        if (!$this->data['category']) {
            $error['categoryError'] = __('Please select category');
        } else {
            $proUser['ProUser']['category_id'] = $this->data['category'];
        }

        $proUser['ProUser']['company_description'] = $this->data['company_description'];
        $proUser['ProUser']['update_date'] = date('Y-m-d H:i:s');

        $proUser['ProUser']['operational_since'] = $this->data['operational_since'];
//        if (strlen($this->data['business_phone_number']) == 0) {
//            $error['businessPhoneNumberError'] = __('Business phone number is required');
//        } else {
//            $proUser['ProUser']['business_phone'] = $this->data['business_phone_number'];
//        }
        $proUser['ProUser']['business_phone'] = $this->data['business_phone_number'];
        $proUser['ProUser']['website_url'] = $this->data['website'];
        $proUser['ProUser']['facebook_link'] = $this->data['facebook_link'];
        $proUser['ProUser']['twitter_link'] = $this->data['twitter_link'];
        $proUser['ProUser']['linkedin_link'] = $this->data['linkedin_link'];
        $proUser['ProUser']['google_link'] = $this->data['google_link'];
        $proUser['ProUser']['youtube_link'] = $this->data['youtube_link'];

        $path_one = 'uploaded_files/pro_user_files/cover_photo/';
        $pathinfo_one = pathinfo($_FILES['cover_photo']['name']);
        $basename_one = strtolower($pathinfo_one['filename']);
        $extension_one = strtolower($pathinfo_one['extension']);
        $file_name_one = $basename_one . time() . '.' . $extension_one;

        $filepath_one = $path_one . $file_name_one;
        if (move_uploaded_file($_FILES['cover_photo']['tmp_name'], $filepath_one)) {
            $proUser['ProUser']['cover_photo'] = $file_name_one;
        }

        $path_two = 'uploaded_files/pro_user_files/business_logo/';
        $pathinfo_two = pathinfo($_FILES['business_logo']['name']);
        $basename_two = strtolower($pathinfo_two['filename']);
        $extension_two = strtolower($pathinfo_two['extension']);
        $file_name_two = $basename_two . time() . '.' . $extension_two;

        $filepath_two = $path_two . $file_name_two;
        if (move_uploaded_file($_FILES['business_logo']['tmp_name'], $filepath_two)) {
            $proUser['ProUser']['business_logo'] = $file_name_two;
        }




//        if (empty($this->data['sub_category'])) {
//            $error['subCategoryError'] = __('Please select at least one sub category');
//        }


        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $user_id)));
        if (!$error) {
            $pro_user_id = $pro_user['ProUser']['id'];
            $this->ProUser->id = $pro_user_id;
            if ($this->ProUser->save($proUser)) {
                $this->ProUserCategorie->deleteAll(array('ProUserCategorie.pro_user_id' => $pro_user['ProUser']['id']));
                foreach ($this->data['sub_category'] as $key => $val) {
                    $this->ProUserCategorie->create();
                    $proUserCategoty['ProUserCategorie']['pro_user_id'] = $pro_user_id;
                    $proUserCategoty['ProUserCategorie']['category_id'] = $this->data['category'];
                    $proUserCategoty['ProUserCategorie']['sub_category_id'] = $val;
                    $proUserCategoty['ProUserCategorie']['create_date'] = date('Y-m-d H:i:s');
                    $this->ProUserCategorie->save($proUserCategoty);
                }

                $this->CompanyLicense->deleteAll(array('CompanyLicense.pro_user_id' => $pro_user['ProUser']['id']));
                if (!empty($this->data['license_number'])) {
                    foreach ($this->data['license_number'] as $key => $val) {
                        if ($val == "") {
                            
                        }
                        $this->CompanyLicense->create();
                        $proUserlicense['CompanyLicense']['pro_user_id'] = $pro_user_id;
                        $proUserlicense['CompanyLicense']['license_number'] = $val;
                        $proUserlicense['CompanyLicense']['license_type'] = $this->data['license_type'][$key];
                        $proUserlicense['CompanyLicense']['expir_date'] = $this->data['expir_date'][$key];
                        $proUserlicense['CompanyLicense']['create_date'] = date('Y-m-d H:i:s');
                        $this->CompanyLicense->save($proUserlicense);
                    }
                }


                if (!empty($_FILES['past_work_pic'])) {
                    $path = 'uploaded_files/pro_user_files/' . $pro_user_id;
                    if (!file_exists($path)) {
                        $folder = mkdir($path, 0777);
                    }

                    foreach ($_FILES['past_work_pic']['name'] as $key => $past_work_pic) {
                        $filename = $path . '/' . $past_work_pic;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($past_work_pic);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['past_work_pic']['tmp_name'][$key], $filename)) {
                                $this->ProUserFile->create();
                                $ProUserFile['ProUserFile']['pro_user_id'] = $pro_user_id;
                                $ProUserFile['ProUserFile']['image'] = $past_work_pic;
                                $ProUserFile['ProUserFile']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProUserFile->save($ProUserFile);
                            }
                        }
                    }
                }


                if ($this->data['service_area_item']) {
                    $this->ProServiceArea->deleteAll(array('ProServiceArea.pro_user_id' => $pro_user['ProUser']['id']));
                    foreach ($this->data['service_area_item'] as $key => $val) {
                        $this->ProServiceArea->create();
                        $ProServiceArea['ProServiceArea']['pro_user_id'] = $pro_user_id;
                        $ProServiceArea['ProServiceArea']['service_area'] = $val;
                        $ProServiceArea['ProServiceArea']['latitude'] = $this->data['service_area_lat'][$key];
                        $ProServiceArea['ProServiceArea']['longitude'] = $this->data['service_area_lng'][$key];
                        $ProServiceArea['ProServiceArea']['create_date'] = date('Y-m-d H:i:s');
                        $this->ProServiceArea->save($ProServiceArea);
                    }
                }

                if (!empty($_FILES['awards_achievements_image'])) {
                    $path = 'uploaded_files/pro_user_files/awards/' . $pro_user_id;
                    $folder = mkdir($path, 0777);


                    foreach ($_FILES['awards_achievements_image']['name'] as $key => $awards_achievements_image) {
                        $filename = $path . '/' . $awards_achievements_image;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($awards_achievements_image);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['awards_achievements_image']['tmp_name'][$key], $filename)) {
                                $this->ProAward->create();
                                $ProAward['ProAward']['pro_user_id'] = $pro_user_id;
                                $ProAward['ProAward']['award_image'] = $awards_achievements_image;
                                $ProAward['ProAward']['award'] = $this->data['awards_achievements_item'][$key];
                                $ProAward['ProAward']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProAward->save($ProAward);
                            }
                        }
                    }
                }

                $this->ProOperatingHour->deleteAll(array('ProOperatingHour.pro_user_id' => $pro_user['ProUser']['id']));

                $this->ProOperatingHour->create();
                $ProOperatingHour['ProOperatingHour']['pro_user_id'] = $pro_user_id;
                if (isset($this->data['mon_start'])) {
                    $ProOperatingHour['ProOperatingHour']['mon_start'] = $this->data['mon_start'];
                }
                if (isset($this->data['mon_end'])) {
                    $ProOperatingHour['ProOperatingHour']['mon_end'] = $this->data['mon_end'];
                }
                if (isset($this->data['tue_start'])) {
                    $ProOperatingHour['ProOperatingHour']['tue_start'] = $this->data['tue_start'];
                }
                if (isset($this->data['tue_start'])) {
                    $ProOperatingHour['ProOperatingHour']['tue_end'] = $this->data['tue_end'];
                }
                if (isset($this->data['wed_start'])) {
                    $ProOperatingHour['ProOperatingHour']['wed_start'] = $this->data['wed_start'];
                }
                if (isset($this->data['wed_end'])) {
                    $ProOperatingHour['ProOperatingHour']['wed_end'] = $this->data['wed_end'];
                }
                if (isset($this->data['thu_start'])) {
                    $ProOperatingHour['ProOperatingHour']['thu_start'] = $this->data['thu_start'];
                }
                if (isset($this->data['thu_end'])) {
                    $ProOperatingHour['ProOperatingHour']['thu_end'] = $this->data['thu_end'];
                }
                if (isset($this->data['fri_start'])) {
                    $ProOperatingHour['ProOperatingHour']['fri_start'] = $this->data['fri_start'];
                }
                if (isset($this->data['fri_end'])) {
                    $ProOperatingHour['ProOperatingHour']['fri_end'] = $this->data['fri_end'];
                }
                if (isset($this->data['sat_start'])) {
                    $ProOperatingHour['ProOperatingHour']['sat_start'] = $this->data['sat_start'];
                }
                if (isset($this->data['sat_end'])) {
                    $ProOperatingHour['ProOperatingHour']['sat_end'] = $this->data['sat_end'];
                }
                if (isset($this->data['sun_start'])) {
                    $ProOperatingHour['ProOperatingHour']['sun_start'] = $this->data['sun_start'];
                }
                if (isset($this->data['sun_end'])) {
                    $ProOperatingHour['ProOperatingHour']['sun_end'] = $this->data['sun_end'];
                }
                $ProOperatingHour['ProOperatingHour']['create_date'] = date('Y-m-d H:i:s');
                $this->ProOperatingHour->save($ProOperatingHour);

                $response['type'] = "success";
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }
        return json_encode($response);
    }

    function admin_add_pro_by_admin() {
        $this->layout = 'admin_pages';
//        $this->set('view_header', 'dashboard');
//        $user_id = $this->Auth->user('id');
        $this->loadModel('Categorie');
        $Categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('Categories', $Categories);



        $state_lists = array('AL' => "Alabama",
            'AK' => "Alaska",
            'AZ' => "Arizona",
            'AR' => "Arkansas",
            'CA' => "California",
            'CO' => "Colorado",
            'CT' => "Connecticut",
            'DE' => "Delaware",
            'DC' => "District Of Columbia",
            'FL' => "Florida",
            'GA' => "Georgia",
            'HI' => "Hawaii",
            'ID' => "Idaho",
            'IL' => "Illinois",
            'IN' => "Indiana",
            'IA' => "Iowa",
            'KS' => "Kansas",
            'KY' => "Kentucky",
            'LA' => "Louisiana",
            'ME' => "Maine",
            'MD' => "Maryland",
            'MA' => "Massachusetts",
            'MI' => "Michigan",
            'MN' => "Minnesota",
            'MS' => "Mississippi",
            'MO' => "Missouri",
            'MT' => "Montana",
            'NE' => "Nebraska",
            'NV' => "Nevada",
            'NH' => "New Hampshire",
            'NJ' => "New Jersey",
            'NM' => "New Mexico",
            'NY' => "New York",
            'NC' => "North Carolina",
            'ND' => "North Dakota",
            'OH' => "Ohio",
            'OK' => "Oklahoma",
            'OR' => "Oregon",
            'PA' => "Pennsylvania",
            'RI' => "Rhode Island",
            'SC' => "South Carolina",
            'SD' => "South Dakota",
            'TN' => "Tennessee",
            'TX' => "Texas",
            'UT' => "Utah",
            'VT' => "Vermont",
            'VA' => "Virginia",
            'WA' => "Washington",
            'WV' => "West Virginia",
            'WI' => "Wisconsin",
            'WY' => "Wyoming");
        $this->loadModel('User');
        $this->set('state_lists', $state_lists);
    }

    function add_pro() {
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $user_id = $this->Auth->user('id');
        $this->loadModel('Categorie');
        $Categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('Categories', $Categories);

        if ($user_id and $this->ProUser->hasProAccount($user_id)) {
            $this->redirect(array('controller' => 'business', 'action' => 'your_business'));
        }

        $state_lists = array('AL' => "Alabama",
            'AK' => "Alaska",
            'AZ' => "Arizona",
            'AR' => "Arkansas",
            'CA' => "California",
            'CO' => "Colorado",
            'CT' => "Connecticut",
            'DE' => "Delaware",
            'DC' => "District Of Columbia",
            'FL' => "Florida",
            'GA' => "Georgia",
            'HI' => "Hawaii",
            'ID' => "Idaho",
            'IL' => "Illinois",
            'IN' => "Indiana",
            'IA' => "Iowa",
            'KS' => "Kansas",
            'KY' => "Kentucky",
            'LA' => "Louisiana",
            'ME' => "Maine",
            'MD' => "Maryland",
            'MA' => "Massachusetts",
            'MI' => "Michigan",
            'MN' => "Minnesota",
            'MS' => "Mississippi",
            'MO' => "Missouri",
            'MT' => "Montana",
            'NE' => "Nebraska",
            'NV' => "Nevada",
            'NH' => "New Hampshire",
            'NJ' => "New Jersey",
            'NM' => "New Mexico",
            'NY' => "New York",
            'NC' => "North Carolina",
            'ND' => "North Dakota",
            'OH' => "Ohio",
            'OK' => "Oklahoma",
            'OR' => "Oregon",
            'PA' => "Pennsylvania",
            'RI' => "Rhode Island",
            'SC' => "South Carolina",
            'SD' => "South Dakota",
            'TN' => "Tennessee",
            'TX' => "Texas",
            'UT' => "Utah",
            'VT' => "Vermont",
            'VA' => "Virginia",
            'WA' => "Washington",
            'WV' => "West Virginia",
            'WI' => "Wisconsin",
            'WY' => "Wyoming");
        $this->loadModel('User');
        $user = $this->User->findById($user_id);
        $this->set('state_lists', $state_lists);
        $this->set('user', $user);
    }

    function edit_pro() {
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $user_id = $this->Auth->user('id');
        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $user_id)));
        $this->set('pro_user', $pro_user);
        $this->loadModel('Categorie');

        $this->loadModel('SubCategorie');
        $SubCategories = $this->SubCategorie->find('all', array('conditions' => array('SubCategorie.category_id' => $pro_user['ProUser']['category_id'])));
        $this->set('sub_categories', $SubCategories);
        
        
        $this->loadModel('ProUserPackage');
        $ProUserPackage = $this->ProUserPackage->find('first', array('conditions' => array('ProUserPackage.pro_user_id' => $pro_user['ProUser']['id'])));
        $this->set('package', $ProUserPackage);

        $this->loadModel('ProUserCategorie');
        $pro_user_sub_categorie = $this->ProUserCategorie->find('list', array('fields' => array('ProUserCategorie.sub_category_id'), 'conditions' => array('ProUserCategorie.pro_user_id' => $pro_user['ProUser']['id'], 'ProUserCategorie.category_id' => $pro_user['ProUser']['category_id'])));
        $pro_user_sub_categorie = array_unique($pro_user_sub_categorie);
        $Categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('pro_user_sub_categorie', $pro_user_sub_categorie);
        $this->set('Categories', $Categories);
        
        $this->loadModel('ProUserNewPackage');
        $ProUserNewPackage = $this->ProUserNewPackage->find('first', array( 'order' => 'ProUserNewPackage.id DESC', 'conditions' => array('ProUserNewPackage.pro_user_id' => $pro_user['ProUser']['id'])));
        $this->set('newpackage', $ProUserNewPackage);

        $state_lists = array('AL' => "Alabama",
            'AK' => "Alaska",
            'AZ' => "Arizona",
            'AR' => "Arkansas",
            'CA' => "California",
            'CO' => "Colorado",
            'CT' => "Connecticut",
            'DE' => "Delaware",
            'DC' => "District Of Columbia",
            'FL' => "Florida",
            'GA' => "Georgia",
            'HI' => "Hawaii",
            'ID' => "Idaho",
            'IL' => "Illinois",
            'IN' => "Indiana",
            'IA' => "Iowa",
            'KS' => "Kansas",
            'KY' => "Kentucky",
            'LA' => "Louisiana",
            'ME' => "Maine",
            'MD' => "Maryland",
            'MA' => "Massachusetts",
            'MI' => "Michigan",
            'MN' => "Minnesota",
            'MS' => "Mississippi",
            'MO' => "Missouri",
            'MT' => "Montana",
            'NE' => "Nebraska",
            'NV' => "Nevada",
            'NH' => "New Hampshire",
            'NJ' => "New Jersey",
            'NM' => "New Mexico",
            'NY' => "New York",
            'NC' => "North Carolina",
            'ND' => "North Dakota",
            'OH' => "Ohio",
            'OK' => "Oklahoma",
            'OR' => "Oregon",
            'PA' => "Pennsylvania",
            'RI' => "Rhode Island",
            'SC' => "South Carolina",
            'SD' => "South Dakota",
            'TN' => "Tennessee",
            'TX' => "Texas",
            'UT' => "Utah",
            'VT' => "Vermont",
            'VA' => "Virginia",
            'WA' => "Washington",
            'WV' => "West Virginia",
            'WI' => "Wisconsin",
            'WY' => "Wyoming");
        $this->set('state_lists', $state_lists);
    }

    function edit_pro_action() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('ProUser');
        $this->loadModel('User');
        $this->loadModel('CompanyLicense');
        $this->loadModel('ProUserCategorie');
        $this->loadModel('ProUserFile');
        $response = array();
        $error = array();
        $proUser = array();
        $proUserCategoty = array();
        $proUserlicense = array();
        $user_id = $this->Auth->user('id');

        if (strlen($this->data['address']) == 0) {
            $error['addressError'] = __('Your address is required');
        } else {
            $proUser['ProUser']['address'] = $this->data['address'];
        }

        if (strlen($this->data['state']) == 0) {
            $error['stateError'] = __('Your state is required');
        } else {
            $proUser['ProUser']['state'] = $this->data['state'];
        }

        if (strlen($this->data['city']) == 0) {
            $error['cityError'] = __('Your city is required');
        } else {
            $proUser['ProUser']['city'] = $this->data['city'];
        }

        if (strlen($this->data['zip_code']) == 0) {
            $error['zipCodeError'] = __('Your zip code is required');
        } elseif (!$this->User->validateUSAZip($this->data['zip_code'])) {
            $error['zipCodeError'] = __('The zip code is not a valid USA zip code');
        } else {
            $proUser['ProUser']['zip_code'] = $this->data['zip_code'];
        }
        if (strlen($this->data['company_name']) == 0) {
            $error['companyNameError'] = __('Your company name is required');
        } else {
            $proUser['ProUser']['company_name'] = $this->data['company_name'];
        }
        if (!$this->data['category']) {
            $error['categoryError'] = __('Please select category');
        } else {
            $proUser['ProUser']['category_id'] = $this->data['category'];
        }

//        $proUser['ProUser']['is_verified'] = 0;
        $proUser['ProUser']['company_description'] = $this->data['company_description'];
        $proUser['ProUser']['update_date'] = date('Y-m-d H:i:s');

        $proUser['ProUser']['operational_since'] = $this->data['operational_since'];
//        if (strlen($this->data['business_phone_number']) == 0) {
//            $error['businessPhoneNumberError'] = __('Business phone number is required');
//        } else {
//            $proUser['ProUser']['business_phone'] = $this->data['business_phone_number'];
//        }
        $proUser['ProUser']['business_phone'] = $this->data['business_phone_number'];
        $proUser['ProUser']['website_url'] = $this->data['website'];
        $proUser['ProUser']['facebook_link'] = $this->data['facebook_link'];
        $proUser['ProUser']['twitter_link'] = $this->data['twitter_link'];
        $proUser['ProUser']['linkedin_link'] = $this->data['linkedin_link'];
        $proUser['ProUser']['google_link'] = $this->data['google_link'];
        $proUser['ProUser']['youtube_link'] = $this->data['youtube_link'];

//        if (empty($this->data['sub_category'])) {
//            $error['subCategoryError'] = __('Please select at least one sub category');
//        }

        $path_one = 'uploaded_files/pro_user_files/cover_photo/';
        $pathinfo_one = pathinfo($_FILES['cover_photo']['name']);
        $basename_one = strtolower($pathinfo_one['filename']);
        $extension_one = strtolower($pathinfo_one['extension']);
        $file_name_one = $basename_one . time() . '.' . $extension_one;

        $filepath_one = $path_one . $file_name_one;
        if (move_uploaded_file($_FILES['cover_photo']['tmp_name'], $filepath_one)) {
            $proUser['ProUser']['cover_photo'] = $file_name_one;
        }

        $path_two = 'uploaded_files/pro_user_files/business_logo/';
        $pathinfo_two = pathinfo($_FILES['business_logo']['name']);
        $basename_two = strtolower($pathinfo_two['filename']);
        $extension_two = strtolower($pathinfo_two['extension']);
        $file_name_two = $basename_two . time() . '.' . $extension_two;

        $filepath_two = $path_two . $file_name_two;
        if (move_uploaded_file($_FILES['business_logo']['tmp_name'], $filepath_two)) {
            $proUser['ProUser']['business_logo'] = $file_name_two;
        }


        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $user_id)));
        if (!$error) {
            $pro_user_id = $this->data['id'];
            $this->ProUser->id = $pro_user_id;
            if ($this->ProUser->save($proUser)) {
                $this->ProUserCategorie->deleteAll(array('ProUserCategorie.pro_user_id' => $pro_user['ProUser']['id']));
                foreach ($this->data['sub_category'] as $key => $val) {
                    $this->ProUserCategorie->create();
                    $proUserCategoty['ProUserCategorie']['pro_user_id'] = $pro_user_id;
                    $proUserCategoty['ProUserCategorie']['category_id'] = $this->data['category'];
                    $proUserCategoty['ProUserCategorie']['sub_category_id'] = $val;
                    $proUserCategoty['ProUserCategorie']['create_date'] = date('Y-m-d H:i:s');
                    $this->ProUserCategorie->save($proUserCategoty);
                }

                $this->CompanyLicense->deleteAll(array('CompanyLicense.pro_user_id' => $pro_user['ProUser']['id']));
                foreach ($this->data['license_number'] as $key => $val) {
                    if ($val == "") {
                        continue;
                    }
                    $this->CompanyLicense->create();
                    $proUserlicense['CompanyLicense']['pro_user_id'] = $pro_user_id;
                    $proUserlicense['CompanyLicense']['license_number'] = $val;
                    $proUserlicense['CompanyLicense']['license_type'] = $this->data['license_type'][$key];
                    $proUserlicense['CompanyLicense']['expir_date'] = $this->data['expir_date'][$key];
                    $proUserlicense['CompanyLicense']['create_date'] = date('Y-m-d H:i:s');
                    $this->CompanyLicense->save($proUserlicense);
                }

                if (!empty($_FILES['past_work_pic'])) {
                    $path = 'uploaded_files/pro_user_files/' . $pro_user_id;
                    if (!file_exists($path)) {
                        $folder = mkdir($path, 0777);
                    }

                    foreach ($_FILES['past_work_pic']['name'] as $key => $past_work_pic) {
                        $filename = $path . '/' . $past_work_pic;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($past_work_pic);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['past_work_pic']['tmp_name'][$key], $filename)) {
                                $this->ProUserFile->create();
                                $ProUserFile['ProUserFile']['pro_user_id'] = $pro_user_id;
                                $ProUserFile['ProUserFile']['image'] = $past_work_pic;
                                $ProUserFile['ProUserFile']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProUserFile->save($ProUserFile);
                            }
                        }
                    }
                }

                $this->ProServiceArea->deleteAll(array('ProServiceArea.pro_user_id' => $pro_user['ProUser']['id']));
                foreach ($this->data['service_area_item'] as $key => $val) {
                    $this->ProServiceArea->create();
                    $ProServiceArea['ProServiceArea']['pro_user_id'] = $pro_user_id;
                    $ProServiceArea['ProServiceArea']['service_area'] = $val;
                    $ProServiceArea['ProServiceArea']['latitude'] = $this->data['service_area_lat'][$key];
                    $ProServiceArea['ProServiceArea']['longitude'] = $this->data['service_area_lng'][$key];
                    $ProServiceArea['ProServiceArea']['create_date'] = date('Y-m-d H:i:s');
                    $this->ProServiceArea->save($ProServiceArea);
                }


                if (!empty($_FILES['awards_achievements_image'])) {
                    $path = 'uploaded_files/pro_user_files/awards/' . $pro_user_id;
                    $folder = mkdir($path, 0777);


                    foreach ($_FILES['awards_achievements_image']['name'] as $key => $awards_achievements_image) {
                        $filename = $path . '/' . $awards_achievements_image;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($awards_achievements_image);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['awards_achievements_image']['tmp_name'][$key], $filename)) {
                                $this->ProAward->create();
                                $ProAward['ProAward']['pro_user_id'] = $pro_user_id;
                                $ProAward['ProAward']['award_image'] = $awards_achievements_image;
                                $ProAward['ProAward']['award'] = $this->data['awards_achievements_item'][$key];
                                $ProAward['ProAward']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProAward->save($ProAward);
                            }
                        }
                    }
                }

                //$this->ProAward->deleteAll(array('ProAward.pro_user_id' => $pro_user['ProUser']['id']));
//                foreach ($this->data['awards_achievements_item'] as $key => $val) {
//                    $this->ProAward->create();
//                    $ProAward['ProAward']['pro_user_id'] = $pro_user_id;
//                    $ProAward['ProAward']['award'] = $val;
//                    $ProAward['ProAward']['create_date'] = date('Y-m-d H:i:s');
//                    $this->ProAward->save($ProAward);
//                }

                $this->ProOperatingHour->deleteAll(array('ProOperatingHour.pro_user_id' => $pro_user['ProUser']['id']));

                $this->ProOperatingHour->create();
                $ProOperatingHour['ProOperatingHour']['pro_user_id'] = $pro_user_id;
                if (isset($this->data['mon_start'])) {
                    $ProOperatingHour['ProOperatingHour']['mon_start'] = $this->data['mon_start'];
                }
                if (isset($this->data['mon_end'])) {
                    $ProOperatingHour['ProOperatingHour']['mon_end'] = $this->data['mon_end'];
                }
                if (isset($this->data['tue_start'])) {
                    $ProOperatingHour['ProOperatingHour']['tue_start'] = $this->data['tue_start'];
                }
                if (isset($this->data['tue_start'])) {
                    $ProOperatingHour['ProOperatingHour']['tue_end'] = $this->data['tue_end'];
                }
                if (isset($this->data['wed_start'])) {
                    $ProOperatingHour['ProOperatingHour']['wed_start'] = $this->data['wed_start'];
                }
                if (isset($this->data['wed_end'])) {
                    $ProOperatingHour['ProOperatingHour']['wed_end'] = $this->data['wed_end'];
                }
                if (isset($this->data['thu_start'])) {
                    $ProOperatingHour['ProOperatingHour']['thu_start'] = $this->data['thu_start'];
                }
                if (isset($this->data['thu_end'])) {
                    $ProOperatingHour['ProOperatingHour']['thu_end'] = $this->data['thu_end'];
                }
                if (isset($this->data['fri_start'])) {
                    $ProOperatingHour['ProOperatingHour']['fri_start'] = $this->data['fri_start'];
                }
                if (isset($this->data['fri_end'])) {
                    $ProOperatingHour['ProOperatingHour']['fri_end'] = $this->data['fri_end'];
                }
                if (isset($this->data['sat_start'])) {
                    $ProOperatingHour['ProOperatingHour']['sat_start'] = $this->data['sat_start'];
                }
                if (isset($this->data['sat_end'])) {
                    $ProOperatingHour['ProOperatingHour']['sat_end'] = $this->data['sat_end'];
                }
                if (isset($this->data['sun_start'])) {
                    $ProOperatingHour['ProOperatingHour']['sun_start'] = $this->data['sun_start'];
                }
                if (isset($this->data['sun_end'])) {
                    $ProOperatingHour['ProOperatingHour']['sun_end'] = $this->data['sun_end'];
                }
                $ProOperatingHour['ProOperatingHour']['create_date'] = date('Y-m-d H:i:s');
                $this->ProOperatingHour->save($ProOperatingHour);




                $response['type'] = "success";
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }
        return json_encode($response);
    }

    function delete_pro_user_file() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $id = $this->data['id'];
        $this->loadModel('ProUserFile');
        $user_id = $this->Auth->user('id');
        $pro_user_file = $this->ProUserFile->findById($id);
        $file = 'uploaded_files/pro_user_files/' . $user_id . '/' . $pro_user_file['ProUserFile']['image'];
        if ($this->ProUserFile->delete($id)) {
            if (file_exists($file)) {
                @unlink($file);
            }

            echo 'success';
        }
    }

    function admin_delete_pro_user_file() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $id = $this->data['id'];
        $this->loadModel('ProUserFile');
        $user_id = $this->Auth->user('id');
        $pro_user_file = $this->ProUserFile->findById($id);
        $file = 'uploaded_files/pro_user_files/' . $user_id . '/' . $pro_user_file['ProUserFile']['image'];
        if ($this->ProUserFile->delete($id)) {
            if (file_exists($file)) {
                @unlink($file);
            }

            echo 'success';
        }
    }

    function admin_get_sub_category() {
        $this->layout = 'ajax';
        $this->loadModel('SubCategorie');
        $sub_categories = $this->SubCategorie->find('all', array('conditions' => array('SubCategorie.category_id' => $this->data['category_id'])));
        if (isset($this->data['another'])) {
            $this->set('another', 'another');
        }
        if (!empty($this->data['from']) && $this->data['from'] == 'project') {
            $this->set('from', $this->data['from']);
        }
        $this->set('sub_categories', $sub_categories);
        $this->set('category_id', $this->data['category_id']);
    }

    function get_sub_category() {
        $this->layout = 'ajax';
        $this->loadModel('SubCategorie');
        $sub_categories = $this->SubCategorie->find('all', array('conditions' => array('SubCategorie.category_id' => $this->data['category_id']), 'order' => 'sub_category ASC'));
        if (isset($this->data['another'])) {
            $this->set('another', 'another');
        }
        if (!empty($this->data['from']) && $this->data['from'] == 'project') {
            $this->set('from', $this->data['from']);
        }
        $this->set('sub_categories', $sub_categories);
        $this->set('category_id', $this->data['category_id']);
    }

    function admin_add_pro_by_admin_action() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('ProUser');
        $this->loadModel('User');
        $this->loadModel('CompanyLicense');
        $this->loadModel('ProUserCategorie');
        $this->loadModel('ProUserFile');
        $response = array();
        $error = array();
        $proUser = array();
        $proUserCategoty = array();
        $proUserlicense = array();



        //if its a new pro registration

        if (strlen($this->data['name']) == 0) {
            $error['nameError'] = __('Your name is required');
        } else {
            $proUser['ProUser']['name'] = $this->data['name'];
        }

        if ($this->data['email']) {

            if (filter_var($this->data['email'], FILTER_VALIDATE_EMAIL) == FALSE) {
                $error['emailError'] = __('Your email is not valid');
            } else {
                $user = $this->User->find('first', array('conditions' => array('User.email' => $this->data['email'])));
                if ($user) {
                    $error['emailError'] = __('This email is already registered');
                } else {
                    $user_data['User']['email'] = $this->data['email'];
                }
            }
        }
        $user_data['User']['create_date'] = date('Y-m-d H:i:s');
        $user_data['User']['user_type'] = 'pro';
        $user_data['User']['confirmed'] = 1;

        if ($this->data['password']) {
            $user_data['User']['password'] = AuthComponent::password($this->data['password']);
        }
        if (strlen($this->data['phone']) == 0) {
            $error['phoneError'] = __('Your phone number is required');
        } else {
            $user_data['User']['phone'] = $this->data['phone'];
        }

        if (strlen($this->data['name']) == 0) {
            $error['nameError'] = __('Your name is required');
        } else {
            $user_data['User']['name'] = $this->data['name'];
        }

        if (strlen($this->data['address']) == 0) {
            $error['addressError'] = __('Your address is required');
        } else {
            $proUser['ProUser']['address'] = $this->data['address'];
        }

        if (strlen($this->data['state']) == 0) {
            $error['stateError'] = __('Your state is required');
        } else {
            $proUser['ProUser']['state'] = $this->data['state'];
        }

        if (strlen($this->data['city']) == 0) {
            $error['cityError'] = __('Your city is required');
        } else {
            $proUser['ProUser']['city'] = $this->data['city'];
        }

        if (strlen($this->data['zip_code']) == 0) {
            $error['zipCodeError'] = __('Your zip code is required');
        } elseif (!$this->User->validateUSAZip($this->data['zip_code'])) {
            $error['zipCodeError'] = __('The zip code is not a valid USA zip code');
        } else {
            $proUser['ProUser']['zip_code'] = $this->data['zip_code'];
        }
        if (strlen($this->data['company_name']) == 0) {
            $error['companyNameError'] = __('Your company name is required');
        } else {
            $proUser['ProUser']['company_name'] = $this->data['company_name'];
        }
        if (!$this->data['category']) {
            $error['categoryError'] = __('Please select category');
        } else {
            $proUser['ProUser']['category_id'] = $this->data['category'];
        }

        $proUser['ProUser']['is_verified'] = 1;
        $proUser['ProUser']['company_description'] = $this->data['company_description'];
        $proUser['ProUser']['create_date'] = date('Y-m-d H:i:s');

        $proUser['ProUser']['operational_since'] = $this->data['operational_since'];
//        if (strlen($this->data['business_phone_number']) == 0) {
//            $error['businessPhoneNumberError'] = __('Business phone number is required');
//        } else {
//            $proUser['ProUser']['business_phone'] = $this->data['business_phone_number'];
//        }

        $proUser['ProUser']['business_phone'] = $this->data['business_phone_number'];
        $proUser['ProUser']['website_url'] = $this->data['website'];
        $proUser['ProUser']['facebook_link'] = $this->data['facebook_link'];
        $proUser['ProUser']['twitter_link'] = $this->data['twitter_link'];
        $proUser['ProUser']['linkedin_link'] = $this->data['linkedin_link'];
        $proUser['ProUser']['google_link'] = $this->data['google_link'];
        $proUser['ProUser']['youtube_link'] = $this->data['youtube_link'];
        $proUser['ProUser']['claim_status'] = 'not_claimed';

        $path_one = 'uploaded_files/pro_user_files/cover_photo/';
        $pathinfo_one = pathinfo($_FILES['cover_photo']['name']);
        $basename_one = strtolower($pathinfo_one['filename']);
        $extension_one = strtolower($pathinfo_one['extension']);
        $file_name_one = $basename_one . time() . '.' . $extension_one;

        $filepath_one = $path_one . $file_name_one;
        if (move_uploaded_file($_FILES['cover_photo']['tmp_name'], $filepath_one)) {
            $proUser['ProUser']['cover_photo'] = $file_name_one;
        }

        $path_two = 'uploaded_files/pro_user_files/business_logo/';
        $pathinfo_two = pathinfo($_FILES['business_logo']['name']);
        $basename_two = strtolower($pathinfo_two['filename']);
        $extension_two = strtolower($pathinfo_two['extension']);
        $file_name_two = $basename_two . time() . '.' . $extension_two;

        $filepath_two = $path_two . $file_name_two;
        if (move_uploaded_file($_FILES['business_logo']['tmp_name'], $filepath_two)) {
            $proUser['ProUser']['business_logo'] = $file_name_two;
        }

//        if (empty($this->data['sub_category'])) {
//            $error['subCategoryError'] = __('Please select at least one sub category');
//        }

        if (empty($this->data['terms_conditions'])) {
            $error['termsConditionsError'] = __('Please check terms and conditions');
        }

        if (!$error) {
            //new pro registration
            if (!$user_id) {
                //if existing user/pro not found
                if (!$user) {
                    $this->User->create();
                    if ($this->User->save($user_data)) {
                        $id = $this->User->getInsertID();
                        $proUser['ProUser']['user_id'] = $id;
//                        if ($user_data['User']['email']) {
//                            $this->sendConfirmationEmail($user_data, $id);
//                        }
                    }
                }
            } else {
                //adding pro profile to user account
                $this->User->id = $user_id;
                $this->User->saveField('user_type', 'pro');
                $this->Session->write('Auth.User.user_type', 'pro');
                $proUser['ProUser']['user_id'] = $user_id;
            }

            if ($proUser['ProUser']['user_id'] and $this->ProUser->save($proUser)) {
                $pro_user_id = $this->ProUser->getInsertID();

                foreach ($this->data['sub_category'] as $key => $val) {
                    $this->ProUserCategorie->create();
                    $proUserCategoty['ProUserCategorie']['pro_user_id'] = $pro_user_id;
                    $proUserCategoty['ProUserCategorie']['category_id'] = $this->data['category'];
                    $proUserCategoty['ProUserCategorie']['sub_category_id'] = $val;
                    $proUserCategoty['ProUserCategorie']['create_date'] = date('Y-m-d H:i:s');
                    $this->ProUserCategorie->save($proUserCategoty);
                }


                foreach ($this->data['license_number'] as $key => $val) {
                    if ($val == "") {
                        continue;
                    }
                    $this->CompanyLicense->create();
                    $proUserlicense['CompanyLicense']['pro_user_id'] = $pro_user_id;
                    $proUserlicense['CompanyLicense']['license_number'] = $val;
                    $proUserlicense['CompanyLicense']['license_type'] = $this->data['license_type'][$key];
                    $proUserlicense['CompanyLicense']['expir_date'] = $this->data['expir_date'][$key];
                    $proUserlicense['CompanyLicense']['create_date'] = date('Y-m-d H:i:s');
                    $this->CompanyLicense->save($proUserlicense);
                }

                if (!empty($_FILES['past_work_pic'])) {
                    $path = 'uploaded_files/pro_user_files/' . $pro_user_id;
                    $folder = mkdir($path, 0777);
                    foreach ($_FILES['past_work_pic']['name'] as $key => $past_work_pic) {
                        $filename = $path . '/' . $past_work_pic;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($past_work_pic);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['past_work_pic']['tmp_name'][$key], $filename)) {
                                $this->ProUserFile->create();
                                $ProUserFile['ProUserFile']['pro_user_id'] = $pro_user_id;
                                $ProUserFile['ProUserFile']['image'] = $past_work_pic;
                                $ProUserFile['ProUserFile']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProUserFile->save($ProUserFile);
                            }
                        }
                    }
                }

                foreach ($this->data['service_area_item'] as $key => $val) {
                    $this->ProServiceArea->create();
                    $ProServiceArea['ProServiceArea']['pro_user_id'] = $pro_user_id;
                    $ProServiceArea['ProServiceArea']['service_area'] = $val;
                    $ProServiceArea['ProServiceArea']['latitude'] = $this->data['service_area_lat'][$key];
                    $ProServiceArea['ProServiceArea']['longitude'] = $this->data['service_area_lng'][$key];
                    $ProServiceArea['ProServiceArea']['create_date'] = date('Y-m-d H:i:s');
                    $this->ProServiceArea->save($ProServiceArea);
                }

                if (!empty($_FILES['awards_achievements_image'])) {
                    $path = 'uploaded_files/pro_user_files/awards/' . $pro_user_id;
                    $folder = mkdir($path, 0777);


                    foreach ($_FILES['awards_achievements_image']['name'] as $key => $awards_achievements_image) {
                        $filename = $path . '/' . $awards_achievements_image;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($awards_achievements_image);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['awards_achievements_image']['tmp_name'][$key], $filename)) {
                                $this->ProAward->create();
                                $ProAward['ProAward']['pro_user_id'] = $pro_user_id;
                                $ProAward['ProAward']['award_image'] = $awards_achievements_image;
                                $ProAward['ProAward']['award'] = $this->data['awards_achievements_item'][$key];
                                $ProAward['ProAward']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProAward->save($ProAward);
                            }
                        }
                    }
                }

                $this->ProOperatingHour->create();
                $ProOperatingHour['ProOperatingHour']['pro_user_id'] = $pro_user_id;
                if (isset($this->data['mon_start'])) {
                    $ProOperatingHour['ProOperatingHour']['mon_start'] = $this->data['mon_start'];
                }
                if (isset($this->data['mon_end'])) {
                    $ProOperatingHour['ProOperatingHour']['mon_end'] = $this->data['mon_end'];
                }
                if (isset($this->data['tue_start'])) {
                    $ProOperatingHour['ProOperatingHour']['tue_start'] = $this->data['tue_start'];
                }
                if (isset($this->data['tue_start'])) {
                    $ProOperatingHour['ProOperatingHour']['tue_end'] = $this->data['tue_end'];
                }
                if (isset($this->data['wed_start'])) {
                    $ProOperatingHour['ProOperatingHour']['wed_start'] = $this->data['wed_start'];
                }
                if (isset($this->data['wed_end'])) {
                    $ProOperatingHour['ProOperatingHour']['wed_end'] = $this->data['wed_end'];
                }
                if (isset($this->data['thu_start'])) {
                    $ProOperatingHour['ProOperatingHour']['thu_start'] = $this->data['thu_start'];
                }
                if (isset($this->data['thu_end'])) {
                    $ProOperatingHour['ProOperatingHour']['thu_end'] = $this->data['thu_end'];
                }
                if (isset($this->data['fri_start'])) {
                    $ProOperatingHour['ProOperatingHour']['fri_start'] = $this->data['fri_start'];
                }
                if (isset($this->data['fri_end'])) {
                    $ProOperatingHour['ProOperatingHour']['fri_end'] = $this->data['fri_end'];
                }
                if (isset($this->data['sat_start'])) {
                    $ProOperatingHour['ProOperatingHour']['sat_start'] = $this->data['sat_start'];
                }
                if (isset($this->data['sat_end'])) {
                    $ProOperatingHour['ProOperatingHour']['sat_end'] = $this->data['sat_end'];
                }
                if (isset($this->data['sun_start'])) {
                    $ProOperatingHour['ProOperatingHour']['sun_start'] = $this->data['sun_start'];
                }
                if (isset($this->data['sun_end'])) {
                    $ProOperatingHour['ProOperatingHour']['sun_end'] = $this->data['sun_end'];
                }
                $ProOperatingHour['ProOperatingHour']['create_date'] = date('Y-m-d H:i:s');
                $this->ProOperatingHour->save($ProOperatingHour);


                $response['type'] = "success";
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }
        return json_encode($response);
    }

    function add_pro_action() {
        $this->autoRender = false;
        $this->layout = 'ajax';

        $this->loadModel('ProUser');
        $this->loadModel('User');
        $this->loadModel('CompanyLicense');
        $this->loadModel('ProUserCategorie');
        $this->loadModel('ProUserFile');
        $this->loadModel('ProServiceArea');
//        $this->loadModel('ProServiceOffer');
        $this->loadModel('ProAward');
        $this->loadModel('ProBrand');
        $this->loadModel('ProOperatingHour');
        $response = array();
        $error = array();
        $proUser = array();
        $proUserCategoty = array();
        $proUserlicense = array();

        $user_id = $this->Auth->User('id');

        //if its a new pro registration
        if (!$user_id) {
            if (strlen($this->data['name']) == 0) {
                $error['nameError'] = __('Your name is required');
            } else {
                $proUser['ProUser']['name'] = $this->data['name'];
            }

            if (strlen($this->data['email']) == 0) {
                $error['emailError'] = __('Your email is required');
            } elseif (filter_var($this->data['email'], FILTER_VALIDATE_EMAIL) == FALSE) {
                $error['emailError'] = __('Your email is not valid');
            } else {
                $user = $this->User->find('first', array('conditions' => array('User.email' => $this->data['email'])));
                if (!$user) {
                    if (strlen($this->data['name']) == 0) {
                        $error['nameError'] = __('Your name is required');
                    } else {
                        $user_data['User']['name'] = $this->data['name'];
                    }
                    if (strlen($this->data['phone']) == 0) {
                        $error['phoneError'] = __('Your phone number is required');
                    } else {
                        $user_data['User']['phone'] = $this->data['phone'];
                    }
                    $user_data['User']['email'] = $this->data['email'];
                    if (strlen($this->data['password']) == 0) {
                        $error['passwordError'] = 'Password is required';
                    } else {
                        $user_data['User']['password'] = AuthComponent::password($this->data['password']);
                    }
                    $user_data['User']['create_date'] = date('Y-m-d H:i:s');
                    $user_data['User']['user_type'] = 'pro';
                } else {
//                $proUser['ProUser']['user_id'] = $user['User']['id'];
                    $error['emailError'] = __('This email is already registered');
                }
            }
        } else {
            if ($this->ProUser->hasProAccount($user_id)) {
                $this->redirect(array('controller' => 'business', 'action' => 'your_business'));
            }
        }

        if (strlen($this->data['address']) == 0) {
            $error['addressError'] = __('Your address is required');
        } else {
            $proUser['ProUser']['address'] = $this->data['address'];
        }

        if (strlen($this->data['state']) == 0) {
            $error['stateError'] = __('Your state is required');
        } else {
            $proUser['ProUser']['state'] = $this->data['state'];
        }

        if (strlen($this->data['city']) == 0) {
            $error['cityError'] = __('Your city is required');
        } else {
            $proUser['ProUser']['city'] = $this->data['city'];
        }

        if (strlen($this->data['zip_code']) == 0) {
            $error['zipCodeError'] = __('Your zip code is required');
        } elseif (!$this->User->validateUSAZip($this->data['zip_code'])) {
            $error['zipCodeError'] = __('The zip code is not a valid USA zip code');
        } else {
            $proUser['ProUser']['zip_code'] = $this->data['zip_code'];
        }
        if (strlen($this->data['company_name']) == 0) {
            $error['companyNameError'] = __('Your company name is required');
        } else {
            $proUser['ProUser']['company_name'] = $this->data['company_name'];
        }
        if (!$this->data['category']) {
            $error['categoryError'] = __('Please select category');
        } else {
            $proUser['ProUser']['category_id'] = $this->data['category'];
        }





        $proUser['ProUser']['is_verified'] = 0;
        $proUser['ProUser']['company_description'] = $this->data['company_description'];
        $proUser['ProUser']['create_date'] = date('Y-m-d H:i:s');

        $proUser['ProUser']['operational_since'] = $this->data['operational_since'];
//        if (strlen($this->data['business_phone_number']) == 0) {
//            $error['businessPhoneNumberError'] = __('Business phone number is required');
//        } else {
//            $proUser['ProUser']['business_phone'] = $this->data['business_phone_number'];
//        }
        $proUser['ProUser']['business_phone'] = $this->data['business_phone_number'];
        $proUser['ProUser']['website_url'] = $this->data['website'];
        $proUser['ProUser']['facebook_link'] = $this->data['facebook_link'];
        $proUser['ProUser']['twitter_link'] = $this->data['twitter_link'];
        $proUser['ProUser']['linkedin_link'] = $this->data['linkedin_link'];
        $proUser['ProUser']['google_link'] = $this->data['google_link'];
        $proUser['ProUser']['youtube_link'] = $this->data['youtube_link'];
        $proUser['ProUser']['claim_status'] = 'connected';

        $path_one = 'uploaded_files/pro_user_files/cover_photo/';
        $pathinfo_one = pathinfo($_FILES['cover_photo']['name']);
        $basename_one = strtolower($pathinfo_one['filename']);
        $extension_one = strtolower($pathinfo_one['extension']);
        $file_name_one = $basename_one . time() . '.' . $extension_one;

        $filepath_one = $path_one . $file_name_one;
        if (move_uploaded_file($_FILES['cover_photo']['tmp_name'], $filepath_one)) {
            $proUser['ProUser']['cover_photo'] = $file_name_one;
        }

        $path_two = 'uploaded_files/pro_user_files/business_logo/';
        $pathinfo_two = pathinfo($_FILES['business_logo']['name']);
        $basename_two = strtolower($pathinfo_two['filename']);
        $extension_two = strtolower($pathinfo_two['extension']);
        $file_name_two = $basename_two . time() . '.' . $extension_two;

        $filepath_two = $path_two . $file_name_two;
        if (move_uploaded_file($_FILES['business_logo']['tmp_name'], $filepath_two)) {
            $proUser['ProUser']['business_logo'] = $file_name_two;
        }

        if (empty($this->data['terms_conditions'])) {
            $error['termsConditionsError'] = __('Please check terms and conditions');
        }



        if (!$error) {
            //new pro registration
            if (!$user_id) {
                //if existing user/pro not found
                if (!$user) {
                    $this->User->create();
                    if ($this->User->save($user_data)) {
                        $id = $this->User->getInsertID();
                        $proUser['ProUser']['user_id'] = $id;


//                        $this->sendConfirmationEmail($user_data, $id);
                    }
                }
            } else {
                //adding pro profile to user account
                $this->User->id = $user_id;
                $this->User->saveField('user_type', 'pro');
                $this->Session->write('Auth.User.user_type', 'pro');
                $proUser['ProUser']['user_id'] = $user_id;
            }

            if ($proUser['ProUser']['user_id'] and $this->ProUser->save($proUser)) {
                $pro_user_id = $this->ProUser->getInsertID();

                foreach ($this->data['sub_category'] as $key => $val) {
                    $this->ProUserCategorie->create();
                    $proUserCategoty['ProUserCategorie']['pro_user_id'] = $pro_user_id;
                    $proUserCategoty['ProUserCategorie']['category_id'] = $this->data['category'];
                    $proUserCategoty['ProUserCategorie']['sub_category_id'] = $val;
                    $proUserCategoty['ProUserCategorie']['create_date'] = date('Y-m-d H:i:s');
                    $this->ProUserCategorie->save($proUserCategoty);
                }

                foreach ($this->data['license_number'] as $key => $val) {
                    if ($val == '') {
                        continue;
                    }
                    $this->CompanyLicense->create();
                    $proUserlicense['CompanyLicense']['pro_user_id'] = $pro_user_id;
                    $proUserlicense['CompanyLicense']['license_number'] = $val;
                    $proUserlicense['CompanyLicense']['license_type'] = $this->data['license_type'][$key];
                    $proUserlicense['CompanyLicense']['expir_date'] = $this->data['expir_date'][$key];
                    $proUserlicense['CompanyLicense']['create_date'] = date('Y-m-d H:i:s');
                    $this->CompanyLicense->save($proUserlicense);
                }

                if (!empty($_FILES['past_work_pic'])) {
                    $path = 'uploaded_files/pro_user_files/' . $pro_user_id;
                    $folder = mkdir($path, 0777);
                    foreach ($_FILES['past_work_pic']['name'] as $key => $past_work_pic) {
                        $filename = $path . '/' . $past_work_pic;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($past_work_pic);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['past_work_pic']['tmp_name'][$key], $filename)) {
                                $this->ProUserFile->create();
                                $ProUserFile['ProUserFile']['pro_user_id'] = $pro_user_id;
                                $ProUserFile['ProUserFile']['image'] = $past_work_pic;
                                $ProUserFile['ProUserFile']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProUserFile->save($ProUserFile);
                            }
                        }
                    }
                }


                foreach ($this->data['service_area_item'] as $key => $val) {
                    $this->ProServiceArea->create();
                    $ProServiceArea['ProServiceArea']['pro_user_id'] = $pro_user_id;
                    $ProServiceArea['ProServiceArea']['service_area'] = $val;
                    $ProServiceArea['ProServiceArea']['latitude'] = $this->data['service_area_lat'][$key];
                    $ProServiceArea['ProServiceArea']['longitude'] = $this->data['service_area_lng'][$key];
                    $ProServiceArea['ProServiceArea']['create_date'] = date('Y-m-d H:i:s');
                    $this->ProServiceArea->save($ProServiceArea);
                }


                foreach ($this->data['brand_name_item'] as $key => $val) {
                    $this->ProBrand->create();
                    $ProBrand['ProBrand']['pro_user_id'] = $pro_user_id;
                    $ProBrand['ProBrand']['brand_name'] = $val;
                    $ProBrand['ProBrand']['create_date'] = date('Y-m-d H:i:s');
                    $this->ProBrand->save($ProBrand);
                }

//                foreach ($this->data['awards_achievements_item'] as $key => $val) {
//                    $this->ProAward->create();
//                    $ProAward['ProAward']['pro_user_id'] = $pro_user_id;
//                    $ProAward['ProAward']['award'] = $val;
//                    $ProAward['ProAward']['create_date'] = date('Y-m-d H:i:s');
//                    $this->ProAward->save($ProAward);
//                }



                if (!empty($_FILES['awards_achievements_image'])) {
                    $path = 'uploaded_files/pro_user_files/awards/' . $pro_user_id;
                    $folder = mkdir($path, 0777);


                    foreach ($_FILES['awards_achievements_image']['name'] as $key => $awards_achievements_image) {
                        $filename = $path . '/' . $awards_achievements_image;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($awards_achievements_image);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['awards_achievements_image']['tmp_name'][$key], $filename)) {
                                $this->ProAward->create();
                                $ProAward['ProAward']['pro_user_id'] = $pro_user_id;
                                $ProAward['ProAward']['award_image'] = $awards_achievements_image;
                                $ProAward['ProAward']['award'] = $this->data['awards_achievements_item'][$key];
                                $ProAward['ProAward']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProAward->save($ProAward);
                            }
                        }
                    }

//                    echo '<pre>';
//                    print_r($ProAward);
//                    '</pre>';
//                    //die;
                }


                $this->ProOperatingHour->create();
                $ProOperatingHour['ProOperatingHour']['pro_user_id'] = $pro_user_id;
                if (isset($this->data['mon_start'])) {
                    $ProOperatingHour['ProOperatingHour']['mon_start'] = $this->data['mon_start'];
                }
                if (isset($this->data['mon_end'])) {
                    $ProOperatingHour['ProOperatingHour']['mon_end'] = $this->data['mon_end'];
                }
                if (isset($this->data['tue_start'])) {
                    $ProOperatingHour['ProOperatingHour']['tue_start'] = $this->data['tue_start'];
                }
                if (isset($this->data['tue_start'])) {
                    $ProOperatingHour['ProOperatingHour']['tue_end'] = $this->data['tue_end'];
                }
                if (isset($this->data['wed_start'])) {
                    $ProOperatingHour['ProOperatingHour']['wed_start'] = $this->data['wed_start'];
                }
                if (isset($this->data['wed_end'])) {
                    $ProOperatingHour['ProOperatingHour']['wed_end'] = $this->data['wed_end'];
                }
                if (isset($this->data['thu_start'])) {
                    $ProOperatingHour['ProOperatingHour']['thu_start'] = $this->data['thu_start'];
                }
                if (isset($this->data['thu_end'])) {
                    $ProOperatingHour['ProOperatingHour']['thu_end'] = $this->data['thu_end'];
                }
                if (isset($this->data['fri_start'])) {
                    $ProOperatingHour['ProOperatingHour']['fri_start'] = $this->data['fri_start'];
                }
                if (isset($this->data['fri_end'])) {
                    $ProOperatingHour['ProOperatingHour']['fri_end'] = $this->data['fri_end'];
                }
                if (isset($this->data['sat_start'])) {
                    $ProOperatingHour['ProOperatingHour']['sat_start'] = $this->data['sat_start'];
                }
                if (isset($this->data['sat_end'])) {
                    $ProOperatingHour['ProOperatingHour']['sat_end'] = $this->data['sat_end'];
                }
                if (isset($this->data['sun_start'])) {
                    $ProOperatingHour['ProOperatingHour']['sun_start'] = $this->data['sun_start'];
                }
                if (isset($this->data['sun_end'])) {
                    $ProOperatingHour['ProOperatingHour']['sun_end'] = $this->data['sun_end'];
                }
                $ProOperatingHour['ProOperatingHour']['create_date'] = date('Y-m-d H:i:s');
                $this->ProOperatingHour->save($ProOperatingHour);


                $response['type'] = "success";
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }
        return json_encode($response);
    }

    function find_more_pro() {
        $this->layout = 'home';
        $this->loadModel('Categorie');
        $this->set('view_header', 'landing');
        $this->Categorie->recursive = -1;
        $categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('categories', $categories);
    }

    function find_pro() {
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->loadModel('SubCategorie');
        $category_id = $this->request->params['named']['category_id'];
        $sub_categories = $this->SubCategorie->find('all', array('conditions' => array('SubCategorie.category_id' => $category_id)));
        $this->set('sub_categories', $sub_categories);
        $this->loadModel('Categorie');
        $categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('categories', $categories);
    }

    function find_contractors() {
        $this->layout = 'home';
        $this->loadModel('ProUserCategorie');
        $this->loadModel('ProUser');
        $this->loadModel('Hire');
        $user_zipcode = $this->Session->read('user_zipcode');
        $category_id = $this->request->params['named']['category_id'];
        $sub_category_id = $this->request->params['named']['sub_category_id'];

        $joins = array();
        $joins[] = array(
            'table' => 'pro_user_categories',
            'conditions' => array(
                'ProUserCategorie.pro_user_id = ProUser.id',
            ),
            'type' => 'INNER',
            'alias' => 'ProUserCategorie'
        );

        $condition = array();
        $condition['ProUserCategorie.category_id'] = $category_id;
        $condition['ProUserCategorie.sub_category_id'] = $sub_category_id;
        $condition['ProUser.is_verified'] = 1;
        if ($user_zipcode) {
            $condition['ProUser.zip_code'] = $user_zipcode;
        }

//        $pro_users = $this->ProUser->find('all', array('joins' => $joins, 'conditions' => array('ProUserCategorie.category_id' => $category_id, 'ProUserCategorie.sub_category_id' => $sub_category_id, 'ProUser.zip_code' => $user_zipcode, 'ProUser.is_verified' => 1)));
        $pro_users = $this->ProUser->find('all', array('joins' => $joins, 'conditions' => $condition));
        foreach ($pro_users as $key => &$pro_user) {
            $pro_users[$key]['project_count'] = $this->Hire->find('count', array('conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'])));
            $this->Hire->virtualFields['proRating'] = 'SUM(Hire.pro_review)';
            $pro_users[$key]['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.proRating'), 'conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed')));
            $this->Hire->virtualFields['reviewCount'] = 'count(Hire.pro_review)';
            $pro_users[$key]['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));
        }

        $this->set('pro_users', $pro_users);
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'no_menu');
    }

    function add_project() {
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $zip_code = '';

        if (isset($this->request->params['named']['zip_code'])) {
            $zip_code = $this->request->params['named']['zip_code'];
            $this->Session->write('user_zipcode', $zip_code);
        } elseif ($this->Session->read('user_zipcode')) {
            $zip_code = $this->Session->read('user_zipcode');
        }

        $category_id = $this->request->params['named']['category_id'];
        $this->set('category_id', $category_id);
        $sub_category_id = $this->request->params['named']['sub_category_id'];
        $this->set('sub_category_id', $sub_category_id);


        $this->loadModel('Categorie');
        $categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('categories', $categories);

        if (isset($this->request->params['named']['specified_pro'])) {
            $this->loadModel('ProUser');
            $specified_pro_user_id = $this->request->params['named']['specified_pro'];
            $specified_pro = $this->ProUser->findById($specified_pro_user_id);
            $this->set('specified_pro', $specified_pro);
        }

        $this->loadModel('User');
        $user_id = $this->Auth->user('id');
        $user = $this->User->findById($user_id);
        $this->set('user', $user);

        if ($zip_code) {
            $this->loadModel('ProUser');
            $proCount = $this->ProUser->find('count', array('conditions' => array('ProUser.category_id' => $category_id, 'ProUser.zip_code' => $zip_code, 'is_verified' => 1)));
            $this->set('search_result', $proCount);
            $current_category = $this->Categorie->findById($category_id);
            $this->set('current_category', $current_category);
        }
        $this->set('zip_code', $zip_code);
    }

    function add_project_action() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('User');
        $this->loadModel('Project');
        $this->loadModel('ProjectFile');
        $this->loadModel('SpecifiedPro');
        $response = array();
        $error = array();
        $Project = array();
        $user_data = array();

        if (strlen($this->data['email']) == 0) {
            $error['emailError'] = __('Your email is required');
        } elseif (filter_var($this->data['email'], FILTER_VALIDATE_EMAIL) == FALSE) {
            $error['emailError'] = __('Your email is not valid');
        } else {
            $user = $this->User->find('first', array('conditions' => array('User.email' => $this->data['email'])));
            if (!$user) {
                if (strlen($this->data['name']) == 0) {
                    $error['nameError'] = __('Your name is required');
                } else {
                    $user_data['User']['name'] = $this->data['name'];
                }
                if (strlen($this->data['phone']) == 0) {
                    $error['phoneError'] = __('Your phone is required');
                } else {
                    $user_data['User']['phone'] = $this->data['phone'];
                }
                $user_data['User']['email'] = $this->data['email'];
                if (strlen($this->data['password']) == 0) {
                    $error['passwordError'] = 'Password is required';
                } else {
                    $user_data['User']['password'] = AuthComponent::password($this->data['password']);
                }
                $user_data['User']['create_date'] = date('Y-m-d H:i:s');
                $user_data['User']['user_type'] = 'user';
            } else {
                $Project['Project']['user_id'] = $user['User']['id'];
            }
        }

        if (strlen($this->data['project_title']) == 0) {
            $error['projectTitleError'] = __('Project title is required');
        } else {
            $Project['Project']['project_title'] = $this->data['project_title'];
        }
        if ($this->data['category_id'] == 0) {
            $error['categoryError'] = __('Category is required');
        } else {
            $Project['Project']['category_id'] = $this->data['category_id'];
        }
        if ($this->data['sub_category_id'] == 0) {
            $error['subCategoryError'] = __('Sub category is required');
        } else {
            $Project['Project']['sub_category_id'] = $this->data['sub_category_id'];
        }

        if (strlen($this->data['details']) == 0) {
            $error['detailsError'] = __('Plase provide as much information as you can for the contractor to give you a cost estimate');
        } else {
            $Project['Project']['details'] = $this->data['details'];
        }
        if (strlen($this->data['materials']) == 0) {
            $error['materialsError'] = __('Project materials is required');
        } else {
            $Project['Project']['materials'] = $this->data['materials'];
        }

        if (strlen($this->data['start_date']) == 0) {
            $error['startDateError'] = __('Start date is required');
        } else {
            $Project['Project']['start_date'] = $this->data['start_date'];
        }
        if (strlen($this->data['budget']) == 0) {
            $error['budgetError'] = __('Budget is required');
        } else {
            $Project['Project']['budget'] = $this->data['budget'];
        }
        if (strlen($this->data['zip_code']) == 0) {
            $error['zipCodeError'] = __('Please set your zip code');
        } elseif (!$this->User->validateUSAZip($this->data['zip_code'])) {
            $error['zipCodeError'] = __('The zip code is not a valid USA zip code');
        } else {
            $Project['Project']['zip_code'] = $this->data['zip_code'];
        }

        if (empty($this->data['terms_conditions'])) {
            $error['termsConditionsError'] = __('Please check terms and conditions');
        }

        $Project['Project']['create_date'] = date('Y-m-d H:i:s');

        if (!$error) {

            if (!$Project['Project']['user_id']) {
                $this->User->create();
                if ($this->User->save($user_data)) {
                    $user_id = $this->User->getInsertID();
                    $Project['Project']['user_id'] = $user_id;
                }
            }

            if ($this->Project->save($Project)) {
                $project_user = $this->User->findById($Project['Project']['user_id']);
                $id = $this->Project->getInsertID();
                foreach ($this->data['specified_pros'] as $key => $specified_pro) {
                    $this->SpecifiedPro->create();
                    $SpecifiedPro['SpecifiedPro']['project_id'] = $id;
                    $SpecifiedPro['SpecifiedPro']['user_id'] = $specified_pro;
                    $SpecifiedPro['SpecifiedPro']['create_date'] = date('Y-m-d H:i:s');
                    $this->SpecifiedPro->save($SpecifiedPro);
                }

                if (!empty($_FILES['project_file'])) {
                    $path = 'uploaded_files/project_files/' . $id;
                    $folder = mkdir($path, 0777);
                    foreach ($_FILES['project_file']['name'] as $key => $project_file) {
                        $filename = $path . '/' . $project_file;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($project_file);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['project_file']['tmp_name'][$key], $filename)) {
                                $this->ProjectFile->create();
                                $ProjectFile['ProjectFile']['project_id'] = $id;
                                $ProjectFile['ProjectFile']['file'] = $filename;
                                $ProjectFile['ProjectFile']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProjectFile->save($ProjectFile);
                            }
                        }
                    }
                }

                $this->loadModel('EmailTemplate');

                $emailTemplateArr = $this->EmailTemplate->findByTemplateName('project_verify_email_to_admin');

                $variables['[{client_name}]'] = $project_user['User']['name'];
                $variables['[{view_project_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $id), true);

                $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';
                if ($this->sendEmail($emailTemplateArr, $variables, Configure::read('settings.config.admin_mail'))) {
                    $response['type'] = "success";
                }
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }
        return json_encode($response);
    }

    function search_pro() {
        $this->layout = 'ajax';
        $this->loadModel('ProUser');
        $this->loadModel('User');
        $search_value = $this->data['name'];
        $category_id = $this->data['category_id'];



        $joins = array();
        $joins[] = array(
            'table' => 'pro_users',
            'conditions' => array(
                'User.id = ProUser.user_id'
            ),
            'type' => 'INNER',
            'alias' => 'ProUser'
        );

        $proUsers = $this->User->find('all', array('joins' => $joins, 'conditions' => array('User.name LIKE' => '%' . $search_value . '%', 'User.user_type' => 'pro', 'ProUser.category_id' => $category_id, 'ProUser.is_verified' => 1), 'order' => 'name ASC'));
        $this->set('proUsers', $proUsers);
    }

    function view_project() {
        $this->layout = 'home';
        $user_type = $this->Auth->user('user_type');
        $user_id = $this->Auth->user('id');
        $project_id = $this->request->params['named']['project_id'];
        $this->loadModel('Project');
        $this->loadModel('ProjectQuote');
        $project = $this->Project->findById($project_id);
        $user_sent_quote = $this->ProjectQuote->find('first', array('conditions' => array('ProjectQuote.user_id' => $user_id, 'ProjectQuote.project_id' => $project_id)));
        $project['user_sent_quote'] = $user_sent_quote;

        if ($user_type == 'user') {
            if ($project['Project']['user_id'] != $user_id) {
                $this->redirect('/');
            }
        } else {
            //Dependent on payment. Need to check either this pro has paid to view this project or not
            //PENDING
        }

        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');

        $this->loadModel('Hire');
        $projectCount = $this->Project->find('count', array('conditions' => array('Project.user_id' => $project['User']['id'])));
        $this->set('projectCount', $projectCount);


        $project['hire_count'] = $this->Hire->find('count', array('conditions' => array('Hire.user_id' => $project['User']['id'])));
        $this->Hire->virtualFields['userRating'] = 'SUM(Hire.user_review)';
        $project['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.userRating'), 'conditions' => array('Hire.user_id' => $project['User']['id'], 'Hire.hire_status' => 'completed')));
        $this->Hire->virtualFields['reviewCount'] = 'count(Hire.user_review)';
        $project['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.user_id' => $project['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.user_review !=' => 0)));
        unset($this->Hire->virtualFields['proRating']);
        unset($this->Hire->virtualFields['reviewCount']);
        $project['reviews'] = $this->Hire->find('all', array('conditions' => array('Hire.user_id' => $project['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.user_review !=' => 0)));
        $this->set('project', $project);
    }

    function submit_a_quote() {
        $this->layout = 'home';
        $project_id = $this->request->params['named']['project_id'];
        $this->loadModel('Project');
        $this->Project->recursive = -1;
        $project = $this->Project->findById($project_id);
        $this->set('project', $project);
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
    }

    function submit_a_quote_action() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('User');
        $this->loadModel('ProjectQuote');
        $this->loadModel('ProjectQuoteFile');
        $this->loadModel('Project');
        $this->loadModel('ProUser');
        $user_id = $this->Session->read('Auth.User.id');
        $project_id = $this->data['project_id'];
        $response = array();
        $error = array();
        $ProjectQuote = array();
        $user_data = array();
        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $user_id)));

        $project = $this->Project->findById($project_id);
        if (strlen($this->data['amount']) == 0) {
            $error['amountError'] = __('Amount is required');
        } else {
            $ProjectQuote['ProjectQuote']['amount'] = $this->data['amount'];
        }
        if (strlen($this->data['start_date']) == 0) {
            $error['startDateError'] = __('Start date is required');
        } else {
            $ProjectQuote['ProjectQuote']['start_date'] = $this->data['start_date'];
        }

        if (strlen($this->data['duration']) == 0) {
            $error['durationError'] = __('Duration is required');
        } else {
            $ProjectQuote['ProjectQuote']['duration'] = $this->data['duration'];
        }


        if (empty($this->data['terms_conditions'])) {
            $error['termsConditionsError'] = __('Please check terms and conditions');
        }
        $ProjectQuote['ProjectQuote']['worked_on_similar_project'] = $this->data['worked_on_similar_project'];
        $ProjectQuote['ProjectQuote']['question_about_project'] = $this->data['question_about_project'];
        $ProjectQuote['ProjectQuote']['user_id'] = $user_id;
        $ProjectQuote['ProjectQuote']['project_id'] = $project_id;
        $ProjectQuote['ProjectQuote']['status'] = 'open';
        $ProjectQuote['ProjectQuote']['create_date'] = date('Y-m-d H:i:s');


        if (!$error) {

            if ($this->ProjectQuote->save($ProjectQuote)) {
                $id = $this->ProjectQuote->getInsertID();
                if (!empty($_FILES['past_work_pic'])) {
                    $path = 'uploaded_files/project_quote_files/' . $id;
                    $folder = mkdir($path, 0777);
                    foreach ($_FILES['past_work_pic']['name'] as $key => $past_work_pic) {
                        $filename = $path . '/' . $past_work_pic;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($past_work_pic);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['past_work_pic']['tmp_name'][$key], $filename)) {
                                $this->ProjectQuoteFile->create();
                                $ProjectQuoteFile['ProjectQuoteFile']['project_quote_id'] = $id;
                                $ProjectQuoteFile['ProjectQuoteFile']['file'] = $filename;
                                $ProjectQuoteFile['ProjectQuoteFile']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProjectQuoteFile->save($ProjectQuoteFile);
                            }
                        }
                    }
                }


                $notification = array();
                $this->Notification->create();
                $notification['Notification']['user_id'] = $project['User']['id'];
                $notification['Notification']['user_type'] = 'user';
                $notification['Notification']['notification_type'] = 'user_received_quote';
                $notification['Notification']['related_table_id'] = $id;
                $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
                if ($this->Notification->save($notification)) {
                    $this->loadModel('EmailTemplate');

                    $emailTemplateArr = $this->EmailTemplate->findByTemplateName('new_proposal_received_email_to_client');

                    $variables['[{company_name}]'] = $pro_user['ProUser']['company_name'];
                    $variables['[{view_quote_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'view_quote', 'quote_id' => $id), true);
                    $variables['[{cancel_project}]'] = Router::url(array('controller' => 'pro', 'action' => 'cancel_project', 'project_id' => $project_id), true);

                    $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                    $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';

                    if ($this->sendEmail($emailTemplateArr, $variables, $project['User']['email'])) {
                        $response['type'] = "success";
                    }
                }
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }
        return json_encode($response);
    }

    function cancel_project() {
        $project_id = $this->request->params['named']['project_id'];
    }

    function view_quote() {
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');
        $quote_id = $this->request->params['named']['quote_id'];
        $this->loadModel('ProjectQuote');
        $this->loadModel('Hire');
        $hire = $this->Hire->find('first', array('conditions' => array('Hire.project_quote_id' => $quote_id)));


        $this->ProjectQuote->Behaviors->load('Containable');
        $options = array(
            'conditions' => array(
                'ProjectQuote.id' => $quote_id
            ), 'contain' => array(
                'User' => array(
                    'ProUser' => array(
                        'CompanyLicense' => array(
                        ),
                        'ProUserCategorie' => array(
                        )
                    )
                ),
                'Project' => array(
                    'User' => array(
                    )
                ),
                'ProjectQuoteFile' => array(
                )
            )
        );


        $project_quote = $this->ProjectQuote->find('first', $options);

        $this->loadModel('Hire');

//        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $user_id)));

        $project_quote['project_count'] = $this->Hire->find('count', array('conditions' => array('Hire.pro_user_id' => $project_quote['ProjectQuote']['user_id'])));
        $this->Hire->virtualFields['proRating'] = 'SUM(Hire.pro_review)';
        $project_quote['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.proRating'), 'conditions' => array('Hire.pro_user_id' => $project_quote['ProjectQuote']['user_id'], 'Hire.hire_status' => 'completed')));
        $this->Hire->virtualFields['reviewCount'] = 'count(Hire.pro_review)';
        $project_quote['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.pro_user_id' => $project_quote['ProjectQuote']['user_id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));
        unset($this->Hire->virtualFields['proRating']);
        unset($this->Hire->virtualFields['reviewCount']);
        $project_quote['reviews'] = $this->Hire->find('all', array('conditions' => array('Hire.pro_user_id' => $project_quote['ProjectQuote']['user_id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));




        $this->set('project_quote', $project_quote);
        $this->set('hire', $hire);
    }

    function hire() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('Hire');
        $this->loadModel('ProjectQuote');
        $this->loadModel('User');
        $project_quote_id = $this->data['project_quote_id'];
        $project_quote = $this->ProjectQuote->findById($project_quote_id);
        $hire = array();
        $hire['Hire']['project_id'] = $project_quote['Project']['id'];
        $hire['Hire']['project_quote_id'] = $project_quote['ProjectQuote']['id'];
        $hire['Hire']['user_id'] = $project_quote['Project']['user_id'];
        $hire['Hire']['pro_user_id'] = $project_quote['ProjectQuote']['user_id'];
        $hire['Hire']['hire_status'] = 'working';
        $hire['Hire']['create_date'] = date('Y-m-d H:i:s');

        $user = $this->User->findById($project_quote['Project']['user_id']);
        $pro_user = $this->User->findById($project_quote['ProjectQuote']['user_id']);

        if ($this->Hire->save($hire)) {
            $id = $this->Hire->getInsertID();
            $this->ProjectQuote->id = $project_quote['ProjectQuote']['id'];
            if ($this->ProjectQuote->saveField('status', 'accepted')) {
                //notifications 
                $notification = array();
                $this->Notification->create();
                $notification['Notification']['user_id'] = $project_quote['User']['id'];
                $notification['Notification']['user_type'] = 'pro';
                $notification['Notification']['notification_type'] = 'pro_quote_accepted';
                $notification['Notification']['related_table_id'] = $id;
                $notification['Notification']['create_date'] = date('Y-m-d H:i:s');

                if ($this->Notification->save($notification)) {
                    $notification = array();
                    $this->Notification->create();
                    $notification['Notification']['user_id'] = $this->Auth->user('id');
                    $notification['Notification']['user_type'] = 'user';
                    $notification['Notification']['notification_type'] = 'user_hired_pro';
                    $notification['Notification']['related_table_id'] = $id;
                    $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
                    if ($this->Notification->save($notification)) {
                        $this->loadModel('EmailTemplate');

                        $emailTemplateArr = $this->EmailTemplate->findByTemplateName('hired_contractor_email_to_client');

                        $variables['[{company_name}]'] = $pro_user['ProUser']['company_name'];
                        $variables['[{view_project_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $project_quote['Project']['id']), true);
                        $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                        $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';

                        if ($this->sendEmail($emailTemplateArr, $variables, $user['User']['email'])) {
                            echo 'success';
                            exit;
                        }
                    }
                }
            }
        }
    }

    function quote_declined() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('ProjectQuote');
        $project_quote_id = $this->data['project_quote_id'];
        $project_quote = $this->ProjectQuote->findById($project_quote_id);

        $this->ProjectQuote->id = $project_quote['ProjectQuote']['id'];
        if ($this->ProjectQuote->saveField('status', 'declined')) {
            //notifications 
            $notification = array();
            $this->Notification->create();
            $notification['Notification']['user_id'] = $project_quote['User']['id'];
            $notification['Notification']['user_type'] = 'pro';
            $notification['Notification']['notification_type'] = 'pro_quote_declined';
            $notification['Notification']['related_table_id'] = $project_quote['ProjectQuote']['id'];
            $notification['Notification']['create_date'] = date('Y-m-d H:i:s');

            if ($this->Notification->save($notification)) {
                echo 'success';
            }
        }
    }

    function admin_get_verified_pro_user() {
        $condition = array();
        $condition['ProUser.is_verified'] = 1;
        if ($_GET['name']) {
            $condition['ProUser.company_name LIKE'] = '%' . $_GET['name'] . '%';
        }

        if ($_GET['description']) {
            $condition['ProUser.company_description LIKE'] = '%' . $_GET['description'] . '%';
        }

        $this->layout = "admin_pages";
        $this->loadModel('ProUser');
        $this->loadModel('ProUserPackage');
        $this->loadModel('ProUserNewPackage');

        $per_page = 5;
        $options = array(
            //'conditions' => array('ProUser.is_verified' => 1),
            'conditions' => $condition,
            'limit' => $per_page,
//            'group' => 'Question.id',
//            'order' => 'Question.date DESC'
        );

        $this->paginate = $options;
        $pro_users = $this->paginate('ProUser');

        foreach ($pro_users as $key => &$val) {
            $val['ProPackage'] = $this->ProUserPackage->find('first', array('conditions' => array('ProUserPackage.pro_user_id' => $val['ProUser']['id'])));
            $val['NewPackage'] = $this->ProUserNewPackage->find('first', array('order' => 'ProUserNewPackage.id DESC', 'conditions' => array('ProUserNewPackage.pro_user_id' => $val['ProUser']['id'])));
            //$pro_users['ProUserPackage'] = $this->ProUserPackage->find('first', array('conditions' => array('ProUserPackage.id' => 1)));
        }

        $this->set("pro_users", $pro_users);
        
    }

    function admin_reset_pass_by_admin() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $this->loadModel('ProUser');
        $this->loadModel('User');
        $this->loadModel('EmailTemplate');
        $pro_id = $this->data['id'];
        $pro_user = $this->ProUser->findById($pro_id);

        $random_pass = $this->generateRandomString(8);
        $new_password = AuthComponent::password($random_pass);
        $this->User->id = $pro_user['User']['id'];
        $this->User->saveField("password", $new_password);

        $emailTemplateArr = $this->EmailTemplate->findByTemplateName('reset_password');

        $link = Router::url(array('controller' => 'home', 'action' => 'index', 'admin' => false), true);
        $variables['[{login_link}]'] = $link;
        $variables['[{new_password}]'] = $random_pass;

        $this->sendEmail($emailTemplateArr, $variables, $pro_user['User']['email']);

        echo 'success';
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function admin_get_unverified_pro_user() {
        $this->layout = "admin_pages";
        $this->loadModel('ProUser');
        $per_page = 5;
        $options = array(
            'conditions' => array('ProUser.is_verified' => 0),
            'limit' => $per_page,
//            'group' => 'Question.id',
//            'order' => 'Question.date DESC'
        );


        $this->paginate = $options;
        $pro_users = $this->paginate('ProUser');
        $this->set("pro_users", $pro_users);
    }

    function admin_get_verified_project() {
        $this->layout = "admin_pages";
        $this->loadModel('Project');
        $per_page = 5;
        $options = array(
            'conditions' => array('Project.is_verified' => 1),
            'limit' => $per_page,
//            'group' => 'Question.id',
//            'order' => 'Question.date DESC'
        );


        $this->paginate = $options;
        $projects = $this->paginate('Project');
        $this->set("projects", $projects);
    }

    function admin_get_unverified_project() {
        $this->layout = "admin_pages";
        $this->loadModel('Project');
        $per_page = 5;
        $options = array(
            'conditions' => array('Project.is_verified' => 0),
            'limit' => $per_page,
//            'group' => 'Question.id',
//            'order' => 'Question.date DESC'
        );


        $this->paginate = $options;
        $projects = $this->paginate('Project');
        $this->set("projects", $projects);
    }

    function admin_project_view() {
        $this->layout = "admin_pages";
        $this->loadModel('Project');
        $this->loadModel('User');
        $id = $this->request->params['named']['project_id'];

        $project = $this->Project->findById($id);
        foreach ($project['SpecifiedPro'] as $key => &$SpecifiedPro) {
            $project['SpecifiedPro'][$key]['user'] = $this->User->findById($SpecifiedPro['user_id']);
        }
        $this->set("project", $project);
    }

    function admin_pro_view() {
        $this->layout = "admin_pages";
        $this->loadModel('ProUser');
        $this->loadModel('User');
        $this->loadModel('Category');
        $this->loadModel('SubCategory');
        $this->loadModel('ProUserCategorie');
        $id = $this->request->params['named']['pro_id'];

        $pro_user = $this->ProUser->findById($id);

        $this->loadModel('Categorie');

        $this->loadModel('SubCategorie');
        $SubCategories = $this->SubCategorie->find('all', array('SubCategorie.category_id' => $pro_user['ProUser']['category_id']));
        $this->set('sub_categories', $SubCategories);

        $this->loadModel('ProUserCategorie');
        $pro_user_sub_categorie = $this->ProUserCategorie->find('list', array('fields' => array('ProUserCategorie.sub_category_id'), 'conditions' => array('ProUserCategorie.pro_user_id' => $pro_user['ProUser']['user_id'], 'ProUserCategorie.category_id' => $pro_user['ProUser']['category_id'])));
        $pro_user_sub_categorie = array_unique($pro_user_sub_categorie);
        $Category = $this->Categorie->findById($pro_user['ProUser']['category_id']);
        $this->set('pro_user_sub_categorie', $pro_user_sub_categorie);
        $this->set('Category', $Category);

        $this->set("pro_user", $pro_user);
        $state_lists = array('AL' => "Alabama",
            'AK' => "Alaska",
            'AZ' => "Arizona",
            'AR' => "Arkansas",
            'CA' => "California",
            'CO' => "Colorado",
            'CT' => "Connecticut",
            'DE' => "Delaware",
            'DC' => "District Of Columbia",
            'FL' => "Florida",
            'GA' => "Georgia",
            'HI' => "Hawaii",
            'ID' => "Idaho",
            'IL' => "Illinois",
            'IN' => "Indiana",
            'IA' => "Iowa",
            'KS' => "Kansas",
            'KY' => "Kentucky",
            'LA' => "Louisiana",
            'ME' => "Maine",
            'MD' => "Maryland",
            'MA' => "Massachusetts",
            'MI' => "Michigan",
            'MN' => "Minnesota",
            'MS' => "Mississippi",
            'MO' => "Missouri",
            'MT' => "Montana",
            'NE' => "Nebraska",
            'NV' => "Nevada",
            'NH' => "New Hampshire",
            'NJ' => "New Jersey",
            'NM' => "New Mexico",
            'NY' => "New York",
            'NC' => "North Carolina",
            'ND' => "North Dakota",
            'OH' => "Ohio",
            'OK' => "Oklahoma",
            'OR' => "Oregon",
            'PA' => "Pennsylvania",
            'RI' => "Rhode Island",
            'SC' => "South Carolina",
            'SD' => "South Dakota",
            'TN' => "Tennessee",
            'TX' => "Texas",
            'UT' => "Utah",
            'VT' => "Vermont",
            'VA' => "Virginia",
            'WA' => "Washington",
            'WV' => "West Virginia",
            'WI' => "Wisconsin",
            'WY' => "Wyoming");

        $this->set('state_lists', $state_lists);
    }

    function admin_project_verification() {
        $this->layout = 'ajax';
        $this->autoRender = FALSE;
        $id = $this->data['id'];
        $status = $this->data['status'];
        $this->loadModel('Project');
        $this->loadModel('ProUser');
        $this->loadModel('Notification');
        $this->loadModel('Categorie');
        $project = $this->Project->findById($id);
        $category = $this->Categorie->findById($project['Project']['category_id']);
        $this->Project->id = $id;
        if ($status == 'checked') {
            if ($this->Project->saveField('is_verified', 1)) {

                if (!empty($project['SpecifiedPro'])) {
                    foreach ($project['SpecifiedPro'] as $key => $specified_pro) {
                        $notify_user = $this->User->findById($specified_pro['user_id']);
                        $notification = array();
                        $this->Notification->create();
                        $notification['Notification']['user_id'] = $specified_pro['user_id'];
                        $notification['Notification']['user_type'] = 'pro';
                        $notification['Notification']['notification_type'] = 'pro_project_submitted_near_you';
                        $notification['Notification']['related_table_id'] = $id;
                        $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
                        if ($this->Notification->save($notification)) {
                            $this->loadModel('EmailTemplate');

                            $emailTemplateArr = $this->EmailTemplate->findByTemplateName('new_project_near_you_email_to_contractor');

                            $variables['[{category}]'] = $category['Categorie']['category'];
                            $variables['[{view_project_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $project['Project']['id']), true);

                            $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                            $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';

                            $this->sendEmail($emailTemplateArr, $variables, $notify_user['User']['email']);
                        }
                    }
                } else {
                    $notify_users = $this->ProUser->find('all', array('conditions' => array('ProUser.zip_code' => $project['Project']['zip_code'], 'ProUser.is_verified' => 1, 'ProUser.category_id' => $project['Project']['category_id'])));

                    foreach ($notify_users as $notify_user) {
                        $notification = array();
                        $this->Notification->create();
                        $notification['Notification']['user_id'] = $notify_user['User']['id'];
                        $notification['Notification']['user_type'] = 'pro';
                        $notification['Notification']['notification_type'] = 'pro_project_submitted_near_you';
                        $notification['Notification']['related_table_id'] = $id;
                        $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
                        if ($this->Notification->save($notification)) {
                            $this->loadModel('EmailTemplate');

                            $emailTemplateArr = $this->EmailTemplate->findByTemplateName('new_project_near_you_email_to_contractor');

                            $variables['[{category}]'] = $category['Categorie']['category'];
                            $variables['[{view_project_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $project['Project']['id'], 'admin' => false), true);

                            $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                            $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';

                            $this->sendEmail($emailTemplateArr, $variables, $notify_user['User']['email']);
                        }
                    }

                    $notification = array();
                    $this->Notification->create();
                    $notification['Notification']['user_id'] = $project['User']['id'];
                    $notification['Notification']['user_type'] = 'user';
                    $notification['Notification']['notification_type'] = 'user_project_approved';
                    $notification['Notification']['related_table_id'] = $id;
                    $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
                    if ($this->Notification->save($notification)) {
                        $this->loadModel('EmailTemplate');

                        $emailTemplateArr = $this->EmailTemplate->findByTemplateName('new_project_submitted_email_to_client');

                        $variables['[{category}]'] = $category['Categorie']['category'];
                        $variables['[{view_project_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $project['Project']['id'], 'admin' => false), true);

                        $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                        $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';

                        $this->sendEmail($emailTemplateArr, $variables, $project['User']['email']);
                    }
                }

                echo 'verified';
            }
        }
    }

    function admin_pro_verification() {
        $this->layout = 'ajax';
        $this->autoRender = FALSE;
        $id = $this->data['id'];
        $status = $this->data['status'];
        $this->loadModel('ProUser');
        $pro_user = $this->ProUser->findById($id);
        $this->ProUser->id = $id;
        if ($status == 'checked') {
            if ($this->ProUser->saveField('is_verified', 1)) {


                $notification = array();
                $this->Notification->create();
                $notification['Notification']['user_id'] = $pro_user['User']['id'];
                $notification['Notification']['user_type'] = 'pro';
                $notification['Notification']['notification_type'] = 'pro_account_approved';
                $notification['Notification']['related_table_id'] = $id;
                $notification['Notification']['create_date'] = date('Y-m-d H:i:s');
                $this->Notification->save($notification);
                echo 'verified';
            }
        }
    }

    function put_review() {
        $this->layout = 'ajax';
        $this->autoRender = FALSE;
        $this->loadModel('Hire');
        $user_id = $this->Auth->user('id');
        $user_type = $this->Auth->user('user_type');
        $id = $this->data['id'];
        $rating = $this->data['rating'];
        $reviewComment = $this->data['reviewComment'];
        $hire_result = $this->Hire->findById($id);




        $hire = array();
        $notification = array();
        $response = array();
        if ($user_type == 'pro') {
            $hire['pro_review'] = $rating;
            $hire['pro_review_comment'] = $reviewComment;
            $hire['pro_review_date'] = date('Y-m-d H:i:s');
            $notification['Notification']['notification_type'] = 'user_received_review';
            $notify_user_id = $hire_result['Hire']['user_id'];
            $notify_user_type = 'user';
        } else if ($user_type == 'user') {
            $hire['user_review'] = $rating;
            $hire['user_review_comment'] = $reviewComment;
            $hire['user_review_date'] = date('Y-m-d H:i:s');
            $notification['Notification']['notification_type'] = 'pro_received_review';
            $notify_user_id = $hire_result['Hire']['pro_user_id'];
            $notify_user_type = 'pro';
        }

        $this->Hire->id = $id;
        if ($this->Hire->save($hire)) {

            $this->Notification->create();
            $notification['Notification']['user_id'] = $notify_user_id;
            $notification['Notification']['user_type'] = $notify_user_type;
            $notification['Notification']['related_table_id'] = $id;
            $notification['Notification']['create_date'] = date('Y-m-d H:i:s');

            if ($this->Notification->save($notification)) {
                $response['user_type'] = $user_type;
                $response['status'] = 'success';
            }
        } else {
            $response['status'] = 'error';
        }

        return json_encode($response);
    }

    function profile($user_id) {
        $this->set('main_menu', 'business');
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->set('view_menu', 'dashboard_sub_menu');

        $view_from = $this->request->params['named']['view_from'];
        if ($view_from) {
            $this->set('view_from', $view_from);
        }


        $this->loadModel('ProUser');
        $this->loadModel('User');
        $this->loadModel('ProUserFile');
        $this->loadModel('ProUserPackage');
        $this->loadModel('Categorie');
        $this->loadModel('SubCategorie');
        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $user_id)));


        // $this->set('user_packages', $ProUserPackage);

        if ($pro_user['ProUser']['operational_since'] != '0000-00-00') {
            $date1 = date_create($pro_user['ProUser']['operational_since']);
            $date2 = date_create(date('Y-m-d'));
            $diff = date_diff($date1, $date2);
            $res = $diff->format("%y years experience");
            $pro_user['ProUser']['experience'] = $res;
        } else {
            $pro_user['ProUser']['experience'] = '';
        }

        $pro_user['category'] = $this->Categorie->findById($pro_user['ProUser']['category_id']);

        foreach ($pro_user['ProUserCategorie'] as $key => &$val) {
            $val['sub_category'] = $this->SubCategorie->findById($val['sub_category_id']);
        }

        $this->loadModel('Hire');

        $pro_user['project_count'] = $this->Hire->find('count', array('conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'])));
        $this->Hire->virtualFields['proRating'] = 'SUM(Hire.pro_review)';
        $pro_user['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.proRating'), 'conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed')));
        $this->Hire->virtualFields['reviewCount'] = 'count(Hire.pro_review)';
        $pro_user['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));
        unset($this->Hire->virtualFields['proRating']);
        unset($this->Hire->virtualFields['reviewCount']);
        $pro_user['reviews'] = $this->Hire->find('all', array('conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));

        
        

        $sql = "SELECT * FROM `pro_users` as ProUser WHERE id in
(SELECT `pro_user_id` FROM `pro_user_packages` WHERE 
(category_id = " . $pro_user['ProUser']['category_id'] . ")"
                . " && (`pro_user_id`> 0) && (`view_count` > 0)) "
                . "&& (user_id !=" . $user_id . ") order by rand() limit 3";
        $pro_users = $this->ProUser->query($sql);
        $package_universal_profile = $this->ProUser->find('all', array('order' => 'rand()', 'limit' => 3, 'conditions' => array('ProUser.package_universal_profile' => 1, 'ProUser.category_id' => $pro_user['ProUser']['category_id'], 'ProUser.user_id !=' => $user_id)));
        $new_users = array_unique(array_merge($pro_users, $package_universal_profile), SORT_REGULAR);
        $pro_user['user_packages'] = $new_users;

        foreach ($pro_user['user_packages'] as $key => &$val) {
            $val['cover_photo'] = $this->ProUserFile->find('first', array('conditions' => array('ProUserFile.pro_user_id' => $val['ProUser']['id'])));
            $this->Hire->virtualFields['proRating'] = 'SUM(Hire.pro_review)';
            $val['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.proRating'), 'conditions' => array('Hire.pro_user_id' => $val['User']['id'], 'Hire.hire_status' => 'completed')));
            $this->Hire->virtualFields['reviewCount'] = 'count(Hire.pro_review)';
            $val['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.pro_user_id' => $val['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));
            unset($this->Hire->virtualFields['proRating']);
            unset($this->Hire->virtualFields['reviewCount']);
            $val['user_details'] = $this->User->findById($val['ProUser']['user_id']);
            
            
            $user_package = $this->ProUserPackage->find('first', array('conditions' => array('ProUserPackage.pro_user_id' => $val['ProUser']['id'])));
            
            if ($val['ProUser']['package_universal_profile'] < 1) {
                if ($user_package['ProUserPackage']['view_count'] > 1) {
                    $ProUserPackage['ProUserPackage']['view_count'] = $user_package['ProUserPackage']['view_count'] - 1;
                    $ProUserPackage['ProUserPackage']['update_date'] = date('Y-m-d H:i:s');
                    $this->ProUserPackage->id = $user_package['ProUserPackage']['id'];
                    $this->ProUserPackage->save($ProUserPackage);
                }
            }
        }

        $this->set('ownprofile', false);
        $this->set('pro_user', $pro_user);
    }

    function admin_add_pro_by_csv() {
        $this->layout = "admin_pages";
        $this->loadModel('ProUser');
    }

    function admin_add_pro_by_csv_action() {
        if (!empty($_FILES['excel_file'])) {
            $file = $_FILES['excel_file'];
            $name_parts = pathinfo($file['name']);
            $dst_folder = 'uploaded_files/temp_file/';
            $csv_filename = time() . $this->Auth->user('id') . '.csv';
            $csv_filename = $dst_folder . $csv_filename;
            if (strtolower($name_parts['extension']) == 'csv' and $file['error'] == 0) {
                if (is_writable($dst_folder)) {
                    move_uploaded_file($file['tmp_name'], $csv_filename);
                    $q_arr = $this->csv_to_array($csv_filename);
                    @unlink($csv_filename);
                }
            }
            $state_lists = array('AL' => "Alabama",
                'AK' => "Alaska",
                'AZ' => "Arizona",
                'AR' => "Arkansas",
                'CA' => "California",
                'CO' => "Colorado",
                'CT' => "Connecticut",
                'DE' => "Delaware",
                'DC' => "District Of Columbia",
                'FL' => "Florida",
                'GA' => "Georgia",
                'HI' => "Hawaii",
                'ID' => "Idaho",
                'IL' => "Illinois",
                'IN' => "Indiana",
                'IA' => "Iowa",
                'KS' => "Kansas",
                'KY' => "Kentucky",
                'LA' => "Louisiana",
                'ME' => "Maine",
                'MD' => "Maryland",
                'MA' => "Massachusetts",
                'MI' => "Michigan",
                'MN' => "Minnesota",
                'MS' => "Mississippi",
                'MO' => "Missouri",
                'MT' => "Montana",
                'NE' => "Nebraska",
                'NV' => "Nevada",
                'NH' => "New Hampshire",
                'NJ' => "New Jersey",
                'NM' => "New Mexico",
                'NY' => "New York",
                'NC' => "North Carolina",
                'ND' => "North Dakota",
                'OH' => "Ohio",
                'OK' => "Oklahoma",
                'OR' => "Oregon",
                'PA' => "Pennsylvania",
                'RI' => "Rhode Island",
                'SC' => "South Carolina",
                'SD' => "South Dakota",
                'TN' => "Tennessee",
                'TX' => "Texas",
                'UT' => "Utah",
                'VT' => "Vermont",
                'VA' => "Virginia",
                'WA' => "Washington",
                'WV' => "West Virginia",
                'WI' => "Wisconsin",
                'WY' => "Wyoming");


            $userData = array();
            $proData = array();
            $this->loadModel('User');
            $this->loadModel('ProUser');
            $this->loadModel('Category');
            $this->loadModel('SubCategory');
            $this->loadModel('ProUserCategorie');
            foreach ($q_arr as $key => $val) {

                if ($val['Email'] == '') {
                    continue;
                } else {
                    $user = $this->User->find('first', array('conditions' => array('User.email' => trim($val['Email']))));
                    if (!$user) {
                        $userData['User']['email'] = trim($val['Email']);
                        $userData['User']['create_date'] = date('Y-m-d H:i:s');
                        $this->User->create();
                    } else {
                        $this->User->id = $user['User']['id'];
                    }
                }

                if ($val['Zip Code'] == '') {
                    continue;
                }

                if ($val['Username']) {
                    $userData['User']['name'] = $val['Username'];
                } else if ($val['Company Name']) {
                    $userData['User']['name'] = $val['Company Name'];
                } else {
                    continue;
                }



                $userData['User']['phone'] = $val['Phone'];

                $userData['User']['user_type'] = 'pro';
                $userData['User']['confirmed'] = '1';


                if ($this->User->save($userData)) {
                    if (!$user) {
                        $id = $this->User->getInsertID();
                    } else {
                        $id = $user['User']['id'];
                    }

                    $proData['ProUser']['user_id'] = $id;
                }
                $proData['ProUser']['address'] = $val['Address'];
                $proData['ProUser']['state'] = array_search($val['State'], $state_lists);
                $proData['ProUser']['city'] = $val['City'];
                if (strlen($val['Zip Code']) == 4) {
                    $proData['ProUser']['zip_code'] = '0' . $val['Zip Code'];
                } else {
                    $proData['ProUser']['zip_code'] = $val['Zip Code'];
                }

                $proData['ProUser']['company_name'] = $val['Company Name'];
                $proData['ProUser']['company_description'] = $val['Company Description'];
                $proData['ProUser']['website_url'] = $val['Website'];

                $category = $this->Category->find('first', array('conditions' => array('Category.category' => trim($val['Category']))));
                if ($category) {
                    $proData['ProUser']['category_id'] = $category['Category']['id'];
                } else {
                    $this->Category->create();
                    $CategoryData['Category']['category'] = trim($val['Category']);
                    $CategoryData['Category']['create_date'] = date('Y-m-d H:i:s');
                    if ($this->Category->save($CategoryData)) {
                        $category_id = $this->Category->getInsertID();
                        $category = $this->Category->find('first', array('conditions' => array('Category.id' => $category_id)));
                        $proData['ProUser']['category_id'] = $category['Category']['id'];
                    }
                }

                $proData['ProUser']['is_verified'] = 1;


                $checkProUser = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $id)));

                if ($checkProUser) {
                    $this->ProUser->id = $checkProUser['ProUser']['id'];
                } else {
                    $proData['ProUser']['create_date'] = date('Y-m-d H:i:s');
                    $this->ProUser->create();
                }

                if ($this->ProUser->save($proData)) {
                    if ($checkProUser) {
                        $pro_user_id = $checkProUser['ProUser']['id'];
                    } else {
                        $pro_user_id = $this->ProUser->getInsertID();
                    }
                }

                if (trim($val['Sub-Category'])) {
//                    $sub_categories = explode(',', $val['Sub-Category']);
                    $subs = preg_split("/\n|,/", $val['Sub-Category']);
                    $sub_categories = [];
                    foreach ($subs as $sub) {
                        if (trim($sub)) {
                            $existingsub = $this->SubCategory->find('first', array('conditions' => array('SubCategory.category_id' => $category['Category']['id'], 'SubCategory.sub_category' => trim($sub))));
                            if (!$existingsub) {
                                $this->SubCategory->create();
                                $SubCategoryData = [];
                                $SubCategoryData['SubCategory']['category_id'] = $category['Category']['id'];
                                $SubCategoryData['SubCategory']['sub_category'] = trim($sub);
                                $SubCategoryData['SubCategory']['create_date'] = date('Y-m-d H:i:s');
                                $this->SubCategory->save($SubCategoryData);
                                $existingsub = $this->SubCategory->findById($this->SubCategory->getInsertID());
                            }
                            $sub_categories[] = $existingsub;
                        }
                    }
                } else {
                    $sub_category_exist = $this->SubCategory->find('first', array('conditions' => array('SubCategory.category_id' => $category['Category']['id'], 'SubCategory.sub_category' => trim($val['Category']))));
                    if ($sub_category_exist) {
                        $sub_categories[] = $sub_category_exist;
                    } else {
                        $this->SubCategory->create();
                        $SubCategoryData = [];
                        $SubCategoryData['SubCategory']['category_id'] = $category['Category']['id'];
                        $SubCategoryData['SubCategory']['sub_category'] = trim($val['Category']);
                        $SubCategoryData['SubCategory']['create_date'] = date('Y-m-d H:i:s');
                        $this->SubCategory->save($SubCategoryData);
                        $sub_categories[] = $this->SubCategory->findById($this->SubCategory->getInsertID());
                    }
                }

                $ProUserCategorieData = array();
                foreach ($sub_categories as $sub_key => $sub_category) {
                    $check_pro_sub_category = $this->ProUserCategorie->find('first', array('conditions' => array('ProUserCategorie.pro_user_id' => $pro_user_id, 'ProUserCategorie.sub_category_id' => $sub_category['SubCategory']['id'])));
                    if (!$check_pro_sub_category) {
                        $this->ProUserCategorie->create();
                        $ProUserCategorieData['ProUserCategorie']['pro_user_id'] = $pro_user_id;
                        $ProUserCategorieData['ProUserCategorie']['category_id'] = $category['Category']['id'];
                        $ProUserCategorieData['ProUserCategorie']['sub_category_id'] = $sub_category['SubCategory']['id'];
                        $ProUserCategorieData['ProUserCategorie']['create_date'] = date('Y-m-d H:i:s');
                        $this->ProUserCategorie->save($ProUserCategorieData);
                    }
                }
            }
        }

        $this->redirect(array("controller" => "pro", "action" => "add_pro_by_csv"));
    }

    private
            function csv_to_array($filename = '') {
        if (!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $delimiter = ';';
        $handle = fopen($filename, 'r');

        if (!$handle) {
            return FALSE;
        }

        $row = fgetcsv($handle, 0, $delimiter);
        if (!$row or count($row) == 1) {
            $delimiter = ',';
        }
        fclose($handle);
        $handle = fopen($filename, 'r');
        $header = NULL;
        $data = array();
        while (($row = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
            if (!$header)
                $header = $row;
            else {
                foreach ($row as $k => $r) {
                    $row[$k] = utf8_encode($r);
                }
                $data[] = array_combine($header, $row);
            }
        }
        fclose($handle);
        return $data;
    }

    function profinder() {
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $this->loadModel('Categorie');
        $this->Categorie->recursive = -1;
        $categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('categories', $categories);
        $this->set('serach_from', 'profinder');
    }

    function pro_finder_lists() {
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');

        $this->loadModel('ProUserCategorie');
        $this->loadModel('ProUser');
        $this->loadModel('Hire');
        $user_zipcode = '';

        if (isset($this->request->params['named']['zip_code'])) {
            $user_zipcode = $this->request->params['named']['zip_code'];
            $this->Session->write('user_zipcode', $user_zipcode);
        }


//        $user_zipcode = $this->Session->read('user_zipcode');
        $category_id = $this->request->params['named']['category_id'];

        $per_page = 20;


        $joins = array();
        $joins[] = array(
            'table' => 'pro_user_categories',
            'conditions' => array(
                'ProUserCategorie.pro_user_id = ProUser.id',
            ),
            'type' => 'LEFT',
            'alias' => 'ProUserCategorie'
        );

        $condition = array();
        $condition['ProUser.category_id'] = $category_id;
        $condition['ProUser.is_verified'] = 1;
        if ($user_zipcode) {
            $condition['ProUser.zip_code'] = $user_zipcode;
        }



        $options = array(
            'conditions' => $condition,
            'limit' => $per_page,
            'joins' => $joins,
            'group' => 'ProUser.user_id',
//            'order' => 'Question.date DESC'
        );


        $this->paginate = $options;
        $pro_users = $this->paginate('ProUser');


//        $pro_users = $this->ProUser->find('all', array('joins' => $joins, 'conditions' => array('ProUserCategorie.category_id' => $category_id, 'ProUserCategorie.sub_category_id' => $sub_category_id, 'ProUser.zip_code' => $user_zipcode, 'ProUser.is_verified' => 1)));
//        $pro_users = $this->ProUser->find('all', array('joins' => $joins, 'conditions' => $condition, 'group' => 'ProUser.user_id'));
        foreach ($pro_users as $key => &$pro_user) {
            $pro_users[$key]['project_count'] = $this->Hire->find('count', array('conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'])));
            $this->Hire->virtualFields['proRating'] = 'SUM(Hire.pro_review)';
            $pro_users[$key]['rating_sum'] = $this->Hire->find('first', array('fields' => array('Hire.proRating'), 'conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed')));
            $this->Hire->virtualFields['reviewCount'] = 'count(Hire.pro_review)';
            $pro_users[$key]['reviewCount'] = $this->Hire->find('first', array('fields' => array('Hire.reviewCount'), 'conditions' => array('Hire.pro_user_id' => $pro_user['User']['id'], 'Hire.hire_status' => 'completed', 'Hire.pro_review !=' => 0)));
        }

        $this->set('pro_users', $pro_users);

        $this->loadModel('Categorie');
        $categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('categories', $categories);
        $this->set('serach_from', 'profinder');
    }

    function submit_project_details() {
        $this->layout = 'home';
        $this->set('view_header', 'dashboard');
        $zip_code = '';

        if (isset($this->request->params['named']['zip_code'])) {
            $zip_code = $this->request->params['named']['zip_code'];
            $this->Session->write('user_zipcode', $zip_code);
        } elseif ($this->Session->read('user_zipcode')) {
            $zip_code = $this->Session->read('user_zipcode');
        }

        $category_id = $this->request->params['named']['category_id'];
        $this->set('category_id', $category_id);



        $this->loadModel('Categorie');
        $categories = $this->Categorie->find('all', array(
            'order' => array('category ASC')
        ));
        $this->set('categories', $categories);

        if (isset($this->request->params['named']['specified_pro'])) {
            $this->loadModel('User');
            $specified_pro_user_id = $this->request->params['named']['specified_pro'];
            $specified_pro = $this->User->findById($specified_pro_user_id);
            $this->set('specified_pro', $specified_pro);
        }

        $this->loadModel('User');
        $user_id = $this->Auth->user('id');
        $user = $this->User->findById($user_id);
        $this->set('user', $user);

        if ($zip_code) {
            $this->loadModel('ProUser');
            $current_category = $this->Categorie->findById($category_id);
            $this->set('current_category', $current_category);
        }
        $this->set('zip_code', $zip_code);
    }

    function submit_project_details_action() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('User');
        $this->loadModel('Project');
        $this->loadModel('ProjectFile');
        $this->loadModel('SpecifiedPro');
        $response = array();
        $error = array();
        $Project = array();
        $user_data = array();

        if (strlen($this->data['email']) == 0) {
            $error['emailError'] = __('Your email is required');
        } elseif (filter_var($this->data['email'], FILTER_VALIDATE_EMAIL) == FALSE) {
            $error['emailError'] = __('Your email is not valid');
        } else {
            $user = $this->User->find('first', array('conditions' => array('User.email' => $this->data['email'])));
            if (!$user) {
                if (strlen($this->data['name']) == 0) {
                    $error['nameError'] = __('Your name is required');
                } else {
                    $user_data['User']['name'] = $this->data['name'];
                }
                if (strlen($this->data['phone']) == 0) {
                    $error['phoneError'] = __('Your phone is required');
                } else {
                    $user_data['User']['phone'] = $this->data['phone'];
                }
                $user_data['User']['email'] = $this->data['email'];
                if (strlen($this->data['password']) == 0) {
                    $error['passwordError'] = 'Password is required';
                } else {
                    $user_data['User']['password'] = AuthComponent::password($this->data['password']);
                }
                $user_data['User']['create_date'] = date('Y-m-d H:i:s');
                $user_data['User']['user_type'] = 'user';
            } else {
                $Project['Project']['user_id'] = $user['User']['id'];
            }
        }

        if (strlen($this->data['project_title']) == 0) {
            $error['projectTitleError'] = __('Project title is required');
        } else {
            $Project['Project']['project_title'] = $this->data['project_title'];
        }
        $Project['Project']['category_id'] = $this->data['category_id'];

        if (strlen($this->data['details']) == 0) {
            $error['detailsError'] = __('Plase provide as much information as you can for the contractor to give you a cost estimate');
        } else {
            $Project['Project']['details'] = $this->data['details'];
        }
        if (strlen($this->data['materials']) == 0) {
            $error['materialsError'] = __('Project materials is required');
        } else {
            $Project['Project']['materials'] = $this->data['materials'];
        }

        if (strlen($this->data['start_date']) == 0) {
            $error['startDateError'] = __('Start date is required');
        } else {
            $Project['Project']['start_date'] = $this->data['start_date'];
        }

        if (strlen($this->data['budget']) == 0) {
            $error['budgetError'] = __('Budget is required');
        } else {
            $Project['Project']['budget'] = $this->data['budget'];
        }


        $Project['Project']['zip_code'] = $this->data['zip_code'];


        if (empty($this->data['terms_conditions'])) {
            $error['termsConditionsError'] = __('Please check terms and conditions');
        }

        $Project['Project']['create_date'] = date('Y-m-d H:i:s');

        if (!$error) {

            if (!$Project['Project']['user_id']) {
                $this->User->create();
                if ($this->User->save($user_data)) {
                    $user_id = $this->User->getInsertID();
                    $Project['Project']['user_id'] = $user_id;
                }
            }

            if ($this->Project->save($Project)) {
                $project_user = $this->User->findById($Project['Project']['user_id']);
                $id = $this->Project->getInsertID();
                foreach ($this->data['specified_pros'] as $key => $specified_pro) {
                    $this->SpecifiedPro->create();
                    $SpecifiedPro['SpecifiedPro']['project_id'] = $id;
                    $SpecifiedPro['SpecifiedPro']['user_id'] = $specified_pro;
                    $SpecifiedPro['SpecifiedPro']['create_date'] = date('Y-m-d H:i:s');
                    $this->SpecifiedPro->save($SpecifiedPro);
                }

                if (!empty($_FILES['project_file'])) {
                    $path = 'uploaded_files/project_files/' . $id;
                    $folder = mkdir($path, 0777);
                    foreach ($_FILES['project_file']['name'] as $key => $project_file) {
                        $filename = $path . '/' . $project_file;
                        $accepted_type = array('jpg', 'png', 'gif');
                        $pathinfo = pathinfo($project_file);
                        if (in_array(strtolower($pathinfo['extension']), $accepted_type)) {
                            if (move_uploaded_file($_FILES['project_file']['tmp_name'][$key], $filename)) {
                                $this->ProjectFile->create();
                                $ProjectFile['ProjectFile']['project_id'] = $id;
                                $ProjectFile['ProjectFile']['file'] = $filename;
                                $ProjectFile['ProjectFile']['create_date'] = date('Y-m-d H:i:s');
                                $this->ProjectFile->save($ProjectFile);
                            }
                        }
                    }
                }

                $this->loadModel('EmailTemplate');

                $emailTemplateArr = $this->EmailTemplate->findByTemplateName('project_verify_email_to_admin');

                $variables['[{client_name}]'] = $project_user['User']['name'];
                $variables['[{view_project_link}]'] = Router::url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $id), true);

                $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';
                if ($this->sendEmail($emailTemplateArr, $variables, Configure::read('settings.config.admin_mail'))) {

                    $emailTemplateArr = $this->EmailTemplate->findByTemplateName('confirmation_of_project_submission');

                    $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                    $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';

                    $this->sendEmail($emailTemplateArr, $variables, $this->data['email']);

                    $response['type'] = "success";
                }
            }
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
        }
        return json_encode($response);
    }

    function claim_business() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('ClaimBusiness');
        $this->loadModel('ProUser');

        $response = array();
        $error = array();
        $claimBusiness = array();

        $claimCount = $this->ClaimBusiness->find('count', array('conditions' => array('ClaimBusiness.claim_user_id' => $this->Auth->user('id'), 'ClaimBusiness.status' => array('accepted', 'claimed'))));

        $pro_user = $this->ProUser->find('first', array('conditions' => array('ProUser.user_id' => $this->data['user_id'])));

        if ($claimCount > 0) {
            //$response['type'] = "error";
            $error['messageError'] = __('You have already claimed');
        }


        if (strlen($this->data['name']) == 0) {
            $error['nameError'] = __('Your Name is required');
        } else {
            $claimBusiness['ClaimBusiness']['name'] = $this->data['name'];
        }

        if (strlen($this->data['email']) == 0) {
            $error['emailError'] = __('Your email is required');
        } elseif (filter_var($this->data['email'], FILTER_VALIDATE_EMAIL) == FALSE) {
            $error['emailError'] = __('Your email is not valid');
        } else {
            $claimBusiness['ClaimBusiness']['email'] = $this->data['email'];
        }

        if (strlen($this->data['phone']) == 0) {
            $error['phoneError'] = __('Your Phone is required');
        } else {
            $claimBusiness['ClaimBusiness']['phone'] = $this->data['phone'];
        }

        $user_id = $this->Auth->user('id');
        $claimBusiness['ClaimBusiness']['message'] = $this->data['message'];
        $claimBusiness['ClaimBusiness']['user_id'] = $this->data['user_id'];
        $claimBusiness['ClaimBusiness']['pro_user_id'] = $pro_user['ProUser']['id'];
        $claimBusiness['ClaimBusiness']['claim_user_id'] = $user_id;
        $claimBusiness['ClaimBusiness']['create_date'] = date('Y-m-d H:i:s');
        $claimBusiness['ClaimBusiness']['status'] = 'claimed';



        if (!$error) {

            $this->ClaimBusiness->create();
            if ($this->ClaimBusiness->save($claimBusiness)) {
                $this->ProUser->id = $pro_user['ProUser']['id'];
                $this->ProUser->saveField('claim_status', 'claimed');

                $this->loadModel('EmailTemplate');

                $emailTemplateArr = $this->EmailTemplate->findByTemplateName('new_contractor_profile_claimed');
                $variables['[{company_name}]'] = $pro_user['ProUser']['company_name'];
                $variables['[{person_name}]'] = $this->data['name'];
                $variables['[{person_email}]'] = $this->data['email'];
                $variables['[{person_phone}]'] = $this->data['phone'];
                $variables['[{person_message}]'] = $this->data['message'];


                $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
                $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';

//                $welcome_mail = $this->EmailTemplate->findByTemplateName('welcome_mail');

                $this->sendEmail($emailTemplateArr, $variables, 'admin@homeprosnow.com');

                $response['type'] = "success";
            } else {
                $response['type'] = "error";
                $response['errorMsg'] = $error;
            }
            return json_encode($response);
        } else {
            $response['type'] = "error";
            $response['errorMsg'] = $error;
            return json_encode($response);
        }
    }

    function admin_claim_change() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('ClaimBusiness');
        $this->loadModel('ProUser');
        $this->loadModel('User');
        $this->loadModel('Project');
        $this->loadModel('ProjectQuote');
        $this->loadModel('SpecifiedPro');
        $this->loadModel('Transaction');
        $this->loadModel('UserAccount');

        $claim_id = $this->data['claim_id'];
        //$user_id = $this->data['user_id'];
        $claim_user_id = $this->data['claim_user_id'];
        $pro_user_id = $this->data['pro_user_id'];

        $pro_user = $this->ProUser->findById($pro_user_id);

        $pro_update = array();
        $pro_update['ProUser']['user_id'] = $claim_user_id;
        $pro_update['ProUser']['claim_status'] = 'connected';
        $this->ProUser->id = $pro_user['ProUser']['id'];
        $this->ProUser->save($pro_update['ProUser']);


        $user_data = $this->User->findById($pro_user['ProUser']['user_id']);
        $this->User->id = $claim_user_id;
        $this->User->save(array('User' => array('user_type' => 'pro', 'profile_pic' => $user_data['User']['profile_pic'])));

        $this->User->delete($pro_user['ProUser']['user_id']);



        $projects = $this->Project->find('all', array('conditions' => array('Project.user_id' => $pro_user['ProUser']['user_id'])));
        if ($projects) {
            foreach ($projects as $project) {
                $project_update = array();
                $project_update['Project']['user_id'] = $claim_user_id;
                $this->Project->id = $project['Project']['id'];
                $this->Project->save($project_update['Project']);
            }
        }

        $specifies = $this->SpecifiedPro->find('all', array('conditions' => array('SpecifiedPro.user_id' => $pro_user['ProUser']['user_id'])));
        if ($specifies) {
            foreach ($specifies as $specifie) {
                $specifie_update = array();
                $specifie_update['SpecifiedPro']['user_id'] = $claim_user_id;
                $this->SpecifiedPro->id = $specifie['SpecifiedPro']['id'];
                $this->SpecifiedPro->save($specifie_update['SpecifiedPro']);
            }
        }

        $quotes = $this->ProjectQuote->find('all', array('conditions' => array('ProjectQuote.user_id' => $pro_user['ProUser']['user_id'])));
        if ($quotes) {
            foreach ($quotes as $quote) {
                $quote_update = array();
                $quote_update['ProjectQuote']['user_id'] = $claim_user_id;
                $this->ProjectQuote->id = $quote['ProjectQuote']['id'];
                $this->ProjectQuote->save($quote_update['ProjectQuote']);
            }
        }

        $accounts = $this->UserAccount->find('all', array('conditions' => array('UserAccount.user_id' => $pro_user['ProUser']['user_id'])));
        if ($accounts) {
            foreach ($accounts as $account) {
                $account_update = array();
                $account_update['UserAccount']['user_id'] = $claim_user_id;
                $this->UserAccount->id = $account['UserAccount']['id'];
                $this->UserAccount->save($account_update['UserAccount']);
            }
        }

        $claim = $this->ClaimBusiness->find('first', array('conditions' => array('ClaimBusiness.id' => $claim_id)));
        $claim['ClaimBusiness']['status'] = 'accepted';
        $this->ClaimBusiness->id = $claim['ClaimBusiness']['id'];
        $this->ClaimBusiness->save($claim['ClaimBusiness']);

        echo 'Success';
    }

    function admin_get_claim_business_list() {
        $this->layout = "admin_pages";
        $this->loadModel('ClaimBusiness');
        $per_page = 5;
        $options = array(
            'conditions' => array('ClaimBusiness.status' => 'claimed'),
            'limit' => $per_page,
//            'group' => 'Question.id',
//            'order' => 'Question.date DESC'
        );


        $this->paginate = $options;
        $claim_business_lists = $this->paginate('ClaimBusiness');
        $this->set("claim_business_lists", $claim_business_lists);
    }

    function admin_delete_claim_list() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $this->loadModel('ClaimBusiness');
        $this->loadModel('ProUser');

        $claim_update = array();
        $claim_update['ClaimBusiness']['status'] = 'deleted';
        $this->ClaimBusiness->id = $this->data['id'];
        $this->ClaimBusiness->save($claim_update['ClaimBusiness']);

        $pro_id = $this->data['id'];
        //$claim = $this->ClaimBusiness->find('first', array('conditions' => array('ClaimBusiness.pro_user_id' => $pro_id)));
        //$this->ClaimBusiness->delete($claim['ClaimBusiness']['id']);

        $this->ProUser->id = $pro_id;
        $this->ProUser->save(array('ProUser' => array('claim_status' => "not_claimed")));

        echo 'success';
    }

}
