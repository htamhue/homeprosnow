<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    var $ext = '.php';
    public $components = array(
        'RequestHandler',
        'Session',
        'Auth',
        'Flash',
        'Cookie',
        'Email'
    );

    function beforeFilter() {
        if (isset($this->request->params['admin'])) {
            $this->Auth->loginAction = array('controller' => 'admin', 'action' => 'index');
        } else {
            $this->Auth->loginAction = array('controller' => 'home', 'action' => 'index');
            if ($this->Auth->user('login_as') == 'admin') {
                $this->Session->setFlash("You are logged in as admin, If you want to visit the site, you need to log out from admin");
                $this->redirect(array("controller" => "admin", "action" => "index"));
            }
        }

        if (!$this->Session->read('user_zipcode') and $_SERVER['REMOTE_ADDR']) {
//            $ch = curl_init('http://ipinfo.io/' . $_SERVER['REMOTE_ADDR']);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_HEADER, false);
//            $data = json_decode(curl_exec($ch));
//            if ($data->postal) {
//                $this->Session->write('user_zipcode', $data->postal);
//                
//            }
            $this->Session->write('user_zipcode', '01701');
        }

        $this->loadModel('FeaturedCategorie');
        $FeaturedCategories = $this->FeaturedCategorie->find('all',array('conditions' => array('FeaturedCategorie.id !=' => 5)));
        $this->set('FeaturedCategories', $FeaturedCategories);

        parent::beforeFilter();
    }

    function sendConfirmationEmail($data, $id) {
        $this->loadModel('EmailTemplate');
        $emailTemplateArr = $this->EmailTemplate->findByTemplateName('account_confirmation');

        $confirmLink = md5("CONFIRM" . $data['User']['email'] . "USER" . $id . "CONFIRM");
        $confirmLink = Router::url(array('controller' => 'users', 'action' => 'confirm_account', $id, $confirmLink), true);
        $variables['[{name}]'] = $data['User']['name'];
        $variables['[{confirm_account_link}]'] = $confirmLink;
        $variables['[{baselink}]'] = Configure::read('settings.config.baselink');
        $variables['[{help_link}]'] = Configure::read('settings.config.baselink') . '/' . 'help';

        $this->sendEmail($emailTemplateArr, $variables, $data['User']['email']);
    }

    function sendEmail($mail_data, $variable_arr = array(), $to_email) {
        $this->autoRender = false;
        $email_content = $mail_data['EmailTemplate']['content'];
        $variable_arr['[{LOGO}]'] = Router::url('/', true) . 'img/mail_img/logo.png';
        $variable_arr['[{copyright}]'] = '&copy; ' . date('Y') . ', Homeprosnow';

        foreach ($variable_arr as $key => $value) {
            $email_content = str_replace($key, $value, $email_content);
        }

        if ($_SERVER['HTTP_HOST'] == 'localhost') {
            file_put_contents('F:/mail.html', $email_content);
            return true;
        }

        $Email = new CakeEmail();
        $Email->config('default');
        $Email->emailFormat('html');

        $Email->from(array('noreply@homeprosnow.com' => 'HomeProsNow'));
        $Email->to($to_email);
        $Email->subject($mail_data['EmailTemplate']['subject']);

        if ($Email->send($email_content)) {
            return true;
        } else {
            echo 'Email not sent';
        }
    }

    function reload_user_data() {
        $this->loadModel('User');
        $user_type = $this->Auth->user('user_type');
        $user_id = $this->Auth->user('id');
        $user = $this->User->findById($user_id);
        $user["User"]["user_type"] = $user_type;
        $this->Auth->login($user['User']);
    }

}
