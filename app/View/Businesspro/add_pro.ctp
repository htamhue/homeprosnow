<!-- Start main content -->
<main class="main-content">
    <form action="#" method="post" id="form-add-project" class="form-new">
        <div class="container pb-5 step step-1 active">
            <div class="row">
                <div class="col-sm-9 col-md-7 mx-auto">
                    <h2 class="my-5 title">How should we contact you?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9 col-md-7 mx-auto border bg-white animation">
                    <div class="mx-3 my-5">
                        <div class="form-row">
                            <div class="form-group form-fname col-12 col-md-6">
                                <label for="fname">First Name</label>
                                <input type="text" class="form-control" name="fname" id="fname" />
                            </div>

                            <div class="form-group form-lname col-12 col-md-6">
                                <label for="lname">Last Name</label>
                                <input type="text" class="form-control" name="lname" id="lname" />
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group form-phone col-12 col-md-6">
                                <label for="phone">Phone Number</label>
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="(000) 000-0000" />
                            </div>

                            <div class="form-group form-email col-12 col-md-6">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="prj_email" id="prj_email" />
                            </div>
                        </div>

                        <div class="form-group form-description">
                            <label for="describe">Describe what you need done</label>
                            <textarea class="form-control" name="describe" id="describe" rows="4"></textarea>
                        </div>

                        <div class="form-row">
                            <div class="form-group form-budget col-12 col-md-6">
                                <label for="budget">Your Budget</label>
                                <input type="text" class="form-control" name="budget" id="budget" placeholder="$">
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-6">
                                <!-- <button class="btn btn-back btn-block">BACK</button> -->
                            </div>
                            <div class="col-6">
                                <button class="btn btn-next btn-block btn-step" data-step="2">NEXT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pb-5 step step-2">
            <div class="row">
                <div class="col-sm-10 col-md-8 mx-auto">
                    <h2 class="my-5 title">Complete your project details</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-md-7 mx-auto border bg-white animation">
                    <div class="mx-3 my-5">
                        <div class="form-group form-question1">
                            <label for="question1">Do you have the materials needed for this project?</label>
                            <select class="form-control" name="question1" id="question1">
                                <option value="select">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>

                        <div class="form-group form-question2">
                            <label for="question2">How soon do you want the work to start?</label>
                            <select class="form-control" name="question2" id="question2">
                                <option value="select">Select</option>
                                <option value="As soon as possible">As soon as possible</option>
                            </select>
                        </div>

                        <div class="form-group form-question3">
                            <label for="question3">Do you want this project to be sent to any specific contractors? (Include their names below)</label>
                            <input type="text" class="form-control" name="question3" id="question3">
                        </div>

                        <div class="row mt-5">
                            <div class="col-6">
                                <button class="btn btn-back btn-block btn-step" data-step="1">BACK</button>
                            </div>
                            <div class="col-6">
                                <button class="btn btn-next btn-block btn-step" data-step="3">NEXT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pb-5 step step-3">
            <div class="row">
                <div class="col-sm-10 col-md-8 mx-auto">
                    <h2 class="my-5 title">Where will the work be done?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-md-7 mx-auto border bg-white animation">
                    <div class="mx-3 my-5">
                        <div class="form-group form-street">
                            <label for="street">Street Address</label>
                            <input type="text" class="form-control" name="street" id="street">
                        </div>

                        <div class="form-row">
                            <div class="form-group form-state col-12 col-md-6">
                                <label for="state">State</label>
                                <select class="form-control" name="state" id="state">
                                    <option value="select">Select</option>
                                    <?php
                                        foreach($state_lists as $key => $state):
                                            echo '<option value="'.$state.'">'.$state.'</option>';
                                        endforeach;
                                    ?>
                                </select>
                            </div>

                            <div class="form-group form-city col-12 col-md-6">
                                <label for="city">Country</label>
                                <select class="form-control" name="country" id="country">
                                    <option value="select">Select</option>
                                    <?php
                                        foreach($countries as $key => $country):
                                            echo '<option value="'.$country['Country']['nicename'].'">'.$country['Country']['name'].'</option>';
                                        endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group form-zipcode col-12 col-md-6">
                                <label for="zipcode">Zip Code</label>
                                <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="10001">
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-6">
                                <button class="btn btn-back btn-block btn-step" data-step="2">BACK</button>
                            </div>
                            <div class="col-6">
                                <button class="btn btn-next btn-block btn-step" data-step="4">NEXT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pb-5 step step-4">
            <div class="row">
                <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <h2 class="my-5 title">Complete your registration</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9 col-md-7 col-lg-5 mx-auto border bg-white animation">
                    <div class="mx-3 my-5">
                        <div class="form-group form-email">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>
                        <div class="form-group form-password">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                        <div class="form-group form-confirm">
                            <label for="repassword">Confirm Password</label>
                            <input type="password" class="form-control" name="repassword" id="repassword">
                        </div>
                        <div class="form-group form-check form-agree">
                            <input type="checkbox" class="form-check-input" name="check" id="check">
                            <label for="check" class="form-check-label">I agree to the terms of use and privacy policy.</label>
                        </div>
                        <div class="row button-group mt-5">
                            <div class="col-12 col-md-6 mb-3">
                                <button class="btn btn-block btn-back btn-step" data-step="3">BACK</button>
                            </div>
                            <div class="col-12 col-md-6">
                                <button class="btn btn-create btn-block" type="submit">CREATE ACCOUNT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</main>
<!-- End main content -->