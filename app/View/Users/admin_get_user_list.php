<script type="text/javascript">
    function deleteSingle(id) {
        var base_path = $("#base_path").val();
        if (confirm('Are you sure to delete this item?')) {
            var url = '<?php echo $this->Html->url(array('controller' => 'user_interests', 'action' => 'delete_interest')); ?>';
            var postData = {'id': id};
            $.post(url, postData, function (data) {
                if (data == 'success') {
                    $('#iD' + id).fadeOut();
                } else {
                    alert(data);
                }
            }, 'text');
        }
    }

    function resetPassword(id) {
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'reset_pass_by_admin')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function (data) {
            if (data == 'success') {
                $('.resetPasswordMessage').removeClass('hidden');
            }
        }, 'text');
    }
    
    function deleteUser(id) {
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'delete_user_by_admin')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function (data) {
            if (data == 'success') {
                location.reload();
            }
        }, 'text');
    }
</script>

<div class="content-box"><!-- Start Content Box -->



    <div class="content-box-header">
        <h3>Users</h3>
    </div> <!-- End .content-box-header -->

    <div class="resetPasswordMessage hidden">
        <br/>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong> Password successfully changed.
        </div>
        <br/>
    </div>

    <div class="content-box-content">
        <form method="get" action="<?php echo $this->webroot . 'admin/users/get_user_list' ?>">
            <div class="form-group">
                <div class="col-sm-2">
                    <label style="font-size: 15px;">User Search</label>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" value="<?php echo $_GET['name']; ?>" name="name" placeholder="Enter Name">
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="<?php echo $_GET['email']; ?>" name="email" placeholder="Enter email">
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="<?php echo $this->webroot . 'admin/users/get_user_list'; ?>" type="submit" class="btn btn-primary">Clear</a>
                </div>
            </div>
            <p><br /></p>
        </form>

        <?php // echo $form->create("UserInterests", array("id" => "user_interest", "url" => BASE_PATH_ADMIN . "/user_interests/delete_all", "name" => "borough")); ?>
        <table>
            <thead>
                
                <tr>
                    <th>Name</th>
                    <th>Photo</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Country</th>
                    <th>State</th>
                    <th>City</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($users as $user) { ?>
                    <tr id="user_<?php echo $user['User']['id']; ?>">
                        <td>
                            <a  target="_blank" href="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'admin_edit_user_by_admin', $user['User']['id'])); ?>">
                                <?php echo $user['User']['name']; ?>
                            </a>
                        </td>
                        <td>
                            <div class="live_company_pic_upload_area">
                                <form autocomplete="off"  action="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'live_profile_pic_upload_by_admin')); ?>" class="updateLiveProfilePic" >
                                    <input type="file" class="live_profile_pic_form" name="profile_pic" id="profile_pic_<?php echo $user['User']['id'] ?>" />
                                    <input type="hidden" name="user_id" value="<?php echo $user['User']['id'] ?>" />
                                    <?php
                                    if (isset($user['User']['profile_pic'])) {
                                        ?>
                                        <label for="profile_pic_<?php echo $user['User']['id'] ?>">
                                            <img id="pro_pic_<?php echo $user['User']['id'] ?>" alt=""  width="50" src="uploaded_files/profile_pic/<?php echo $user['User']['id']; ?>/<?php echo $user['User']['profile_pic']; ?>" />
                                        </label>

                                        <?php
                                    } else {
                                        ?>
                                        <label for="profile_pic_<?php echo $user['User']['id'] ?>">
                                            <img alt="" id="pro_pic_<?php echo $user['User']['id'] ?>" for="profile_pic_<?php echo $user['User']['id'] ?>" width="50" src="img/company_logo.jpg" />
                                        </label>

                                        <?php
                                    }
                                    ?>

                                </form>

                            </div>
                        </td>
                        <td><?php echo $user['User']['email']; ?></td>
                        <td><?php echo $user['User']['phone']; ?></td>
                        <td><?php echo $user['User']['address']; ?></td>
                        <td><?php echo $user['User']['country']; ?></td>
                        <td><?php echo $user['User']['state']; ?></td>
                        <td><?php echo $user['User']['city']; ?></td>
                        
                        <td>
                            <a target="_blank" href="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'admin_edit_user_by_admin', $user['User']['id'])); ?>">Edit</a>
                        </td>
                    </tr>
                    
                    
                    
                    
                     <!-- Modal -->
        
                    
                <?php } ?>
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="6">


                        <?php if ($this->params['paging']['User']['pageCount'] > 1) { ?>
                            <div class="pagination">
                                <ul>
                                    <li class="page-prev"><?php echo $this->Paginator->prev(); ?></li>
                                    <li><?php echo $this->Paginator->numbers(); ?></li>
                                    <li class="page-next"><?php echo $this->Paginator->next(); ?></li>
                                </ul>
                            </div> <!-- End .pagination -->
                        <?php } ?>
                        <div class="clear"></div>
                    </td>
                </tr>
            </tfoot>

        </table>



       

<?php // $form->end();  ?>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->


<!-- Modal -->



<script type="text/javascript">
    $(function () {

        var options = {
            type: 'post',
            dataType: 'json',
            beforeSubmit: function (arr, $form, options) {
                $('.error_msg').hide();
            },
            success: responsefunc
        };

        $('.updateLiveProfilePic').submit(function () {
            $(this).ajaxSubmit(options);
            //$(".addpictureblock").modal('hide');
            return false;
        });


        $(document).on('change', '.live_profile_pic_form', function () {
            var that = $(this);
            that.closest('form').submit();

        });




    });

    function responsefunc(responseText, statusText, xhr, $form) {
        //        alert_r(responseText);
        if (responseText.type == 'error') {
            $('.emailError').html(responseText.error.emailError).show();
            $('.imageError').html(responseText.error.imageError).show();
        } else {
            var path = 'uploaded_files/profile_pic/' + responseText.user.User.id + '/' + responseText.user.User.profile_pic;
            $('#pro_pic_' + responseText.user.User.id).attr('src', path);
        }
    }



//             .on('change', '.live_profile_pic_form', function () {
//            var that = $(this);
//            that.closest('form').submit();
//
//        });


</script>

<script type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $('a.new_package').click(function (e) {
            e.preventDefault();
            var uid = $(this).data("uid");
            var cid = $(this).data('cid');
            $('[name=pro_user_id]').val(uid);
            $('[name=category_id]').val(cid);
            $('#packageModal_'+ uid).modal('show');
        });

        $('.addPackage').submit(function (e) {
            e.preventDefault();
            var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'pro_user_package')); ?>';
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                success: function (data) {
                    $('<p class="pe"></p>').insertAfter('[name=package_universal]');
                    if (data == 'Error') {
                        $('.pe').remove();
                        $('<p class="pe" style="color: #ff0000;">Choose a Package</p>').insertAfter('[name=package_universal]');
                    } else {
                        $('.pe').remove();
                        $('.package_box').html(data).show();
                        setTimeout(function () {
                            $('.package_box').fadeOut();
                            $('.modal').modal('hide');
                        }, 3000);
                    }

                }
            });
        });
    })
</script>

