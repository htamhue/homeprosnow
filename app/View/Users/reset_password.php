<div class="reset_password_area">
    <div class="reset_password_heading">
        Update Password
    </div>
    <form autocomplete="off"  action="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'reset_password_action')); ?>" id="changePassword" >
        <div class="reset_password_body">

            <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
            <input type="hidden" name="key" value="<?php echo $key; ?>" />
            <input name="newPassword" placeholder="New Password" type="password" />

            <input name="confirmNewPassword" placeholder="Confirm New Password" type="password" />

            <div class="error_msg passwordError">
            </div>
            <a href="javascript:" class="change_pass common_button">Change Password</a>
        </div>
        <input type="submit" value="submit"  style="display: none;"/>
    </form>
</div>

<script type="text/javascript">
    $(function() {
        $('.change_pass').click(function() {
            $('#changePassword').submit();
        });
        var options = {
            success: showResponse,
            type: 'post',
            dataType: 'json'

        };
        // bind to the form's submit event 
        $('#changePassword').submit(function() {

            $(this).ajaxSubmit(options);

            return false;
        });
    });
    function showResponse(responseText, statusText, xhr, $form) {
//        alert_r(responseText);
        if (responseText.type == 'error') {

            if (responseText.errorMsg.passwordError) {
                $('.reset_password_area').find('.passwordError').html(responseText.errorMsg.passwordError).show();
            }

        } else if (responseText.type == 'success') {
            $('#successModal').find('.details').html('successfully changed your password');
            $('#successModal').modal();
        }

    }

</script>