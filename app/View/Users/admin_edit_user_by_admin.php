
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Edit User</h3>
    </div> <!-- End .content-box-header -->
    <br/>

    <div class="alert alert-success message_box">

    </div>
    <br/>
    <div class="content-box-content">
        <div  class="update_pro_area_by_admin">
            <div class="col-sm-12">
                <div  class="update_user_basic_info_area">
                    <div id="block1" class="profile_info">
                        <h3>Basic Info Update</h3>
                        <form autocomplete="off"  action="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'update_user_profile_by_admin')); ?>" id="updateProfile" >
                            <div class="profile_pic">
                                <?php
                                if ($user['User']['profile_pic']) {
                                    ?>
                                    <img class="profile_image" alt="" src="uploaded_files/profile_pic/<?php echo $user['User']['id'] ?>/<?php echo $user['User']['profile_pic'] ?>"/>
                                <?php } else { ?>
                                    <img class="profile_image" alt="" src="img/default_pro_pic.png"/>
                                <?php } ?>
                                <div class="profile_pic_upload">
                                    <input type="file" name="profile_pic" id="profile_pic" />
                                    <label for="profile_pic">
                                        <img alt="" src="img/pencil_icon.png" />
                                    </label>
                                </div>

                            </div>
                            <div class="error_msg imageError"></div>
                            <div class="clear"></div>
                            <input type="hidden" name="user_id" value="<?php echo $user['User']['id']; ?>" />

                            <div class="col-sm-6">
                                <label>Name</label>
                                <input type="text" name="name" value="<?php echo $user['User']['name']; ?>" class="form-control" />
                                <div class="error_msg nameError"></div>
                                <br/>
                                <br/>
                            </div>
                            <div class="col-sm-6">
                                <label>Email</label>
                                <input type="text" value="<?php echo $user['User']['email']; ?>" name="email" class="form-control" />
                                <div class="error_msg emailError"></div>
                                <br/>
                                <br/>
                            </div>
                            <div class="clear"></div>
                            <div class="col-sm-6">
                                <label>Phone</label>
                                <input type="text" value="<?php echo $user['User']['phone']; ?>" name="phone" class="form-control" />
                                <br/>
                                <br/>
                            </div>
                            <div class="col-sm-6">
                                <label>Address</label>
                                <input type="text" value="<?php echo $user['User']['address']; ?>" name="address" class="form-control" />
                                <br/>
                                <br/>
                            </div>
                            <div class="clear"></div>
                            <div class="col-sm-6">
                                <label>Country</label>

                                <select name="country" class="form-control" >
                                    <option value="">Select Country</option>
                                    <?php
                                    foreach ($countries as $key => $val) {
                                        ?>
                                        <option <?php echo ($val['Country']['nicename'] == $user['User']['country'] ? 'selected' : ''); ?> value="<?php echo $val['Country']['nicename']; ?>">
                                            <?php echo $val['Country']['nicename']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <br/>
                                <br/>
                            </div>
                            <div class="col-sm-6">
                                <label>State</label>
                                <select name="state" class="form-control" >
                                    <option value="">Select State</option>
                                    <?php
                                    foreach ($state_lists as $key => $val) {
                                        ?>
                                        <option <?php echo ($val == $user['User']['state'] ? 'selected' : ''); ?> value="<?php echo $val; ?>">
                                            <?php echo $val; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <br/>
                                <br/>
                            </div>
                            <div class="clear"></div>
                            <div class="col-sm-6">
                                <label>City</label>
                                <input type="text" value="<?php echo $user['User']['city']; ?>" name="city" class="form-control" />
                                <br/>
                                <br/>
                            </div>
                            <div class="col-sm-6">
                                <label>Zip code</label>
                                <input type="text" value="<?php echo $user['User']['zip_code']; ?>" name="zip_code" class="form-control" />
                                <br/>
                                <br/>
                            </div>
                            <div class="clear"></div>
                            <br/>
                            <input type="submit" class="btn btn-success" value="Update Info" />
                            <a href="javascript:" onclick="resetPassword(<?php echo $pro_user['ProUser']['id']; ?>)" class="btn btn-success">
                                Reset Password
                            </a>

                        </form>


                    </div>
                    <script type="text/javascript">
                        $(function() {

                            $('.profile_info').on('click', '.profile_update_btn', function() {
                                var parent = $(this).closest('.profile_info');
                                parent.find('.text').hide();
                                parent.find('.user_name,.user_email,.user_phone').find('input').show();
                                parent.find('.profile_pic_upload').show();
                            });

                            var options = {
                                type: 'post',
                                dataType: 'json',
                                beforeSubmit: function(arr, $form, options) {
                                    $('.error_msg').hide();
                                },
                                success: responsefunc
                            };

                            $('#updateProfile').submit(function() {
                                $(this).ajaxSubmit(options);
                                //$(".addpictureblock").modal('hide');
                                return false;
                            });


                            $('.profile_info').on('change', '#profile_pic', function() {
                                var that = $(this);
                                $('.imageError').hide();
                                var file_extension_array = ["jpg", "png", "gif"];
                                var file_extension = that.val().split('.').pop();
                                if ($.inArray(file_extension.toLowerCase(), file_extension_array) !== -1) {
                                    readURL(this);
                                } else {
                                    $('.imageError').html('jpg, png or gif file are allowed!!').show();
                                }

                            });

                        });

                        function responsefunc(responseText, statusText, xhr, $form) {
                            //        alert_r(responseText);
                            if (responseText.type == 'error') {
                                $('.emailError').html(responseText.error.emailError).show();
                                $('.imageError').html(responseText.error.imageError).show();
                                $('.nameError').html(responseText.error.nameError).show();
                            } else {
                                $('.message_box').html('Successfully Updated').show();
                                setTimeout(function() {
                                    $('.message_box').fadeOut();
                                }, 3000);

                            }
                        }

                        function readURL(input) {
                            if (input.files && input.files[0]) {

                                var reader = new FileReader();
                                reader.onload = function(e) {

                                    $('.profile_image').attr('src', e.target.result);
                                }
                                reader.readAsDataURL(input.files[0]);
                            }
                        }
                        function readAward(input, str) {
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();
                                reader.onload = function(e) {
                                    $('[name=temp_award_image]').val(e.target.result);
                                }

                                reader.readAsDataURL(input.files[0]);
                            }
                        }
                    </script>
                </div>
            </div>






            <div class="clear"></div>
        </div>

    </div> <!-- End .content-box-content -->
    <div class="alert alert-success message_box">

    </div>
    <br/>

</div> <!-- End .content-box -->

<script type="text/javascript">
    function resetPassword(id) {
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'reset_pass_by_admin')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function(data) {
            if (data == 'success') {
                $('.message_box').html('Password successfully changed.').show();
                setTimeout(function() {
                    $('.message_box').fadeOut();
                }, 3000);
            }
        }, 'text');
    }
    function deleteClaimList(id) {
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'delete_claim_list')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function(data) {
            if (data == 'success') {
                $('.message_box').html('Claim successfully deleted.').show();
                setTimeout(function() {
                    $('.message_box').fadeOut();
                }, 3000);
                $('.deleteClaimList').remove();
            }
        }, 'text');
    }
</script>
