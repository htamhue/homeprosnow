<div class="start_a_projects">

    <div class="get_quotes_area">
        <div class="get_quotes_heading">
            Submint a quote & get hired!
        </div>
        <div class="get_quotes_details">
            <?php echo $this->Element('quote_left_menu'); ?>
            <form autocomplete="off" action="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'submit_a_quote_action')); ?>" id="SubmitAQuote" >

                <input type="hidden" name="project_id" value="<?php echo $project['Project']['id']; ?>" />
                <input type="hidden" name="user_id" value="<?php echo $project['Project']['user_id']; ?>" />
                <div class="col-md-9 project_form">
                    <div class="project_details_heading">
                        Submit a Quote
                    </div>

                    <div class="input_list">
                        <div class="input_text">
                            Cost estimate?
                        </div>
                        <div class="budget_input">
                            <input type="text" name="amount" placeholder="" class="company_name" />
                            <div class="doller_icon">
                                $
                            </div>
                        </div>
                        <div class="amountError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            How soon can you start
                        </div>
                        <input type="text" name="start_date" class="company_name" />
                        <div class="startDateError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            How long would you take to complete this project?
                        </div>
                        <input type="text" name="duration" class="company_name" />
                        <div class="durationError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Have you worked on similar project before?
                        </div>
                        <input type="text" name="worked_on_similar_project" class="company_name" />
                        <div class="workedSimilarError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Do you have any question about the project?
                        </div>
                        <textarea name="question_about_project" class="company_description"></textarea>
                    </div>


                    <div class="file_upload_area">
                        <div class="upload_file_info">
                            Add images of past work similar to this if any <span>(15mb max file size)</span> 
                        </div>
                        <div class="view_file">

                        </div>
                        <div class="clear"></div>
                        <div class="single_file_upload">
                            <input type="file" name="past_work_pic[]" id="project_attachment_1" class="project_attachment" />
                            <label for="project_attachment_1" class="common_button project_attachment_btn">
                                <img alt="" src="img/attachment_icon.png" />
                                Add Attachment
                            </label>
                        </div>

                    </div>

                    <div class="term_condition_area">
                        <input type="checkbox" name="terms_conditions" value="1" id="term_condition_checkbox"/>
                        <label class="term_condition_label" for="term_condition_checkbox"></label>
                        <span class="term_condition">
                            I accept the Homeprosnow hiring terms and conditions
                        </span>
                        <div class="clear"></div>
                        <div class="termsConditionsError error_msg"></div>
                    </div>

                    <a href="javascript:" class="submit_request common_button">
                        Send Quote
                    </a>

                </div>
                <input type="submit" value="submit"  style="display: none;"/>
            </form>

            <div class="clear"></div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(function() {
        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd"
        });
        $('#successModal').on('hidden.bs.modal', function() {
            window.location.href = "<?php echo $this->Html->Url(array('controller' => 'home')); ?>";
        })

        $('.project_form').on('change', '.project_attachment', function() {
            $('.error_msg').remove();
            var that = $(this);
            var str = that.attr('id');
            var res = str.substring(19);
            var countVal = parseInt(res) + 1;
            var file_extension_array = ["jpg", "png", "gif"];
            var file_extension = that.val().split('.').pop();
            if ($.inArray(file_extension.toLowerCase(), file_extension_array) !== -1) {
                readURL(this, str);
                $("[for=" + str + "]").remove();
                var fileHtml = '<input type="file" name="past_work_pic[]" id="project_attachment_' + countVal + '" class="project_attachment" />';
                fileHtml += '<label for="project_attachment_' + countVal + '" class="common_button project_attachment_btn">';
                fileHtml += '<img alt="" src="img/attachment_icon.png" />';
                fileHtml += ' Add Attachment';
                fileHtml += ' </label>';
                $('.single_file_upload').append(fileHtml);
            } else {
                $('.single_file_upload').prepend('<span class="error_msg">jpg, png or gif file are allowed!!</span>');
            }



        });

        $('.project_form').on('click', '.delete_img', function() {
            var removeFile = $(this).attr('rel');
            $("#" + removeFile).remove();
            $(this).closest('.single_file').remove();
        });






        $('.submit_request').click(function() {
            $('#SubmitAQuote').submit();
        });
        var options = {
            target: 'pro/submit_a_quote_action', // target element(s) to be updated with server response 
            beforeSubmit: showRequest, // pre-submit callback 
            success: showResponse, // post-submit callback 

            // other available options: 
            //url:       url         // override for form's 'action' attribute 
            type: 'post', // 'get' or 'post', override for form's 'method' attribute 
            dataType: 'json'        // 'xml', 'script', or 'json' (expected server response type) 
                    //clearForm: true        // clear all form fields after successful submit 
                    //resetForm: true        // reset the form after successful submit 

                    // $.ajax options can be used here too, for example: 
                    //timeout:   3000 
        };
        // bind to the form's submit event 
        $('#SubmitAQuote').submit(function() {
            // inside event callbacks 'this' is the DOM element so we first 
            // wrap it in a jQuery object and then invoke ajaxSubmit 
            $(this).ajaxSubmit(options);
            // !!! Important !!! 
            // always return false to prevent standard browser submit and page navigation 
            return false;
        });
    });
    // pre-submit callback 
    function showRequest(formData, jqForm, options) {
        $('.project_form').find('.error_msg').hide();

        return true;
    }

// post-submit callback 
    function showResponse(responseText, statusText, xhr, $form) {

        if (responseText.type == 'error') {
//            alert_r(responseText);
            if (responseText.errorMsg.amountError) {
                $('.project_form').find('.amountError').html(responseText.errorMsg.amountError).show();
            }
            if (responseText.errorMsg.startDateError) {
                $('.project_form').find('.startDateError').html(responseText.errorMsg.startDateError).show();
            }
            if (responseText.errorMsg.durationError) {
                $('.project_form').find('.durationError').html(responseText.errorMsg.durationError).show();
            }
            if (responseText.errorMsg.termsConditionsError) {
                $('.project_form').find('.termsConditionsError').html(responseText.errorMsg.termsConditionsError).show();
            }


        } else if (responseText.type == 'success') {
            $('#successModal').modal();
            $('#successModal').find('.details').html('Your quote has been sent to the client. You should receive a notificaton from the client soon.')
        }

    }

    function readURL(input, str) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                var picHtml = '<div class="single_file"><img alt="" width="50" src="' + e.target.result + '" /> '
                picHtml += '<a href="javascript:" rel="' + str + '" class="delete_img"><img alt="" src="img/cross_icon.png" /></a> </div>'
                $('.view_file').append(picHtml);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>