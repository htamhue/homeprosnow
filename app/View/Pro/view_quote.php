<?php
//    $project_id = $project_quote['Project']['id'];
$project_quote_id = $project_quote['ProjectQuote']['id']
?>

<div class="dashboad_content">
    <div class="view_project">
        <div class="col-md-9 porject_details_area view_quoute_area">
            <div class="company_details_area">
                <div class="col-sm-2 col-md-2">
                    <div class="company_logo">
                        <?php if ($project_quote['User']['profile_pic']) { ?>
                            <img alt="" src="uploaded_files/profile_pic/<?php echo $project_quote['User']['id']; ?>/<?php echo $project_quote['User']['profile_pic']; ?>" />
                        <?php } else { ?>
                            <img alt="" src="img/company_logo.jpg" />
                        <?php } ?>
                    </div>
                </div>

                <div class="company_details col-sm-4 col-md-8">
                    <div class="company_name">
                        <?php echo $project_quote['User']['ProUser'][0]['company_name']; ?>
                    </div>
                    <div class="clear"></div>
                    <div class="company_moto">
                        &nbsp;
                    </div>
                    <div class="company_basic_info">
                        <ul>
                            <li>
                                <img alt="" src="img/location_icon.png" />
                                <span>
                                    <?php echo $project_quote['User']['ProUser'][0]['city']; ?>
                                </span>
                            </li>
                            <li>
                                <img alt="" src="img/verified_icon.png" />
                                <span>
                                    <?php if ($project_quote['project_count'] > 1) { ?>
                                        <?php echo $project_quote['project_count']; ?> Projects
                                    <?php } else { ?>
                                        <?php echo $project_quote['project_count']; ?> Project
                                    <?php } ?>
                                </span>
                            </li>
                            <?php
                            if ($project_quote['reviewCount']['Hire']['reviewCount']) {
                                ?>
                                <li class="user_review_area">
                                    <?php
                                    $reviewPercent = ($project_quote['rating_sum']['Hire']['proRating'] / $project_quote['reviewCount']['Hire']['reviewCount']) * 20;
                                    ?>
                                    <div class="rating_area">
                                        <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                    </div>
                                    <div class="review_text">
                                        <?php echo $project_quote['reviewCount']['Hire']['reviewCount'] ?> Reviews
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>

                </div>

                <div class="verified_pro col-sm-2 col-md-2">
                    <?php
                    if ($project_quote['User']['ProUser'][0]['is_verified']) {
                        ?>
                        <img alt="" src="img/pro_verified_icon.png"/>
                        <?php
                    }
                    ?>

                </div>
                <div class="clear"></div>
            </div>
            <div class="project_details">
                <div class="project_details_heading">
                    HOW SOON CAN YOU START?
                </div>
                <div class="project_details_text">
                    <?php echo $project_quote['ProjectQuote']['start_date']; ?>
                </div>

                <div class="project_details_heading">
                    HAVE YOU WORKED ON SIMILAR PROJECTS BEFORE?
                </div>
                <div class="project_details_text">
                    <?php echo $project_quote['ProjectQuote']['worked_on_similar_project']; ?>
                </div>
                <div class="project_details_heading">
                    DO YOU HAVE QUESTIONS ABOUT THE PROJECT?
                </div>
                <div class="project_details_text">
                    <?php echo $project_quote['ProjectQuote']['question_about_project']; ?>
                </div>
                <div class="project_details_heading">
                    PHOTOS OF PAST WORK SIMILAR TO THIS
                </div>
                <div class="project_details_photo">
                    <?php
                    foreach ($project_quote['ProjectQuoteFile'] as $ProjectQuoteFile) {
                        ?>
                        <img alt="" src="<?php echo $ProjectQuoteFile['file'] ?>" width="100"/>
                    <?php } ?>

                </div>
            </div>

            <?php
            if ($project_quote['reviewCount']['Hire']['reviewCount']) {
                ?>
                <div class="reviews_area">
                    <div class="reviews_heading">
                        <?php
                        $reviewPercent = ($project_quote['rating_sum']['Hire']['proRating'] / $project_quote['reviewCount']['Hire']['reviewCount']) * 20;
                        ?>
                        <div class="rating_area">
                            <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                        </div>
                        <div class="review_text">
                            <?php echo $project_quote['reviewCount']['Hire']['reviewCount'] ?> Reviews
                        </div>
                    </div>
                    <div class="reviews_list_area">
                        <ul>
                            <?php
                            foreach ($project_quote['reviews'] as $review) {
                                ?>
                                <li>
                                    <div class="username_rating">
                                        <div class="username"><?php echo $review['User']['name']; ?></div>
                                        <div class="rating">
                                            <div class="rating_area">
                                                <div class="rated_area" style="width: <?php echo 20 * $review['Hire']['pro_review']; ?>%"></div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="review_date">
                                        <?php echo $review['Hire']['pro_review_date']; ?>
                                    </div>
                                    <div class="review_text">
                                        <?php echo $review['Hire']['pro_review_comment']; ?>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!--                <a href="javascript:" class="view_more_btn">
                                        View More
                                    </a>-->
                </div>
                <?php
            }
            ?>

        </div>
        <div class="col-md-3 user_details_area">

            <div class="user_project_hire">
                <div class="project_count">
                    COST ESTIMATE
                    <br/>
                    $<?php echo $project_quote['ProjectQuote']['amount']; ?>
                </div>
                <div class="hire_count">
                    DURATION
                    <br/>
                    <?php echo $project_quote['ProjectQuote']['duration']; ?>
                </div>
                <div class="clear"></div>
                <div class="button_area">
                    <?php
                    if (empty($hire) && $this->Session->read('Auth.User.user_type') == 'user') {
                        ?>
                        <a href="javascript:" class="accept_btn selected">
                            Accept
                        </a>
                        <?php
                    }
                    ?>
                    <?php
                    if (($hire['Hire']['payment_status'] == 'requested' || $hire['Hire']['payment_status'] == 'paid') && $this->Session->read('Auth.User.id') == $hire['Hire']['user_id']) {
                        $payment_btn = '';
                        $payment_text = 'Paid';
                        if ($hire['Hire']['payment_status'] == 'requested') {
                            $payment_btn = 'payment';
                            $payment_text = 'Pay';
                        }
                        ?>
                        <a href="javascript:" hire_id="<?php echo $hire['Hire']['id']; ?>" class="selected <?php echo $payment_btn; ?> payment_btn">
                            <?php echo $payment_text; ?>
                        </a>
                        <?php
                    }
                    ?>

                    <a href="<?php echo $this->Html->Url(array('controller' => 'messages', 'action' => 'index', 'project_quote_id' => $project_quote_id)); ?>" class="message_btn">
                        Message
                    </a>
                    <?php
                    if (empty($hire) && $this->Session->read('Auth.User.user_type') == 'user') {
                        ?>
                        <a href="javascript:" class="decline_btn">
                            Decline
                        </a>
                        <?php
                    }
                    ?>

                </div>
            </div>

            <div class="license_area">
                <div class="license_heading">
                    LICENSE
                </div>
                <div class="license_details">
                    <?php
                    foreach ($project_quote['User']['ProUser'][0]['CompanyLicense'] as $CompanyLicense) {
                        ?>
                        <div class="single_license">
                            <span class="license_num">
                                LICENSE NO: <?php echo $CompanyLicense['license_number'] ?>
                            </span>
                            <br/>
                            <span>
                                <?php echo $CompanyLicense['license_type'] ?>
                            </span>
                            <br/>
                            <span>
                                Active until: <?php echo $CompanyLicense['expir_date'] ?>
                            </span>     
                        </div>
                        <?php
                    }
                    ?>




                </div>
            </div>


        </div>
        <div class="clear"></div>
    </div>

</div>

<script type="text/javascript">
    $(function() {
        $('.button_area').on('click', '.accept_btn', function() {
            $.ajax({
                url: "pro/hire",
                type: 'POST',
                async: false,
                data: {project_quote_id: <?php echo $project_quote_id; ?>},
                success: function(data) {
                    if (data == 'success') {
                        $('.button_area').find('.accept_btn').remove();
                        $('.button_area').find('.decline_btn').remove();
                    }

                }
            });
        });
        $('.button_area').on('click', '.decline_btn', function() {
            $.ajax({
                url: "pro/quote_declined",
                type: 'POST',
                async: false,
                data: {project_quote_id: <?php echo $project_quote_id; ?>},
                success: function(data) {
                    if (data == 'success') {
                        $('.button_area').find('.accept_btn').remove();
                        $('.button_area').find('.decline_btn').remove();
                    }

                }
            });
        });
        $('.button_area').on('click', '.payment', function() {
            var that = $(this);
            var hire_id = that.attr('hire_id');
//            $('#prePayment').modal();
            if (hire_id == 'undefined') {
                return;
            }
            $.ajax({
                url: "payment/pre_payment",
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {hire_id: hire_id},
                success: function(data) {
                    $('#prePayment').find('.project_name').html(data.project_name);
                    $('#prePayment').find('.contractor_name').html(data.contractor_name);
                    $('#prePayment').find('.quote_amount').html('$' + data.quote_amount);
                    $('#prePayment').find('.payemnt_confirm_btn').attr('hire_id', data.hire_id);
                    $('#prePayment').modal();
//                    alert_r(data);
//                    if (data == 'success') {
//                        $('.button_area').find('.accept_btn').remove();
//                        $('.button_area').find('.decline_btn').remove();
//                    }

                }
            });
        });

        $('#prePayment').on('click', '.payemnt_confirm_btn', function() {
            var that = $(this);
            var hire_id = that.attr('hire_id');
            $.ajax({
                url: "payment/payment_confirm",
                type: 'POST',
                async: false,
                data: {hire_id: hire_id},
                success: function(data) {

                    if (data == 'success') {
                        $('#prePayment').modal('hide');
                        $('.payment_btn').html('Paid').removeClass('payment');
                    }

                }
            });
        });

        $('.payment_cancel').click(function() {
            $('#prePayment').modal('hide');
        });
    });
</script>