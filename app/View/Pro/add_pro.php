<div class="start_a_projects">

    <div class="get_quotes_area">
        <div class="get_quotes_heading">
            Get more leads for your business from our community
        </div>
        <div class="get_quotes_details">
            <?php echo $this->Element('pro_left_menu'); ?>
            <form autocomplete="off" action="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro_action')); ?>" id="addPro" >
                <div class="col-md-9 project_form">
                    <div class="project_details_heading">
                        Become a pro
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Choose your line of work
                        </div>
                        <select name="category" class="select_category">
                            <option value="0">Select Category</option>
                            <?php
                            foreach ($Categories as $Categorie) {
                                ?>
                                <option value="<?php echo $Categorie['Categorie']['id']; ?>">
                                    <?php echo $Categorie['Categorie']['category']; ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                        <div class="categoryError error_msg"></div>
                    </div>

                    <div class="input_list sub_category_area">
                        <div class="input_text">
                            Sub-category
                        </div>
                        <div class="single_sub_category_area">

                        </div>

                    </div>
                    <div class="input_list row">
                        <div class="col-sm-8">
                            <div class="input_text">
                                What is the name of your company?
                            </div>
                            <input type="text" name="company_name" class="company_name" />
                            <div class="companyNameError error_msg"></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input_text">
                                Operational Since
                            </div>
                            <input name="operational_since" type="text" class=" operational_since" placeholder="YYYY"/>
                            <!--<input type="text" name="operational_since" class="operational_since" />-->
                            <div class="operationalSinceError error_msg"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Describe your company briefly
                        </div>
                        <textarea name="company_description" class="company_description"></textarea>
                    </div>
                    <div class="input_list row">
                        <div class="col-sm-6">
                            <div class="input_text">
                                Business phone number
                            </div>
                            <input type="text" name="business_phone_number" class="business_phone_number" />
                            <div class="businessPhoneNumberError error_msg"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input_text">
                                Website URL
                            </div>
                            <input type="text" name="website" class="website" />
                            <div class="websiteError error_msg"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="input_list row service_area_container">
                        <div class="col-sm-10">
                            <div class="input_text">
                                Service Areas
                            </div>
                            <input type="text" name="service_area" class="service_area" id="service_area" />
                            <div class="serviceAreaError error_msg"></div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input_text">
                                &nbsp;
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-sm-12">
                            <div class="item_list_container service_area_listing_container">
                            </div>
                        </div>
                    </div>

                    <div class="input_list row awards_achievements_container">
                        <div class="col-sm-10">
                            <div class="input_text">
                                Awards & Achievements
                            </div>
                            <input type="text" name="awards_achievements" placeholder="Award Title" class="awards_achievements" />
                            <input type="hidden" name="temp_award_image" value="" />
                            <div class="single_file_upload awards_upload">
                                <input type="file" name="awards_achievements_image[]" id="awards_image_1" class="awards_attachment" />
                                <label for="awards_image_1" class="common_button project_attachment_btn">
                                    <img alt="" src="img/attachment_icon.png" />
                                    Add Attachment
                                </label>
                            </div>
                            <div class="serviceOfferError error_msg"></div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input_text">
                                &nbsp;
                            </div>
                            <a href="javascript:" class="add_another_awards_achievements">
                                + 
                            </a>
                        </div>
                        <div class="clear"></div>
                        <div class="col-sm-12">
                            <div class="item_list_container awards_achievements_listing_container">
                            </div>
                        </div>
                    </div>




                    <div class="input_list row">
                        <div class="col-sm-5">
                            <div class="input_text">
                                Cover Photo
                            </div>
                            <div class="">


                                <div class="image_upload">
                                    <input type="file" name="cover_photo" id="cover_photo" class="image_upload_button">
                                    <label for="cover_photo" class="common_button image_upload_button_label">
                                        <img alt="" src="img/attachment_icon.png">
                                        Add Attachment
                                    </label>
                                </div>

                            </div>
                            <div class="coverPhotoError error_msg"></div>
                        </div>
                        <div class="col-sm-5">
                            <div class="input_text">
                                Busniess Logo
                            </div>
                            <div class="">
                                
                                <div class="image_upload">
                                    <input type="file" name="business_logo" id="business_logo" class="image_upload_button">
                                    <label for="business_logo" class="common_button image_upload_button_label">
                                        <img alt="" src="img/attachment_icon.png">
                                        Add Attachment
                                    </label>
                                </div>
                            </div>
                            <div class="busniessLogoError error_msg"></div>
                        </div>
                    </div>


                    <div class="input_list row brand_name_container">
                        <div class="col-sm-10">
                            <div class="input_text">
                                Brand Name
                            </div>
                            <input type="text" name="brand_name" class="brand_name" />
                            <div class="BrandNameError error_msg"></div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input_text">
                                &nbsp;
                            </div>
                            <a href="javascript:" class="add_another_brand_name">
                                + 
                            </a>
                        </div>
                        <div class="clear"></div>
                        <div class="col-sm-12">
                            <div class="item_list_container brand_name_listing_container">
                            </div>
                        </div>
                    </div>



                    <div class="input_list operating_hours_container">
                        <div class="input_text">
                            Operating Hours
                        </div>
                        <div class="row single_operating_hours">
                            <div class="col-sm-2">
                                Monday
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="start" name="mon_start" value="9am" />
                            </div>
                            <div class="col-sm-1 separation">
                                -
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="end" name="mon_end" value="7pm"/>
                            </div>
                            <div class="col-sm-2">
                                <input type="checkbox" class="UnavailableCheck" /> Unavailable
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row single_operating_hours">
                            <div class="col-sm-2">
                                Tuesday
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="start" name="tue_start" value="9am" />
                            </div>
                            <div class="col-sm-1 separation">
                                -
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="end" name="tue_end" value="7pm"/>
                            </div>
                            <div class="col-sm-2">
                                <input type="checkbox" class="UnavailableCheck" /> Unavailable
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row single_operating_hours">
                            <div class="col-sm-2">
                                Wednesday
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="start" name="wed_start" value="9am" />
                            </div>
                            <div class="col-sm-1 separation">
                                -
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="end" name="wed_end" value="7pm"/>
                            </div>
                            <div class="col-sm-2">
                                <input type="checkbox" class="UnavailableCheck" /> Unavailable
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row single_operating_hours">
                            <div class="col-sm-2">
                                Thursday
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="start" name="thu_start" value="9am" />
                            </div>
                            <div class="col-sm-1 separation">
                                -
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="end" name="thu_end" value="7pm"/>
                            </div>
                            <div class="col-sm-2">
                                <input type="checkbox" class="UnavailableCheck" /> Unavailable
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row single_operating_hours">
                            <div class="col-sm-2">
                                Friday
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="start" name="fri_start" value="9am" />
                            </div>
                            <div class="col-sm-1 separation">
                                -
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="end" name="fri_end" value="7pm"/>
                            </div>
                            <div class="col-sm-2">
                                <input type="checkbox" class="UnavailableCheck" /> Unavailable
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row single_operating_hours">
                            <div class="col-sm-2">
                                Saturday
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="start" name="sat_start" value="9am" />
                            </div>
                            <div class="col-sm-1 separation">
                                -
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="end" name="sat_end" value="7pm"/>
                            </div>
                            <div class="col-sm-2">
                                <input type="checkbox" class="UnavailableCheck" /> Unavailable
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row single_operating_hours">
                            <div class="col-sm-2">
                                Sunday
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="start" name="sun_start" value="9am" />
                            </div>
                            <div class="col-sm-1 separation">
                                -
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="end" name="sun_end" value="7pm"/>
                            </div>
                            <div class="col-sm-2">
                                <input type="checkbox" class="UnavailableCheck" /> Unavailable
                            </div>
                            <div class="clear"></div>
                        </div>

                    </div>


                    <div class="company_license_area">
                        <div class="license_list ">
                            <div class="license_part col-md-4">
                                <div class="input_list">
                                    <div class="license_heading">
                                        Company license number
                                    </div>
                                    <div class="license_input">
                                        <input name="license_number[]" type="text" />
                                    </div>
                                    <div class="licenseNumberError error_msg"></div>
                                </div>
                            </div>
                            <div class="license_part col-md-4">
                                <div class="input_list">
                                    <div class="license_heading">
                                        License type
                                    </div>
                                    <div class="license_input">
                                        <input name="license_type[]" type="text" />
                                    </div>
                                    <div class="licenseTypeError error_msg"></div>
                                </div>
                            </div>
                            <div class="license_part col-md-4">
                                <div class="input_list">
                                    <div class="license_heading">
                                        Active until
                                    </div>
                                    <div class="license_input">
                                        <input name="expir_date[]" type="text" class="datepicker" placeholder="DD/MM/YYYY"/>
                                    </div>
                                    <div class="expirDateError error_msg"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <a href="javascript:" class="add_another_license">
                        + ADD ANOTHER LICENSE
                    </a>
                    <div class="file_upload_area">
                        <div class="upload_file_info">
                            Add photos of past work <span>(15mb max file size)</span> 
                        </div>
                        <div class="view_file">

                        </div>
                        <div class="clear"></div>
                        <div class="single_file_upload sfu">
                            <input type="file" name="past_work_pic[]" id="project_attachment_1" class="project_attachment" />
                            <label for="project_attachment_1" class="common_button project_attachment_btn">
                                <img alt="" src="img/attachment_icon.png" />
                                Add Attachment
                            </label>
                        </div>
                    </div>


                    <div class="input_list social_media_container">
                        <div class="input_text">
                            Social Media Pages (Optional)
                        </div>
                        <div class="row social_item">
                            <div class="col-sm-1">
                                <img alt="" width="40" src="img/facebook_icon.png" />
                            </div>
                            <div class="col-sm-10">
                                <input type="text" name="facebook_link" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row social_item">
                            <div class="col-sm-1">
                                <img alt="" width="40" src="img/twitter_icon.png" />
                            </div>
                            <div class="col-sm-10">
                                <input type="text" name="twitter_link" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row social_item">
                            <div class="col-sm-1">
                                <img alt="" width="40" src="img/linkedin_icon.png" />
                            </div>
                            <div class="col-sm-10">
                                <input type="text" name="linkedin_link" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row social_item">
                            <div class="col-sm-1">
                                <img alt="" width="40" src="img/google_icon.png" />
                            </div>
                            <div class="col-sm-10">
                                <input type="text" name="google_link" />
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row social_item">
                            <div class="col-sm-1">
                                <img alt="" width="40" src="img/youtube_icon.png" />
                            </div>
                            <div class="col-sm-10">
                                <input type="text" name="youtube_link" />
                            </div>
                            <div class="clear"></div>
                        </div>

                    </div>


                    <div class="project_details_heading">
                        Contact Details
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Your full name
                        </div>
                        <input type="text" value="<?php echo $user['User']['name'] ?>" <?php if ($user['User']['name']) { ?> disabled <?php } ?> name="name" class="project_user_name" />
                        <?php if ($user['User']['name']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['name'] ?>" name="name" class="project_user_name" />
                        <?php } ?>

                        <div class="nameError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Email Address
                        </div>
                        <input type="text" value="<?php echo $user['User']['email'] ?>" <?php if ($user['User']['email']) { ?> disabled <?php } ?> name="email" class="project_user_email" />
                        <?php if ($user['User']['email']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['email'] ?>" name="email" class="project_user_email" /> 
                        <?php } ?>

                        <div class="emailError error_msg"></div>
                    </div>
                    <?php if (empty($user)) { ?>
                        <div class="input_list">
                            <div class="input_text">
                                Password
                            </div>
                            <input type="password" name="password" class="project_user_phone" />
                            <div class="passwordError error_msg"></div>
                        </div>
                    <?php } ?>
                    <div class="input_list">
                        <div class="input_text">
                            Phone Number
                        </div>
                        <input type="text" value="<?php echo $user['User']['phone'] ?>" <?php if ($user['User']['phone']) { ?> disabled <?php } ?> name="phone" class="project_user_phone" />
                        <?php if ($user['User']['phone']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['phone'] ?>" name="phone" class="project_user_phone" />
                        <?php } ?>
                        <div class="phoneError error_msg"></div>
                    </div>

                    <div class="input_text">
                        Address
                    </div>
                    <div class="address_area">
                        <div class="input_list street_address">
                            <input type="text" name="address" class="street_address" placeholder="Street address" />
                            <div class="addressError error_msg"></div>
                        </div>
                        <div class="clear"></div>
                        <div class="input_list">
                            <select name="state" class="state">
                                <option value="0"> Select state</option>
                                <?php
                                foreach ($state_lists as $key => $state_list) {
                                    ?>
                                    <option value="<?php echo $key ?>"> <?php echo $state_list; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <div class="stateError error_msg"></div>
                        </div>
                        <div class="input_list">
                            <input name="city" type="text" class="city" placeholder="City" />
                            <div class="cityError error_msg"></div>
                        </div>
                        <div class="input_list">
                            <input name="zip_code" type="text" class="zip_code " placeholder="Zip code" />
                            <div class="zipCodeError error_msg"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="term_condition_area">
                        <input type="checkbox" name="terms_conditions" value="1" id="term_condition_checkbox"/>
                        <label class="term_condition_label" for="term_condition_checkbox"></label>
                        <span class="term_condition">
                            I accept the Homeprosnow hiring terms and conditions
                        </span>
                        <div class="clear"></div>
                        <div class="termsConditionsError error_msg"></div>
                    </div>
                    <div class="progress">
                        <div class="bar"></div>
                        <div class="percent">0%</div>
                    </div>
                    <!--                    <a href="javascript:" class="submit_request common_button">
                                            Submit Request
                                        </a>-->
                    <button type="submit" class="btn submit_request common_button">
                        Submit Request
                    </button>

                </div>
                <input type="submit" value="submit"  style="display: none;"/>
            </form>

            <div class="another_license_copy">
                <div class="license_list ">
                    <div class="license_part col-md-4">

                        <div class="license_input">
                            <input name="license_number[]" type="text" />
                        </div>
                    </div>
                    <div class="license_part col-md-4">

                        <div class="license_input">
                            <input name="license_type[]" type="text" />
                        </div>
                    </div>
                    <div class="license_part col-md-4">

                        <div class="license_input date_input">
                            <input  name="expir_date[]"type="text"  placeholder="DD/MM/YYYY"/>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <a href="javascript:" class="remove_license_list">
                        <img alt="" src="img/cross_icon.png"/>
                    </a>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

</div>

<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo Configure::read('settings.gmap_api_key'); ?>&libraries=places"
></script>

<script type="text/javascript">
    $(function () {
        $(".operational_since").datepicker({
            minView: 2,
            dateFormat: "yy-mm-dd",
            changeYear: true,
        });

        $('.operating_hours_container').on('click', '.UnavailableCheck', function () {
            var parent = $(this).closest('.single_operating_hours');
            if ($(this).prop('checked') == true) {
                parent.find('.start').val('').attr('disabled', 'disabled');
                parent.find('.end').val('').attr('disabled', 'disabled');
            } else {
                parent.find('.start').val('9am').removeAttr('disabled');
                parent.find('.end').val('7pm').removeAttr('disabled');
            }
        });

        //Google map autocomplete
        var input = document.getElementById('service_area');
        var searchBox = new google.maps.places.SearchBox(input);
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            var latitude = places[0].geometry.location.lat();
            var longitude = places[0].geometry.location.lng();
            var serviceAreaText = $('#service_area').val();
            var html = '<div class="single_service_area_listing">';
            html += '<span class="service_area_text">';
            html += serviceAreaText;
            html += '</span> <span class="service_area_removed"> <i class="fa fa-times-circle "></i></span>';
            html += '<input type="hidden" name="service_area_item[]" value="' + serviceAreaText + '" />';
            html += '<input type="hidden" name="service_area_lat[]" value="' + latitude + '" />';
            html += '<input type="hidden" name="service_area_lng[]" value="' + longitude + '" />';
            html += '</div>';
            $('.service_area_listing_container').prepend(html);
            $('#service_area').val('');
        });

        $('.service_area_listing_container').on('click', '.service_area_removed', function () {
            $(this).closest('.single_service_area_listing').remove();
        });
        //added service area functionality  END Here.

        //added service offer functionality  START Here.
        $('.service_offer_container').on('click', '.add_another_service_offer', function () {
            var parent = $(this).closest('.service_offer_container');
            var serviceOfferText = parent.find('.service_offer').val();
            if (serviceOfferText.length == 0) {
                parent.find('.serviceOfferError').html('Please insert service offer');
            } else {
                var html = '<div class="single_service_offer_listing">';
                html += '<span class="service_area_text">';
                html += serviceOfferText;
                html += '</span> <span class="service_offer_removed"> <i class="fa fa-times-circle "></i></span>';
                html += '<input type="hidden" name="service_offer_item[]" value="' + serviceOfferText + '" /> </div>';
                $('.service_offer_listing_container').prepend(html);
                parent.find('.service_offer').val('');
            }
        });

        $('.service_offer_listing_container').on('click', '.service_offer_removed', function () {
            $(this).closest('.single_service_offer_listing').remove();
        });
        //added service offer functionality  END Here.


        //added brand name  functionality  Start Here.
        $('.brand_name_container').on('click', '.add_another_brand_name', function () {
            var parent = $(this).closest('.brand_name_container');
            var brandNameText = parent.find('.brand_name').val();
            if (brandNameText.length == 0) {
                parent.find('.brandNameError').html('Please insert service offer');
            } else {
                var html = '<div class="single_brand_name_listing">';
                html += '<span class="service_area_text">';
                html += brandNameText;
                html += '</span> <span class="brand_name_removed"> <i class="fa fa-times-circle "></i></span>';
                html += '<input type="hidden" name="brand_name_item[]" value="' + brandNameText + '" /> </div>';
                $('.brand_name_listing_container').prepend(html);
                parent.find('.brand_name').val('');
            }
        });

        $('.brand_name_listing_container').on('click', '.brand_name_removed', function () {
            $(this).closest('.single_brand_name_listing').remove();
        });

        //added brand name  functionality  END Here.



        //added service area functionality  START Here.
        $('.awards_achievements_container').on('click', '.add_another_awards_achievements', function () {
            var parent = $(this).closest('.awards_achievements_container');
            var serviceOfferText = parent.find('.awards_achievements').val();
            var temp_award_image = $('[name=temp_award_image]').val();
            if ((!serviceOfferText) || !(temp_award_image)) {
                parent.find('.serviceOfferError').html('Please insert awards or achievements');
            } else {
                var html = '<div class="single_awards_achievements_listing">';
                html += '<span class="service_area_text">';
                html += serviceOfferText;
                html += '<br /><img width="100" height="100" src="' + temp_award_image + '">';
                html += '</span> <span class="awards_achievements_removed"> <i class="fa fa-times-circle "></i></span>';
                html += '<input type="hidden" name="awards_achievements_item[]" value="' + serviceOfferText + '" /> </div>';
                //html += '<input type="hidden" name="awards_achievements_image[]" value="' + temp_award_image + '" /> </div>';
                $('.awards_achievements_listing_container').prepend(html);
                parent.find('.awards_achievements').val('');
                $('[name=temp_award_image]').val('');
            }
        });

        $('.awards_achievements_listing_container').on('click', '.awards_achievements_removed', function () {
            $(this).closest('.single_awards_achievements_listing').remove();
            $('[name=temp_award_image]').val('');
        });
        //added service area functionality  END Here.




        $('.project_form').on('change', '.awards_attachment', function () {
            $('.error_msg').remove();
            var that = $(this);
            var str = that.attr('id');
            var res = str.substring(19);
            var countVal = parseInt(res) + 1;

            var file_extension_array = ["jpg", "png", "gif"];
            var file_extension = that.val().split('.').pop();

//            if ($('[name=awards_achievements]').val()) {
            if ($.inArray(file_extension.toLowerCase(), file_extension_array) !== -1) {
                readAward(this, str);

                var that = $(this);
                var str = that.attr('id');
                //var res = str.substring(19);
                var res = str.split('_').pop()
                var countVal = parseInt(res) + 1;

                //alert(res);

                var newInsert = '<div class="single_file_upload awards_upload">';
                newInsert += '<input type="file" name="awards_achievements_image[]" id="awards_image_' + countVal + '" class="awards_attachment" />';
                newInsert += '<label for="awards_image_' + countVal + '" class="common_button project_attachment_btn">';
                newInsert += '<img alt="" src="img/attachment_icon.png" />';
                newInsert += 'Add Attachment';
                newInsert += '</label>';
                newInsert += '</div><div class="serviceOfferError error_msg"></div>';

                $(newInsert).insertAfter(".awards_achievements");


                /**/
                $(this).closest('.awards_upload').hide();


            } else {
                $('.awards_achievements_container .col-sm-10').append('<span class="error_msg">jpg, png or gif file are allowed!!</span>');
            }
//            } else {
//                $('.awards_achievements_container .col-sm-10').append('<span class="error_msg">Please insert awards or achievements</span>');
//            }
        });




        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd"
        });

        $('.project_form').on('change', '.project_attachment', function () {
            $('.error_msg').remove();
            var that = $(this);
            var str = that.attr('id');
            var res = str.substring(19);
            var countVal = parseInt(res) + 1;

            var file_extension_array = ["jpg", "png", "gif"];
            var file_extension = that.val().split('.').pop();
            if ($.inArray(file_extension.toLowerCase(), file_extension_array) !== -1) {
                readURL(this, str);
                $("[for=" + str + "]").remove();
                var fileHtml = '<input type="file" name="past_work_pic[]" id="project_attachment_' + countVal + '" class="project_attachment" />';
                fileHtml += '<label for="project_attachment_' + countVal + '" class="common_button project_attachment_btn">';
                fileHtml += '<img alt="" src="img/attachment_icon.png" />';
                fileHtml += ' Add Attachment';
                fileHtml += ' </label>';
                $('.sfu').append(fileHtml);
            } else {
                $('.sfu').prepend('<span class="error_msg">jpg, png or gif file are allowed!!</span>');
            }
        });

        $('.project_form').on('click', '.delete_img', function () {
            var removeFile = $(this).attr('rel');
            $("#" + removeFile).remove();
            $(this).closest('.single_file').remove();
        });

        $('.project_form').on('change', '.select_category', function () {
            var that = $(this);
            $.ajax({
                url: "pro/get_sub_category",
                type: 'POST',
                async: false,
                data: {category_id: that.val()},
                success: function (data) {
                    if (that.parents('.project_form').find('.sub_category_area').find('.single_sub_category_area').html(data)) {
                        that.parents('.project_form').find('.sub_category_area').show();
                    }
                }
            });
        });

        $('.project_form').on('click', '.add_another_license', function () {
            var newHtml = $('.another_license_copy').html();
            if ($('.company_license_area').append(newHtml)) {
                $('.company_license_area').last('.license_list ').find('.date_input input').datepicker({dateFormat: "yy-mm-dd"});
            }
//             $( ".datepicker" ).datepicker();
        });
        $('.project_form').on('click', '.remove_license_list', function () {
            $(this).closest('.license_list').remove();
        });
        $('.submit_request').click(function () {
            $('#addPro').submit();
        });
        var options = {
            target: 'pro/add_pro_action', // target element(s) to be updated with server response 
            beforeSubmit: showRequest, // pre-submit callback 
            success: showResponse, // post-submit callback 
            uploadProgress: function (event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                $('.progress').show();
                $('.progress').find('.bar').width(percentVal)
                $('.progress').find('.percent').html(percentVal);
                //console.log(percentVal, position, total);
            },
            // other available options: 
            //url:       url         // override for form's 'action' attribute 
            type: 'post', // 'get' or 'post', override for form's 'method' attribute 
            dataType: 'json'        // 'xml', 'script', or 'json' (expected server response type) 
                    //clearForm: true        // clear all form fields after successful submit 
                    //resetForm: true        // reset the form after successful submit 

                    // $.ajax options can be used here too, for example: 
                    //timeout:   3000 
        };
        // bind to the form's submit event 
        $('#addPro').submit(function () {
            // inside event callbacks 'this' is the DOM element so we first 
            // wrap it in a jQuery object and then invoke ajaxSubmit 
            $(this).ajaxSubmit(options);
            // !!! Important !!! 
            // always return false to prevent standard browser submit and page navigation 
            return false;
        });
    });
    // pre-submit callback 
    function showRequest(formData, jqForm, options) {
        $('.project_form').find('.error_msg').hide();
        $('.project_form').find('.submit_request.common_button').attr('disabled', 'disabled');
        return true;
    }

// post-submit callback 
    function showResponse(responseText, statusText, xhr, $form) {

        if (responseText.type == 'error') {
//            alert_r(responseText);
            if (responseText.errorMsg.nameError) {
                $('.project_form').find('.nameError').html(responseText.errorMsg.nameError).show();
            }
            if (responseText.errorMsg.emailError) {
                $('.project_form').find('.emailError').html(responseText.errorMsg.emailError).show();
            }
            if (responseText.errorMsg.passwordError) {
                $('.project_form').find('.passwordError').html(responseText.errorMsg.passwordError).show();
            }
            if (responseText.errorMsg.phoneError) {
                $('.project_form').find('.phoneError').html(responseText.errorMsg.phoneError).show();
            }
            if (responseText.errorMsg.addressError) {
                $('.project_form').find('.addressError').html(responseText.errorMsg.addressError).show();
            }
            if (responseText.errorMsg.cityError) {
                $('.project_form').find('.cityError').html(responseText.errorMsg.cityError).show();
            }
            if (responseText.errorMsg.zipCodeError) {
                $('.project_form').find('.zipCodeError').html(responseText.errorMsg.zipCodeError).show();
            }
            if (responseText.errorMsg.companyNameError) {
                $('.project_form').find('.companyNameError').html(responseText.errorMsg.companyNameError).show();
            }
            if (responseText.errorMsg.categoryError) {
                $('.project_form').find('.categoryError').html(responseText.errorMsg.categoryError).show();
            }
//            if (responseText.errorMsg.subCategoryError) {
//                $('.project_form').find('.subCategoryError').html(responseText.errorMsg.subCategoryError).show();
//            }

            if (responseText.errorMsg.expirDateError) {
                $('.project_form').find('.expirDateError').html(responseText.errorMsg.expirDateError).show();
            }
            if (responseText.errorMsg.licenseNumberError) {
                $('.project_form').find('.licenseNumberError').html(responseText.errorMsg.licenseNumberError).show();
            }
            if (responseText.errorMsg.licenseTypeError) {
                $('.project_form').find('.licenseTypeError').html(responseText.errorMsg.licenseTypeError).show();
            }
            if (responseText.errorMsg.termsConditionsError) {
                $('.project_form').find('.termsConditionsError').html(responseText.errorMsg.termsConditionsError).show();
            }
            if (responseText.errorMsg.stateError) {
                $('.project_form').find('.stateError').html(responseText.errorMsg.stateError).show();
            }
//            if (responseText.errorMsg.businessPhoneNumberError) {
//                $('.project_form').find('.businessPhoneNumberError').html(responseText.errorMsg.businessPhoneNumberError).show();
//            }

            $('.project_form').find('.submit_request.common_button').removeAttr('disabled');
        } else if (responseText.type == 'success') {
            $('#successModal').modal();
        }

        $('#successModal').on('hidden.bs.modal', function () {
            window.location.href = "<?php echo $this->Html->Url(array('controller' => 'home')); ?>";
        });
    }

    function readURL(input, str) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var picHtml = '<div class="single_file"><img alt="" width="50" src="' + e.target.result + '" /> '
                picHtml += '<a href="javascript:" rel="' + str + '" class="delete_img"><img alt="" src="img/cross_icon.png" /></a> </div>'
                $('.view_file').append(picHtml);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readAward(input, str) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('[name=temp_award_image]').val(e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>


<!--<div id="successModal" class="modal fade projectSubmitted " role="dialog">
    <div class="modal-dialog">
         Modal content
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="projectSubmittedArea">
                <img alt="" src="img/tick_mark_big.png"/>
                <div class="congratulations">
                    Congratulations!
                </div>
                <div class="details">
                    You are added as pro.
                </div>
            </div>
        </div>
    </div>
</div>-->