
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Pro view</h3>
        <div class="varified_area">
            <?php if ($pro_user['ProUser']['is_verified']) { ?> 
                Verified
            <?php } else { ?>
                <span>Verify</span><input type="checkbox" rel="<?php echo $pro_user['ProUser']['id']; ?>" class="VerifyProUser"  />
            <?php } ?>
        </div>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">

        <div class="listing">
            <label>
                Category : 
            </label>
            <span>
                <?php echo $Category['Categorie']['category']; ?>
            </span>
        </div>
        <div class="listing " >
            <label>Sub-category :</label> 
            <div class="sub_category_list">
                <?php
                foreach ($sub_categories as $sub_categorie) {
                    if (!in_array($sub_categorie['SubCategorie']['id'], $pro_user_sub_categorie)) {
                        continue;
                    }
                    ?>
                    <div class="single_sub_category">

                        <div style="padding: 10px; ">
                            <?php echo $sub_categorie['SubCategorie']['sub_category'] ?>
                        </div>

                        
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="listing">
            <label>
                Company name : 
            </label>
            <span>
                <?php echo $pro_user['ProUser']['company_name']; ?>
            </span>
        </div>

        <div class="listing">
            <label>
                Company Description : 
            </label>
            <span>
                <?php echo $pro_user['ProUser']['company_description']; ?>
            </span>
        </div>

        <div class="listing">
            <div class="license_left">
                <label>Company license :</label>
            </div>
            <div class="license_right">
                <table>
                    <tr>
                        <th>Company license number </th>
                        <th>License type </th>
                        <th>Active until </th>
                    </tr>
                    <?php foreach ($pro_user['CompanyLicense'] as $CompanyLicense) { ?>
                        <tr>
                            <td><?php echo $CompanyLicense['license_number']; ?></td>
                            <td><?php echo $CompanyLicense['license_type']; ?></td>
                            <td><?php echo $CompanyLicense['expir_date']; ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <div class="clear"></div>
        </div>


        <div class="listing">
            <label>
                Past work file : 
            </label>
            <span>
                <?php
                foreach ($pro_user['ProUserFile'] as $ProUserFile) {
                    ?>
                    <img alt="" style="width: 100px; margin-right: 10px;" src="uploaded_files/pro_user_files/<?php echo $ProUserFile['id'] ?>/<?php echo $ProUserFile['image'] ?>" />
                    <?php
                }
                ?>
            </span>
        </div>
        <div class="listing">
            <label>
                User name : 
            </label>
            <span>
                <?php echo $pro_user['User']['name']; ?>
            </span>
        </div>

        <div class="listing">
            <label>
                Email : 
            </label>
            <span>
                <?php echo $pro_user['User']['email']; ?>
            </span>
        </div>

        <div class="listing">
            <label>
                Phone : 
            </label>
            <span>
                <?php echo $pro_user['User']['phone']; ?>
            </span>
        </div>

        <div class="listing">
            <label>
                Address : 
            </label>
            <span>
                <?php echo $pro_user['ProUser']['address']; ?>
            </span>
        </div>
        <div class="listing">
            <label>
                State : 
            </label>
            <span>
                <?php echo $state_lists[$pro_user['ProUser']['state']]; ?>
            </span>
        </div>
        <div class="listing">
            <label>
                City : 
            </label>
            <span>
                <?php echo $pro_user['ProUser']['city']; ?>
            </span>
        </div>
        <div class="listing">
            <label>
                Zip code : 
            </label>
            <span>
                <?php echo $pro_user['ProUser']['zip_code']; ?>
            </span>
        </div>

    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

<script type="text/javascript">
    $(function() {
        $('.VerifyProUser').click(function() {

            var that = $(this);
            var id = $(this).attr('rel');
            var parent = that.parents('.varified_area');
            var status;
            if ($(this).is(':checked')) {
                status = 'checked';
                $.ajax({
                    url: "<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'pro_verification')); ?>",
                    type: 'POST',
                    async: false,
                    data: {id: id, status: status},
                    success: function(data) {
                        if (data == 'verified') {
                            parent.find('span').html('Verified');
                            that.remove();

                        }

                    }
                });
            }


        });
    });
</script>