
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Edit Pro</h3>
    </div> <!-- End .content-box-header -->
    <br/>

    <div class="alert alert-success message_box">

    </div>
    <br/>
    <div class="content-box-content">
        <div  class="update_pro_area_by_admin">
            <div class="col-sm-6">
                <div  class="update_user_basic_info_area">
                    <div id="block1" class="profile_info">
                        <h3>Basic Info Update</h3>
                        <form autocomplete="off"  action="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'update_user_profile_by_admin')); ?>" id="updateProfile" >
                            <div class="profile_pic">
                                <?php
                                if ($user['User']['profile_pic']) {
                                    ?>
                                    <img class="profile_image" alt="" src="uploaded_files/profile_pic/<?php echo $user['User']['id'] ?>/<?php echo $user['User']['profile_pic'] ?>"/>
                                <?php } else { ?>
                                    <img class="profile_image" alt="" src="img/default_pro_pic.png"/>
                                <?php } ?>
                                <div class="profile_pic_upload">
                                    <input type="file" name="profile_pic" id="profile_pic" />
                                    <label for="profile_pic">
                                        <img alt="" src="img/pencil_icon.png" />
                                    </label>
                                </div>

                            </div>
                            <div class="error_msg imageError"></div>
                            <div class="clear"></div>
                            <input type="hidden" name="user_id" value="<?php echo $user['User']['id']; ?>" />
                            <div class="user_name">
                                <img alt="" src="img/username_icon.png" />
                                <input type="text" name="name" value="<?php echo $user['User']['name']; ?>" class="userNameInput" />

                            </div>
                            <div class="user_email">
                                <img alt="" src="img/usermail_icon.png" />

                                <input type="text" value="<?php echo $user['User']['email']; ?>" name="email" class="emalInput" />
                                <div class="error_msg emailError"></div>
                            </div>
                            <div class="user_phone">
                                <img alt="" src="img/userphone_icon.png" />

                                <input type="text" value="<?php echo $user['User']['phone']; ?>" name="phone" class="phoneInput" />
                            </div>


                            <div class="progress">
                                <div class="bar"></div>
                                <div class="percent">0%</div>
                            </div>

                            <br/>
                            <input type="submit" class="btn btn-success" value="Update Info" />
                            <a href="javascript:" onclick="resetPassword(<?php echo $pro_user['ProUser']['id']; ?>)" class="btn btn-success">
                                Reset Password
                            </a>
                            <?php
                            if ($claimCount) {
                                ?>
                                <a href="javascript:" onclick="deleteClaimList(<?php echo $pro_user['ProUser']['id']; ?>)" class="btn btn-success deleteClaimList">
                                    Delete Claim
                                </a>
                            <?php } ?>
                        </form>


                    </div>
                    <script type="text/javascript">
                        $(function () {

                            $('.profile_info').on('click', '.profile_update_btn', function () {
                                var parent = $(this).closest('.profile_info');
                                parent.find('.text').hide();
                                parent.find('.user_name,.user_email,.user_phone').find('input').show();
                                parent.find('.profile_pic_upload').show();
                            });

                            var options = {
                                type: 'post',
                                dataType: 'json',
                                beforeSubmit: function (arr, $form, options) {
                                    $('.error_msg').hide();
                                },
                                success: responsefunc
                            };

                            $('#updateProfile').submit(function () {
                                $(this).ajaxSubmit(options);
                                //$(".addpictureblock").modal('hide');
                                return false;
                            });


                            $('.profile_info').on('change', '#profile_pic', function () {
                                var that = $(this);
                                $('.imageError').hide();
                                var file_extension_array = ["jpg", "png", "gif"];
                                var file_extension = that.val().split('.').pop();
                                if ($.inArray(file_extension.toLowerCase(), file_extension_array) !== -1) {
                                    readURL(this);
                                } else {
                                    $('.imageError').html('jpg, png or gif file are allowed!!').show();
                                }

                            });

                        });

                        function responsefunc(responseText, statusText, xhr, $form) {
                            //        alert_r(responseText);
                            if (responseText.type == 'error') {
                                $('.emailError').html(responseText.error.emailError).show();
                                $('.imageError').html(responseText.error.imageError).show();
                            } else {
                                $('.message_box').html('Successfully Updated').show();
                                setTimeout(function () {
                                    $('.message_box').fadeOut();
                                }, 3000);

                            }
                        }

                        function readURL(input) {
                            if (input.files && input.files[0]) {

                                var reader = new FileReader();
                                reader.onload = function (e) {

                                    $('.profile_image').attr('src', e.target.result);
                                }
                                reader.readAsDataURL(input.files[0]);
                            }
                        }
                        function readAward(input, str) {
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    $('[name=temp_award_image]').val(e.target.result);
                                }

                                reader.readAsDataURL(input.files[0]);
                            }
                        }
                    </script>
                </div>
            </div>
            <div class="col-sm-6">
                <div  class="update_user_basic_info_area">
                    <div  id="block2" class="profile_info">
                        <h3>Package Information</h3>
                        
                        <?php //print_r($package); die; ?>
                        <p></p>
                        <p><label>Universal Home Page Package : <?php echo  ($pro_user['ProUser']['package_universal'] == 1) ? 'Yes': 'No'; ?></label></p>
                        <p><label>Universal Profile Page Package : <?php echo  ($pro_user['ProUser']['package_universal_profile'] == 1) ? 'Yes': 'No'; ?></label></p>
                        
                        <p><label>Home Package View Count : <?php echo  ($package['ProUserPackage']['view_count_home']) ? $package['ProUserPackage']['view_count_home'] : '0' ?></label></p>
                        <p><label>Profile Page View Count : <?php echo  ($package['ProUserPackage']['view_count']) ? $package['ProUserPackage']['view_count'] : '0' ?></label></p>
                        
                        
                        <?php        
                        if($newpackage['ProUserNewPackage']['package_home'] == 1) {
                            $package_text_home = '<p><label>Last Home Package : Package 1 (100 views)</label></p>';
                        } else if($newpackage['ProUserNewPackage']['package_home'] == 2) {
                            $package_text_home = '<p><label>Last Home Package : Package 2 (200 views)</label></p>';
                        } else if($newpackage['ProUserNewPackage']['package_home'] == 3) {
                            $package_text_home = '<p><label>Last Home Package : Package 3 (300 views)</label></p>';
                        }else {
                            $package_text_home = '';
                        }
                        
                        echo $package_text_home;
                        
                        if($newpackage['ProUserNewPackage']['package'] == 1) {
                            $package_text = '<p><label>Last Profile Package : Package 1 (100 views)</label></p>';
                        } else if($newpackage['ProUserNewPackage']['package'] == 2) {
                            $package_text = '<p><label>Last Profile Package : Package 2 (200 views)</label></p>';
                        } else if($newpackage['ProUserNewPackage']['package'] == 3) {
                            $package_text = '<p><label>Last Profile Package : Package 3 (300 views)</label></p>';
                        }else {
                            $package_text = '';
                        }
                        echo $package_text;
                        
                        ?>
                        
<!--                        <p><label>Last Home Package : <?php echo  ($newpackage['ProUserNewPackage']['view_count_home']) ? $package['ProUserPackage']['view_count_home'] : '0' ?></label></p>
                        <p><label>Last Profile Package : <?php echo  ($newpackage['ProUserNewPackage']['view_count']) ? $package['ProUserPackage']['view_count'] : '0' ?></label></p>-->
                        
                    </div>
                </div>
            </div>
            <div class="clear"></div>

            <!--            <div class="col-sm-12">
                            <div class="update_pro_user_area">
                                <div class="dashboard_area">
                                    <div class="start_a_projects">
                                        <div class="get_quotes_area" style="background: #fff;">
                                            <div class="get_quotes_details">
                                                <form autocomplete="off"  id="addPackage" >
                                                    <input type="hidden" name="pro_user_id" value="<?php echo $user['User']['id'] ?>" />
                                                    <input type="hidden" name="category_id" value="<?php echo $pro_user['ProUser']['category_id'] ?>" />
                                                    <div class="col-md-9 project_form">
                                                        <div class="project_details_heading">
                                                            Update a Pro User Package
            
                                                        </div>
                                                        <div style="display: none;" class="alert alert-success package_box"></div>
                                                        <div class="input_list">
            
                                                            <div class="input_text">
                                                                Choose Package
                                                            </div>
                                                            <select name="package" class="select_category">
                                                                <option value="1">Package 1: $50 - Gets the contractor 100 views</option>
                                                                <option value="2">Package 2: $75 - Gets the contractor 200 views</option>
                                                                <option value="3">Package 3: $100 - Gets the contractor 300 views</option>
                                                            </select>
                                                            <br />
                                                            <br />
                                                            <input type="submit" class="btn btn-success" value="Update Package">
                                                        </div>
                                                    </div>
                                                </form>
            
                                            </div>
            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->

            <div class="col-sm-12">
                <div class="update_pro_user_area">




                    <link rel="stylesheet" type="text/css" href="css/style.css" />
                    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
                    <div class="dashboard_area">
                        <div class="start_a_projects">

                            <div class="get_quotes_area" style="background: #fff;">

                                <div class="get_quotes_details">

                                    <form autocomplete="off" action="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'edit_pro_by_admin_action')); ?>" id="addPro" >
                                        <input type="hidden" name="user_id" value="<?php echo $user['User']['id'] ?>" />
                                        <div class="col-md-10 project_form">
                                            <div class="project_details_heading">
                                                Update a pro
                                            </div>
                                            <div class="input_list">
                                                <div class="input_text">
                                                    Choose your line of work
                                                </div>
                                                <select name="category" class="select_category">
                                                    <option value="0">Select Category</option>
                                                    <?php
                                                    foreach ($Categories as $Categorie) {
                                                        ?>
                                                        <option <?php if ($pro_user['ProUser']['category_id'] == $Categorie['Categorie']['id']) { ?> selected  <?php } ?> value="<?php echo $Categorie['Categorie']['id']; ?>">
                                                            <?php echo $Categorie['Categorie']['category']; ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <div class="categoryError error_msg"></div>
                                            </div>

                                            <div class="input_list sub_category_area" style="display: block;">
                                                <div class="input_text">
                                                    Sub-category
                                                </div>
                                                <div class="single_sub_category_area">
                                                    <div class="sub_category_list">

                                                        <?php
                                                        foreach ($sub_categories as $sub_categorie) {
                                                            ?>
                                                            <div class="single_sub_category">
                                                                <input class="SubCategoryCheck" name="sub_category[]" <?php if (in_array($sub_categorie['SubCategorie']['id'], $pro_user_sub_categorie)) { ?> checked <?php } ?> id="sub_cat_<?php echo $sub_categorie['SubCategorie']['id'] ?>" value="<?php echo $sub_categorie['SubCategorie']['id'] ?>" type="checkbox" />
                                                                <label for="sub_cat_<?php echo $sub_categorie['SubCategorie']['id'] ?>"></label>
                                                                <span>
                                                                    <?php echo $sub_categorie['SubCategorie']['sub_category'] ?>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                        <?php } ?>
                                                        <div class="subCategoryError error_msg"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input_list row">
                                                <div class="col-sm-8">
                                                    <div class="input_text">
                                                        What is the name of your company?
                                                    </div>
                                                    <input type="text" name="company_name" value="<?php echo $pro_user['ProUser']['company_name']; ?>"  class="company_name" />
                                                    <div class="companyNameError error_msg"></div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="input_text">
                                                        Operational Since
                                                    </div>
                                                    <input type="text" name="operational_since" value="<?php echo $pro_user['ProUser']['operational_since']; ?>"  class="operational_since" />
                                                    <div class="operationalSinceError error_msg"></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input_list row">
                                                <div class="col-sm-6">
                                                    <div class="input_text">
                                                        Business phone number
                                                    </div>
                                                    <input type="text" name="business_phone_number" value="<?php echo $pro_user['ProUser']['business_phone']; ?>" class="business_phone_number" />
                                                    <!--                                                    <div class="businessPhoneNumberError error_msg"></div>-->
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="input_text">
                                                        Website URL
                                                    </div>
                                                    <input type="text" name="website" value="<?php echo $pro_user['ProUser']['website_url']; ?>" class="website" />
                                                    <div class="websiteError error_msg"></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input_list row service_area_container">
                                                <div class="col-sm-10">
                                                    <div class="input_text">
                                                        Service Areas
                                                    </div>
                                                    <input type="text" name="service_area" class="service_area" id="service_area" />
                                                    <div class="serviceAreaError error_msg"></div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="input_text">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="col-sm-12">
                                                    <div class="item_list_container service_area_listing_container">
                                                        <?php
                                                        foreach ($pro_user['ProServiceArea'] as $key => $val) {
                                                            ?>
                                                            <div class="single_service_area_listing">
                                                                <span class="service_area_text">
                                                                    <?php echo $val['service_area']; ?>
                                                                </span>
                                                                <span class="service_area_removed">
                                                                    <i class="fa fa-times-circle "></i>
                                                                </span>
                                                                <input type="hidden" name="service_area_item[]" value="<?php echo $val['service_area']; ?>">
                                                                <input type="hidden" name="service_area_lat[]" value="<?php echo $val['latitude']; ?>">
                                                                <input type="hidden" name="service_area_lng[]" value="<?php echo $val['longitude']; ?>">
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input_list row awards_achievements_container">
                                                <div class="col-sm-11">
                                                    <div class="input_text">
                                                        Awards & Achievements
                                                    </div>
                                                    <input type="text" name="awards_achievements" placeholder="Award Title" class="awards_achievements" />
                                                    <input type="hidden" name="temp_award_image" value="" />
                                                    <div class="single_file_upload awards_upload">
                                                        <input type="file" name="awards_achievements_image[]" id="awards_image_1" class="awards_attachment" />
                                                        <label for="awards_image_1" class="common_button project_attachment_btn">
                                                            <img alt="" src="img/attachment_icon.png" />
                                                            Add Attachment
                                                        </label>
                                                    </div>
                                                    <div class="serviceOfferError error_msg"></div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <div class="input_text">
                                                        &nbsp;
                                                    </div>
                                                    <a href="javascript:" class="add_another_awards_achievements">
                                                        + 
                                                    </a>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="col-sm-12">
                                                    <div class="item_list_container awards_achievements_listing_container">
                                                        <?php
                                                        foreach ($pro_user['ProAward'] as $key => $val) {
                                                            ?>
                                                            <div class="single_awards_achievements_listing">
                                                                <span class="service_area_text">
                                                                    <?php echo $val['award']; ?>
                                                                    <?php if ($val['award_image']) { ?>
                                                                        <br />
                                                                        <img width="100" height="50" src="uploaded_files/pro_user_files/awards/<?php echo $pro_user['ProUser']['id'] ?>/<?php echo $val['award_image'] ?>">
                                                                    <?php } ?>
                                                                </span>
                                                                <span rel="<?php echo $val['id'] ?>" class="awards_achievements_removed single-att-remove">
                                                                    <i class="fa fa-times-circle "></i>
                                                                </span>

                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input_list row">
                                                <div class="col-sm-5">
                                                    <div class="input_text">
                                                        Cover Photo
                                                    </div>
                                                    <div class="">
                                                        <div class="image_upload">
                                                            <input type="file" name="cover_photo" id="cover_photo" class="image_upload_button">
                                                            <label for="cover_photo" class="common_button image_upload_button_label">
                                                                <img alt="" src="img/attachment_icon.png">
                                                                Add Attachment
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    if ($pro_user['ProUser']['cover_photo']) {
                                                        $c = 'uploaded_files/pro_user_files/cover_photo/' . $pro_user['ProUser']['cover_photo'];
                                                        if (file_exists($c)) {
                                                            ?>
                                                            <img width="150" height="100" src="<?php echo $c; ?>">
                                                        <?php }
                                                    }
                                                    ?>
                                                    <div class="coverPhotoError error_msg"></div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div class="input_text">
                                                        Busniess Logo
                                                    </div>
                                                    <div class="">

                                                        <div class="image_upload">
                                                            <input type="file" name="business_logo" id="business_logo" class="image_upload_button">
                                                            <label for="business_logo" class="common_button image_upload_button_label">
                                                                <img alt="" src="img/attachment_icon.png">
                                                                Add Attachment
                                                            </label>
                                                        </div>

                                                        <?php
                                                        if ($pro_user['ProUser']['business_logo']) {
                                                            $b = 'uploaded_files/pro_user_files/business_logo/' . $pro_user['ProUser']['business_logo'];
                                                            if (file_exists($b)) {
                                                                ?>
                                                                <img width="150" height="100" src="<?php echo $b; ?>">
                                                            <?php }
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="busniessLogoError error_msg"></div>
                                                </div>
                                            </div>


                                            <div class="input_list operating_hours_container">
                                                <div class="input_text">
                                                    Operating Hours
                                                </div>
                                                <div class="row single_operating_hours">
                                                    <div class="col-sm-2">
                                                        Monday
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="start" name="mon_start" value="<?php echo $pro_user['ProOperatingHour'][0]['mon_start'] ?>" />
                                                    </div>
                                                    <div class="col-sm-1 separation">
                                                        -
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="end" name="mon_end" value="<?php echo $pro_user['ProOperatingHour'][0]['mon_end'] ?>"/>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="checkbox" <?php if ($pro_user['ProOperatingHour'][0]['mon_end'] == '' && $pro_user['ProOperatingHour'][0]['mon_start'] == '') { ?>  checked="checked" <?php } ?> class="UnavailableCheck" /> Unavailable
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row single_operating_hours">
                                                    <div class="col-sm-2">
                                                        Tuesday
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="start" name="tue_start" value="<?php echo $pro_user['ProOperatingHour'][0]['tue_start'] ?>" />
                                                    </div>
                                                    <div class="col-sm-1 separation">
                                                        -
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="end" name="tue_end" value="<?php echo $pro_user['ProOperatingHour'][0]['tue_end'] ?>"/>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="checkbox" <?php if ($pro_user['ProOperatingHour'][0]['tue_end'] == '' && $pro_user['ProOperatingHour'][0]['tue_start'] == '') { ?>  checked="checked" <?php } ?> class="UnavailableCheck" /> Unavailable
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row single_operating_hours">
                                                    <div class="col-sm-2">
                                                        Wednesday
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="start" name="wed_start" value="<?php echo $pro_user['ProOperatingHour'][0]['wed_start'] ?>" />
                                                    </div>
                                                    <div class="col-sm-1 separation">
                                                        -
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="end" name="wed_end" value="<?php echo $pro_user['ProOperatingHour'][0]['wed_end'] ?>"/>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="checkbox" <?php if ($pro_user['ProOperatingHour'][0]['wed_end'] == '' && $pro_user['ProOperatingHour'][0]['wed_start'] == '') { ?>  checked="checked" <?php } ?> class="UnavailableCheck" /> Unavailable
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row single_operating_hours">
                                                    <div class="col-sm-2">
                                                        Thursday
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="start" name="thu_start" value="<?php echo $pro_user['ProOperatingHour'][0]['thu_start'] ?>" />
                                                    </div>
                                                    <div class="col-sm-1 separation">
                                                        -
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="end" name="thu_end" value="<?php echo $pro_user['ProOperatingHour'][0]['thu_end'] ?>"/>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="checkbox" <?php if ($pro_user['ProOperatingHour'][0]['thu_end'] == '' && $pro_user['ProOperatingHour'][0]['thu_start'] == '') { ?>  checked="checked" <?php } ?> class="UnavailableCheck" /> Unavailable
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row single_operating_hours">
                                                    <div class="col-sm-2">
                                                        Friday
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="start" name="fri_start" value="<?php echo $pro_user['ProOperatingHour'][0]['fri_start'] ?>" />
                                                    </div>
                                                    <div class="col-sm-1 separation">
                                                        -
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="end" name="fri_end" value="<?php echo $pro_user['ProOperatingHour'][0]['fri_end'] ?>"/>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="checkbox" <?php if ($pro_user['ProOperatingHour'][0]['fri_end'] == '' && $pro_user['ProOperatingHour'][0]['fri_start'] == '') { ?>  checked="checked" <?php } ?> class="UnavailableCheck" /> Unavailable
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row single_operating_hours">
                                                    <div class="col-sm-2">
                                                        Saturday
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="start" name="sat_start" value="<?php echo $pro_user['ProOperatingHour'][0]['sat_start'] ?>" />
                                                    </div>
                                                    <div class="col-sm-1 separation">
                                                        -
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="end" name="sat_end" value="<?php echo $pro_user['ProOperatingHour'][0]['sat_end'] ?>"/>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="checkbox" <?php if ($pro_user['ProOperatingHour'][0]['sat_end'] == '' && $pro_user['ProOperatingHour'][0]['sat_start'] == '') { ?>  checked="checked" <?php } ?> class="UnavailableCheck" /> Unavailable
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row single_operating_hours">
                                                    <div class="col-sm-2">
                                                        Sunday
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="start" name="sun_start" value="<?php echo $pro_user['ProOperatingHour'][0]['sun_start'] ?>" />
                                                    </div>
                                                    <div class="col-sm-1 separation">
                                                        -
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="end" name="sun_end" value="<?php echo $pro_user['ProOperatingHour'][0]['sun_end'] ?>"/>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="checkbox" <?php if ($pro_user['ProOperatingHour'][0]['sun_end'] == '' && $pro_user['ProOperatingHour'][0]['sun_start'] == '') { ?>  checked="checked" <?php } ?> class="UnavailableCheck" /> Unavailable
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>

                                            </div>

                                            <div class="input_list">
                                                <div class="input_text">
                                                    Describe your company briefly
                                                </div>
                                                <textarea name="company_description" class="company_description"><?php echo $pro_user['ProUser']['company_description']; ?></textarea>
                                            </div>

                                            <div class="company_license_area">
                                                <div class="license_list">
                                                    <div class="license_part col-md-4">

                                                        <div class="license_heading">
                                                            Company license number
                                                        </div>
                                                    </div>
                                                    <div class="license_part col-md-4">

                                                        <div class="license_heading">
                                                            License type
                                                        </div>
                                                    </div>
                                                    <div class="license_part col-md-4">
                                                        <div class="license_heading">
                                                            Active until
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
<?php foreach ($pro_user['CompanyLicense'] as $company_license) { ?>
                                                    <div class="license_list edit_license">
                                                        <div class="license_part col-md-4">
                                                            <div class="input_list">
                                                                <div class="license_input">
                                                                    <input name="license_number[]" value="<?php echo $company_license['license_number']; ?>" type="text" />
                                                                </div>
                                                                <div class="licenseNumberError error_msg"></div>
                                                            </div>
                                                        </div>
                                                        <div class="license_part col-md-4">
                                                            <div class="input_list">
                                                                <div class="license_input">
                                                                    <input name="license_type[]" value="<?php echo $company_license['license_type']; ?>" type="text" />
                                                                </div>
                                                                <div class="licenseTypeError error_msg"></div>
                                                            </div>
                                                        </div>
                                                        <div class="license_part col-md-4">
                                                            <div class="input_list">
                                                                <div class="license_input">
                                                                    <input name="expir_date[]" type="text" value="<?php echo $company_license['expir_date']; ?>" class="datepicker" placeholder="DD/MM/YYYY"/>
                                                                </div>
                                                                <div class="expirDateError error_msg"></div>
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <a href="javascript:"  class="delete_license">
                                                            <img src="img/cross_icon.png" alt="">
                                                        </a>
                                                    </div>
<?php } ?>
                                            </div>
                                            <a href="javascript:" class="add_another_license">
                                                + ADD ANOTHER LICENSE
                                            </a>
                                            <div class="file_upload_area">
                                                <div class="upload_file_info">
                                                    Add photos of past work <span>(15mb max file size)</span> 
                                                </div>
                                                <div class="view_file">
<?php foreach ($pro_user['ProUserFile'] as $pro_user_file) { ?>
                                                        <div class="single_file existing_img">
                                                            <img width="50" src="uploaded_files/pro_user_files/<?php echo $pro_user['ProUser']['id'] ?>/<?php echo $pro_user_file['image']; ?>" alt=""> 
                                                            <a class="delete_img" rel="<?php echo $pro_user_file['id'] ?>" href="javascript:">
                                                                <img src="img/cross_icon.png" alt="">
                                                            </a>
                                                        </div>
<?php } ?>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="single_file_upload">
                                                    <input type="file" name="past_work_pic[]" id="project_attachment_1" class="project_attachment" />
                                                    <label for="project_attachment_1" class="common_button project_attachment_btn">
                                                        <img alt="" src="img/attachment_icon.png" />
                                                        Add Attachment
                                                    </label>
                                                </div>

                                            </div>

                                            <div class="input_list social_media_container">
                                                <div class="input_text">
                                                    Social Media Pages (Optional)
                                                </div>
                                                <div class="row social_item">
                                                    <div class="col-sm-1">
                                                        <img alt="" width="40" src="img/facebook_icon.png" />
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <input type="text" value="<?php echo $pro_user['ProUser']['facebook_link']; ?>" name="facebook_link" />
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row social_item">
                                                    <div class="col-sm-1">
                                                        <img alt="" width="40" src="img/twitter_icon.png" />
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <input type="text" value="<?php echo $pro_user['ProUser']['twitter_link']; ?>" name="twitter_link" />
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row social_item">
                                                    <div class="col-sm-1">
                                                        <img alt="" width="40" src="img/linkedin_icon.png" />
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <input type="text" value="<?php echo $pro_user['ProUser']['linkedin_link']; ?>" name="linkedin_link" />
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row social_item">
                                                    <div class="col-sm-1">
                                                        <img alt="" width="40" src="img/google_icon.png" />
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <input type="text" value="<?php echo $pro_user['ProUser']['google_link']; ?>" name="google_link" />
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row social_item">
                                                    <div class="col-sm-1">
                                                        <img alt="" width="40" src="img/youtube_icon.png" />
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <input type="text" value="<?php echo $pro_user['ProUser']['youtube_link']; ?>" name="youtube_link" />
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>

                                            </div>


                                            <div class="project_details_heading">
                                                Contact Details
                                            </div>

                                            <div class="input_text">
                                                Address
                                            </div>
                                            <div class="address_area">
                                                <div class="input_list street_address">
                                                    <input type="text" name="address" class="street_address" value="<?php echo $pro_user['ProUser']['address']; ?>" placeholder="Street address" />
                                                    <div class="addressError error_msg"></div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="input_list">
                                                    <select name="state" class="state">
                                                        <option value="0"> Select state</option>
                                                        <?php
                                                        foreach ($state_lists as $key => $state_list) {
                                                            ?>
                                                            <option <?php if ($pro_user['ProUser']['state'] == $key) { ?> selected  <?php } ?> value="<?php echo $key ?>"> <?php echo $state_list; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <div class="stateError error_msg"></div>
                                                </div>
                                                <div class="input_list">
                                                    <input name="city" type="text" class="city" value="<?php echo $pro_user['ProUser']['city'] ?>" placeholder="City" />
                                                    <div class="cityError error_msg"></div>
                                                </div>
                                                <div class="input_list">
                                                    <input name="zip_code" type="text" class="zip_code" value="<?php echo $pro_user['ProUser']['zip_code']; ?>" placeholder="Zip code" />
                                                    <div class="zipCodeError error_msg"></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>



                                            <a href="javascript:" class="submit_request common_button">
                                                Submit Request
                                            </a>

                                        </div>
                                        <input type="submit" value="submit"  style="display: none;"/>
                                    </form>
                                    <div class="another_license_copy">
                                        <div class="license_list ">
                                            <div class="license_part col-md-4">

                                                <div class="license_input">
                                                    <input name="license_number[]" type="text" />
                                                </div>
                                            </div>
                                            <div class="license_part col-md-4">

                                                <div class="license_input">
                                                    <input name="license_type[]" type="text" />
                                                </div>
                                            </div>
                                            <div class="license_part col-md-4">

                                                <div class="license_input date_input">
                                                    <input  name="expir_date[]"type="text"  placeholder="DD/MM/YYYY"/>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <a href="javascript:" class="remove_license_list">
                                                <img alt="" src="img/cross_icon.png"/>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo Configure::read('settings.gmap_api_key'); ?>&libraries=places"></script>

                    <script type="text/javascript">
                        $(function () {

                            $(".operational_since").datepicker({
                                minView: 2,
                                dateFormat: "yy-mm-dd",
                                changeYear: true,
                            });

                            $('.UnavailableCheck').each(function () {
                                var parent = $(this).closest('.single_operating_hours');
                                if ($(this).prop('checked') == true) {
                                    parent.find('.start').val('').attr('disabled', 'disabled');
                                    parent.find('.end').val('').attr('disabled', 'disabled');
                                }
                            })
                            $('.operating_hours_container').on('click', '.UnavailableCheck', function () {
                                var parent = $(this).closest('.single_operating_hours');
                                if ($(this).prop('checked') == true) {
                                    parent.find('.start').val('').attr('disabled', 'disabled');
                                    parent.find('.end').val('').attr('disabled', 'disabled');
                                } else {
                                    parent.find('.start').val('9am').removeAttr('disabled');
                                    parent.find('.end').val('7pm').removeAttr('disabled');
                                }
                            });


                            //Google map autocomplete
                            var input = document.getElementById('service_area');
                            var searchBox = new google.maps.places.SearchBox(input);
                            searchBox.addListener('places_changed', function () {
                                var places = searchBox.getPlaces();
                                var latitude = places[0].geometry.location.lat();
                                var longitude = places[0].geometry.location.lng();
                                var serviceAreaText = $('#service_area').val();
                                var html = '<div class="single_service_area_listing">';
                                html += '<span class="service_area_text">';
                                html += serviceAreaText;
                                html += '</span> <span class="service_area_removed"> <i class="fa fa-times-circle "></i></span>';
                                html += '<input type="hidden" name="service_area_item[]" value="' + serviceAreaText + '" />';
                                html += '<input type="hidden" name="service_area_lat[]" value="' + latitude + '" />';
                                html += '<input type="hidden" name="service_area_lng[]" value="' + longitude + '" />';
                                html += '</div>';
                                $('.service_area_listing_container').prepend(html);
                                $('#service_area').val('');
                            });

                            $('.service_area_listing_container').on('click', '.service_area_removed', function () {
                                $(this).closest('.single_service_area_listing').remove();
                            });
                            //added service area functionality  END Here.

                            //added service offer functionality  START Here.
                            $('.service_offer_container').on('click', '.add_another_service_offer', function () {
                                var parent = $(this).closest('.service_offer_container');
                                var serviceOfferText = parent.find('.service_offer').val();
                                if (serviceOfferText.length == 0) {
                                    parent.find('.serviceOfferError').html('Please insert service offer');
                                } else {
                                    var html = '<div class="single_service_offer_listing">';
                                    html += '<span class="service_area_text">';
                                    html += serviceOfferText;
                                    html += '</span> <span class="service_offer_removed"> <i class="fa fa-times-circle "></i></span>';
                                    html += '<input type="hidden" name="service_offer_item[]" value="' + serviceOfferText + '" /> </div>';
                                    $('.service_offer_listing_container').prepend(html);
                                    parent.find('.service_offer').val('');
                                }
                            });

                            $('.service_offer_listing_container').on('click', '.service_offer_removed', function () {
                                $(this).closest('.single_service_offer_listing').remove();
                            });
                            //added service offer functionality  END Here.

                            $('.awards_achievements_container').on('click', '.add_another_awards_achievements', function () {
                                var parent = $(this).closest('.awards_achievements_container');
                                var serviceOfferText = parent.find('.awards_achievements').val();
                                var temp_award_image = $('[name=temp_award_image]').val();
                                if ((!serviceOfferText) || !(temp_award_image)) {
                                    parent.find('.serviceOfferError').html('Please insert awards or achievements');
                                } else {
                                    var html = '<div class="single_awards_achievements_listing">';
                                    html += '<span class="service_area_text">';
                                    html += serviceOfferText;
                                    html += '<br /><img width="100" height="100" src="' + temp_award_image + '">';
                                    html += '</span> <span class="awards_achievements_removed"> <i class="fa fa-times-circle "></i></span>';
                                    html += '<input type="hidden" name="awards_achievements_item[]" value="' + serviceOfferText + '" /> </div>';
                                    //html += '<input type="hidden" name="awards_achievements_image[]" value="' + temp_award_image + '" /> </div>';
                                    $('.awards_achievements_listing_container').prepend(html);
                                    parent.find('.awards_achievements').val('');
                                    $('[name=temp_award_image]').val('');
                                }
                            });

                            $('.awards_achievements_listing_container').on('click', '.awards_achievements_removed', function () {
                                $(this).closest('.single_awards_achievements_listing').remove();
                                $('[name=temp_award_image]').val('');
                            });





                            $('.awards_achievements_listing_container').on('click', '.single-att-remove', function (e) {
                                //alert("Sa");
                                e.preventDefault();
                                var that = $(this);
                                var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'pro_user_award_delete')); ?>';
                                var data = {
                                    'aw_id': $(this).attr('rel')
                                };

                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data) {
                                        if (data == 'success') {
                                        }


                                    }
                                });
                            });
                            $('.project_form').on('change', '.awards_attachment', function () {
                                $('.error_msg').remove();
                                var that = $(this);
                                var str = that.attr('id');
                                var res = str.substring(19);
                                var countVal = parseInt(res) + 1;

                                var file_extension_array = ["jpg", "png", "gif"];
                                var file_extension = that.val().split('.').pop();

//            if ($('[name=awards_achievements]').val()) {
                                if ($.inArray(file_extension.toLowerCase(), file_extension_array) !== -1) {
                                    readAward(this, str);

                                    var that = $(this);
                                    var str = that.attr('id');
                                    //var res = str.substring(19);
                                    var res = str.split('_').pop()
                                    var countVal = parseInt(res) + 1;

                                    //alert(res);

                                    var newInsert = '<div class="single_file_upload awards_upload">';
                                    newInsert += '<input type="file" name="awards_achievements_image[]" id="awards_image_' + countVal + '" class="awards_attachment" />';
                                    newInsert += '<label for="awards_image_' + countVal + '" class="common_button project_attachment_btn">';
                                    newInsert += '<img alt="" src="img/attachment_icon.png" />';
                                    newInsert += 'Add Attachment';
                                    newInsert += '</label>';
                                    newInsert += '</div><div class="serviceOfferError error_msg"></div>';

                                    $(newInsert).insertAfter(".awards_achievements");


                                    /**/
                                    $(this).closest('.awards_upload').hide();


                                } else {
                                    $('.awards_achievements_container .col-sm-10').append('<span class="error_msg">jpg, png or gif file are allowed!!</span>');
                                }
//            } else {
//                $('.awards_achievements_container .col-sm-10').append('<span class="error_msg">Please insert awards or achievements</span>');
//            }
                            });

                            //added service area functionality  END Here.

                            $(".datepicker").datepicker({
                                dateFormat: "yy-mm-dd"
                            });

                            $('.view_file').on('click', '.existing_img .delete_img', function () {
                                var that = $(this);
                                var id = that.attr('rel');
                                var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'delete_pro_user_file')); ?>';
                                $.ajax({
                                    url: url,
                                    type: 'POST',
                                    async: false,
                                    data: {id: id},
                                    success: function (data) {
                                        if (data == 'success') {
                                            that.closest('.single_file').remove();
                                        }

                                    }
                                });
                            });
                            $('.license_list').on('click', '.delete_license', function () {
                                $(this).closest('.license_list').remove();

                            });

                            $('.project_form').on('change', '.project_attachment', function () {
                                $('.error_msg').remove();
                                var that = $(this);
                                var str = that.attr('id');
                                var res = str.substring(19);
                                var countVal = parseInt(res) + 1;
                                var file_extension_array = ["jpg", "png", "gif"];
                                var file_extension = that.val().split('.').pop();
                                if ($.inArray(file_extension.toLowerCase(), file_extension_array) !== -1) {
                                    readURL(this, str);
                                    $("[for=" + str + "]").remove();
                                    var fileHtml = '<input type="file" name="past_work_pic[]" id="project_attachment_' + countVal + '" class="project_attachment" />';
                                    fileHtml += '<label for="project_attachment_' + countVal + '" class="common_button project_attachment_btn">';
                                    fileHtml += '<img alt="" src="img/attachment_icon.png" />';
                                    fileHtml += ' Add Attachment';
                                    fileHtml += ' </label>';
                                    $(this).parent('.single_file_upload').append(fileHtml);
                                } else {
                                    $(this).parent('.single_file_upload').prepend('<span class="error_msg">jpg, png or gif file are allowed!!</span>');
                                }



                            });

                            $('.project_form').on('click', '.delete_img', function () {
                                var removeFile = $(this).attr('rel');
                                $("#" + removeFile).remove();
                                $(this).closest('.single_file').remove();
                            });

                            $('.project_form').on('change', '.select_category', function () {
                                var that = $(this);
                                $.ajax({
                                    url: "admin/pro/get_sub_category",
                                    type: 'POST',
                                    async: false,
                                    data: {category_id: that.val()},
                                    success: function (data) {
                                        if (that.parents('.project_form').find('.sub_category_area').find('.single_sub_category_area').html(data)) {
                                            that.parents('.project_form').find('.sub_category_area').show();
                                        }
                                    }
                                });
                            });



                            $('.project_form').on('click', '.add_another_license', function () {
                                var newHtml = $('.another_license_copy').html();
                                if ($('.company_license_area').append(newHtml)) {
                                    $('.company_license_area').last('.license_list ').find('.date_input input').datepicker({dateFormat: "yy-mm-dd"});
                                }
                                //             $( ".datepicker" ).datepicker();
                            });
                            $('.project_form').on('click', '.remove_license_list', function () {
                                $(this).closest('.license_list').remove();
                            });
                            $('.submit_request').click(function () {
                                $('#addPro').submit();
                            });
                            var options2 = {
                                target: 'pro/edit_pro_action', // target element(s) to be updated with server response 
                                beforeSubmit: showRequest, // pre-submit callback 
                                success: showResponse, // post-submit callback 
                                uploadProgress: function (event, position, total, percentComplete) {
                                    var percentVal = percentComplete + '%';
                                    $('.progress').show();
                                    $('.progress').find('.bar').width(percentVal)
                                    $('.progress').find('.percent').html(percentVal);
                                    //console.log(percentVal, position, total);
                                },

                                // other available options: 
                                //url:       url         // override for form's 'action' attribute 
                                type: 'post', // 'get' or 'post', override for form's 'method' attribute 
                                dataType: 'json'        // 'xml', 'script', or 'json' (expected server response type) 
                                        //clearForm: true        // clear all form fields after successful submit 
                                        //resetForm: true        // reset the form after successful submit 

                                        // $.ajax options can be used here too, for example: 
                                        //timeout:   3000 
                            };
                            // bind to the form's submit event 
                            $('#addPro').submit(function () {
                                // inside event callbacks 'this' is the DOM element so we first 
                                // wrap it in a jQuery object and then invoke ajaxSubmit 
                                $(this).ajaxSubmit(options2);
                                // !!! Important !!! 
                                // always return false to prevent standard browser submit and page navigation 
                                return false;
                            });


                            //        $(".SubCategoryCheck").each(function() {
                            //            if (!$(this).is(':checked')) {
                            //                $(this).closest('.single_sub_category').find('.services_list').remove();
                            //            }
                            //        });
                        });
                        // pre-submit callback 
                        function showRequest(formData, jqForm, options) {
                            $('.project_form').find('.error_msg').hide();

                            return true;
                        }

                        // post-submit callback 
                        function showResponse(responseText, statusText, xhr, $form) {

                            if (responseText.type == 'error') {
                                //            alert_r(responseText);
                                if (responseText.errorMsg.nameError) {
                                    $('.project_form').find('.nameError').html(responseText.errorMsg.nameError).show();
                                }
                                if (responseText.errorMsg.emailError) {
                                    $('.project_form').find('.emailError').html(responseText.errorMsg.emailError).show();
                                }
                                if (responseText.errorMsg.passwordError) {
                                    $('.project_form').find('.passwordError').html(responseText.errorMsg.passwordError).show();
                                }
                                if (responseText.errorMsg.phoneError) {
                                    $('.project_form').find('.phoneError').html(responseText.errorMsg.phoneError).show();
                                }
                                if (responseText.errorMsg.addressError) {
                                    $('.project_form').find('.addressError').html(responseText.errorMsg.addressError).show();
                                }
                                if (responseText.errorMsg.cityError) {
                                    $('.project_form').find('.cityError').html(responseText.errorMsg.cityError).show();
                                }
                                if (responseText.errorMsg.zipCodeError) {
                                    $('.project_form').find('.zipCodeError').html(responseText.errorMsg.zipCodeError).show();
                                }
                                if (responseText.errorMsg.companyNameError) {
                                    $('.project_form').find('.companyNameError').html(responseText.errorMsg.companyNameError).show();
                                }
                                if (responseText.errorMsg.categoryError) {
                                    $('.project_form').find('.categoryError').html(responseText.errorMsg.categoryError).show();
                                }
                                if (responseText.errorMsg.subCategoryError) {
                                    $('.project_form').find('.subCategoryError').html(responseText.errorMsg.subCategoryError).show();
                                }
                                if (responseText.errorMsg.expirDateError) {
                                    $('.project_form').find('.expirDateError').html(responseText.errorMsg.expirDateError).show();
                                }
                                if (responseText.errorMsg.licenseNumberError) {
                                    $('.project_form').find('.licenseNumberError').html(responseText.errorMsg.licenseNumberError).show();
                                }
                                if (responseText.errorMsg.licenseTypeError) {
                                    $('.project_form').find('.licenseTypeError').html(responseText.errorMsg.licenseTypeError).show();
                                }
                                if (responseText.errorMsg.termsConditionsError) {
                                    $('.project_form').find('.termsConditionsError').html(responseText.errorMsg.termsConditionsError).show();
                                }
                                if (responseText.errorMsg.stateError) {
                                    $('.project_form').find('.stateError').html(responseText.errorMsg.stateError).show();
                                }
//                                if (responseText.errorMsg.businessPhoneNumberError) {
//                                    $('.project_form').find('.businessPhoneNumberError').html(responseText.errorMsg.businessPhoneNumberError).show();
//                                }

                            } else if (responseText.type == 'success') {
                                $('.message_box').html('Successfully Updated').show();
                                setTimeout(function () {
                                    $('.message_box').fadeOut();
                                }, 3000);
                            }

                        }

                        function readURL(input, str) {

                            if (input.files && input.files[0]) {
                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    var picHtml = '<div class="single_file"><img alt="" width="50" src="' + e.target.result + '" /> '
                                    picHtml += '<a href="javascript:" rel="' + str + '" class="delete_img"><img alt="" src="img/cross_icon.png" /></a> </div>'
                                    $('.view_file').append(picHtml);
                                }

                                reader.readAsDataURL(input.files[0]);
                            }
                        }

                    </script>
                </div>
            </div>  
            <div class="clear"></div>
        </div>

    </div> <!-- End .content-box-content -->
    <div class="alert alert-success message_box">

    </div>
    <br/>

</div> <!-- End .content-box -->

<script type="text/javascript">
    function resetPassword(id) {
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'reset_pass_by_admin')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function (data) {
            if (data == 'success') {
                $('.message_box').html('Password successfully changed.').show();
                setTimeout(function () {
                    $('.message_box').fadeOut();
                }, 3000);
            }
        }, 'text');
    }
    function deleteClaimList(id) {
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'delete_claim_list')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function (data) {
            if (data == 'success') {
                $('.message_box').html('Claim successfully deleted.').show();
                setTimeout(function () {
                    $('.message_box').fadeOut();
                }, 3000);
                $('.deleteClaimList').remove();
            }
        }, 'text');
    }
</script>

<script>
    $(document).ready(function () {
        $('#addPackage').submit(function (e) {
            e.preventDefault();
            var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'pro_user_package')); ?>';
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                success: function (data) {
                    //alert(data);
                    $('.package_box').html(data).show();
                    setTimeout(function () {
                        $('.package_box').fadeOut();
                    }, 3000);
                }
            });
        });


        
        $("#block2").css({ 'height': $('#block1').outerHeight() + "px" });
    });

</script>