<script type="text/javascript">
    function deleteSingle(id) {
        var base_path = $("#base_path").val();
        if (confirm('Are you sure to delete this item?')) {
            var url = '<?php echo $this->Html->url(array('controller' => 'user_interests', 'action' => 'delete_interest')); ?>';
            var postData = {'id': id};
            $.post(url, postData, function(data) {
                if (data == 'success') {
                    $('#iD' + id).fadeOut();
                }
                else {
                    alert(data);
                }
            }, 'text');
        }
    }
</script>

<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Projects</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">

        <?php // echo $form->create("UserInterests", array("id" => "user_interest", "url" => BASE_PATH_ADMIN . "/user_interests/delete_all", "name" => "borough")); ?>
        <table>
            <thead>
                <tr>
                    <th colspan="4">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_unverified_project')); ?>" class="unverified <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_unverified_project') echo "selected"; ?> ">Unverified</a>
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_verified_project')); ?>" class="verified <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_verified_project') echo "selected"; ?> ">Verified</a>
                    </th>
                </tr>
                <tr>
                    <!--<th><input class="check-all" type="checkbox" /></th>-->
                    <!--<th>Id</th>-->
                    <th>Project Title</th>
                    <th>Description</th>
                    <th>Verify Project</th>
                    <th>Sent to Pro</th>
                    <!--<th>Actions</th>-->
                </tr>
            </thead>

            <tbody>
                <?php foreach ($projects as $project) { ?>
                    <tr id="project_<?php echo $project['Project']['id']; ?>">
                        <td>
                            <a target="_blank" href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'project_view', 'project_id' => $project['Project']['id'])); ?>">
                                <?php echo $project['Project']['project_title']; ?>
                            </a>

                        </td>
                        <td><?php echo $project['Project']['details']; ?></td>
                        <td>
                            <?php if ($project['Project']['is_verified']) { ?>
                                Verified
                            <?php } ?>
                            <!--<input type="checkbox" rel="<?php echo $project['Project']['id']; ?>" class="VerifyProject" <?php if ($project['Project']['is_verified']) { ?> checked <?php } ?> />-->
                        </td>
                        <td>
                            <a data-project-id="<?php echo $project['Project']['id']; ?>" class="btn btn-success sent_to_pros">Sent to Pro</a>
                        </td>    
<!--                        <td>
                             Icons 
                                <a href="/user_interests/edit_interest/<?php echo $project['Project']['id']; ?>" title="Edit">
                                <img src="admin_images/icons/pencil.png" alt="Edit" />
                            </a>
                            <a href="javascript:void(0);" title="Delete"  onClick="return deleteSingle('<?php echo $project['Project']['id']; ?>');">
                                <img src="admin_images/icons/cross.png" alt="Delete" />
                            </a> 
                        </td>-->
                    </tr>
                    
                    
                    <!-- Modal -->
        <div class="modal fade" id="projectModal_<?php echo $project['Project']['id']; ?>"  tabindex="-1" role="dialog" aria-labelledby="projectModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="project_sent_pros_<?php echo $project['Project']['id']; ?>" autocomplete="off" class="project_sent_pros">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Sent to Pro</h5>
                        </div>
                        <div class="modal-body">
                            <ul style="max-height: 250px; overflow-y: scroll;">
                                <?php foreach ($project['Project']['prousers'] as $prouser) { ?>
                                    <li><label>
                                            <input type="checkbox" value="<?php echo $prouser['User']['id'] ?>" name="sent_pro[]" />
                                            &nbsp; &nbsp;
                                            <?php echo (in_array($prouser['User']['id'], $project['Project']['specify'])) ? "<b style='color: #0a0'>" . $prouser['User']['name'] . "</b>" : $prouser['User']['name']; ?>
                                            <?php //echo $prouser['User']['name'] ?>
                                        </label>
                                    </li>
                                <?php } ?>
                            </ul>
                            <input type="hidden" name="project_id" value="<?php echo $project['Project']['id'] ?>" />
                            <input type="hidden" name="category_id" value="<?php echo $project['Project']['category_id'] ?>" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Send</button>
                        </div>
                    </form>  
                </div>
            </div>
        </div>
                <?php } ?>
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="6">

                        <?php if ($this->params['paging']['Project']['pageCount'] > 1) { ?>
                            <div class="pagination">
                                <ul>
                                    <li class="page-prev"><?php echo $this->Paginator->prev(); ?></li>
                                    <li><?php echo $this->Paginator->numbers(); ?></li>
                                    <li class="page-next"><?php echo $this->Paginator->next(); ?></li>
                                </ul>
                            </div> <!-- End .pagination -->
                        <?php } ?>
                        <div class="clear"></div>
                    </td>
                </tr>
            </tfoot>

        </table>
        <?php // $form->end(); ?>
    </div> <!-- End .content-box-content -->
</div> <!-- End .content-box -->
<script type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $('a.sent_to_pros').click(function (e) {
            e.preventDefault();
            var project_id = $(this).data("project-id");
            $('#projectModal_'+ project_id).modal('show');
            $('.msgclass').remove();
        });

        $('.project_sent_pros').submit(function (e) {
            e.preventDefault();
            $('.msgclass').remove();
            $.ajax({
                    url: "<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'project_sent_pros')); ?>",
                    type: 'POST',
                    async: false,
                    data: $(this).serialize(),
                    success: function (data) {
                        $(".project_sent_pros ul li input").prop('checked', false);
                        if (data == 'error') {
                            $(".modal-footer").prepend('<p class="msgclass"><label style="color:#ff0000">Select atleast one Pro.</label></p>');
                        } else {
                            $(".modal-footer").prepend('<p class="msgclass"><label style="color:#0a0">Sent Successfuly.</label></p>');
                            //$('.modal').modal('hide');
                        }
                    }
                });
        })
    })
</script>

