<?php
$blur_class = '';
if (!$this->Session->read('Auth.User.id')) {
    $blur_class = 'blurMode';
}
?>

<div class="dashboad_content por_finder_list_container <?php echo $blur_class; ?>">
    <div class="view_project">
        <div class="por_finder_list_area">
            <?php
            $category_id = $this->request->params['named']['category_id'];
            foreach ($pro_users as $pro_user) {
                ?>
                <div class="single_por_finder_list">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="company_info_area">
                                <div class="col-sm-3 col-md-2">
                                    <div class="company_logo">
                                        <?php if ($pro_user['User']['profile_pic']) { ?>
                                            <img alt="" src="uploaded_files/profile_pic/<?php echo $pro_user['User']['id']; ?>/<?php echo $pro_user['User']['profile_pic']; ?>" />
                                        <?php } else { ?>
                                            <img alt="" src="img/company_logo.jpg" />
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="company_details col-sm-6 col-md-7">
                                    <div class="company_name">
                                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'profile', $pro_user['ProUser']['user_id'], 'view_from' => 'profinderlist')) ?>">
                                            <?php echo $pro_user['ProUser']['company_name']; ?>
                                        </a>

                                    </div>
                                    <div class="clear"></div>
                                    <div class="company_moto">
                                        <?php echo $pro_user['ProUser']['company_description']; ?>
                                    </div>
                                    <div class="company_basic_info">
                                        <ul>
                                            <li>
                                                <img alt="" src="img/location_icon.png" />
                                                <span><?php echo $pro_user['ProUser']['city']; ?></span>
                                            </li>

                                            <li>
                                                <img alt="" src="img/verified_icon.png" />
                                                <span>
                                                    <?php if ($pro_user['project_count'] > 1) { ?>
                                                        <?php echo $pro_user['project_count']; ?> Projects
                                                    <?php } else { ?>
                                                        <?php echo $pro_user['project_count']; ?> Project
                                                    <?php } ?>
                                                </span>
                                            </li>
                                            <li class="user_review_area">
                                                <?php
                                                if ($pro_user['reviewCount']['Hire']['reviewCount']) {
                                                    $reviewPercent = ($pro_user['rating_sum']['Hire']['proRating'] / $pro_user['reviewCount']['Hire']['reviewCount']) * 20;
                                                } else {
                                                    $reviewPercent = 0;
                                                }
                                                ?>

                                                <div class="rating_area">
                                                    <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                                </div>
                                                <div class="review_text">
                                                    <?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> Reviews
                                                </div>

                                            </li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="verified_pro col-sm-3 col-md-3">
                                    <img alt="" src="img/pro_verified_icon.png"/>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="request_a_quote_btn_area">
                                <a  href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'submit_project_details', 'category_id' => $category_id, 'specified_pro' => $pro_user['ProUser']['user_id'])) ?>" class="request_quote common_button">REQUEST A QUOTE</a>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            <?php } ?>




            <?php if ($this->params['paging']['ProUser']['pageCount'] > 1) { ?>
                <div  style="text-align: center;">
                    <ul class="pagination">
                        <?php
                        echo $this->Paginator->first(__('First', true), array('tag' => 'li', 'escape' => false), array('type' => "button", 'class' => "btn btn-default"));
                        echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
                        echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                        echo $this->Paginator->last(__('Last', true), array('tag' => 'li', 'escape' => false), array('type' => "button", 'class' => "btn btn-default"));
                        ?>
                    </ul> 
                </div>

            <?php } ?>



        </div>
    </div>

</div>




<script type="text/javascript">
    $(function () {
<?php
if (!$this->Session->read('Auth.User.id')) {
    ?>
            $('.login_signin').modal({backdrop: 'static', keyboard: false});
            $('#SignInForm').hide();
            $('#SignUpForm').show();
<?php } ?>
    });
</script>