<?php
if (isset($from) && $from == 'project') {
    ?>
    <select class="select_service"  name="service_id">
        <option value="0">Select service</option>
        <?php foreach ($sub_category['Service'] as $service) { ?>
            <option value="<?php echo $service['id']; ?>" ><?php echo $service['service']; ?></option>
        <?php } ?>
    </select>
    <?php
} else {
    ?>
    <div class="services_list"  id="services_block_<?php echo $sub_category_id; ?>">
        <div class="services_heading">
            Select the services you offer of <?php echo $sub_category['SubCategory']['sub_category']; ?>
        </div>
        <div class="services">
            <?php
            foreach ($sub_category['Service'] as $service) {
                ?>
                <div class="single_services col-md-6">
                    <input name="services[<?php echo $service['sub_category_id'] ?>][]" id="services_<?php echo $service['id']; ?>" value="<?php echo $service['id']; ?>" type="checkbox" />
                    <label for="services_<?php echo $service['id']; ?>"></label>
                    <span>
                        <?php echo $service['service']; ?>
                    </span>
                    <div class="clear"></div>
                </div>
                <?php
            }
            ?>
            <div class="clear"></div>
        </div>

    </div>
    <?php
}
?>





