<div class="category_area">
    <div class="heading_area">
        <div class="main_heading">
            Reach Customers Near You & Grow Your Business
        </div>
        <div class="heading_border">

        </div>
        <div class="sub_main_heading">
            Along with sending project leads to you, we also setup your website and help you market your business
        </div>
    </div>

    <!--    <div class="col-sm-4 category">
            <a href="<?php echo $this->Html->Url(array('controller' => 'home', 'action' => 'lead_generation')); ?>">
                <div class="category_img">
                    <img alt="" width="100%" src="img/lead_generation.jpg" />
                </div>
                <div class="category_text">
                    <span class="category_real_text">
                        Lead Generation
                    </span>
                </div>
            </a>
        </div>
    
        <div class="col-sm-4 category">
            <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'web_service')); ?>">
                <div class="category_img">
                    <img alt="" width="100%" src="img/website_development_hosting.jpg" />
                </div>
                <div class="category_text">
                    <span class="category_real_text">
                        Website Development
                    </span>
                </div>
            </a>
        </div>
    
        <div class="col-sm-4 category">
            <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'social_media_marketing')); ?>">
                <div class="category_img">
                    <img alt="" width="100%" src="img/social_media_marketing.jpg" />
                </div>
                <div class="category_text">
                    <span class="category_real_text">
                        Social Media Marketing
                    </span>
                </div>
            </a>
        </div>
    
        <div class="col-sm-4 category">
            <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'graphic_design')); ?>">
                <div class="category_img">
                    <img alt="" width="100%" src="img/rewarding_partnerships.jpg" />
                </div>
                <div class="category_text">
                    <span class="category_real_text">
                        Graphic Design
                    </span>
                </div>
            </a>
        </div>
    
        <div class="col-sm-4 category">
            <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'email_marketing')); ?>">
                <div class="category_img">
                    <img alt="" width="100%" src="img/email_marketing.jpg" />
                </div>
                <div class="category_text">
                    <span class="category_real_text">
                        Email Marketing 
                    </span>
                </div>
            </a>
        </div>
    
        <div class="col-sm-4 category">
            <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'seo')); ?>">
                <div class="category_img">
                    <img alt="" width="100%" src="img/seo.jpg" />
                </div>
                <div class="category_text">
                    <span class="category_real_text">
                        SEO
                    </span>
                </div>
            </a>
        </div>
    
        <div class="clear"></div>-->



    <div class="pro_category_area">
        <div class="single_category">
            <div class="single_category_content">
                <a href="<?php echo $this->Html->Url(array('controller' => 'home', 'action' => 'lead_generation')); ?>">
                    <div class="category_image">
                        <img class="img-responsive" alt="" src="img/pro_category/lead-generation.png" />
                    </div>
                    <div class="category_text" title="LEAD GENERATION">
                        LEAD GENERATION
                    </div>
                </a>
            </div>
        </div>
        <div class="single_category">
            <div class="single_category_content">
                <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'web_service')); ?>">
                    <div class="category_image">
                        <img class="img-responsive" alt="" src="img/pro_category/website-development.png" />
                    </div>
                    <div class="category_text" title="WEBSITE DEVELOPMENT">
                        WEBSITE DEVELOPMENT
                    </div>
                </a>
            </div>
        </div>
        <div class="single_category">
            <div class="single_category_content">
                <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'social_media_marketing')); ?>">
                    <div class="category_image">
                        <img class="img-responsive" alt="" src="img/pro_category/social-media-marketing.png" />
                    </div>
                    <div class="category_text" title="SOCIAL MEDIA MARKETING">
                        SOCIAL MEDIA MARKETING
                    </div>
                </a>
            </div>
        </div>
        <div class="single_category">
            <div class="single_category_content">
                <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'graphic_design')); ?>">
                    <div class="category_image">
                        <img class="img-responsive" alt="" src="img/pro_category/graphic-design.png" />
                    </div>
                    <div class="category_text" title="GRAPHIC DESIGN">
                        GRAPHIC DESIGN
                    </div>
                </a>
            </div>
        </div>
        <div class="single_category">
            <div class="single_category_content">
                <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'email_marketing')); ?>">
                    <div class="category_image">
                        <img class="img-responsive" alt="" src="img/pro_category/email-marketing.png" />
                    </div>
                    <div class="category_text" title="EMAIL MARKETING">
                        EMAIL MARKETING
                    </div>
                </a>
            </div>
        </div>

        <div class="single_category">
            <div class="single_category_content">
                <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'seo')); ?>">
                    <div class="category_image">
                        <img class="img-responsive" alt="" src="img/pro_category/seo.png" />
                    </div>
                    <div class="category_text" title="SEO">
                        SEO
                    </div>
                </a>
            </div>
        </div>
    </div>

</div>



<div class="category_area">
    <div class="heading_area">
        <div class="main_heading">
            Learn how Homeprosnow fuels your business
        </div>

    </div>

    <div class="pro_video">
       <div class="vid_inner">
           <iframe width="725" height="433"  src="https://www.youtube.com/embed/<?php echo $landing_video['HomeprosVideo']['video_code']; ?>?rel=0&amp;theme=light&amp;frameborder=0" frameborder="0" allowfullscreen></iframe>
       </div>
   </div>
</div>
<div class="how_it_work_area pro_page">

    <div class="heading_area">
        <div class="main_heading">
            How It Works?
        </div>
        <div class="heading_border">

        </div>
        <div class="sub_main_heading">
            Register with Homeprosnow and start receiving quality leads for projects near you.
        </div>
    </div>
    <div class="how_it_work_type_area">
        <div class="how_it_work_type col-sm-4">
            <div class="type_image share_details_icon">
                <img alt="" src="img/publish_your_project.png" />
            </div>
            <div class="type_heading">
                Sign Up
            </div>
            <div class="type_text">
                Register on Homeprosnow
                with your details
            </div>
        </div>
        <div class="how_it_work_type col-sm-4">
            <div class="type_image responses_icon">
                <img alt="" src="img/get_responses.png" />
            </div>
            <div class="type_heading">
                Receive Leads
            </div>
            <div class="type_text">
                Receive quality leads and
                exchange details with clients
            </div>
        </div>
        <div class="how_it_work_type col-sm-4">
            <div class="type_image hire_icon">
                <img alt="" src="img/hire_confidently.png" />
            </div>
            <div class="type_heading">
                Get Hired
            </div>
            <div class="type_text">
                Grow your network and get
                hired to do what you love
            </div>
        </div>

        <div class="clear"></div>

    </div>
</div>


<div class="business_adding_area pro_bottom">

    <div class="heading_area">
        <div class="main_heading">
            Want to add your business?
        </div>
        <div class="heading_border">

        </div>
        <div class="sub_main_heading">
            Join thousands of pros in the United States who are growing with us.
        </div>
    </div>


    <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro')); ?>" class="common_button business_list_btn">GET STARTED</a> 
</div>

<section class="placeholder full_width">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-5 ">
                <div class="top_banner_head">
                    Get a fully managed custom website for your business
                </div>             
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="top_banner_img">
                    <img class="img img-responsive" src="images/placeholder_banner.png"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="top_banner_start_btn">
                    <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'web_service')); ?>" class="btn btn-warning start_btn">Free Sample Website</a>
                </div>
            </div>
        </div>
    </div>
</section>