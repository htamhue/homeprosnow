<div class="more_categorie">
    <ul>
        <?php
        foreach ($categories as $categorie) {
            ?>
            <li>
                <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'find_pro','category_id' => $categorie['Categorie']['id'])); ?>">
                    <div>
                        <?php echo $categorie['Categorie']['category']; ?>
                    </div>
                    <img alt="" src="uploaded_files/category_img/<?php echo $categorie['Categorie']['category_image']; ?>" />
                </a>
            </li>
            <?php
        }
        ?>
        
    </ul>
    <div class="clear"></div>
</div>