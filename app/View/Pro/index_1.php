<div class="category_area">
    <div class="heading_area">
        <div class="main_heading">
            Reach Customers Near You & Grow Your Business
        </div>
        <div class="heading_border">

        </div>
        <div class="sub_main_heading">
            Along with sending project leads to you, we also setup your website and help you market your business
        </div>
    </div>

    <div class="col-sm-4 category">
        <a href="<?php echo $this->Html->Url(array('controller' => 'home', 'action' => 'lead_generation')); ?>">
            <div class="category_img">
                <img alt="" width="100%" src="img/lead_generation.jpg" />
                <!--                    <div class="category_overlay">
                
                                    </div>-->
            </div>

            <div class="category_text">
                <span class="category_real_text">
                    Lead Generation
                </span>
            </div>
        </a>

    </div>
    <div class="col-sm-4 category">
        <a href="<?php echo $this->Html->Url(array('controller' => 'home', 'action' => 'rewarding_partnerships')); ?>">
            <div class="category_img">
                <img alt="" width="100%" src="img/rewarding_partnerships.jpg" />
                <!--                    <div class="category_overlay">
                
                                    </div>-->
            </div>

            <div class="category_text">
                <span class="category_real_text">
                    Rewarding Partnerships
                </span>
            </div>
        </a>

    </div>
    <div class="col-sm-4 category">
        <a href="<?php echo $this->Html->Url(array('controller' => 'home', 'action' => 'web_development')); ?>">
            <div class="category_img">
                <img alt="" width="100%" src="img/website_development_hosting.jpg" />
                <!--                    <div class="category_overlay">
                
                                    </div>-->
            </div>

            <div class="category_text">
                <span class="category_real_text">
                    Website Development& Hosting
                </span>
            </div>
        </a>

    </div>
    <div class="col-sm-4 category">
        <a href="<?php echo $this->Html->Url(array('controller' => 'home', 'action' => 'email_marketing')); ?>">
            <div class="category_img">
                <img alt="" width="100%" src="img/email_marketing.jpg" />
                <!--                    <div class="category_overlay">
                
                                    </div>-->
            </div>

            <div class="category_text">
                <span class="category_real_text">
                    Email Marketing 
                </span>
            </div>
        </a>
    </div>
    <div class="col-sm-4 category">
        <a href="<?php echo $this->Html->Url(array('controller' => 'home', 'action' => 'social_media_marketing')); ?>">
            <div class="category_img">
                <img alt="" width="100%" src="img/social_media_marketing.jpg" />
                <!--                    <div class="category_overlay">
                
                                    </div>-->
            </div>

            <div class="category_text">
                <span class="category_real_text">
                    Social Media Marketing
                </span>
            </div>
        </a>
    </div>
    <div class="col-sm-4 category">
        <a href="<?php echo $this->Html->Url(array('controller' => 'home', 'action' => 'seo')); ?>">
            <div class="category_img">
                <img alt="" width="100%" src="img/seo.jpg" />
                <!--                    <div class="category_overlay">
                
                                    </div>-->
            </div>

            <div class="category_text">
                <span class="category_real_text">
                    SEO
                </span>
            </div>
        </a>
    </div>


    <div class="clear"></div>

</div>



<div class="category_area">
    <div class="heading_area">
        <div class="main_heading">
            Learn how Homeprosnow fuels your business
        </div>

    </div>

    <div class="pro_video">
        <iframe width="725" height="433" src="https://www.youtube.com/embed/<?php echo $landing_video['HomeprosVideo']['video_code']; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
</div>
<div class="how_it_work_area pro_page">

    <div class="heading_area">
        <div class="main_heading">
            How It Works?
        </div>
        <div class="heading_border">

        </div>
        <div class="sub_main_heading">
            Register with Homeprosnow and start receiving quality leads for projects near you.
        </div>
    </div>
    <div class="how_it_work_type_area">
        <div class="how_it_work_type col-sm-4">
            <div class="type_image share_details_icon">
                <img alt="" src="img/publish_your_project.png" />
            </div>
            <div class="type_heading">
                Sign Up
            </div>
            <div class="type_text">
                Register on Homeprosnow
                with your details
            </div>
        </div>
        <div class="how_it_work_type col-sm-4">
            <div class="type_image responses_icon">
                <img alt="" src="img/get_responses.png" />
            </div>
            <div class="type_heading">
                Receive Leads
            </div>
            <div class="type_text">
                Receive quality leads and
                exchange details with clients
            </div>
        </div>
        <div class="how_it_work_type col-sm-4">
            <div class="type_image hire_icon">
                <img alt="" src="img/hire_confidently.png" />
            </div>
            <div class="type_heading">
                Get Hired
            </div>
            <div class="type_text">
                Grow your network and get
                hired to do what you love
            </div>
        </div>

        <div class="clear"></div>

    </div>
</div>


<div class="business_adding_area pro_bottom">
    
    <div class="heading_area">
        <div class="main_heading">
            Want to add your business?
        </div>
        <div class="heading_border">

        </div>
        <div class="sub_main_heading">
            Join thousands of pros in the United States who are growing with us.
        </div>
    </div>


    <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro')); ?>" class="common_button business_list_btn">GET STARTED</a> 
</div>

<section class="grow full_width">
    <div class="container">
        <div class="grow_heading">How do we help you grow?</div>
        <div class="grow_heading_text">
            Stay ahead of the competition by building your presence online so potential customers can find you easily. Our services drive more leads to your website so you can scale your business
            without having to worry about how to manage it.
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="grow_content">
                    <div class="grow_img">
                        <img class="img img-responsive" src="images/graphic.png"/>
                    </div>
                    <div class="grow_name">Graphic Design</div>
                    <div class="grow_text">
                        Create branded flyersbrochures, coupons, etc.with the help of our talented designers.
                    </div>
                    <div class="grow_button">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'web_service', 'action' => 'index')); ?>" class="btn btn-warning start_btn">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="grow_content">
                    <div class="grow_img">
                        <img class="img img-responsive" src="images/layer2.png"/>
                    </div>
                    <div class="grow_name">Search Marketing</div>
                    <div class="grow_text">
                        Get listed on all major search engines - Google, Bing, etc. so customers can find you easily.
                    </div>
                    <div class="grow_button">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'web_service', 'action' => 'index')); ?>" class="btn btn-warning start_btn">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="grow_content">
                    <div class="grow_img">
                        <img class="img img-responsive" src="images/social_media.png"/>
                    </div>
                    <div class="grow_name">Social Media</div>
                    <div class="grow_text">
                        Connect with customers on popular social media platforms and engage with them directly.
                    </div>
                    <div class="grow_button">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'web_service', 'action' => 'index')); ?>" class="btn btn-warning start_btn">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="grow_content">
                    <div class="grow_img">
                        <img class="img img-responsive" src="images/mail.png"/>
                    </div>
                    <div class="grow_name">Email Marketing</div>
                    <div class="grow_text">
                        Keep your customers updated with new offers & promotions via email marketing.
                    </div>
                    <div class="grow_button">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'web_service', 'action' => 'index')); ?>" class="btn btn-warning start_btn">LEARN MORE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>