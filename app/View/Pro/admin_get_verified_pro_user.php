<script type="text/javascript">
    function deleteSingle(id) {
        var base_path = $("#base_path").val();
        if (confirm('Are you sure to delete this item?')) {
            var url = '<?php echo $this->Html->url(array('controller' => 'user_interests', 'action' => 'delete_interest')); ?>';
            var postData = {'id': id};
            $.post(url, postData, function (data) {
                if (data == 'success') {
                    $('#iD' + id).fadeOut();
                } else {
                    alert(data);
                }
            }, 'text');
        }
    }

    function resetPassword(id) {
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'reset_pass_by_admin')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function (data) {
            if (data == 'success') {
                $('.resetPasswordMessage').removeClass('hidden');
            }
        }, 'text');
    }
    
    function deleteUser(id) {
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'delete_user_by_admin')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function (data) {
            if (data == 'success') {
                location.reload();
            }
        }, 'text');
    }
</script>

<div class="content-box"><!-- Start Content Box -->



    <div class="content-box-header">
        <h3>Pro</h3>
    </div> <!-- End .content-box-header -->

    <div class="resetPasswordMessage hidden">
        <br/>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong> Password successfully changed.
        </div>
        <br/>
    </div>

    <div class="content-box-content">
        <form method="get" action="<?php echo $this->webroot . 'admin/pro/get_verified_pro_user' ?>">
            <div class="form-group">
                <div class="col-sm-2">
                    <label style="font-size: 15px;">Pro User Search</label>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" value="<?php echo $_GET['name']; ?>" name="name" placeholder="Enter Name">
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control" value="<?php echo $_GET['description']; ?>" name="description" placeholder="Enter Description">
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="<?php echo $this->webroot . 'admin/pro/get_verified_pro_user'; ?>" type="submit" class="btn btn-primary">Clear</a>
                </div>
            </div>
            <p><br /></p>
        </form>





        <?php // echo $form->create("UserInterests", array("id" => "user_interest", "url" => BASE_PATH_ADMIN . "/user_interests/delete_all", "name" => "borough")); ?>
        <table>
            <thead>
                <tr>
                    <th colspan="3">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_verified_pro_user')); ?>" class="verified <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_verified_pro_user') echo "selected"; ?> ">Verified</a>
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_unverified_pro_user')); ?>" class="unverified <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_unverified_pro_user') echo "selected"; ?> ">Unverified</a>
                    </th>
                    <th>
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro_by_admin')); ?>" class=" ">Add pro</a>
                    </th>
                </tr>
                <tr>
                    <!--<th><input class="check-all" type="checkbox" /></th>-->
                    <!--<th>Id</th>-->
                    <th>Company Name</th>
                    <th>Company Logo</th>
                    <th>Description</th>
                    <th>Sponsored view count</th>
                    <th>Add Sponsored view count</th>
                    <th>Verify Pro</th>
                    <th>Reset Password</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($pro_users as $pro_user) { ?>
                    <tr id="pro_user_<?php echo $pro_user['ProUser']['id']; ?>">
                        <td>
                            <a  target="_blank" href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'pro_view', 'pro_id' => $pro_user['ProUser']['id'])); ?>">
                                <?php echo $pro_user['ProUser']['company_name']; ?>
                            </a>
                        </td>
                        <td>
                            <div class="live_company_pic_upload_area">
                                <form autocomplete="off"  action="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'live_profile_pic_upload_by_admin')); ?>" class="updateLiveProfilePic" >
                                    <input type="file" class="live_profile_pic_form" name="profile_pic" id="profile_pic_<?php echo $pro_user['User']['id'] ?>" />
                                    <input type="hidden" name="user_id" value="<?php echo $pro_user['User']['id'] ?>" />
                                    <?php
                                    if (isset($pro_user['User']['profile_pic'])) {
                                        ?>
                                        <label for="profile_pic_<?php echo $pro_user['User']['id'] ?>">
                                            <img id="pro_pic_<?php echo $pro_user['User']['id'] ?>" alt=""  width="50" src="uploaded_files/profile_pic/<?php echo $pro_user['User']['id']; ?>/<?php echo $pro_user['User']['profile_pic']; ?>" />
                                        </label>

                                        <?php
                                    } else {
                                        ?>
                                        <label for="profile_pic_<?php echo $pro_user['User']['id'] ?>">
                                            <img alt="" id="pro_pic_<?php echo $pro_user['User']['id'] ?>" for="profile_pic_<?php echo $pro_user['User']['id'] ?>" width="50" src="img/company_logo.jpg" />
                                        </label>

                                        <?php
                                    }
                                    ?>

                                </form>

                            </div>

                        </td>
                        <td><?php echo $pro_user['ProUser']['company_description']; ?></td>
                        <td><?php echo $pro_user['ProPackage']['ProUserPackage']['view_count']; ?></td>
                        <td><a data-cid="<?php echo $pro_user['ProUser']['category_id']; ?>" data-uid="<?php echo $pro_user['ProUser']['id']; ?>" href="" class="btn btn-success new_package">Add</a></td>              
                        <td>
                            <?php //echo ($pro_user['ProPackage']['ProUserPackage']['package']) ? 'Package:'.$pro_user['ProPackage']['ProUserPackage']['package'] : '' ; ?>
                        </td>
                        <td>
                            <?php if ($pro_user['ProUser']['is_verified']) { ?>
                                Verified
                            <?php } ?>
                            <input type="checkbox" rel="<?php echo $pro_user['ProUser']['id']; ?>" class="VerifyProUser" <?php if ($pro_user['ProUser']['is_verified']) { ?> checked <?php } ?> />
                        </td>
                        <td>
                            <a href="javascript:" onclick="resetPassword(<?php echo $pro_user['ProUser']['id']; ?>)" class="btn btn-success">
                                Reset Password
                            </a>
                            <br/>
                            <br/>
                            <a href="javascript:" onclick="deleteUser(<?php echo $pro_user['User']['id']; ?>)" class="btn btn-success">
                                Delete
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'admin_edit_pro_by_admin', $pro_user['User']['id'])); ?>">Edit</a>
                        </td>
    <!--                        <td>
                             Icons 
                                <a href="/user_interests/edit_interest/<?php echo $project['Project']['id']; ?>" title="Edit">
                                <img src="admin_images/icons/pencil.png" alt="Edit" />
                            </a>
                            <a href="javascript:void(0);" title="Delete"  onClick="return deleteSingle('<?php echo $pro_user['ProUser']['id']; ?>');">
                                <img src="admin_images/icons/cross.png" alt="Delete" />
                            </a> 
                        </td>-->
                    </tr>
                    
                    
                    
                    
                     <!-- Modal -->
        <div class="modal fade" id="packageModal_<?php echo $pro_user['ProUser']['id']; ?>"  tabindex="-1" role="dialog" aria-labelledby="packageModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form autocomplete="off" class="addPackage">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Sponsored view count</h5>
                            <!--                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>-->
                        </div>
                        <div class="modal-body">
                            <div style="display: none;" class="alert alert-success package_box"></div>
                            <div class="form-group">
                                
                                
                             <?php        
                        if($pro_user['NewPackage']['ProUserNewPackage']['package_home'] == 1) {
                            $package_text_home = '<span style="color: #0a0">(Old Package : Package 1)</span>';
                        } else if($pro_user['NewPackage']['ProUserNewPackage']['package_home'] == 2) {
                            $package_text_home = '<span style="color: #0a0">(Old : Package 2)</span>';
                        } else if($pro_user['NewPackage']['ProUserNewPackage']['package_home'] == 3) {
                            $package_text_home = '<span style="color: #0a0">(Old Package : Package 3)</span>';
                        }else {
                            $package_text_home = '';
                        }

                        
                        if($pro_user['NewPackage']['ProUserNewPackage']['package'] == 1) {
                            $package_text = '<span style="color: #0a0">(Old Package : Package 1)</span>';
                        } else if($pro_user['NewPackage']['ProUserNewPackage']['package'] == 2) {
                            $package_text = '<span style="color: #0a0">(Old : Package 2)</span>';
                        } else if($pro_user['NewPackage']['ProUserNewPackage']['package'] == 3) {
                            $package_text = '<span style="color: #0a0">(Old Package : Package 3)</span>';
                        }else {
                            $package_text = '';
                        }
                        
                        
                        ?>
                                
                                
                                
                                
                                <label for="package">Choose Package Profile Page <?php echo $package_text; ?></label>
                                <select name="package" class="form-control  select_category">
                                    <option value="0">None</option>
                                    <option value="1">Package 1: $50 - Gets the contractor 100 views</option>
                                    <option value="2">Package 2: $75 - Gets the contractor 200 views</option>
                                    <option value="3">Package 3: $100 - Gets the contractor 300 views</option>
                                </select>
                                
                                <input type="hidden" name="pro_user_id" value="">
                                <input type="hidden" name="category_id" value="">
                            </div>
                            <div class="form-group">
                                <label for="package_home">Choose Package Home Page <?php echo $package_text_home; ?></label>
                                <select name="package_home" class="form-control  select_category">
                                    <option value="0">None</option>
                                    <option value="1">Package 1: $50 - Gets the contractor 100 views</option>
                                    <option value="2">Package 2: $75 - Gets the contractor 200 views</option>
                                    <option value="3">Package 3: $100 - Gets the contractor 300 views</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="package_universal_profile">Universal Package Profile Page&nbsp;&nbsp;&nbsp; 
                                    <input type="checkbox" value="1"  
                                           <?php echo ($pro_user['ProUser']['package_universal_profile'] == 1) ? 'checked' : ''; ?> 
                                           name="package_universal_profile" /> </label>
                            </div>
                            <div class="form-group">
                                <label for="package_universal">Universal Package Home Page&nbsp;&nbsp;&nbsp; 
                                    <input type="checkbox" value="1" 
                                    <?php echo ($pro_user['ProUser']['package_universal'] == 1) ? 'checked' : ''; ?> 
                                           name="package_universal" /> </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>  
                </div>
            </div>
        </div>
                    
                <?php } ?>
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="6">


                        <?php if ($this->params['paging']['ProUser']['pageCount'] > 1) { ?>
                            <div class="pagination">
                                <ul>
                                    <li class="page-prev"><?php echo $this->Paginator->prev(); ?></li>
                                    <li><?php echo $this->Paginator->numbers(); ?></li>
                                    <li class="page-next"><?php echo $this->Paginator->next(); ?></li>
                                </ul>
                            </div> <!-- End .pagination -->
                        <?php } ?>
                        <div class="clear"></div>
                    </td>
                </tr>
            </tfoot>

        </table>



       

<?php // $form->end();  ?>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->


<!-- Modal -->



<script type="text/javascript">
    $(function () {

        var options = {
            type: 'post',
            dataType: 'json',
            beforeSubmit: function (arr, $form, options) {
                $('.error_msg').hide();
            },
            success: responsefunc
        };

        $('.updateLiveProfilePic').submit(function () {
            $(this).ajaxSubmit(options);
            //$(".addpictureblock").modal('hide');
            return false;
        });


        $(document).on('change', '.live_profile_pic_form', function () {
            var that = $(this);
            that.closest('form').submit();

        });




    });

    function responsefunc(responseText, statusText, xhr, $form) {
        //        alert_r(responseText);
        if (responseText.type == 'error') {
            $('.emailError').html(responseText.error.emailError).show();
            $('.imageError').html(responseText.error.imageError).show();
        } else {
            var path = 'uploaded_files/profile_pic/' + responseText.user.User.id + '/' + responseText.user.User.profile_pic;
            $('#pro_pic_' + responseText.user.User.id).attr('src', path);
        }
    }



//             .on('change', '.live_profile_pic_form', function () {
//            var that = $(this);
//            that.closest('form').submit();
//
//        });


</script>

<script type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $('a.new_package').click(function (e) {
            e.preventDefault();
            var uid = $(this).data("uid");
            var cid = $(this).data('cid');
            $('[name=pro_user_id]').val(uid);
            $('[name=category_id]').val(cid);
            $('#packageModal_'+ uid).modal('show');
        });

        $('.addPackage').submit(function (e) {
            e.preventDefault();
            var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'pro_user_package')); ?>';
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                success: function (data) {
                    $('<p class="pe"></p>').insertAfter('[name=package_universal]');
                    if (data == 'Error') {
                        $('.pe').remove();
                        $('<p class="pe" style="color: #ff0000;">Choose a Package</p>').insertAfter('[name=package_universal]');
                    } else {
                        $('.pe').remove();
                        $('.package_box').html(data).show();
                        setTimeout(function () {
                            $('.package_box').fadeOut();
                            $('.modal').modal('hide');
                        }, 3000);
                    }

                }
            });
        });
    })
</script>

