<div class="dashboad_content">
    <div class="view_project">
        <div class="col-md-9 porject_details_area">
            <div class="project_details_info">
                <ul>
                    <li>
                        <img alt="" src="img/pro_needed_icon.png"/>
                        <div class="project_type">
                            <?php echo $project['Categorie']['category']; ?>
                            <br/>
                            <span>PRO NEEDED</span>
                        </div>

                    </li>
                    <li>
                        <img alt="" src="img/budget_icon.png"/>
                        <div class="project_type">
                            $<?php echo $project['Project']['budget']; ?>
                            <br/>
                            <span>Budget</span>
                        </div>

                    </li>
                    <li>
                        <img alt="" src="img/start_icon.png"/>
                        <div class="project_type">
                            <?php echo $project['Project']['start_date']; ?>
                            <br/>
                            <span>Start Date</span>
                        </div>

                    </li>
                    <?php if ($project['Project']['is_verified']) { ?>
                        <li class="project_verified">
                            <img alt="" src="img/project_verified_icon.png"/>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="project_details">
                <div class="project_details_heading">
                    Project Title
                </div>
                <div class="project_details_text">
                    <?php echo $project['Project']['project_title']; ?>
                </div>
                <div class="project_details_heading">
                    Project Details
                </div>
                <div class="project_details_text">
                    <?php echo $project['Project']['details']; ?>
                </div>

                <div class="project_details_heading">
                    DOES THE CLIENT HAVE THE MATERIALS NEEDED?
                </div>
                <div class="project_details_text">
                    <?php echo $project['Project']['materials']; ?>
                </div>
                <div class="project_details_heading">
                    Photo
                </div>
                <div class="project_details_photo">
                    <?php
                    foreach ($project['ProjectFile'] as $projectFile) {
                        ?>
                        <img alt="" src="<?php echo $projectFile['file']; ?>" width="100"/>

                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 user_details_area">
            <div class="userName_photo">
                <div class="user_photo">
                    <?php if ($project['User']['profile_pic']) { ?>
                        <img alt="" src="uploaded_files/profile_pic/<?php echo $project['User']['id']; ?>/<?php echo $project['User']['profile_pic']; ?>"/>
                    <?php } else { ?>
                        <img alt="" src="img/default_pro_pic.png"/>
                    <?php } ?>

                </div>
                <div class="user_name">
                    <?php echo $project['User']['name']; ?>
                </div>
                <div class="user_reviews">

                    <?php
                    $reviewPercent = ($project['rating_sum']['Hire']['userRating'] / $project['reviewCount']['Hire']['reviewCount']) * 20;
                    ?>
                    <div class="rating_area">
                        <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                    </div>
                    <div class="review_text">
                        <?php echo $project['reviewCount']['Hire']['reviewCount'] ?> Reviews
                    </div>


                    <div class="clear"></div>
                </div>
                <?php if (!empty($project['user_sent_quote']) && $project['user_sent_quote']['ProjectQuote']['status'] == 'open' ) { ?>
                    <a href="javascript:" class="common_button send_quote">
                        Quote sent
                    </a>
                <?php } else if (!empty($project['user_sent_quote']) && $project['user_sent_quote']['ProjectQuote']['status'] == 'accepted' ) { ?>
                    <a href="javascript:" class="common_button send_quote">
                        You are hired
                    </a>
                <?php } else if (!empty($project['user_sent_quote']) && $project['user_sent_quote']['ProjectQuote']['status'] == 'declined' ) { ?>
                    <a href="javascript:" class="common_button send_quote">
                        Declined
                    </a>
                <?php } else if (!empty($project['user_sent_quote']) && $project['user_sent_quote']['ProjectQuote']['status'] == 'cancelled' ) { ?>
                    <a href="javascript:" class="common_button send_quote">
                        Cancelled
                    </a>
                <?php } else if ($this->Session->read('Auth.User.user_type') == 'pro') { ?>
                    <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'submit_a_quote', 'project_id' => $project['Project']['id'])); ?>" class="common_button send_quote">
                        Send Quote
                    </a>
                <?php } ?>
            </div>
            <div class="user_contact">
                <div class="contact_heading">
                    Email
                </div>
                <div class="contact_address">
                    <?php echo $project['User']['email']; ?>
                </div>
                <div class="contact_heading">
                    PHONE NUMER
                </div>
                <div class="contact_address">
                    <?php echo $project['User']['phone']; ?>
                </div>
            </div>

            <div class="user_project_hire">
                <div class="project_count">
                    Project
                    <br/>
                    <?php echo $projectCount; ?>
                </div>
                <div class="hire_count">
                    Hires
                    <br/>
                    <?php echo $project['hire_count']; ?>
                </div>
                <div class="clear"></div>
            </div>

        </div>
        <div class="clear"></div>
    </div>

</div>