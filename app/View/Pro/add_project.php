<div class="start_a_projects">
    <?php
    if ($search_result) {
        ?>
        <div class="search_result_area">
            <div class="search_result_heading">
                Search Results
            </div>
            <div class="search_result">
                We found <span><?php echo $search_result . ' ' . $current_category['Categorie']['category']; ?> </span> near you
            </div>
        </div>
    <?php } ?>
    <div class="get_quotes_area">
        <div class="get_quotes_heading">
            Get Quotes from Top Professionals Near You
        </div>
        <div class="get_quotes_details">
            <div class="col-md-3 get_quotes_link">
                <div class="get_quotes_link_heading">
                    How to get quotes?
                </div>
                <div class="how_it_work_type">
                    <div class="type_image share_details_icon">
                        <img alt="" src="img/share_details_icon.png" />
                    </div>
                    <div class="type_heading">
                        Share details of your project
                    </div>
                    <div class="type_text">
                        We will match your requirements to
                        the right pros
                    </div>
                </div>
                <div class="how_it_work_type ">
                    <div class="type_image responses_icon">
                        <img alt="" src="img/responses_icon.png" />
                    </div>
                    <div class="type_heading">
                        Get responses within minutes
                    </div>
                    <div class="type_text">
                        See their cost estimate and ask
                        more questions
                    </div>
                </div>
                <div class="how_it_work_type ">
                    <div class="type_image hire_icon">
                        <img alt="" src="img/hire_icon.png" />
                    </div>
                    <div class="type_heading">
                        Hire with confidence
                    </div>
                    <div class="type_text">
                        Book the one you want based on their
                        price, portfolio and reviews
                    </div>
                </div>
            </div>
            <form autocomplete="off" action="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_project_action')); ?>" id="addProject" >

                <div class="col-md-9 project_form">
                    <div class="project_details_heading">
                        Project Details
                    </div>

                    <div class="input_list" <?php if ($zip_code) echo 'style="display: none;"'; ?>>
                        <div class="input_text">
                            Please enter your zip code
                        </div>
                        <input type="text" name="zip_code" class="zip_code_input" value="<?php echo $zip_code; ?>" />
                        <div class="zipCodeError error_msg"></div>
                    </div>

                    <div class="input_list">
                        <div class="input_text">
                            Select Category
                        </div>
                        <select class="select_category" name="category_id">
                            <option value="0">Select category</option>
                            <?php foreach ($categories as $category) { ?>
                                <option value="<?php echo $category['Categorie']['id'] ?>" ><?php echo $category['Categorie']['category'] ?></option>
                            <?php } ?>
                        </select>
                        <div class="categoryError error_msg"></div>
                    </div>

                    <div class="input_list subcategory_area">
                        <div class="input_text">
                            Select sub-category
                        </div>
                        <div class="sub_category_input">

                        </div>
                        <div class="subCategoryError error_msg"></div>
                    </div>



                    <div class="input_list">
                        <div class="input_text">
                            Project Title
                        </div>
                        <input type="text" name="project_title" class="project_materials" />
                        <div class="projectTitleError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            What do you need done? Provide as much information as you can for the contractor to give
                            you a cost estimate
                        </div>
                        <textarea class="project_info" name="details"></textarea>
                        <div class="detailsError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Do you have the materials needed for this project?
                        </div>
                        <input type="text" name="materials" class="project_materials" />
                        <div class="materialsError error_msg"></div>
                    </div>

                    <div class="input_list">
                        <div class="input_text">
                            How soon do you want the work to start?
                        </div>
                        <input type="text" name="start_date" class="project_materials" />
                        <div class="startDateError error_msg"></div>
                    </div>

                    <div class="input_list">
                        <div class="input_text">
                            What is your budget for this job?
                        </div>
                        <div class="budget_input">
                            <input type="text" name="budget" placeholder="" class="project_budget" />
                            <div class="doller_icon">
                                $
                            </div>
                        </div>
                        <div class="budgetError error_msg"></div>
                    </div>
                    <div class="input_list specific_contractors_area">
                        <div class="input_text">
                            Do you want this project to be sent to any specific contractors? <span>(Include their names below)</span>
                        </div>
                        <input type="text" class="specific_contractors" />
                        <div class="specific_contractors_searches_area">

                        </div>
                        <div class="specific_contractors_searched_area">
                            <?php if ($specified_pro) { ?>
                                <div class="single_searched_contractor">

                                    <input type="hidden" name="specified_pros[]" value="<?php echo $specified_pro['User']['id']; ?>"/>
                                    <span class="contractor_name"><?php echo $specified_pro['User']['name']; ?></span>
                                    <a href="javascript:" class="delete_contractor">
                                        <img alt="" src="img/cross_icon.png"/>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="file_upload_area">
                        <div class="upload_file_info">
                            Add relevant images to make your project requirements clearer. <span>(15mb max file size)</span> 
                        </div>
                        <div class="view_file">

                        </div>
                        <div class="clear"></div>
                        <div class="single_file_upload">
                            <input type="file" name="project_file[]" id="project_attachment_1" class="project_attachment" />
                            <label for="project_attachment_1" class="common_button project_attachment_btn">
                                <img alt="" src="img/attachment_icon.png" />
                                Add Attachment
                            </label>
                        </div>
                    </div>

                    <div class="project_details_heading">
                        Contact Details
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Your full name
                        </div>
                        <input type="text" value="<?php echo $user['User']['name'] ?>" <?php if ($user['User']['name']) { ?> disabled <?php } ?> name="name" class="project_user_name" />
                        <?php if ($user['User']['name']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['name'] ?>"  name="name" class="project_user_name" />
                        <?php } ?> 
                        <div class="nameError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Email Address
                        </div>
                        <input type="text" value="<?php echo $user['User']['email'] ?>" <?php if ($user['User']['email']) { ?> disabled <?php } ?> name="email" class="project_user_email" />
                        <?php if ($user['User']['email']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['email'] ?>"  name="email" class="project_user_email" />
                        <?php } ?>
                        <div class="emailError error_msg"></div>
                    </div>
                    <?php if (empty($user)) { ?>
                        <div class="input_list">
                            <div class="input_text">
                                Password
                            </div>
                            <input type="password" name="password" class="project_user_phone" />
                            <div class="passwordError error_msg"></div>
                        </div>
                    <?php } ?>
                    <div class="input_list">
                        <div class="input_text">
                            Phone Number
                        </div>
                        <input type="text" value="<?php echo $user['User']['phone'] ?>" <?php if ($user['User']['phone']) { ?> disabled <?php } ?>  name="phone" class="project_user_phone" />
                        <?php if ($user['User']['phone']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['phone'] ?>"   name="phone" class="project_user_phone" />
                        <?php } ?> 
                        <div class="phoneError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Address
                        </div>
                        <input type="text" value="<?php echo $user['User']['address'] ?>" <?php if ($user['User']['address']) { ?> disabled <?php } ?>  name="address" class="project_user_address" />
                        <?php if ($user['User']['address']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['address'] ?>"   name="address" class="project_user_address" />
                        <?php } ?> 
                        <div class="addressError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            City
                        </div>
                        <input type="text" value="<?php echo $user['User']['city'] ?>" <?php if ($user['User']['city']) { ?> disabled <?php } ?>  name="city" class="project_user_city" />
                        <?php if ($user['User']['city']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['city'] ?>"   name="city" class="project_user_city" />
                        <?php } ?> 
                        <div class="cityError error_msg"></div>
                    </div>

                    <div class="input_list">
                        <div class="input_text">
                            State
                        </div>
                        <select <?php if ($user['User']['state']) { ?> disabled <?php } ?> name="state" class="form-control" >
                            <option value="">Select State</option>
                            <?php
                            foreach ($state_lists as $key => $val) {
                                ?>
                                <option <?php echo ($val == $user['User']['state'] ? 'selected' : ''); ?> value="<?php echo $val; ?>">
                                    <?php echo $val; ?>
                                </option>
                            <?php } ?>
                        </select>
                        <?php if ($user['User']['state']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['state'] ?>"   name="state" class="project_user_country" />
                        <?php } ?> 
                        <div class="stateError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Country
                        </div>

                        <select  name="country" <?php if ($user['User']['country']) { ?> disabled <?php } ?>  class="countryInput" >
                            <option value="">Select Country</option>
                            <?php
                            foreach ($countries as $key => $val) {
                                ?>
                                <option <?php echo ($val['Country']['nicename'] == $user['User']['country'] ? 'selected' : ''); ?> value="<?php echo $val['Country']['nicename']; ?>">
                                    <?php echo $val['Country']['nicename']; ?>
                                </option>
                            <?php } ?>
                        </select>

                        <?php if ($user['User']['country']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['country'] ?>"   name="country" class="project_user_country" />
                        <?php } ?>

                        <div class="countryError error_msg"></div>
                    </div>
                    <div class="input_list">
                        <div class="input_text">
                            Zip Code
                        </div>
                        <input type="text" value="<?php echo $zip_code ?>" <?php if ($user['User']['zip_code']) { ?> disabled <?php } ?>  name="zip_code" class="project_user_zip_code" />
                        <?php if ($user['User']['zip_code']) { ?> 
                            <input type="hidden" value="<?php echo $user['User']['zip_code'] ?>"   name="zip_code" class="project_user_zip_code" />
                        <?php } ?> 
                        <div class="zipCodeError error_msg"></div>
                    </div>

                    <div class="term_condition_area">
                        <input type="checkbox" name="terms_conditions" value="1" id="term_condition_checkbox"/>
                        <label class="term_condition_label" for="term_condition_checkbox"></label>
                        <span class="term_condition">
                            I accept the Homeprosnow hiring terms and conditions
                        </span>
                        <div class="clear"></div>
                        <div class="termsConditionsError error_msg"></div>
                    </div>

                    <a href="javascript:" class="submit_request common_button">
                        Submit Request
                    </a>
                </div>
                <input type="submit" value="submit"  style="display: none;"/>
            </form> 
            <div class="clear"></div>
        </div>
    </div>

</div>

<script type="text/javascript">

    var category_id = <?php echo $this->request->params['named']['category_id'] ? $this->request->params['named']['category_id'] : 0; ?>;
    var subcategory_id = <?php echo $this->request->params['named']['sub_category_id'] ? $this->request->params['named']['sub_category_id'] : 0; ?>;

    $(function() {



        $('.input_list').on('change', '.select_category', function() {
            var that = $(this);
            category_id = parseInt(that.val());

            if (!category_id) {
                that.closest('.project_form').find('.sub_category_input').html('');
                that.closest('.project_form').find('.subcategory_area').hide();

            } else {
                $.ajax({
                    url: "pro/get_sub_category",
                    type: 'POST',
                    data: {category_id: that.val(), from: 'project'},
                    success: function(data) {
                        that.closest('.project_form').find('.sub_category_input').html(data);
                        that.closest('.project_form').find('.subcategory_area').show();
                        if (subcategory_id) {
                            $('.input_list .select_sub_category').val(subcategory_id).change();
                        }
                    }
                });
            }
        });
        $('.input_list .select_category').val(category_id).change();

        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd"
        });
        $('.project_form').on('change', '.project_attachment', function() {
            $('.single_file_upload').find('.error_msg').remove();
            var that = $(this);
            var str = that.attr('id');
            var res = str.substring(19);
            var countVal = parseInt(res) + 1;

            var file_extension_array = ["jpg", "png", "gif"];
            var file_extension = that.val().split('.').pop();
            if ($.inArray(file_extension.toLowerCase(), file_extension_array) !== -1) {
                readURL(this, str);
                $("[for=" + str + "]").remove();
                var fileHtml = '<input type="file" name="project_file[]" id="project_attachment_' + countVal + '" class="project_attachment" />';
                fileHtml += '<label for="project_attachment_' + countVal + '" class="common_button project_attachment_btn">';
                fileHtml += '<img alt="" src="img/attachment_icon.png" />';
                fileHtml += ' Add Attachment';
                fileHtml += ' </label>';
                $('.single_file_upload').append(fileHtml);
            } else {
                $('.single_file_upload').prepend('<span class="error_msg">jpg, png or gif file are allowed!!</span>');
            }

        });

        $('.project_form').on('click', '.delete_img', function() {
            var removeFile = $(this).attr('rel');
            $("#" + removeFile).remove();
            $(this).closest('.single_file').remove();
        });
        $('.project_form').on('keyup', '.specific_contractors', function() {

            var that = $(this);
            var value = that.val();

            $.ajax({
                url: "pro/search_pro",
                type: 'POST',
                async: false,
                data: {name: value, category_id: category_id},
                success: function(data) {
                    that.closest('.specific_contractors_area').find('.specific_contractors_searches_area').html(data).show();

                }
            });
        });
        $('.project_form').on('click', function(evt) {
            if (!$(evt.target).is(".select_contractor")) {
                $('.specific_contractors_searches_area').hide();
                $(this).val('');
            }

        });

        $('.project_form').on('click', '.select_contractor', function() {
            var pro_user_id = $(this).attr('pro_user_id');
            var pro_user_name = $(this).attr('pro_user_name');
            var html = '<div class="single_searched_contractor">';
            html += '<input type="hidden" name="specified_pros[]" value="' + pro_user_id + '"/>';
            html += '<span class="contractor_name">' + pro_user_name + '</span>';
            html += '<a href="javascript:" class="delete_contractor">';
            html += '<img alt="" src="img/cross_icon.png"/>';
            html += '</a>';
            html += '</div>';
            $(this).closest('.specific_contractors_area').find('.specific_contractors_searched_area').prepend(html);
            $(this).closest('.specific_contractors_searches_area').hide();
            $(this).closest('.specific_contractors_area').find('.specific_contractors').val('');
        });
        $('.project_form').on('click', '.delete_contractor', function() {
            $(this).closest('.single_searched_contractor').remove();
        });
//        $( ".specific_contractors" ).keypress(function() {
//            alert($(this).val());
//        });



        $('.submit_request').click(function() {
            if ($('.zip_code_input').val($('.zip_code_search').val())) {
                $('#addProject').submit();
            }

        });
        var options = {
            target: 'pro/add_pro_action', // target element(s) to be updated with server response 
            beforeSubmit: showRequest, // pre-submit callback 
            success: showResponse, // post-submit callback 

            // other available options: 
            //url:       url         // override for form's 'action' attribute 
            type: 'post', // 'get' or 'post', override for form's 'method' attribute 
            dataType: 'json'        // 'xml', 'script', or 'json' (expected server response type) 
                    //clearForm: true        // clear all form fields after successful submit 
                    //resetForm: true        // reset the form after successful submit 

                    // $.ajax options can be used here too, for example: 
                    //timeout:   3000 
        };
        // bind to the form's submit event 
        $('#addProject').submit(function() {
            // inside event callbacks 'this' is the DOM element so we first 
            // wrap it in a jQuery object and then invoke ajaxSubmit 
            $(this).ajaxSubmit(options);
            // !!! Important !!! 
            // always return false to prevent standard browser submit and page navigation 
            return false;
        });
        $('#successModal').on('hidden.bs.modal', function() {
            window.location.href = "<?php echo $this->Html->Url(array('controller' => 'home')); ?>";
        })

    });

    function showRequest(formData, jqForm, options) {
        $('.project_form').find('.error_msg').hide();

        return true;
    }

// post-submit callback 
    function showResponse(responseText, statusText, xhr, $form) {
//        alert_r(responseText);
        if (responseText.type == 'error') {
//            alert_r(responseText);
            if (responseText.errorMsg.nameError) {
                $('.project_form').find('.nameError').html(responseText.errorMsg.nameError).show();
            }
            if (responseText.errorMsg.emailError) {
                $('.project_form').find('.emailError').html(responseText.errorMsg.emailError).show();
            }
            if (responseText.errorMsg.categoryError) {
                $('.project_form').find('.categoryError').html(responseText.errorMsg.categoryError).show();
            }
            if (responseText.errorMsg.subCategoryError) {
                $('.project_form').find('.subCategoryError').html(responseText.errorMsg.subCategoryError).show();
            }

            if (responseText.errorMsg.phoneError) {
                $('.project_form').find('.phoneError').html(responseText.errorMsg.phoneError).show();
            }

            if (responseText.errorMsg.projectTitleError) {
                $('.project_form').find('.projectTitleError').html(responseText.errorMsg.projectTitleError).show();
            }
            if (responseText.errorMsg.detailsError) {
                $('.project_form').find('.detailsError').html(responseText.errorMsg.detailsError).show();
            }
            if (responseText.errorMsg.materialsError) {
                $('.project_form').find('.materialsError').html(responseText.errorMsg.materialsError).show();
            }
            if (responseText.errorMsg.startDateError) {
                $('.project_form').find('.startDateError').html(responseText.errorMsg.startDateError).show();
            }
            if (responseText.errorMsg.budgetError) {
                $('.project_form').find('.budgetError').html(responseText.errorMsg.budgetError).show();
            }
            if (responseText.errorMsg.termsConditionsError) {
                $('.project_form').find('.termsConditionsError').html(responseText.errorMsg.termsConditionsError).show();
            }
            if (responseText.errorMsg.zipCodeError) {
                $('.project_form').find('.zipCodeError').html(responseText.errorMsg.zipCodeError).show();
            }


        } else if (responseText.type == 'success') {
            $('#successModal').modal();
            $('#successModal').find('.details').html('Your project has been successfully submitted. Our project coordinator will get in touch with you soon.')
        }

    }

    function readURL(input, str) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                var picHtml = '<div class="single_file"><img alt="" width="50" src="' + e.target.result + '" /> '
                picHtml += '<a href="javascript:" rel="' + str + '" class="delete_img"><img alt="" src="img/cross_icon.png" /></a> </div>'
                $('.view_file').append(picHtml);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>