<div class="dashboad_content">
    <div class="view_project">
        <div class="porject_details_area view_quoute_area find_contractor">
            <?php
            $category_id = $this->request->params['named']['category_id'];
            $sub_category_id = $this->request->params['named']['sub_category_id'];
            foreach ($pro_users as $pro_user) {
                ?>
                <div class="company_details_area">
                    <div class="col-sm-2 col-md-2">
                        <div class="company_logo">
                            <?php if ($pro_user['User']['profile_pic']) { ?>
                                <img alt="" src="uploaded_files/profile_pic/<?php echo $pro_user['User']['id']; ?>/<?php echo $pro_user['User']['profile_pic']; ?>" />
                            <?php } else { ?>
                                <img alt="" src="img/company_logo.jpg" />
                            <?php } ?>
                        </div>
                    </div>

                    <div class="company_details col-sm-4 col-md-6">
                        <div class="company_name">
                            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'profile', $pro_user['ProUser']['user_id'])) ?>">
                                <?php echo $pro_user['ProUser']['company_name']; ?>
                            </a>

                        </div>
                        <div class="clear"></div>
                        <div class="company_moto">
                            <?php echo $pro_user['ProUser']['company_description']; ?>
                        </div>
                        <div class="company_basic_info">
                            <ul>
                                <li>
                                    <img alt="" src="img/location_icon.png" />
                                    <span><?php echo $pro_user['ProUser']['city']; ?></span>
                                </li>

                                <li>
                                    <img alt="" src="img/verified_icon.png" />
                                    <span>
                                        <?php if ($pro_user['project_count'] > 1) { ?>
                                            <?php echo $pro_user['project_count']; ?> Projects
                                        <?php } else { ?>
                                            <?php echo $pro_user['project_count']; ?> Project
                                        <?php } ?>
                                    </span>
                                </li>
                                <li class="user_review_area">
                                    <?php
                                    $reviewPercent = ($pro_user['rating_sum']['Hire']['proRating'] / $pro_user['reviewCount']['Hire']['reviewCount']) * 20;
                                    ?>

                                    <div class="rating_area">
                                        <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                    </div>
                                    <div class="review_text">
                                        <?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> Reviews
                                    </div>

                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="verified_pro col-sm-2 col-md-2">
                        <img alt="" src="img/project_verified_icon.png"/>
                    </div>
                    <div class=" col-sm-4 col-md-2">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_project', 'category_id' => $category_id, 'sub_category_id' => $sub_category_id, 'specified_pro' => $pro_user['ProUser']['id'])) ?>" class="request_quote">REQUEST A QUOTE</a>
                    </div>
                    <div class="clear"></div>
                </div>
            <?php } ?>
        </div>
        <div class="clear"></div>
    </div>

</div>