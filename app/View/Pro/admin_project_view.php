
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Project view</h3>
        <div class="varified_area">
            <?php if ($project['Project']['is_verified']) { ?>
                <span>Verified</span>
            <?php } else {
                ?>
                <span>Verify</span><input type="checkbox" rel="<?php echo $project['Project']['id']; ?>" class="VerifyProject" <?php if ($project['Project']['is_verified']) { ?> checked <?php } ?> />
            <?php }
            ?>

        </div>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <div class="row">
            <div class="col-md-7">
                <div class="listing">
                    <label>
                        Category : 
                    </label>
                    <span>
                        <?php echo $project['Categorie']['category']; ?>
                    </span>
                </div>

                <div class="listing">
                    <label>
                        Sub Category : 
                    </label>
                    <span>
                        <?php echo $project['SubCategorie']['sub_category']; ?>
                    </span>
                </div>



                <div class="listing">
                    <label>
                        Project Title : 
                    </label>
                    <span>
                        <?php echo $project['Project']['project_title']; ?>
                    </span>
                </div>

                <div class="listing">
                    <label>
                        Project details : 
                    </label>
                    <span>
                        <?php echo $project['Project']['details']; ?>
                    </span>
                </div>

                <div class="listing">
                    <label>
                        Materials needed for this project : 
                    </label>
                    <span>
                        <?php echo $project['Project']['materials']; ?>
                    </span>
                </div>

                <div class="listing">
                    <label>
                        Work to start : 
                    </label>
                    <span>
                        <?php echo $project['Project']['start_date']; ?>
                    </span>
                </div>

                <div class="listing">
                    <label>
                        Budget for this job : 
                    </label>
                    <span>
                        $<?php echo $project['Project']['budget']; ?>
                    </span>
                </div>

                <div class="listing">
                    <label>
                        Specified contractors : 
                    </label>
                    <span>
                        <?php
                        foreach ($project['SpecifiedPro'] as $SpecifiedPro) {
                            echo $SpecifiedPro['user']['User']['name'];
                            ?>
                            <br/>
                            <?php
                        }
                        ?>
                    </span>
                </div>

                <div class="listing">
                    <label>
                        Relevant images : 
                    </label>
                    <span>
                        <?php
                        foreach ($project['ProjectFile'] as $ProjectFile) {
                            ?>
                            <img alt="" style="width: 100px; margin-right: 10px;" src="<?php echo $ProjectFile['file'] ?>" />
                            <?php
                        }
                        ?>
                    </span>
                </div>

                <div class="listing">
                    <label>
                        User name : 
                    </label>
                    <span>
                        <?php echo $project['User']['name']; ?>
                    </span>
                </div>

                <div class="listing">
                    <label>
                        Email : 
                    </label>
                    <span>
                        <?php echo $project['User']['email']; ?>
                    </span>
                </div>

                <div class="listing">
                    <label>
                        Phone : 
                    </label>
                    <span>
                        <?php echo $project['User']['phone']; ?>
                    </span>
                </div>
            </div> 
            <?php
            if ($project['Project']['is_verified']) {

                if ($prousers) {
                    //print_r($prousers);
//              /  print_r($specify);
                    ?>
                    <div class="col-md-5">
                        <h4>Sent to Pro</h4>
                        <form class="project_sent_pros" action="">
                            <ul>
                                <?php foreach ($prousers as $prouser) { ?>
                                    <li><label>
                                            <input type="checkbox" value="<?php echo $prouser['User']['id'] ?>" name="sent_pro[]" />
                                            &nbsp; &nbsp;
                                            <?php echo (in_array($prouser['User']['id'], $specify)) ? "<b style='color: #0a0'>" . $prouser['User']['name'] . "</b>" : $prouser['User']['name']; ?>
                                            <?php //echo $prouser['User']['name'] ?>
                                        </label>
                                    </li>
                                <?php } ?>
                            </ul>
                            <input type="hidden" name="project_id" value="<?php echo $project['Project']['id'] ?>" />
                            <input type="hidden" name="category_id" value="<?php echo $project['Project']['category_id'] ?>" />
                            <ul>    
                                <li><input type="submit" name="submit" value="Send" class="btn btn-success"></li>
                            </ul>
                        </form>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->
<style>

</style>

<script type="text/javascript">
    $(function () {
        $('.VerifyProject').click(function () {
            var that = $(this);
            var id = $(this).attr('rel');
            var status;
            if ($(this).is(':checked')) {
                status = 'checked';
                $.ajax({
                    url: "<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'project_verification')); ?>",
                    type: 'POST',
                    async: false,
                    data: {id: id, status: status},
                    success: function (data) {
                        if (data == 'verified') {
                            that.closest('.varified_area').find('span').html('Verified');
                            that.remove();
                        }
                        window.location.href = "<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'get_verified_project')); ?>";
                    }
                });
            }


        });
    });


    $(document).ready(function () {
        $('.project_sent_pros').submit(function (e) {
            e.preventDefault();
            $('.msgclass').remove();
            var checked = $(".project_sent_pros ul li input:checked").length > 0;
            if (!checked) {
                $(".project_sent_pros ul:last").prepend('<li class="msgclass"><label style="color:#ff0000">Select atleast one Pro.</label></li>');
            } else {
                $.ajax({
                    url: "<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'project_sent_pros')); ?>",
                    type: 'POST',
                    async: false,
                    data: $(this).serialize(),
                    success: function (data) {
                        $(".project_sent_pros ul li input").prop('checked', false);
                        if (data == 'error') {
                            $(".project_sent_pros ul:last").prepend('<li class="msgclass"><label style="color:#ff0000">Select atleast one Pro.</label></li>');
                        } else {
                            $(".project_sent_pros ul:last").prepend('<li class="msgclass"><label style="color:#0a0">Sent Successfuly.</label></li>');
                        }
                    }
                });
            }
        })
    })
</script>