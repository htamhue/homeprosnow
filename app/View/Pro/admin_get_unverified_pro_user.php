<script type="text/javascript">
    function deleteSingle(id) {
        var base_path = $("#base_path").val();
        if (confirm('Are you sure to delete this item?')) {
            var url = '<?php echo $this->Html->url(array('controller' => 'user_interests', 'action' => 'delete_interest')); ?>';
            var postData = {'id': id};
            $.post(url, postData, function (data) {
                if (data == 'success') {
                    $('#iD' + id).fadeOut();
                } else {
                    alert(data);
                }
            }, 'text');
        }
    }
    function resetPassword(id) {
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'reset_pass_by_admin')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function (data) {
            if (data == 'success') {
                $('.resetPasswordMessage').removeClass('hidden');
            }
        }, 'text');
    }
</script>

<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Pro</h3>
    </div> <!-- End .content-box-header -->
    <div class="resetPasswordMessage hidden">
        <br/>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Success!</strong> Password successfully changed.
        </div>
        <br/>
    </div>
    <div class="content-box-content">

        <?php // echo $form->create("UserInterests", array("id" => "user_interest", "url" => BASE_PATH_ADMIN . "/user_interests/delete_all", "name" => "borough")); ?>
        <table>
            <thead>
                <tr>
                    <th colspan="3">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_verified_pro_user')); ?>" class="verified <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_verified_pro_user') echo "selected"; ?> ">Verified</a>
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_unverified_pro_user')); ?>" class="unverified <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_unverified_pro_user') echo "selected"; ?> ">Unverified</a>
                    </th>
                    <th>
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro_by_admin')); ?>" class=" ">Add pro</a>
                    </th>
                </tr>
                <tr>
                    <!--<th><input class="check-all" type="checkbox" /></th>-->
                    <!--<th>Id</th>-->
                    <th>Company Name</th>
                    <th>Company Logo</th>
                    <th>Description</th>
                    <th>Verify Pro</th>
                    <th>Reset Password</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($pro_users as $pro_user) { // print_r($pro_user) ?>
                    <tr id="pro_user_<?php echo $pro_user['ProUser']['id']; ?>">
                        <td>
                            <a target="_blank" href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'pro_view', 'pro_id' => $pro_user['ProUser']['id'])); ?>">
                                <?php echo $pro_user['ProUser']['company_name']; ?>
                            </a>
                        </td>
                        <td>
                            <div class="live_company_pic_upload_area">
                                <form autocomplete="off"  action="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'live_profile_pic_upload_by_admin')); ?>" class="updateLiveProfilePic" >
                                    <input type="file" class="live_profile_pic_form" name="profile_pic" id="profile_pic_<?php echo $pro_user['User']['id'] ?>" />
                                    <input type="hidden" name="user_id" value="<?php echo $pro_user['User']['id'] ?>" />
                                    <?php
                                    if (isset($pro_user['User']['profile_pic'])) {
                                        ?>
                                        <label for="profile_pic_<?php echo $pro_user['User']['id'] ?>">
                                            <img id="pro_pic_<?php echo $pro_user['User']['id'] ?>" alt=""  width="50" src="uploaded_files/profile_pic/<?php echo $pro_user['User']['id']; ?>/<?php echo $pro_user['User']['profile_pic']; ?>" />
                                        </label>

                                        <?php
                                    } else {
                                        ?>
                                        <label for="profile_pic_<?php echo $pro_user['User']['id'] ?>">
                                            <img alt="" id="pro_pic_<?php echo $pro_user['User']['id'] ?>" for="profile_pic_<?php echo $pro_user['User']['id'] ?>" width="50" src="img/company_logo.jpg" />
                                        </label>

                                        <?php
                                    }
                                    ?>

                                </form>

                            </div>

                        </td>
                        <td><?php echo $pro_user['ProUser']['company_description']; ?></td>
                        <td>
                            <input type="checkbox" rel="<?php echo $pro_user['ProUser']['id']; ?>" class="VerifyProUser" <?php if ($pro_user['ProUser']['is_verified']) { ?> checked <?php } ?> />
                        </td>
                        <td>
                            <a href="javascript:" onclick="resetPassword(<?php echo $pro_user['ProUser']['id']; ?>)" class="btn btn-success">
                                Reset Password
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'admin_edit_pro_by_admin', $pro_user['User']['id'])); ?>">Edit</a>
                        </td>
    <!--                        <td>
                             Icons 
                                <a href="/user_interests/edit_interest/<?php echo $project['Project']['id']; ?>" title="Edit">
                                <img src="admin_images/icons/pencil.png" alt="Edit" />
                            </a>
                            <a href="javascript:void(0);" title="Delete"  onClick="return deleteSingle('<?php echo $pro_user['ProUser']['id']; ?>');">
                                <img src="admin_images/icons/cross.png" alt="Delete" />
                            </a> 
                        </td>-->
                    </tr>
                <?php } ?>
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="6">


                        <?php if ($this->params['paging']['ProUser']['pageCount'] > 1) { ?>
                            <div class="pagination">
                                <ul>
                                    <li class="page-prev"><?php echo $this->Paginator->prev(); ?></li>
                                    <li><?php echo $this->Paginator->numbers(); ?></li>
                                    <li class="page-next"><?php echo $this->Paginator->next(); ?></li>
                                </ul>
                            </div> <!-- End .pagination -->
                        <?php } ?>
                        <div class="clear"></div>
                    </td>
                </tr>
            </tfoot>

        </table>
        <?php // $form->end(); ?>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

<script type="text/javascript">
    $(function () {
        $('.VerifyProUser').click(function () {
            var that = $(this);
            var id = $(this).attr('rel');
            var status;
            if ($(this).is(':checked')) {
                status = 'checked';
                $.ajax({
                    url: "<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'pro_verification')); ?>",
                    type: 'POST',
                    async: false,
                    data: {id: id, status: status},
                    success: function (data) {
                        if (data == 'verified') {
                            $('#pro_user_' + id).remove();
                        } else {
                            that.removedAttr('checked');
                        }

                    }
                });
            }


        });
    });
</script>


<script type="text/javascript">
    $(function () {

        var options = {
            type: 'post',
            dataType: 'json',
            beforeSubmit: function (arr, $form, options) {
                $('.error_msg').hide();
            },
            success: responsefunc
        };

        $('.updateLiveProfilePic').submit(function () {
            $(this).ajaxSubmit(options);
            //$(".addpictureblock").modal('hide');
            return false;
        });


        $(document).on('change', '.live_profile_pic_form', function () {
            var that = $(this);
            that.closest('form').submit();

        });

    });

    function responsefunc(responseText, statusText, xhr, $form) {
        //        alert_r(responseText);
        if (responseText.type == 'error') {
            $('.emailError').html(responseText.error.emailError).show();
            $('.imageError').html(responseText.error.imageError).show();
        } else {
            var path = 'uploaded_files/profile_pic/' + responseText.user.User.id + '/' + responseText.user.User.profile_pic;
            $('#pro_pic_' + responseText.user.User.id).attr('src', path);
        }
    }


</script>