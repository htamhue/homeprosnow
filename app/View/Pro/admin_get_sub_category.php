<?php
if (isset($from) && $from == 'project') {
    ?>
    <select class="select_sub_category" name="sub_category_id">
        <option value="0">Select sub-category</option>
        <?php foreach ($sub_categories as $sub_categorie) { ?>
            <option value="<?php echo $sub_categorie['SubCategorie']['id'] ?>" ><?php echo $sub_categorie['SubCategorie']['sub_category'] ?></option>
        <?php } ?>
    </select>
    <?php
} else {
    ?>
    <div class="sub_category_list">
        <?php
        foreach ($sub_categories as $sub_categorie) {
            ?>
            <div class="single_sub_category">
                <input class="SubCategoryCheck" name="sub_category[]" id="sub_cat_<?php echo $sub_categorie['SubCategorie']['id'] ?>" value="<?php echo $sub_categorie['SubCategorie']['id'] ?>" type="checkbox" />
                <label for="sub_cat_<?php echo $sub_categorie['SubCategorie']['id'] ?>"></label>
                <span>
                    <?php echo $sub_categorie['SubCategorie']['sub_category'] ?>
                </span>
                <div class="clear"></div>
                
            </div>
        <?php } ?>
        <div class="subCategoryError error_msg"></div>

    </div>
    <?php
}