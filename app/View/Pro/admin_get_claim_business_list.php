<script type="text/javascript">
    function deleteSingle(id) {
        var base_path = $("#base_path").val();
        if (confirm('Are you sure to delete this item?')) {
            var url = '<?php echo $this->Html->url(array('controller' => 'user_interests', 'action' => 'delete_interest')); ?>';
            var postData = {'id': id};
            $.post(url, postData, function (data) {
                if (data == 'success') {
                    $('#iD' + id).fadeOut();
                } else {
                    alert(data);
                }
            }, 'text');
        }
    }

    function resetPassword(id) {
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'reset_pass_by_admin')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function (data) {
            if (data == 'success') {
                $('.resetPasswordMessage').removeClass('hidden');
            }
        }, 'text');
    }
</script>

<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Claim business lists</h3>
    </div> <!-- End .content-box-header -->



    <div class="content-box-content">

        <?php // echo $form->create("UserInterests", array("id" => "user_interest", "url" => BASE_PATH_ADMIN . "/user_interests/delete_all", "name" => "borough")); ?>
        <table>
            <thead>
<!--                <tr>
                    <th colspan="3">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_verified_pro_user')); ?>" class="verified <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_verified_pro_user') echo "selected"; ?> ">Verified</a>
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_unverified_pro_user')); ?>" class="unverified <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_unverified_pro_user') echo "selected"; ?> ">Unverified</a>
                    </th>
                    <th>
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro_by_admin')); ?>" class=" ">Add pro</a>
                    </th>
                </tr>-->
                <tr>
                    <!--<th><input class="check-all" type="checkbox" /></th>-->
                    <!--<th>Id</th>-->
                    <th>Company Name</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Message</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($claim_business_lists as $claim_business) { ?>
                    <tr id="pro_user_<?php echo $claim_business['ClaimBusiness']['id']; ?>">
                        <td>
                            <a target="_blank" href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'admin_edit_pro_by_admin', $claim_business['ProUser']['user_id'])); ?>">
                                <?php echo $claim_business['ProUser']['company_name']; ?>
                            </a>
                        </td>
                        <td>
                            <?php echo $claim_business['ClaimBusiness']['name']; ?>

                        </td>
                        <td>
                            <?php echo $claim_business['ClaimBusiness']['email']; ?>
                        </td>
                        <td>
                            <?php echo $claim_business['ClaimBusiness']['phone']; ?>
                        </td>
                        <td>
                            <?php echo $claim_business['ClaimBusiness']['message']; ?>
                        </td>
                        <td>

                            
                            <a href="javascript:" onclick="deleteClaimList(<?php echo $claim_business['ClaimBusiness']['id']; ?>)" class="btn btn-success deleteClaimList">
                                    Delete
                                </a>
                            
                            <a 
                                data-claim_id="<?php echo $claim_business['ClaimBusiness']['id']; ?>" 
                                data-user_id="<?php echo $claim_business['ClaimBusiness']['user_id']; ?>" 
                                data-claim_user_id="<?php echo $claim_business['ClaimBusiness']['claim_user_id']; ?>" 
                                data-pro_user_id="<?php echo $claim_business['ClaimBusiness']['pro_user_id']; ?>" 
                               class="btn btn-success acceptClaim">Accept</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="6">


                        <?php if ($this->params['paging']['ProUser']['pageCount'] > 1) { ?>
                            <div class="pagination">
                                <ul>
                                    <li class="page-prev"><?php echo $this->Paginator->prev(); ?></li>
                                    <li><?php echo $this->Paginator->numbers(); ?></li>
                                    <li class="page-next"><?php echo $this->Paginator->next(); ?></li>
                                </ul>
                            </div> <!-- End .pagination -->
                        <?php } ?>
                        <div class="clear"></div>
                    </td>
                </tr>
            </tfoot>

        </table>
        <?php // $form->end(); ?>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

<script type="text/javascript">
    $(function () {

        var options = {
            type: 'post',
            dataType: 'json',
            beforeSubmit: function (arr, $form, options) {
                $('.error_msg').hide();
            },
            success: responsefunc
        };

        $('.updateLiveProfilePic').submit(function () {
            $(this).ajaxSubmit(options);
            //$(".addpictureblock").modal('hide');
            return false;
        });


        $(document).on('change', '.live_profile_pic_form', function () {
            var that = $(this);
            that.closest('form').submit();

        });

    });

    function responsefunc(responseText, statusText, xhr, $form) {
        //        alert_r(responseText);
        if (responseText.type == 'error') {
            $('.emailError').html(responseText.error.emailError).show();
            $('.imageError').html(responseText.error.imageError).show();
        } else {
            var path = 'uploaded_files/profile_pic/' + responseText.user.User.id + '/' + responseText.user.User.profile_pic;
            $('#pro_pic_' + responseText.user.User.id).attr('src', path);
        }
    }
    
    function deleteClaimList(id) {
        var that = $(this);
        var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'delete_claim_list')); ?>';
        var postData = {'id': id};
        $.post(url, postData, function (data) {
            if (data == 'success') {
//                $('.message_box').html('Claim successfully deleted.').show();
//                setTimeout(function () {
//                    $('.message_box').fadeOut();
//                }, 3000);
//                //$('.deleteClaimList').remove();
                $('#pro_user_'+id).remove();
            }
        }, 'text');
    }
    
    $(document).ready(function (){
        $('.acceptClaim').click(function (e) {
            e.preventDefault();
            var that = $(this);
            var claim_id = $(this).data('claim_id');
            var user_id = $(this).data('user_id');
            var claim_user_id = $(this).data('claim_user_id');
            var pro_user_id = $(this).data('pro_user_id');
            var data = {claim_id: claim_id, user_id: user_id, claim_user_id: claim_user_id, pro_user_id: pro_user_id};
            
            var url = '<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'claim_change')); ?>';
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    that.closest('tr').remove();
                }
            });
        });
    })

</script>