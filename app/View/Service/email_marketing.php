<section class="middle">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="middle_content_right_text">
                        A few reasons why we might be the right fit:
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/email_setup.png"/>
                    </div>
                    <div class="content_name">Email Setup</div>
                    <div class="content_text">
                        We setup a professional email
                        for you and your team with 
                        your business domain.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/launch.png"/>
                    </div>
                    <div class="content_name">Launch Promotions</div>
                    <div class="content_text">
                        Communicate offers and
                        promotions to your subscribers
                        to drive better leads.
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/notepad.png"/>
                    </div>
                    <div class="content_name">Email Strategy</div>
                    <div class="content_text">
                        Follow our winning email
                        marketing strategy to take your
                        business to new heights.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/pre_built.png"/>
                    </div>
                    <div class="content_name">Pre-Built Templates</div>
                    <div class="content_text">
                        Need to send out a quick email?
                        Get going with one of our pre-
                        built email templates.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/logos.png"/>
                    </div>
                    <div class="content_name">Customized Emails</div>
                    <div class="content_text">
                        Don't like our pre-built templates? 
                        Our team can help you design a 
                        fully custom email.from scratch.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/pen.png"/>
                    </div>
                    <div class="content_name">Engaging Copy</div>
                    <div class="content_text">
                        Our copywriters ensure that the
                        content in your email resonates
                        with your audience.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/increase.png"/>
                    </div>
                    <div class="content_name">Increase Subscribers</div>
                    <div class="content_text">
                        Increase your subscriber base
                        with regular company updates
                        and information.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/repost.png"/>
                    </div>
                    <div class="content_name">Track Results</div>
                    <div class="content_text">
                        Track how your email stratefy is
                        performaing with powerful reports
                        that measure success.
                    </div>
                </div>
            </div>

            <div class="clear"></div>
        </div>
        <div class="middle_banner">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="middle_banner_heading">
                        Boost conversions and increase sales with Email Marketing.
                    </div>
                    <div class="middle_banner_text">
                        Homeprosnow helps time-strapped businesses
                        quickly and easily nurture customer relationships 
                        that lead to business growth. With our support,
                        you can save time and drive revenue by sending 
                        the right email, to the right person at the right 
                        time.
                    </div>
                    <div class="middle_banner_text">
                        Our analytics help you understand what makes 
                        better campaigns and monitor your results 
                        wherever you are.
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="middle_banner_img">
                        <img class="img img-responsive" src="images/email_marketing_middle_banner.png"/>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
