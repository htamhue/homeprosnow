<section class="middle">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="middle_content_right_text">
                        A few reasons why we might be the right fit:
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/profile.png"/>
                    </div>
                    <div class="content_name">Social Media Setup</div>
                    <div class="content_text">
                        We help setup your social media accounts on Facebook, Twitter and Instagram.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/network.png"/>
                    </div>
                    <div class="content_name">Competitor Analysis</div>
                    <div class="content_text">
                        Get details on what's working and not working for your competitors on their social channels.
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/notepad.png"/>
                    </div>
                    <div class="content_name">Marketing Strategy</div>
                    <div class="content_text">
                        Get an unbeatable marketing
                        strategy for your brand to
                        grow on all social platforms.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/facebook.png"/>
                    </div>
                    <div class="content_name">Facebook Ads</div>
                    <div class="content_text">
                        Drive users to your websites
                        with geotargeted Facebook Ads
                        that are built to convert.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/community.png"/>
                    </div>
                    <div class="content_name">Community Management</div>
                    <div class="content_text">
                        Your dedicated community
                        manager keeps your followers
                        engaged at all times.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/slack.png"/>
                    </div>
                    <div class="content_name">Hashtag Research</div>
                    <div class="content_text">
                        Direct the right hashtags back
                        to your social pages to convert
                        curious users.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/filter.png"/>
                    </div>
                    <div class="content_name">Leads Filtering</div>
                    <div class="content_text">
                        We filter the leads that come
                        through your social channels so
                        you can increase sales
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/repost.png"/>
                    </div>
                    <div class="content_name">Insightful Reports</div>
                    <div class="content_text">
                        Monthly reports help you measure
                        the performance of your social
                        pages across all platforms
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="middle_banner">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="middle_banner_heading">
                        Grow brand awareness, traffic and engagement.
                    </div>
                    <div class="middle_banner_text">
                        Homeprosnow deploys social media marketing programs that build authentic social media relationships between your business  and your target audience
                    </div>
                    <div class="middle_banner_text">
                        Our unique approach to social media marketing is part of an adaptive strategy to help your brand  tell its story. Connecting with your social media audience while humanizing your brand is a vital component to convert potential customers.
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="middle_banner_img">
                        <img class="img img-responsive" src="images/middle_banner.png"/>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
