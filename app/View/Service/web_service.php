<section class="step">
    <div class="container">
        <div class="step_head">
            <span class="list"><i class="fa fa-circle "></i></span> <span class="step_heading ">Step 1:</span> <span class="step_heding_right"> Pick a website template you love.</span>  
        </div>
        <div class="template">
            <div class="row">
                <?php foreach ($template_lists as $template) { ?>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="template_body">
                            <div class="template_img">
                                <img class="img img-responsive" src="uploaded_files/website_templates/<?php echo $template['WebsiteTemplate']['image']; ?>"/>
                            </div>
                            <div class="template_name">
                                <?php echo $template['WebsiteTemplate']['title']; ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
<!--                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="template_body">
                        <div class="template_img">
                            <img class="img img-responsive" src="images/template1.png"/>
                        </div>
                        <div class="template_name">
                            Template 2
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="template_body">
                        <div class="template_img">
                            <img class="img img-responsive" src="images/template2.png"/>
                        </div>
                        <div class="template_name">
                            Template 3
                        </div>
                    </div>
                </div> 
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="template_body">
                        <div class="template_img">
                            <img class="img img-responsive" src="images/template.png"/>
                        </div>
                        <div class="template_name">
                            Template 1
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="template_body">
                        <div class="template_img">
                            <img class="img img-responsive" src="images/template1.png"/>
                        </div>
                        <div class="template_name">
                            Template 2
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="template_body">
                        <div class="template_img">
                            <img class="img img-responsive" src="images/template2.png"/>
                        </div>
                        <div class="template_name">
                            Template 3
                        </div>
                    </div>
                </div> 
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="template_body">
                        <div class="template_img">
                            <img class="img img-responsive" src="images/template.png"/>
                        </div>
                        <div class="template_name">
                            Template 1
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="template_body">
                        <div class="template_img">
                            <img class="img img-responsive" src="images/template1.png"/>
                        </div>
                        <div class="template_name">
                            Template 2
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="template_body">
                        <div class="template_img">
                            <img class="img img-responsive" src="images/template2.png"/>
                        </div>
                        <div class="template_name">
                            Template 3
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
        <!-- /****** step-2********/-->
        <div class="step_head">
            <span class="list"><i class="fa fa-circle "></i></span> <span class="step_heading ">Step 2 </span> <span class="step_heding_right">: Pair it with exceptional digital and print marketing.</span>  
        </div>
        <div class="template">
            <div class="step_heading_text">
                Reach more customers with our powerful online and print marketing services.
            </div>
            <div class="step_img">
                <img class="img img-responsive" src="images/step2.png"/>
            </div>
        </div>
        <!-- /****** step-3********/-->
        <div class="step_head">
            <span class="list"><i class="fa fa-circle "></i></span> <span class="step_heading ">Step 3</span> <span class="step_heding_right"> : Get your website built and hosted by a professional team</span>  
        </div>
        <div class="template">
            <div class="step_heading_text">
                See your new website live. Promote it by sharing it with your network.
            </div>
            <div class="step_img">
                <img class="img img-responsive" src="images/step3.png"/>
            </div>
        </div>
        <!-- /****** step-4********/-->
        <div class="step_head">
            <span class="list"><i class="fa fa-circle "></i></span> <span class="step_heading ">BONUS </span> <span class="step_heding_right">: Get listed as a Homeprosnow Verified Contractor.</span>  
        </div>
        <div class="template bonus">
            <div class="step_heading_text">
                Once your website is ready and live, we also add you to our premium directory of verified contractors to drive
                new project leads from homeowners near you.
            </div>
            <div class="step_img">
                <img class="img img-responsive" src="images/bonus.png"/>
            </div>
        </div>
    </div>
</section>
  