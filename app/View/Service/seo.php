<section class="middle">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="middle_content_right_text">
                        A few reasons why we might be the right fit:
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/keword.png"/>
                    </div>
                    <div class="content_name">Keyword Research</div>
                    <div class="content_text">
                        Learn what keywords your
                        customers use the most to find
                        your services on search engines.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/link_building.png"/>
                    </div>
                    <div class="content_name">Link Building</div>
                    <div class="content_text">
                        Improve your website's ranking
                        by getting external pages to link
                        back to your site.
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/filter.png"/>
                    </div>
                    <div class="content_name">Search Optimization</div>
                    <div class="content_text">
                        Convert visitors to customers
                        easily with our expert search
                        optimization strategies
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/search_engine.png"/>
                    </div>
                    <div class="content_name">Search Engine Indexing</div>
                    <div class="content_text">
                        Help customers find you quickly
                        by listing your business with all
                        the popular search engines.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/network.png"/>
                    </div>
                    <div class="content_name">Competitor Analysis</div>
                    <div class="content_text">
                        Learn how your competitors are
                        ranking against keywords relevant
                        to you business.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/notepad.png"/>
                    </div>
                    <div class="content_name">Keyword Rich Articles</div>
                    <div class="content_text">
                        Increase your website traffic
                        with keyword rich articles
                        that attract your audience.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/url_optimization.png"/>
                    </div>
                    <div class="content_name">URL Optimization</div>
                    <div class="content_text">
                        Optimize individual URLs on
                        your website so that they rank
                        better on search engines.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/repost.png"/>
                    </div>
                    <div class="content_name">Powerful Analytics</div>
                    <div class="content_text">
                        Get detailed analytics of your SEO
                        performance with reports that
                        track your website's ranking.
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="middle_banner">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="middle_banner_heading">
                        We optimize your site so you receive quality leads.
                    </div>
                    <div class="middle_banner_text">
                        We gather intel on your market & methodically
                        analyze data to determine the exact search 
                        volume and competition for the key phrases 
                        being searched by your prospects. 
                    </div>
                    <div class="middle_banner_text">
                        We investigate the tactics your top competitors 
                        use and reverse engineer them for even better 
                        results. This in depth research stacks the odds 
                        of success in your favor by uncovering the best 
                        keyword phrases to target.
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="middle_banner_img">
                        <img class="img img-responsive" src="images/seo_middle_banner.png"/>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
