<section class="middle">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="middle_content_right_text">
                        A few reasons why we might be the right fit:
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/bussiness_card.png"/>
                    </div>
                    <div class="content_name">Business Cards</div>
                    <div class="content_text">
                        Introduce yourself to customers
                        with a business card design that
                        is as unique as you are.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/logos.png"/>
                    </div>
                    <div class="content_name">Logos</div>
                    <div class="content_text">
                        Distinguish yourself from the
                        competition with a logo that
                        represents your brand.
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/flayer.png"/>
                    </div>
                    <div class="content_name">Flyers & Brochures</div>
                    <div class="content_text">
                        Print marketing flyers and
                        brochures that attract your
                        customers instantly.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/stationery.png"/>
                    </div>
                    <div class="content_name">Stationery</div>
                    <div class="content_text">
                        Give your business a professional
                        touch with branded stationery
                        for your employees.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/cloth.png"/>
                    </div>
                    <div class="content_name">Clothing & Merchandise</div>
                    <div class="content_text">
                        Turn your business into a quality
                        brand with professional clothing
                        and merchandise for your staff.
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/truck.png"/>
                    </div>
                    <div class="content_name">Car or Truck Wrap</div>
                    <div class="content_text">
                        Advertise your brand while
                        you're on the go with a car
                        or truck wrap.
                    </div>
                </div>
            </div>              
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/direct_mail.png"/>
                    </div>
                    <div class="content_name">Direct Mail</div>
                    <div class="content_text">
                        Attract more leads with direct
                        mail design that speaks directly 
                        to your customers
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="middle_content">
                    <div class="content_img">
                        <img class="img img-responsive" src="images/trade.png"/>
                    </div>
                    <div class="content_name">Trade Show Banners</div>
                    <div class="content_text">
                        Draw visitors to your booth at
                        your next trade show with
                        compelling banners.
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="middle_banner">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="middle_banner_heading">
                        We design so you can execute
                    </div>
                    <div class="middle_banner_text">
                        Our team of talented graphic designers ensure
                        that your business's design needs are met in
                        record time so you can market and grow your
                        brand quickly and efficiently. 
                    </div>
                    <div class="middle_banner_text">
                        Whether you're looking for a new logo to go
                        with your website or create a completely new
                        brand identity for your business, our team can 
                        take care of every little detail.
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="middle_banner_img">
                        <img class="img img-responsive" src="images/graphic_page_middle_banner.png"/>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
