<ul>
    <?php
    if (!empty($notifications)) {
        foreach ($notifications as $notification) {

            if ($notification['Notification']['notification_type'] == 'pro_quote_accepted') {
                if (!$notification['Notification']['read_on']) {
                    $class = 'unRead';
                    $btnText = 'View';
                    $btnClass = 'view_btn';
                } else {
                    $class = '';
                    $btnText = 'Viewed';
                    $btnClass = 'viewed_btn';
                }
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>" class="<?php echo $class; ?>" >
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        Quote accepted
                    </div>
                    <div class="details">
                        Your Quote is accepted.
                    </div>
                    <div class="action_area">
                        <a target="_blank" href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'view_quote', 'quote_id' => $notification['hire']['Hire']['project_quote_id'])); ?>" class="<?php echo $btnClass; ?>">
                            <?php echo $btnText; ?>
                        </a>

                    </div>
                </li>
                <?php
            } else if ($notification['Notification']['notification_type'] == 'pro_quote_declined') {
                if (!$notification['Notification']['read_on']) {
                    $class = 'unRead';
                    $btnText = 'View';
                    $btnClass = 'view_btn';
                } else {
                    $class = '';
                    $btnText = 'Viewed';
                    $btnClass = 'viewed_btn';
                }
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>"  class="<?php echo $class; ?>" >
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        Quote Declined
                    </div>
                    <div class="details">
                        Your Quote is Declined.
                    </div>
                    <div class="action_area">
                        <a target="_blank" href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'view_quote', 'quote_id' => $notification['project_quote']['ProjectQuote']['id'])); ?>" class="<?php echo $btnClass; ?>">
                            <?php echo $btnText; ?>
                        </a>

                    </div>
                </li>
                <?php
            } else if ($notification['Notification']['notification_type'] == 'pro_project_submitted_near_you') {
                if (!$notification['Notification']['read_on']) {
                    $class = 'unRead';
                    $btnText = 'View';
                    $btnClass = 'view_btn view_new_project';
                } else {
                    $class = '';
                    $btnText = 'Viewed';
                    $btnClass = 'viewed_btn';
                }
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>"  class="<?php echo $class; ?>">
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        New Project!
                    </div>
                    <div class="details">
                        There is a new project for <span class="category_name"><?php echo $notification['project']['Categorie']['category'] ?></span> near you.
                    </div>
                    <div class="action_area">
                        <a project_id="<?php echo $notification['Notification']['related_table_id']; ?>" href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $notification['Notification']['related_table_id'])); ?>" class="<?php echo $btnClass; ?>">
                            <?php echo $btnText; ?>
                        </a>
                        <?php if (!$notification['Notification']['read_on']) { ?>
                            <a href="javascript:" rel="<?php echo $notification['Notification']['id']; ?>" class="discard_btn">
                                Discard
                            </a>
                        <?php } ?>
                    </div>
                </li>
                <?php
            } else if ($notification['Notification']['notification_type'] == 'user_received_quote') {
                if (!$notification['Notification']['read_on']) {
                    $class = 'unRead';
                    $btnText = 'View';
                    $btnClass = 'view_btn';
                } else {
                    $class = '';
                    $btnText = 'Viewed';
                    $btnClass = 'viewed_btn';
                }
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>"  class="<?php echo $class; ?>" >
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        You have new quote!
                    </div>
                    <div class="details">
                        You have new quote. From <span class="category_name"><?php echo $notification['project_quote']['User']['name']; ?></span>
                    </div>
                    <div class="action_area">
                        <a target="_blank" href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'view_quote', 'quote_id' => $notification['project_quote']['ProjectQuote']['id'])); ?>" class="<?php echo $btnClass; ?>">
                            <?php echo $btnText; ?>
                        </a>

                    </div>
                </li> 
                <?php
            } else if ($notification['Notification']['notification_type'] == 'user_received_review') {
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>" <?php if (!$notification['Notification']['read_on']) { ?> class="unRead" <?php } ?>>
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        You have received review!
                    </div>
                    <div class="details">
                        You received review. From <span class="category_name"><?php echo $notification['pro_user']['User']['name']; ?></span>
                    </div>

                </li> 
                <?php
            } else if ($notification['Notification']['notification_type'] == 'pro_received_review') {
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>" <?php if (!$notification['Notification']['read_on']) { ?> class="unRead" <?php } ?>>
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        You have received review!
                    </div>
                    <div class="details">
                        You received review. From <span class="category_name"><?php echo $notification['hire']['User']['name']; ?></span>
                    </div>

                </li> 
                <?php
            } else if ($notification['Notification']['notification_type'] == 'user_project_approved') {
                if (!$notification['Notification']['read_on']) {
                    $class = 'unRead';
                    $btnText = 'View';
                    $btnClass = 'view_btn';
                } else {
                    $class = '';
                    $btnText = 'Viewed';
                    $btnClass = 'viewed_btn';
                }
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>"  class="<?php echo $class; ?>">
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        Approved Project!
                    </div>
                    <div class="details">
                        Your project approved by <span class="category_name">Admin</span>.
                    </div>
                    <div class="action_area">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $notification['Notification']['related_table_id'])); ?>" class="<?php echo $btnClass; ?>">
                            <?php echo $btnText; ?>
                        </a>

                    </div>
                </li>
                <?php
            } else if ($notification['Notification']['notification_type'] == 'pro_account_approved') {
                if (!$notification['Notification']['read_on']) {
                    $class = 'unRead';
                    $btnText = 'View';
                    $btnClass = 'view_btn';
                } else {
                    $class = '';
                    $btnText = 'Viewed';
                    $btnClass = 'viewed_btn';
                }
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>"  class="<?php echo $class; ?>" >
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        You Have Been Approved!
                    </div>
                    <div class="details">
                        Your business has been approved. You should start receiving leads soon.
                    </div>
                    <div class="action_area">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'profile', $notification['User']['id'])); ?>" class="<?php echo $btnClass; ?>">
                            <?php echo $btnText; ?>
                        </a>

                    </div>
                </li>
                <?php
            } else if ($notification['Notification']['notification_type'] == 'user_payment_request_received') {
                if (!$notification['Notification']['read_on']) {
                    $class = 'unRead';
                    $btnText = 'View';
                    $btnClass = 'view_btn';
                } else {
                    $class = '';
                    $btnText = 'Viewed';
                    $btnClass = 'viewed_btn';
                }
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>"  class="<?php echo $class; ?>" >
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        Payment Request!
                    </div>
                    <div class="details">
                        Your have received payment request by <span class="category_name"><?php echo $notification['pro_user']['User']['name'] ?></span>.
                    </div>
                    <div class="action_area">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'view_quote', 'quote_id' => $notification['hire']['Hire']['project_quote_id'])); ?>" class="<?php echo $btnClass; ?>">
                            <?php echo $btnText; ?>
                        </a>

                    </div>
                </li>
                <?php
            } else if ($notification['Notification']['notification_type'] == 'user_paid_a_pro') {
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>" <?php if (!$notification['Notification']['read_on']) { ?> class="unRead" <?php } ?>>
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        You paid a pro!
                    </div>
                    <div class="details">
                        You have paid <span class="category_name">$<?php echo $notification['project_quote']['ProjectQuote']['amount']; ?></span> for <span class="category_name"><?php echo $notification['project_quote']['Project']['project_title']; ?> </span> to <span class="category_name"><?php echo $notification['project_quote']['User']['name']; ?></span>
                    </div>

                </li> 
                <?php
            } else if ($notification['Notification']['notification_type'] == 'pro_received_payment') {
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>" <?php if (!$notification['Notification']['read_on']) { ?> class="unRead" <?php } ?>>
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        You received payment!
                    </div>
                    <div class="details">
                        You have received <span class="category_name">$<?php echo $notification['project_quote']['ProjectQuote']['amount']; ?></span> from <span class="category_name"><?php echo $notification['hire']['User']['name']; ?> </span> for <span class="category_name"><?php echo $notification['project_quote']['Project']['project_title']; ?></span>
                    </div>

                </li> 
                <?php
            } else if ($notification['Notification']['notification_type'] == 'pro_project_fee_applied') {
                if (!$notification['Notification']['read_on']) {
                    $class = 'unRead';
                    $btnText = 'View';
                    $btnClass = 'view_btn';
                } else {
                    $class = '';
                    $btnText = 'Viewed';
                    $btnClass = 'viewed_btn';
                }
                ?>
                <li rel="<?php echo $notification['Notification']['id']; ?>"  class="<?php echo $class; ?>" >
                    <div class="project_title">
                        <img alt="" src="img/dollar_icon.png"/>
                        Fee applied!
                    </div>
                    <div class="details">
                        Your have paid <span class="category_name">$<?php echo Configure::read('settings.payment.project_view_amount') ?></span> for view project.
                    </div>
                    <div class="action_area">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $notification['project']['Project']['id'])); ?>" class="<?php echo $btnClass; ?>">
                            <?php echo $btnText; ?>
                        </a>

                    </div>
                </li>
                <?php
            }
        }
    } else {
        ?>
        <li>
            No notification!!
        </li>
    <?php } ?>
</ul>

