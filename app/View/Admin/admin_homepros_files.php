<script type="text/javascript">

    function deleteSingle(id) {
        var base_path = $("#base_path").val();
        if (confirm('Are you sure to delete this item?')) {
            var url = '<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_delete_files')); ?>';
            var postData = {'id': id};
            $.post(url, postData, function(data) {
                if (data == 'success') {
                    $('#iD_' + id).fadeOut();
                }
                else {
                    alert(data);
                }
            }, 'text');
        }
    }
</script>
<div class="admin_add_button">
    <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_add_homepros_file')); ?>">Add homepros file</a>
</div>
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>homepros file</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">

        <?php // echo $form->create("UserInterests", array("id" => "user_interest", "url" => BASE_PATH_ADMIN . "/user_interests/delete_all", "name" => "borough")); ?>
        <table>
            <thead>
<!--                <tr>
                    <th colspan="4">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_verified_pro_user')); ?>" class="verified <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_verified_pro_user') echo "selected"; ?> ">Verified</a>
                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_unverified_pro_user')); ?>" class="unverified <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_unverified_pro_user') echo "selected"; ?> ">Unverified</a>
                    </th>
                </tr>-->
                <tr>
                    <!--<th><input class="check-all" type="checkbox" /></th>-->
                    <!--<th>Id</th>-->
                    <th>File info</th>
                    <th>File name</th>

                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($homepros_files as $homepros_file) { ?>
                <tr id="iD_<?php echo $homepros_file['HomeprosFile']['id']; ?>">
                        <td><?php echo $homepros_file['HomeprosFile']['file_info']; ?></td>
                        <td><?php echo $homepros_file['HomeprosFile']['homepros_file']; ?></td>

                        <td>
                            <!-- Icons -->
            <!--                    <a href="/user_interests/edit_interest/<?php echo $project['Project']['id']; ?>" title="Edit">
                                <img src="admin_images/icons/pencil.png" alt="Edit" />
                            </a>-->
                            <a href="javascript:void(0);" title="Delete"  onClick="return deleteSingle('<?php echo $homepros_file['HomeprosFile']['id']; ?>');">
                                <img src="admin_images/icons/cross.png" alt="Delete" />
                            </a> 
                        </td>
                    </tr>
                <?php } ?>
            </tbody>

<!--            <tfoot>
                <tr>
                    <td colspan="6">
                        <div class="bulk-actions align-left">
                            <select name="data[TalentCategory][action]">
                                <option value="">Choose an action...</option>
                                <option value="delete">Delete</option>
                            </select>
                            <a class="button" href="javascript:void(0);" onclick="document.user_interest.submit();">Apply to selected</a>
                        </div>

                        <?php if ($this->params['paging']['ProUser']['pageCount'] > 1) { ?>
                            <div class="pagination">
                                <ul>
                                    <li class="page-prev"><?php echo $this->Paginator->prev(); ?></li>
                                    <li><?php echo $this->Paginator->numbers(); ?></li>
                                    <li class="page-next"><?php echo $this->Paginator->next(); ?></li>
                                </ul>
                            </div>  End .pagination 
                        <?php } ?>
                        <div class="clear"></div>
                    </td>
                </tr>
            </tfoot>-->

        </table>
        <?php // $form->end(); ?>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

<script type="text/javascript">
    $(function() {
        $('.VerifyProUser').click(function() {
            var that = $(this);
            var id = $(this).attr('rel');
            var stats = '';
            if ($(this).is(':checked')) {
                status = 'checked';
            } else {
                status = 'unchecked';
            }
            $.ajax({
                url: "<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'pro_verification')); ?>",
                type: 'POST',
                async: false,
                data: {id: id, status: status},
                success: function(data) {
                    if (data == 'verified') {
                        that.attr('checked');
                    } else {
                        that.removedAttr('checked');
                    }

                }
            });

        });
    });
</script>