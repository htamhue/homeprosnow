
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Add Recommendation</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <form accept-charset="utf-8" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_add_recommendation')); ?>">
            <label>
                Cover photo : 
            </label>
            <input type="file" name="cover_photo" />
            <br/>
            <br/>
            <label>
                Profile Photo: 
            </label>
            <input type="file" name="profile_photo" />
            <br/>
            <br/>
            <label>
                Client name
            </label>
            <input type="text" name="data[client_name]" />
            <br/>
            <br/>
            <label>
                Contractor name
            </label>
            <input type="text" name="data[contractor_name]" />
            <br/>
            <br/>
            <label>
                Rating
            </label>
            <input type="text" name="data[rating]" />
            <br/>
            <br/>
            <label>
                Recommendation
            </label>
            <input type="text" name="data[recommendation]" />
            <br/>
            <br/>
            <input type="submit" value="save"/>
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

