
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Edit Category</h3>
    </div> <!-- End .content-box-header -->
<?php 
    $var = '';
?>
    <div class="content-box-content">
        <form accept-charset="utf-8" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_edit_category')); ?>">
            <input type="hidden" value="<?php echo $category['Categorie']['id'] ?>" name="data[category_id]" />
            <label>
                Category : 
            </label>
            <input type="text" value="<?php echo $category['Categorie']['category'] ?>" name="data[category]" />
            <br/>
            <br/>
            <label>
                Category File: 
            </label>
            <br/>
            <br/>
            <img alt="" width="80px" src="uploaded_files/category_img/<?php echo $category['Categorie']['category_image'] ?>" />
            <br/>
            <br/>
            <input type="file" name="category_file" />
            <br/>
            <br/>
            <input type="submit" value="save"/>
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

