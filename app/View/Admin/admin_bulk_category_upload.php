<div class="content-box-header">
    <h3>Bulk Upload Category/Subcategory</h3>
</div>
<div class="clear"></div>
<div class="content-box-content">
    <div id="tab1" class="tab-content default-tab" style="display: block;">
        <div class="align-right bulk-actions">
        </div>
        <div class="clear"></div>
        <div class="line"></div>
        <?php echo $this->Form->create('Admin', array('type' => "file", 'id' => 'frm1')); ?>
        <fieldset>
            <p>
                <?php echo $this->Form->input('csv_file', array('label' => 'CSV File', 'type' => 'file', "class" => "text-input small-input")); ?>  
            </p>
            <p>
                <?php echo $this->Form->submit(__('Upload'), array('onclick' => '', "type" => "submit", "class" => "button")); ?>
            </p>
        </fieldset>
        <?php echo $this->Form->end(); ?>
    </div>
</div>