
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Edit Recommendation</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <form accept-charset="utf-8" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_edit_recommendation')); ?>">
            <input type="hidden" name="data[id]" value="<?php echo $recommendation['Recommendation']['id'] ?>" />
            <label>
                Cover photo : 
            </label>
            <img alt="" width="100" src="uploaded_files/recommendation_img/cover_photo/<?php echo $recommendation['Recommendation']['cover_photo']; ?>" />
            <br>
            <br>
            <input type="file" name="cover_photo" />
            <br/>
            <br/>
            <label>
                Profile Photo: 
            </label>
            <img alt="" width="100" src="uploaded_files/recommendation_img/profile_photo/<?php echo $recommendation['Recommendation']['profile_photo']; ?>" />
            <br>
            <br>
            
            <input type="file" name="profile_photo" />
            <br/>
            <br/>
            <label>
                Client name
            </label>
            <input type="text" value="<?php echo $recommendation['Recommendation']['client_name']; ?>" name="data[client_name]" />
            <br/>
            <br/>
            <label>
                Contractor name
            </label>
            <input type="text" value="<?php echo $recommendation['Recommendation']['contractor_name']; ?>" name="data[contractor_name]" />
            <br/>
            <br/>
            <label>
                Rating
            </label>
            <input type="text" value="<?php echo $recommendation['Recommendation']['rating']; ?>" name="data[rating]" />
            <br/>
            <br/>
            <label>
                Recommendation
            </label>
            <input type="text" value="<?php echo $recommendation['Recommendation']['recommendation']; ?>" name="data[recommendation]" />
            <br/>
            <br/>
            <input type="submit" value="Update"/>
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

