
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Add Category</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <form accept-charset="utf-8" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_add_category')); ?>">
            <label>
                Category : 
            </label>
            <input type="text" name="data[category]" />
            <br/>
            <br/>
            <label>
                Category File: 
            </label>
            <input type="file" name="category_file" />
            <br/>
            <br/>
            <input type="submit" value="save"/>
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

