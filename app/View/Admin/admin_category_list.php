<script type="text/javascript">

    function deleteSingle(id) {
        var base_path = $("#base_path").val();
        if (confirm('Are you sure to delete this item?')) {
            var url = '<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_delete_category')); ?>';
            var postData = {'id': id};
            $.post(url, postData, function(data) {
                if (data == 'success') {
                    $('#iD_' + id).fadeOut();
                }
                else {
                    alert(data);
                }
            }, 'text');
        }
    }
</script>
<div class="admin_add_button">
    <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_add_category')); ?>">Add category</a>
</div>
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Category</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <table>
            <thead>

                <tr>
                    <th>Category</th>

                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($categories as $categorie) { ?>
                    <tr id="iD_<?php echo $categorie['Categorie']['id']; ?>">
                        <td><?php echo $categorie['Categorie']['category']; ?></td>

                        <td>
                            <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_edit_category', 'category_id' => $categorie['Categorie']['id'])); ?>" title="Edit">
                                <img src="admin_images/icons/pencil.png" alt="Edit" />
                            </a>
                            <a href="javascript:void(0);" title="Delete"  onClick="return deleteSingle('<?php echo $categorie['Categorie']['id']; ?>');">
                                <img src="admin_images/icons/cross.png" alt="Delete" />
                            </a> 
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

<script type="text/javascript">
    $(function() {
        $('.VerifyProUser').click(function() {
            var that = $(this);
            var id = $(this).attr('rel');
            var stats = '';
            if ($(this).is(':checked')) {
                status = 'checked';
            } else {
                status = 'unchecked';
            }
            $.ajax({
                url: "<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'pro_verification')); ?>",
                type: 'POST',
                async: false,
                data: {id: id, status: status},
                success: function(data) {
                    if (data == 'verified') {
                        that.attr('checked');
                    } else {
                        that.removedAttr('checked');
                    }

                }
            });

        });
    });
</script>