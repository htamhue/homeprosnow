
<!--<div class="admin_add_button">
    <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_add_homepros_file')); ?>">Add homepros file</a>
</div>-->
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>homepros file</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">

        <table>
            <thead>

                <tr>
                    <th>Video info</th>
                    <th>Video</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($homepros_videos as $homepros_video) { ?>
                    <tr id="iD_<?php echo $homepros_video['HomeprosVideo']['id']; ?>">
                        <td><?php echo $homepros_video['HomeprosVideo']['video_info']; ?></td>
                        <td>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $homepros_video['HomeprosVideo']['video_code']; ?>" frameborder="0" allowfullscreen></iframe>
                        </td>

                        <td>
                            <!-- Icons -->
                            <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_edit_homepros_video', 'id' => $homepros_video['HomeprosVideo']['id'])); ?>" title="Edit">
                                <img src="admin_images/icons/pencil.png" alt="Edit" />
                            </a>
    <!--                            <a href="javascript:void(0);" title="Delete"  onClick="return deleteSingle('<?php echo $homepros_video['HomeprosFile']['id']; ?>');">
                            <img src="admin_images/icons/cross.png" alt="Delete" />
                        </a> -->
                        </td>
                    </tr>
                <?php } ?>
            </tbody>



        </table>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

