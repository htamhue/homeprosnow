<script type="text/javascript">

    function deleteSingle(id) {
        var base_path = $("#base_path").val();
        if (confirm('Are you sure to delete this item?')) {
            var url = '<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_delete_sub_category')); ?>';
            var postData = {'id': id};
            $.post(url, postData, function(data) {
                if (data == 'success') {
                    $('#iD_' + id).fadeOut();
                }
                else {
                    alert(data);
                }
            }, 'text');
        }
    }
</script>
<div class="admin_add_button">
    <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_add_sub_category')); ?>">Add Sub category</a>
</div>
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Sub Category</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">

        <table>
            <thead>

                <tr>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($sub_categories as $sub_categorie) { ?>
                    <tr id="iD_<?php echo $sub_categorie['SubCategorie']['id']; ?>">
                        <td><?php echo $sub_categorie['category']['Categorie']['category']; ?></td>
                        <td><?php echo $sub_categorie['SubCategorie']['sub_category']; ?></td>

                        <td>
                            <!-- Icons -->
                            <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_edit_sub_category' ,'sub_category_id' =>  $sub_categorie['SubCategorie']['id'])); ?>" title="Edit">
                                    <img src="admin_images/icons/pencil.png" alt="Edit" />
                                </a>
                            <a href="javascript:void(0);" title="Delete"  onClick="return deleteSingle('<?php echo $sub_categorie['SubCategorie']['id']; ?>');">
                                <img src="admin_images/icons/cross.png" alt="Delete" />
                            </a> 
                        </td>
                    </tr>
                <?php } ?>
            </tbody>

<!--            <tfoot>
                <tr>
                    <td colspan="6">
                        <div class="bulk-actions align-left">
                            <select name="data[TalentCategory][action]">
                                <option value="">Choose an action...</option>
                                <option value="delete">Delete</option>
                            </select>
                            <a class="button" href="javascript:void(0);" onclick="document.user_interest.submit();">Apply to selected</a>
                        </div>

                        <?php if ($this->params['paging']['Categorie']['pageCount'] > 1) { ?>
                            <div class="pagination">
                                <ul>
                                    <li class="page-prev"><?php echo $this->Paginator->prev(); ?></li>
                                    <li><?php echo $this->Paginator->numbers(); ?></li>
                                    <li class="page-next"><?php echo $this->Paginator->next(); ?></li>
                                </ul>
                            </div>  End .pagination 
                        <?php } ?>
                        <div class="clear"></div>
                    </td>
                </tr>
            </tfoot>-->

        </table>
        <?php // $form->end(); ?>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

<script type="text/javascript">
    $(function() {
        $('.VerifyProUser').click(function() {
            var that = $(this);
            var id = $(this).attr('rel');
            var stats = '';
            if ($(this).is(':checked')) {
                status = 'checked';
            } else {
                status = 'unchecked';
            }
            $.ajax({
                url: "<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'pro_verification')); ?>",
                type: 'POST',
                async: false,
                data: {id: id, status: status},
                success: function(data) {
                    if (data == 'verified') {
                        that.attr('checked');
                    } else {
                        that.removedAttr('checked');
                    }

                }
            });

        });
    });
</script>