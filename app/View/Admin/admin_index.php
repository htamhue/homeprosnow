<div id="login-wrapper" class="png_bg">
    <div id="login-top">
        <h1>homeprosnow</h1>
        <!-- Logo (221px width) -->
        <img id="logo" src="img/logo.png" alt="" />
    </div> <!-- End #logn-top -->
    <div id="login-content">
        <form accept-charset="utf-8" method="post" id="login_form" action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_index')); ?>">
            <div class="notification information png_bg">
                <div>
                    Admin's Login. This login screen is valid for administrator only
                </div>
            </div>
            <?php echo $this->Session->flash(); ?>
            <p>
                <label>Email</label>
                <input type="text" maxlength="50" autocomplete="off" id="username" class="text-input" name="data[email]">
            <div id="invalid-username" class="validation_error"></div>
            </p>
            <div class="clear"></div>
            <p>
                <label>Password</label>
                <input type="password" autocomplete="off" id="password" class="text-input" name="data[passwrd]">
            <div id="invalid-password" class="validation_error"></div>
            </p>
            <div class="clear"></div>
            <p id="remember-password">
                <input type="checkbox" name="data[remember]" id="remember" /> Remember me
            </p>
            <div class="clear"></div>
            <p>
                <input class="button" type="submit" id="sub_login" value="Sign In" />
            </p>
        </form>
        
    </div> <!-- End #login-content -->
</div> <!-- End #login-wrapper -->