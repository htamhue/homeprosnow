
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Add Sub Category</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <form accept-charset="utf-8" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_add_sub_category')); ?>">
            <label>
                Category : 
            </label>
            <select name="data[category_id]">
                <option value="0">
                    Select category
                </option>
                <?php
                foreach ($categories as $categorie) {
                    ?>
                    <option value="<?php echo $categorie['Categorie']['id'] ?>"><?php echo $categorie['Categorie']['category'] ?></option>
                    <?php
                }
                ?>
            </select>
            <br/>
            <br/>
            <label>
                Sub Category : 
            </label>
            <input type="text" name="data[sub_category]" />
            <br/>
            <br/>
            
            <input type="submit" value="save"/>
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

