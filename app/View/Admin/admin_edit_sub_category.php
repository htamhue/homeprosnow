
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Edit Sub Category</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <form accept-charset="utf-8" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_edit_sub_category')); ?>">
            <input type="hidden" name="data[id]" value="<?php echo  $sub_category['SubCategorie']['id']?>" />
            <label>
                Category : 
            </label>
            <select name="data[category_id]">
                <option value="0">
                    Select category
                </option>
                <?php
                foreach ($categories as $categorie) {
                    ?>
                <option <?php if ($sub_category['SubCategorie']['category_id'] == $categorie['Categorie']['id']) { ?>  selected <?php } ?> value="<?php echo $categorie['Categorie']['id'] ?>"><?php echo $categorie['Categorie']['category'] ?></option>
                    <?php
                }
                ?>
            </select>
            <br/>
            <br/>
            <label>
                Sub Category : 
            </label>
            <input type="text" value="<?php echo $sub_category['SubCategorie']['sub_category'] ?>" name="data[sub_category]" />
            <br/>
            <br/>

            <input type="submit" value="save"/>
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

