
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Edit website template</h3>
    </div> <!-- End .content-box-header -->
<?php 
    $var = '';
?>
    <div class="content-box-content">
        <form accept-charset="utf-8" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_edit_website_template')); ?>">
            <input type="hidden" value="<?php echo $website_template['WebsiteTemplate']['id'] ?>" name="data[website_template_id]" />
            <label>
                Title : 
            </label>
            <input type="text" value="<?php echo $website_template['WebsiteTemplate']['title'] ?>" name="data[title]" />
            <br/>
            <br/>
            <label>
                Website Template File: 
            </label>
            <br/>
            <br/>
            <img alt="" width="80px" src="uploaded_files/website_templates/<?php echo $website_template['WebsiteTemplate']['image'] ?>" />
            <br/>
            <br/>
            <input type="file" name="image" />
            <br/>
            <br/>
            <input type="submit" value="save"/>
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

