<?php // echo $html->script(array("admin_change.js", "errormessages.js")); ?>

<div class="content-box"><!-- Start Content Box -->

<div class="content-box-header">

<h3><?php echo $pageTitle; ?></h3>

</div> <!-- End .content-box-header -->

<div class="content-box-content">

<?php echo $form->create("Admin",array("type"=>"file","id"=>"admin","url"=>BASE_PATH_ADMIN."/admin/change")); ?>

<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
<p>
<label>Username</label>
<?php echo $form->input("username",array("class"=>"text-input medium-input","type"=>"text","id"=>"username","div"=>false,"label"=>false,"value"=>$username)); ?>
<span class="input-notification error png_bg" id="invalid-username"></span>
</p>

<p>
<label>Email</label>
<?php echo $form->input("email",array("class"=>"text-input medium-input","type"=>"text","id"=>"email","div"=>false,"label"=>false,"value"=>$user_email)); ?>
<span class="input-notification error png_bg" id="invalid-email"></span>
</p>

<p>
<label>Current Password</label>
<?php echo $form->input("current_password", array("id"=>"old_password", "label"=>false, 'div'=>false,'class'=>'text-input medium-input','type'=>'password')); ?>
<span class="input-notification error png_bg" id="invalid-old_password"></span>
</p>

<p>
<label>New Password</label>
<?php echo $form->input("passwrd", array("id"=>"new_password", "label"=>false, 'div'=>false,'class'=>'text-input medium-input','type'=>'password')); ?>
<span class="input-notification error png_bg" id="invalid-new_password"></span>
</p>

<p>
<label>Confirm Password</label>
<?php echo $form->input("confirm_password", array("id"=>"confirm_password", "label"=>false, 'div'=>false,'class'=>'text-input medium-input','type'=>'password')); ?>
<span class="input-notification error png_bg" id="invalid-confirm_password"></span>
</p>

<p>
<input class="button" type="submit" value="Submit" id="sub_btn" />
<input type="hidden" name="data[Admin][id]" id="id" value="<?php echo $admin_id; ?>"  />
</p>
</fieldset>

<div class="clear"></div><!-- End .clear -->

</form>

</div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->