
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Add website template</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <form accept-charset="utf-8" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_add_website_template')); ?>">
            <label>
                Title : 
            </label>
            <input type="text" name="data[title]" />
            <br/>
            <br/>
            <label>
                Website Template File: 
            </label>
            <input type="file" name="image" />
            <br/>
            <br/>
            <input type="submit" value="save"/>
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

