
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Featured category</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">

        <?php // echo $form->create("UserInterests", array("id" => "user_interest", "url" => BASE_PATH_ADMIN . "/user_interests/delete_all", "name" => "borough")); ?>
        <table>
            <thead>

                <tr>
                    <!--<th><input class="check-all" type="checkbox" /></th>-->
                    <!--<th>Id</th>-->
                    <th>Category</th>
                    <th>Image</th>

                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($FeaturedCategories as $FeaturedCategorie) { ?>
                    <tr id="iD_<?php echo $FeaturedCategorie['FeaturedCategorie']['id']; ?>">
                        <td><?php echo $FeaturedCategorie['Categorie']['category']; ?></td>
                        <td><img height="50" alt="" src="uploaded_files/featured_category_image/<?php echo $FeaturedCategorie['FeaturedCategorie']['image']; ?>"/></td>
                        <td>
                            <!-- Icons -->
                            <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_edit_featured_category', 'featured_category_id' => $FeaturedCategorie['FeaturedCategorie']['id'])); ?>" title="Edit">
                                <img src="admin_images/icons/pencil.png" alt="Edit" />
                            </a>
<!--                            <a href="javascript:void(0);" title="Delete"  onClick="return deleteSingle('<?php echo $FeaturedCategorie['FeaturedCategorie']['id']; ?>');">
                                <img src="admin_images/icons/cross.png" alt="Delete" />
                            </a> -->
                        </td>
                    </tr>
                <?php } ?>
            </tbody>

<!--            <tfoot>
                <tr>
                    <td colspan="6">
                        <div class="bulk-actions align-left">
                            <select name="data[TalentCategory][action]">
                                <option value="">Choose an action...</option>
                                <option value="delete">Delete</option>
                            </select>
                            <a class="button" href="javascript:void(0);" onclick="document.user_interest.submit();">Apply to selected</a>
                        </div>

            <?php if ($this->params['paging']['Categorie']['pageCount'] > 1) { ?>
                                <div class="pagination">
                                    <ul>
                                        <li class="page-prev"><?php echo $this->Paginator->prev(); ?></li>
                                        <li><?php echo $this->Paginator->numbers(); ?></li>
                                        <li class="page-next"><?php echo $this->Paginator->next(); ?></li>
                                    </ul>
                                </div>  End .pagination 
            <?php } ?>
                        <div class="clear"></div>
                    </td>
                </tr>
            </tfoot>-->

        </table>
        <?php // $form->end(); ?>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

