
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Edit Featured category</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <form accept-charset="utf-8" id="temp_featured_img_upload" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_temp_featured_img_upload')); ?>">
            <label>
                Category : 
            </label>

            <input type="hidden" name="data[id]" value="<?php echo $FeaturedCategorie['FeaturedCategorie']['id']; ?>" />
            <select name="data[category_id]">
                <?php foreach ($Categories as $Categorie) { ?>
                    <option value="<?php echo $Categorie['Categorie']['id']; ?>" <?php if ($Categorie['Categorie']['id'] == $FeaturedCategorie['Categorie']['id']) { ?> selected="selected" <?php } ?>><?php echo $Categorie['Categorie']['category']; ?></option>
                <?php } ?>
            </select>
            <br/>
            <br/>
            <label>
                Featured Image : 
            </label>
            <span>
                <img alt="" height="100" src="uploaded_files/featured_category_image/<?php echo $FeaturedCategorie['FeaturedCategorie']['image']; ?>" />
            </span>

            <br/>
            <br/>

            <label>
                Upload Featured Image:
            </label>
            <span>
                <input type="file" name="featured_iamge" /> 
            </span>
            <br/>
            <br/>
            <input type="submit" value="Temporary upload" />
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

