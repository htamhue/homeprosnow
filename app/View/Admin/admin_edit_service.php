
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Eidt service</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <form accept-charset="utf-8" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_edit_service')); ?>">
            <input type="hidden" value="<?php echo $service['Service']['id'] ?>" name="data[id]" />
            <label>
                Category : 
            </label>
            <select class="category_input" name="data[category_id]">
                <option value="0">
                    Select category
                </option>
                <?php
                foreach ($categories as $categorie) {
                    ?>
                    <option <?php if ($categorie['Categorie']['id'] == $service['Service']['category_id']) { ?> selected <?php } ?> value="<?php echo $categorie['Categorie']['id'] ?>"><?php echo $categorie['Categorie']['category'] ?></option>
                    <?php
                }
                ?>
            </select>
            <br/>
            <br/>
            <label>
                Sub Category : 
            </label>
            <select class="sub_category_input" name="data[sub_category_id]">
                <option value="0">
                    Select category
                </option>
                <?php
                foreach ($sub_categories as $sub_categorie) {
                    ?>
                    <option <?php if ($sub_categorie['SubCategorie']['id'] == $service['Service']['sub_category_id']) { ?> selected <?php } ?>  value="<?php echo $sub_categorie['SubCategorie']['id'] ?>"><?php echo $sub_categorie['SubCategorie']['sub_category'] ?></option>
                    <?php
                }
                ?>
            </select>
            <br/>
            <br/>
            <label>
                Service : 
            </label>
            <input type="text" value="<?php echo $service['Service']['service']; ?>" name="data[service]" />
            <br/>
            <br/>

            <input type="submit" value="save"/>
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

<script type="text/javascript">
    $(function() {
        $('.content-box-content').on('change', '.category_input', function() {
            var category_id = $(this).val();
            var url = '<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_get_sub_category_by_id')); ?>';
            var postData = {'category_id': category_id};
            $.post(url, postData, function(data) {
                $('.sub_category_input').html(data);
            }, 'text');
        });
    });
</script>

