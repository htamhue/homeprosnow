<link rel="stylesheet" type="text/css" href="jquery.imgareaselect-0.9.10/css/imgareaselect-animated.css" />
<script type="text/javascript" src="jquery.imgareaselect-0.9.10/scripts/jquery.imgareaselect.js"></script>
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Edit Featured category</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
<!--        <form accept-charset="utf-8" id="temp_featured_img_upload" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_featured_img_upload')); ?>">

            <label>
                Featured Image : 
            </label>
            <span>
                <img alt="" src="<?php echo $featured_iamge; ?>" />
            </span>


            <input type="submit" value="Upload" />
        </form>-->





        <script type="text/javascript">
            $(function () {
                var featured_category_id = <?php echo $FeaturedCategorieId; ?>;
                $('#resize_form').submit(function (event) {
//                if (typeof (thumbAspectRatio) !== 'undefined') {
//                    var bigImgCropped = $('input[name="width"]').val();
//                    var thumbCropped = $('input[name="twidth"]').val();
//                    if (bigImgCropped == '' || thumbCropped == '') {
//                        alert('Please, crop both full image and thumbnail versions before uploading');
//                        event.preventDefault;
//                        return false;
//                    }
//                } else {
                    var bigImgCropped = $('input[name="width"]').val();
                    var thumbCropped = $('input[name="twidth"]').val();
                    if (bigImgCropped == '' || thumbCropped == '') {
                        alert('Please, crop your photo before uploading');
                        event.preventDefault;
                        return false;
                    }
//                }
                });

                var aspectRatio = '871:593';



                $('.image_resize .the_image').imgAreaSelect({
                    handles: true,
                    aspectRatio: aspectRatio,
                    onSelectEnd: function (img, selection) {
                        $('input[name="data[x1]"]').val(selection.x1);
                        $('input[name="data[y1]"]').val(selection.y1);
                        $('input[name="data[x2]"]').val(selection.x2);
                        $('input[name="data[y2]"]').val(selection.y2);
                        $('input[name="data[width]"]').val($('.image_resize .the_image').width());
                        if (selection.x1 == selection.x2) {
                            $('input[name="data[width]"]').val('');
                        }
                    }
                });
            });



        </script>

        <div class="image_resize">
            <h2>
                Featured Image : 
            </h2>
            <img class="the_image" src="uploaded_files/temp_file/<?php echo $featured_iamge; ?>" />

        </div>

        <form id="resize_form" action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_featured_img_upload')); ?>" method="post">
            <input type="hidden" name="data[x1]" value="" />
            <input type="hidden" name="data[y1]" value="" />
            <input type="hidden" name="data[x2]" value="" />
            <input type="hidden" name="data[y2]" value="" />
            <input type="hidden" name="data[width]" value="" />
            <input type="hidden" name="data[id]" value="<?php echo $FeaturedCategorieId ?>" />
            <input type="hidden" name="data[image_name]" value="<?php echo $featured_iamge; ?>" />
            <button type="submit" class="resize_submit">Upload</button>
        </form>

    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

