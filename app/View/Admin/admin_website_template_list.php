<script type="text/javascript">

    function deleteSingle(id) {
        var base_path = $("#base_path").val();
        if (confirm('Are you sure to delete this item?')) {
            var url = '<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_delete_website_template')); ?>';
            var postData = {'id': id};
            $.post(url, postData, function(data) {
                if (data == 'success') {
                    $('#iD_' + id).fadeOut();
                }
                else {
                    alert(data);
                }
            }, 'text');
        }
    }
</script>
<div class="admin_add_button">
    <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_add_website_template')); ?>">Add website template</a>
</div>
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Website Template</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <table>
            <thead>

                <tr>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($template_lists as $template) { ?>
                    <tr id="iD_<?php echo $template['WebsiteTemplate']['id']; ?>">
                        <td><?php echo $template['WebsiteTemplate']['title']; ?></td>
                        <td>
                            <img alt="" width="100" src="uploaded_files/website_templates/<?php echo $template['WebsiteTemplate']['image']; ?>" />
                        </td>
                        <td>
                            <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_edit_website_template', 'website_template_id' => $template['WebsiteTemplate']['id'])); ?>" title="Edit">
                                <img src="admin_images/icons/pencil.png" alt="Edit" />
                            </a>
                            <a href="javascript:void(0);" title="Delete"  onClick="return deleteSingle('<?php echo $template['WebsiteTemplate']['id']; ?>');">
                                <img src="admin_images/icons/cross.png" alt="Delete" />
                            </a> 
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

