<script type="text/javascript">

    function deleteSingle(id) {
        var base_path = $("#base_path").val();
        if (confirm('Are you sure to delete this item?')) {
            var url = '<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_delete_recommendation')); ?>';
            var postData = {'id': id};
            $.post(url, postData, function(data) {
                if (data == 'success') {
                    $('#iD_' + id).fadeOut();
                }
                else {
                    alert(data);
                }
            }, 'text');
        }
    }
</script>
<div class="admin_add_button">
    <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_add_recommendation')); ?>">Add recommendation</a>
</div>
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>Recommendation</h3>
    </div> <!-- End .content-box-header -->

    <div class="content-box-content">
        <table>
            <thead>

                <tr>
                    <th>Cover photo</th>
                    <th>Profile photo</th>
                    <th>Client name</th>
                    <th>Contractor name</th>
                    <th>Rating</th>
                    <th>Recommendation</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($recommendations as $recommendation) { ?>
                    <tr id="iD_<?php echo $recommendation['Recommendation']['id']; ?>">
                        <td>
                            <img alt="" width="100" src="uploaded_files/recommendation_img/cover_photo/<?php echo $recommendation['Recommendation']['cover_photo']; ?>" />
                        </td>
                        <td>
                            <img alt="" width="100" src="uploaded_files/recommendation_img/profile_photo/<?php echo $recommendation['Recommendation']['profile_photo']; ?>" />
                        </td>
                        <td>
                            <?php echo $recommendation['Recommendation']['client_name']; ?>
                        </td>
                        <td>
                            <?php echo $recommendation['Recommendation']['contractor_name']; ?>
                        </td>
                        <td>
                            <?php echo $recommendation['Recommendation']['rating']; ?>
                        </td>
                        <td>
                            <?php echo $recommendation['Recommendation']['recommendation']; ?>
                        </td>

                        <td>
                            <a href="<?php echo $this->Html->url(array('controller' => 'admin', 'action' => 'admin_edit_recommendation', 'recommendation_id' => $recommendation['Recommendation']['id'])); ?>" title="Edit">
                                <img src="admin_images/icons/pencil.png" alt="Edit" />
                            </a>
                            <a href="javascript:void(0);" title="Delete"  onClick="return deleteSingle('<?php echo $recommendation['Recommendation']['id']; ?>');">
                                <img src="admin_images/icons/cross.png" alt="Delete" />
                            </a> 
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

<script type="text/javascript">
    $(function() {
        $('.VerifyProUser').click(function() {
            var that = $(this);
            var id = $(this).attr('rel');
            var stats = '';
            if ($(this).is(':checked')) {
                status = 'checked';
            } else {
                status = 'unchecked';
            }
            $.ajax({
                url: "<?php echo $this->Html->url(array('controller' => 'pro', 'action' => 'pro_verification')); ?>",
                type: 'POST',
                async: false,
                data: {id: id, status: status},
                success: function(data) {
                    if (data == 'verified') {
                        that.attr('checked');
                    } else {
                        that.removedAttr('checked');
                    }

                }
            });

        });
    });
</script>