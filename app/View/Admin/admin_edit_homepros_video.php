
<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3>homepros video update</h3>
    </div> <!-- End .content-box-header -->



    <div class="content-box-content">
        <form id="edit_homepros_video" accept-charset="utf-8" method="post"  enctype="multipart/form-data"  action="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_edit_homepros_video')); ?>">
            <input type="hidden" name="id" value="<?php echo $homepros_video['HomeprosVideo']['id']; ?>"
                   <label>
                Video Info
            </label>
            <input type="text" disabled value="<?php echo $homepros_video['HomeprosVideo']['video_info'] ?>" name="data[video_info]" />
            <br/>
            <br/>
            <label>
                Video link
            </label>
            <input type="text" class="video_link_input"  />
            <input type="hidden" class="video_code" name="data[video_code]" value="<?php echo $homepros_video['HomeprosVideo']['video_code'] ?>" />
            <br/>
            <br/>
            <input type="submit" value="Update"/>
        </form>



    </div> <!-- End .content-box-content -->

</div> <!-- End .content-box -->

<script type="text/javascript">

    $(function() {
        $('#edit_homepros_video').on('blur', '.video_link_input', function() {
            var urlYoutube = /(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?\s]*)/;
            var youtubeLink = $(this).val();
            if (youtubeLink.match(urlYoutube)) {
                var parts = youtubeLink.match(urlYoutube)
                var videoId = parts[2];
                $('.video_code').val(videoId);
            } else {
                alert('Wrong video link !!');
                $(this).val('').focus();
            }



        });
    });
</script>