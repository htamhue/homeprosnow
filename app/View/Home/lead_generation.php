<div class="pro_feature_area">
    <div class="heading_area">
        <div class="main_heading">
            Get Quality Leads for Project Near You
        </div>
        <div class="heading_border">

        </div>
    </div>

    <div class="pro_feature_details">
        Thousands of customers publish projects on Homeprosnow to find world class contractors in their area.
        We match these projects with qualified pros and feed you leads regularly. Every lead sent to you is first
        verified by our team of experts to check the authenticity of the job. Once the job is verified, we send it to
        you so you never run out of work.
    </div>
    <div class="por_feature_type">
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Get quality leads sent straight to you
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Bag more projects & increase revenues
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Never run out of work
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                All leads are verified by our team
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Leads are for top paying projects near you
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Grow your business swiftly & successfully 
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="pro_feature_star">
        ***
    </div>
    <div class="heading_area">
        <div class="main_heading">
            Register your business for free
        </div>

    </div>
    <a href="<?php echo $this->Html->Url(array('controller' => 'pro','action' => 'add_pro')); ?>" class="common_button business_list_btn">List your business for free</a> 


</div>