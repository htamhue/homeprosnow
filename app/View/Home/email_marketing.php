<div class="pro_feature_area">
    <div class="heading_area">
        <div class="main_heading">
            Reach Potential Customers with Powerful Emails
        </div>
        <div class="heading_border">

        </div>
    </div>

    <div class="pro_feature_details">
        Along with establishing your online presence with a cutting edge website, Homeprosnow also specializes
        in driving your brand online with our winning marketing strategies. We use the power of emails to reach
        potential customers for your business by creating awesome newsletters, promotions and more. Our
        email marketing team ensures that your customers stay regularly updated with your business services.
    </div>
    <div class="por_feature_type">
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Increase your brand presence
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Communicate real-time, personal emails
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Promote offers to your customers
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Reduce your overhead costs
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Drive sales & increase revenues
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Track sales and user engagement
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="pro_feature_star">
        ***
    </div>
    <div class="heading_area">
        <div class="main_heading">
            Register your business for free
        </div>

    </div>
    <a href="<?php echo $this->Html->Url(array('controller' => 'pro','action' => 'add_pro')); ?>" class="common_button business_list_btn">List your business for free</a> 


</div>