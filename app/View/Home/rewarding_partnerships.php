<div class="pro_feature_area">
    <div class="heading_area">
        <div class="main_heading">
            Build Successfull Partnerships
        </div>
        <div class="heading_border">

        </div>
    </div>

    <div class="pro_feature_details">
        At Homeprosnow, we help you build key partnerships with featured contractors so you can exchange
        leads, get paid and grow your business faster. Our partnership program is open to all contractors 
        registered with us and we encourage you to take advantage of this. Our team of experts also offer
        continuous support to ensure that the partnerships are rewarding for both parties.
    </div>
    <div class="por_feature_type">
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Build mutually beneficial partnerships
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Get more quality projects referred to you
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Expand and grow your business
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Earn more revenue & increase sales
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Have a constant flow of quality work
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Get continuous support from our team
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="pro_feature_star">
        ***
    </div>
    <div class="heading_area">
        <div class="main_heading">
            Register your business for free
        </div>

    </div>
    <a href="<?php echo $this->Html->Url(array('controller' => 'pro','action' => 'add_pro')); ?>" class="common_button business_list_btn">List your business for free</a> 


</div>