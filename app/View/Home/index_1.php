<div class="category_area">
    <!--    <div class="heading">
            Find professionals near you
        </div>-->
    <div class="heading_area">
        <div class="main_heading">
            Find Professionals Near You
        </div>
        <div class="heading_border">

        </div>
        <div class="sub_main_heading">
            Find top rated pros near you and get your next job done successfully.
        </div>
    </div>
    <?php
    foreach ($FeaturedCategories as $FeaturedCategorie) {
        ?>
        <div class="col-sm-4 category">
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'find_pro', 'category_id' => $FeaturedCategorie['Categorie']['id'])); ?>">
                <div class="category_img">
                    <img alt="" width="100%" src="uploaded_files/featured_category_image/<?php echo $FeaturedCategorie['FeaturedCategorie']['image']; ?>" />
                    <!--                    <div class="category_overlay">
                    
                                        </div>-->
                </div>

                <div class="category_text">
                    <span class="category_real_text">
                        <?php echo $FeaturedCategorie['Categorie']['category']; ?>
                    </span>
                </div>
            </a>

        </div>
    <?php } ?>

    <div class="clear"></div>
    <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'find_more_pro')); ?>" class="common_button find_more_btn">Find more pros</a>
</div>
<div class="how_it_work_area">
    <!--    <div class="heading">
            How it works?
        </div>-->

    <div class="heading_area">
        <div class="main_heading">
            How It Works?
        </div>
        <div class="heading_border">

        </div>
        <div class="sub_main_heading">
            Publish your project and hire experienced contractors within minutes.
        </div>
    </div>

    <div class="how_it_work_type_area">
        <div class="how_it_work_type col-sm-4">
            <div class="type_image share_details_icon">
                <img alt="" src="img/publish_your_project.png" />
            </div>
            <div class="type_heading">
                Publish Your Project
            </div>
            <div class="type_text">
                Post your project and we’ll match
                it with the right pros
            </div>
        </div>
        <div class="how_it_work_type col-sm-4">
            <div class="type_image responses_icon">
                <img alt="" src="img/get_responses.png" />
            </div>
            <div class="type_heading">
                Get Responses
            </div>
            <div class="type_text">
                See cost estimates and ask
                more questions
            </div>
        </div>
        <div class="how_it_work_type col-sm-4">
            <div class="type_image hire_icon">
                <img alt="" src="img/hire_confidently.png" />
            </div>
            <div class="type_heading">
                Hire Confidently
            </div>
            <div class="type_text">
                Check reviews, prices and
                hire with ease
            </div>
        </div>
        
        <div class="clear"></div>
        
        <div class="normal_border"></div>
        
        <div class="hiw_video">
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $landing_video['HomeprosVideo']['video_code']; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</div>

<div class="recommendation_area">
    <!--    <div class="heading">
            Customer Recommendations
        </div>
        <div class="sub_heading">
            Our pros are backed by excellent reviews from the community
        </div>-->

    <div class="heading_area">
        <div class="main_heading">
            Customer Recommendations
        </div>
        <div class="heading_border">

        </div>
        <div class="sub_main_heading">
            Our pros are backed by excellent reviews from the community
        </div>
    </div>


    <div  class="recommendation_group row">
        <?php
        foreach ($recommendations as $recommendation) {
            $recommendation_url = '';
            if ($recommendation['Recommendation']['client_name'] == 'Paul') {
                $recommendation_url = 'http://www.taozenflooring.com';
            } else if ($recommendation['Recommendation']['client_name'] == 'Susan') {
                $recommendation_url = 'http://www.statelineflooring.com';
            } else if ($recommendation['Recommendation']['client_name'] == 'Fred') {
                $recommendation_url = 'http://www.doublepainting.com';
            }
            ?>
            <div class="col-sm-6 col-md-4 single_recommendation">
                <div class="recommendation">
                    <div class="recommendation_banner">
                        <img alt="" width="100%" src="uploaded_files/recommendation_img/cover_photo/<?php echo $recommendation['Recommendation']['cover_photo'] ?>" />
                    </div>
                    <div class="recommendation_user">
                        <a href="<?php echo $recommendation_url; ?>">
                            <img alt="" src="uploaded_files/recommendation_img/profile_photo/<?php echo $recommendation['Recommendation']['profile_photo'] ?>" />
                        </a>
                    </div>
                    <div class="recommendation_content">
                        <div class="recommendation_heading">
                            <?php echo $recommendation['Recommendation']['client_name']; ?> Hired <?php echo $recommendation['Recommendation']['contractor_name']; ?>
                        </div>
                        <div class="recommendation_rating">
                            <?php
                            $rating = 20 * $recommendation['Recommendation']['rating'];
                            ?>
                            <div class="rating_area">
                                <div class="rated_area" style="width: <?php echo $rating; ?>%;"></div>
                            </div>
                        </div>
                        <div class="recommendation_text">
                            <?php echo $recommendation['Recommendation']['recommendation']; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="clear"></div>
    </div>
</div>
<div class="business_adding_area">
    <!--    <div class="heading">
            Want to add your business?
        </div>
        <div class="sub_heading">
            Join thousands of pros in the United States who are growing with us
        </div>-->

    <div class="heading_area">
        <div class="main_heading">
            <!--Want To Get Quality Leads?-->
            Get a quote for your project today
        </div>
        <div class="heading_border">

        </div>
        <div class="sub_main_heading">
            <!--Join thousands of pros in the United States who are growing with us.-->
            Connect with verified pros and get all the help you need
        </div>
    </div>

    <a href="<?php echo $this->Html->Url(array('controller' => 'pro','action'=>'add_project')); ?>" class="common_button business_list_btn">
        <!--List your business for free-->
        Get A Quote
    </a> 
</div>