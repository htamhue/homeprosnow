<div class="pro_feature_area">
    <div class="heading_area">
        <div class="main_heading">
            Get Ranked Higher on Search Engines
        </div>
        <div class="heading_border">

        </div>
    </div>

    <div class="pro_feature_details">
        Our SEO team utilizes the practice of search engine optimization to increase the number of visitors to your
        website. We do this by obtaining high ranking placements on search engines to improve your website’s
        search rankings on all significant seach engines like Google,Yahoo, Bing, etc. More visitors to your website
        directly results in more revenue for your business.
    </div>
    <div class="por_feature_type">
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Optimize your website for search engines
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Drive more visitors to your website
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Get ranked on the first page of Google
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Increase direct streams of revenue
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Reach more people with our SEO services
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Get on-going support from our team
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="pro_feature_star">
        ***
    </div>
    <div class="heading_area">
        <div class="main_heading">
            Register your business for free
        </div>

    </div>
    <a href="<?php echo $this->Html->Url(array('controller' => 'pro','action' => 'add_pro')); ?>" class="common_button business_list_btn">List your business for free</a> 


</div>