<div class="start_a_projects">

    <div class="get_quotes_area" style="background: white;">
        <div class="get_quotes_heading" style="background: #fbf9f3; padding: 17px 0;">
            Contact Our Support Team
        </div>

        <?php
        $faqs = [
            [
                'heading' => "How do customers use Homeprosnow?",
                'details' => "Hiring a contractor on Homeprosnow is quick and hassle free. You can scan through our list of verified pros and request a quote from any one of them. You can also submit a project for our team to handpick then top 5 pros around you. We connect you with the contractors and you get to hire the one that best fits your requirements."
            ], [
                'heading' => "Do I pay Homeprosnow a fee to hire a contractor?",
                'details' => "No, you don’t pay any fee to submit a project and hire a contractor on Homeprosnow. We built the platform to ensure that you can hire with ease without hurting your pocket."
            ], [
                'heading' => "As a contract, how do I sign up and get leads?",
                'details' => "Signing up to Homeprosnow is quick, free and takes less than a minute. We need a few details to verify your business and then you are all set. Click here to sign up. (Add a hyperlink to the become a pro page on the word ‘here’)"
            ], [
                'heading' => "What are the benefits of joining Homeprosnow?",
                'details' => "Homeprosnow was designed to connect exceptional contractors with quality projects. Our aim has always been to ensure you get really good projects that add value to your service. We check every lead that comes through our platform to ensure it is genuine and has all the requirements need to pass on to you.\n\nAdditionally, contractor can register for free to Homeprosnow. They only pay to get featured on our site or pay per lead."
            ], [
                'heading' => " Can customers review a contractor on Homeprosnow?",
                'details' => "We believe in operating with absolute transparency so our customers and contractors know exactly how we’re helping them. Part of that is the ability for customers to share their feedback with others on our platform. This is a great resource for any contractor that delivers exceptional work."
            ],
        ];
        ?>

        <div class="row">
            <div class="col-sm-6 faq_area">
                <div class="panel-group">
                    <?php foreach ($faqs as $k => $faq) { ?>
                        <div class="panel <?php echo $k == 0 ? 'panel-default' : ''; ?>">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <?php echo $faq['heading']; ?>
                                </h4>
                                <a data-toggle="collapse" href="#q<?php echo $k; ?>" class="collapse_btn <?php echo $k != 0 ? 'collapsed' : ''; ?>">
                                    <span class="fa fa-chevron-down down_arrow"></span>
                                    <span class="fa fa-chevron-up up_arrow"></span>
                                </a>
                            </div>
                            <div id="q<?php echo $k; ?>" class="panel-collapse collapse <?php echo $k == 0 ? 'in' : ''; ?>">
                                <div class="panel-body">
                                    <?php echo $faq['details']; ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div> 
            </div>
            <div class="col-sm-6">
                <div class="get_quotes_details">
                    <form action="<?php echo $this->Html->Url(array('controller' => 'home', 'action' => 'contact_submit')); ?>" id="contactForm" >
                        <div class="col-md-12 project_form">

                            <div class="input_list">
                                <div class="input_text">
                                    Your Name
                                </div>
                                <input type="text" name="name" class="name" />
                                <div class="nameError error_msg"></div>
                            </div>

                            <div class="input_list">
                                <div class="input_text">
                                    Your Email
                                </div>
                                <input type="text" name="email" class="email" />
                                <div class="emailError error_msg"></div>
                            </div>

                            <div class="input_list">
                                <div class="input_text">
                                    Message
                                </div>
                                <textarea name="message" class="message"></textarea>
                                <div class="msgError error_msg"></div>
                            </div>

                            <a href="javascript:" class="submit_request common_button">
                                Submit Request
                            </a>

                        </div>
                    </form>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>

    <br />
    <br />

    <script type="text/javascript">
        $(function () {

            $('.submit_request').click(function () {
                $('#contactForm').submit();
            });
            var options = {
                beforeSubmit: showRequest, // pre-submit callback 
                success: showResponse, // post-submit callback 
                type: 'post', // 'get' or 'post', override for form's 'method' attribute 
                dataType: 'json'        // 'xml', 'script', or 'json' (expected server response type) 
            };
            // bind to the form's submit event 
            $('#contactForm').submit(function () {
                $(this).ajaxSubmit(options);
                return false;
            });
        });
        function showRequest(formData, jqForm, options) {
            $('#contactForm').find('.error_msg').hide();
            return true;
        }

        // post-submit callback 
        function showResponse(responseText, statusText, xhr, $form) {
            if (responseText.type == 'error') {
                //            alert_r(responseText);
                if (responseText.errorMsg.nameError) {
                    $('#contactForm').find('.nameError').html(responseText.errorMsg.nameError).show();
                }
                if (responseText.errorMsg.emailError) {
                    $('#contactForm').find('.emailError').html(responseText.errorMsg.emailError).show();
                }
                if (responseText.errorMsg.msgError) {
                    $('#contactForm').find('.msgError').html(responseText.errorMsg.msgError).show();
                }

            } else if (responseText.type == 'success') {
                $('#successModal').modal();
            }

            $('#successModal').on('hidden.bs.modal', function () {
                window.location.href = "<?php echo $this->Html->Url(array('controller' => 'home')); ?>";
            });
        }

    </script>


    <div id="successModal" class="modal fade projectSubmitted " role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="projectSubmittedArea">
                    <img alt="" src="img/tick_mark_big.png"/>
                    <div class="congratulations">
                        Congratulations!
                    </div>
                    <div class="details">
                        Your request has been submitted. 
                        Our supprot team will get in touch with you soon.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style type="text/css">
        .search_area{
            display: none;
        }
        .faq_area .panel{
            position: relative;
            border-color: #f2f2f2 !important;
            margin-bottom: 20px;
        }
        .faq_area .panel .collapse_btn{
            position: absolute;
            top: 13px;
            right: 13px;
            border-radius: 30px;
            padding: 4px;
            background: #f2f5f7;
            color: #919799 !important;
            text-decoration: none;
            outline: none;
        }
        .faq_area .panel .collapse_btn .down_arrow{
            display: none;
        }
        .faq_area .panel .collapse_btn.collapsed .up_arrow{
            display: none;
        }
        .faq_area .panel .collapse_btn.collapsed .down_arrow{
            display: block;
        }
        .faq_area .panel-heading{
            background: white !important;
            padding: 17px 15px;
            color: #242424 !important;
        }
        .faq_area .panel-heading h1{
            font-family: "Open Sans",sans-serif;
            font-weight: bold !important;
            font-size: 17px !important;
        }
        .faq_area .panel-body{
            border-top: none !important;
            padding-top: 0 !important;
            color: #242424 !important;
            font-family: "Open Sans",sans-serif;
            font-size: 15px;
        }

        .dashboard_area .start_a_projects .get_quotes_area .project_form{
            background: #eee;
        }
    </style>