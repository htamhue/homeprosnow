<div class="about_us_area">

    <div class="row">
        <div class="col-sm-6 col-xs-10  center-block">
            <div class="about_us_text_area">
                <div class="about_us_text_heding">
                    Our values bind us. Our focus drives us.
                </div>
                <div class="about_us_text">
                    At Homeprosnow, we connect homeowners with top rated contractors, keeping in mind our 
                    goal to make every home beautiful. Our mission has always been to deliver exceptional service 
                    to our customers and constant support to our contractors. 
                </div>
                <div class="about_us_border">

                </div>
            </div>
        </div>
    </div>

    <div class="about_us_image">
        <img alt="" src="img/about_us_image_one.jpg" />
    </div>
    <div class="row">
        <div class="col-sm-6 col-xs-10  center-block">
            <div class="about_us_text_area">
                <div class="about_us_text_heding">
                    Less house, more home.
                </div>
                <div class="about_us_text">
                    We turn houses into homes and we look at every project as an opportunity to create a 
                    lasting  impression. It’s why  our customers have always been at the center of everything we
                    do at Homeprosnow. 
                </div>
                <div class="about_us_border">

                </div>
            </div>
        </div>
    </div>
    <div class="about_us_image">
        <img alt="" src="img/about_us_image_two.jpg" />
    </div>
    <div class="row">
        <div class="col-sm-6 col-xs-10 center-block">
            <div class="about_us_text_area">
                <div class="about_us_text_heding">
                    Trusted contractors, backed by excellent reviews.
                </div>
                <div class="about_us_text">
                    Our contractors are vetted by our team of experts and have years of quality work to back them up.
                    We understand the importance of completing every project successfully and our team provides
                    on-going support to our contractors so you never have to look elsewhere.
                </div>
                <div class="about_us_border">

                </div>
            </div>
        </div>
    </div>
</div>