<div class="pro_feature_area">
    <div class="heading_area">
        <div class="main_heading">
            Catapult Your Business with Social Media Marketing
        </div>
        <div class="heading_border">

        </div>
    </div>

    <div class="pro_feature_details">
        Millions of customers use social media every day. Our team of social media specialists work hard to ensure
        that these customers know about your business and are able to reach you when they have a new project
        that is relevant to you services. We use targetted ads to reach the right audience in your area & constantly
        remind them about your business services.
    </div>
    <div class="por_feature_type">
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Reach more people with targeted ads
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Get higher conversions rates
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Improve your brand loyalty
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Drive sales in a cost-effective way
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Gain customer insights & analytics
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Direct more users to you website
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="pro_feature_star">
        ***
    </div>
    <div class="heading_area">
        <div class="main_heading">
            Register your business for free
        </div>

    </div>
    <a href="<?php echo $this->Html->Url(array('controller' => 'pro','action' => 'add_pro')); ?>" class="common_button business_list_btn">List your business for free</a> 


</div>