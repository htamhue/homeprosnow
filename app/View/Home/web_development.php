<div class="pro_feature_area">
    <div class="heading_area">
        <div class="main_heading">
            Get A Stunning Website for Your Business
        </div>
        <div class="heading_border">

        </div>
    </div>

    <div class="pro_feature_details">
        With customers constantly browsing the web to look for world class contractors, not having a website
for your business can be a major disadvantage. At Homeprosnow, we help you fill that gap with a state
of the art website that will not only build your online presence but also get customers to reach you directly.
We also host your website on our own servers so you can focus more on growing your business.
    </div>
    <div class="por_feature_type">
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Get a state of the art website
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Establish your online presence
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Make it easy for customers to find you
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Promote your services online
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Get more leads directly from your site
            </div>
        </div>
        <div class="col-sm-6">
            <div class="single_pro_feature_type">
                <img alt="" src="img/feature_tick_mark.png" />
                Free hosting when we build your website
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="pro_feature_star">
        ***
    </div>
    <div class="heading_area">
        <div class="main_heading">
            Register your business for free
        </div>

    </div>
    <a href="<?php echo $this->Html->Url(array('controller' => 'pro','action' => 'add_pro')); ?>" class="common_button business_list_btn">List your business for free</a> 


</div>