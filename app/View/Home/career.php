<div class="career_area">

    <div class="row">
        <div class="col-sm-6 col-xs-10  center-block">
            <div class="about_us_text_area">
                <!--                <div class="about_us_text_heding">
                                    Our values bind us. Our focus drives us.
                                </div>-->
                <div class="about_us_text">
                    If there aren't any open positions you're re interested in, you can still send us your cv.
                    <br/>
                    We'll keep you in mind when a new spot opens that fits your qualifications.
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="headding">Open Positions</div>
        </div>
        <div class="col-sm-4">
            <div class="single_position">
                <div class="position_heading">
                    Sales Executive
                </div>
                <div class="position_text">
                    Identify new opportunities
                    and prospects by evaluating 
                    their position in the industry;
                    researching and analyzing
                    sales options.
                </div>
                <a class="apply_btn"  href="mailto:info@homeprosnow.com?subject=<?php echo urlencode('Apply for Sales Executive') ; ?>">Apply Now</a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="single_position">
                <div class="position_heading">
                    Marketing Manager
                </div>
                <div class="position_text">
                    Coordinate marketing
                    efforts for the company and
                    create new marketing
                    strategies to improve lead
                    generation campaigns.
                </div>
                <a class="apply_btn" href="mailto:info@homeprosnow.com?subject=<?php echo urlencode('Apply for Marketing Manager'); ?>">Apply Now</a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="single_position">
                <div class="position_heading">
                    Graphic Designer
                </div>
                <div class="position_text">
                    Create visual concepts
                    to communicate ideas that
                    inspire, inform or captivate
                    customers. Manage overall
                    design activities.
                </div>
                <a class="apply_btn" href="mailto:info@homeprosnow.com?subject=<?php echo urlencode('Apply for Graphic Designer');?>">Apply Now</a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="single_position">
                <div class="position_heading">
                    Customer Service Rep
                </div>
                <div class="position_text">
                    Answer service questions
                    from customers; suggesting
                    information about products 
                    and services.
                </div>
                <a class="apply_btn" href="mailto:info@homeprosnow.com?subject=<?php echo urlencode('Apply for Customer Service Rep'); ?>">Apply Now</a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="single_position">
                <div class="position_heading">
                    Quality Analyst
                </div>
                <div class="position_text">
                    Conduct quality checks on
                    contractors and projects
                    and verify leads that are
                    generated from sales and
                    marketing efforts.
                </div>
                <a class="apply_btn" href="mailto:info@homeprosnow.com?subject=<?php echo urlencode('Apply for Quality Analyst');?>">Apply Now</a>
            </div>
        </div>

    </div>


</div>