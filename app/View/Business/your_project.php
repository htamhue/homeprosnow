<div class="dashboad_content">
    <div class="your_project_area col-md-8">
        <div class="your_project_menu">
            <ul>
                <li id="current_project" class=" selected">
                    <a href="javascript:">Active</a>
                </li>
                <li id="complete_project">
                    <a href="javascript:">Completed</a>
                </li>
                <li id="canceled_project" >
                    <a href="javascript:">Cancelled</a>
                </li>
            </ul>
        </div>
        <div class="your_project_body">
            <div class="current_project project_area">
                <?php
                if ($projects['active_project']) {
                    foreach ($projects['active_project'] as $active_project) {
                        ?>
                        <div class="your_project">
                            <div class="your_project_heading">
                                <div class="your_project_heading_left">
                                    <?php echo $active_project['project_category']['Categorie']['category']; ?>
                                </div>
                                <div class="your_project_heading_right">
                                    $<?php echo $active_project['project_quote']['ProjectQuote']['amount']; ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="your_project_main">
                                <div class="col-md-3">
                                    <div class="project_user_pic">
                                        <?php
                                        if ($active_project['User']['profile_pic']) {
                                            ?>
                                            <img alt="" src="uploaded_files/profile_pic/<?php echo $active_project['User']['id'] ?>/<?php echo $active_project['User']['profile_pic'] ?>"/>
                                        <?php } else { ?>
                                            <img alt="" src="img/default_pro_pic.png"/>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="project_user_info">
                                        <div class="project_user_name">
                                            <?php echo $active_project['User']['name'] ?>
                                        </div>
                                        <?php if ($active_project['reviewCount']['Hire']['reviewCount']) { ?>
                                            <div class="project_user_rating">
                                                <?php
                                                $reviewPercent = ($active_project['rating_sum']['Hire']['userRating'] / $active_project['reviewCount']['Hire']['reviewCount']) * 20;
                                                ?>
                                                <div class="rating_area">
                                                    <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                                </div>
                                                <div class="review_text">
                                                    <?php echo $active_project['reviewCount']['Hire']['reviewCount'] ?> Reviews
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        <?php } ?>
                                        <div class="project_user_email">
                                            <?php echo $active_project['User']['email'] ?>
                                        </div>
                                        <div class="project_user_phone">
                                            <?php echo $active_project['User']['phone'] ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="request_payment_btn">
                                        <a href="javascript:" hire_id="<?php echo $active_project['Hire']['id']; ?>" class="common_button payment_request">
                                            Request Payment
                                        </a>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    No project found!!
                <?php } ?>
            </div>
            <div class="complete_project project_area">
                <?php
                if ($projects['complete_project']) {
                    foreach ($projects['complete_project'] as $active_project) {
                        ?>
                        <div class="your_project">
                            <div class="your_project_heading">
                                <div class="your_project_heading_left">
                                    <?php echo $active_project['project_category']['Categorie']['category']; ?>
                                </div>
                                <div class="your_project_heading_right">
                                    $<?php echo $active_project['project_quote']['ProjectQuote']['amount']; ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="your_project_main">
                                <div class="col-md-3">
                                    <div class="project_user_pic">
                                        <?php
                                        if ($active_project['User']['profile_pic']) {
                                            ?>
                                            <img alt="" src="uploaded_files/profile_pic/<?php echo $active_project['User']['id'] ?>/<?php echo $active_project['User']['profile_pic'] ?>"/>
                                        <?php } else { ?>
                                            <img alt="" src="img/default_pro_pic.png"/>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="project_user_info">
                                        <div class="project_user_name">
                                            <?php echo $active_project['User']['name'] ?>
                                        </div>
                                        <?php if ($active_project['reviewCount']['Hire']['reviewCount']) { ?>
                                            <div class="project_user_rating">
                                                <?php
                                                $reviewPercent = ($active_project['rating_sum']['Hire']['userRating'] / $active_project['reviewCount']['Hire']['reviewCount']) * 20;
                                                ?>
                                                <div class="rating_area">
                                                    <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                                </div>
                                                <div class="review_text">
                                                    <?php echo $active_project['reviewCount']['Hire']['reviewCount'] ?> Reviews
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        <?php } ?>
                                        <div class="project_user_email">
                                            <?php echo $active_project['User']['email'] ?>
                                        </div>
                                        <div class="project_user_phone">
                                            <?php echo $active_project['User']['phone'] ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <?php if (!$active_project['Hire']['pro_review']) { ?>
                                        <div class="review_button">
                                            <a href="javascript:" id="hire_<?php echo $active_project['Hire']['id'] ?>" rel="<?php echo $active_project['Hire']['id'] ?>" class="common_button">
                                                Review
                                            </a>
                                        </div>
                                    <?php } else { ?>

                                        <a href="javascript:" id="hire_<?php echo $active_project['Hire']['id'] ?>" hire_id="<?php echo $active_project['Hire']['id'] ?>" class="show_review common_button">
                                            Show Review
                                        </a>

                                    <?php } ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    No project found!!
                <?php } ?>
            </div>
            <div class="canceled_project project_area">
                <?php
                if ($projects['canceled_project']) {
                    foreach ($projects['canceled_project'] as $active_project) {
                        ?>
                        <div class="your_project">
                            <div class="your_project_heading">
                                <div class="your_project_heading_left">
                                    <?php echo $active_project['project_category']['Categorie']['category']; ?>
                                </div>
                                <div class="your_project_heading_right">
                                    $<?php echo $active_project['project_quote']['ProjectQuote']['amount']; ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="your_project_main">
                                <div class="col-md-3">
                                    <div class="project_user_pic">
                                        <?php
                                        if ($active_project['User']['profile_pic']) {
                                            ?>
                                            <img alt="" src="uploaded_files/profile_pic/<?php echo $active_project['User']['id'] ?>/<?php echo $active_project['User']['profile_pic'] ?>"/>
                                        <?php } else { ?>
                                            <img alt="" src="img/default_pro_pic.png"/>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="project_user_info">
                                        <div class="project_user_name">
                                            <?php echo $active_project['User']['name'] ?>
                                        </div>
                                        <?php if ($active_project['reviewCount']['Hire']['reviewCount']) { ?>
                                            <div class="project_user_rating">
                                                <?php
                                                $reviewPercent = ($active_project['rating_sum']['Hire']['userRating'] / $active_project['reviewCount']['Hire']['reviewCount']) * 20;
                                                ?>
                                                <div class="rating_area">
                                                    <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                                </div>
                                                <div class="review_text">
                                                    <?php echo $active_project['reviewCount']['Hire']['reviewCount'] ?> Reviews
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        <?php } ?>
                                        <div class="project_user_email">
                                            <?php echo $active_project['User']['email'] ?>
                                        </div>
                                        <div class="project_user_phone">
                                            <?php echo $active_project['User']['phone'] ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="request_payment_btn">
                                        <a href="javascript:" class="common_button">
                                            Request Payment
                                        </a>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    No project found!!
                    <?php
                }
                ?>
            </div>



        </div>
    </div>
</div>


<script type="text/javascript">
    $(function() {
        $('.your_project_menu').on('click', 'li', function() {
            $('.project_area').hide();
            $('.' + $(this).attr('id')).show();
            $('.your_project_menu').find('li').removeClass('selected');
            $(this).addClass('selected');
        });

        $('.your_project').on('click', '.review_button a', function() {
            var id = $(this).attr('rel');
            $('#givenRating').modal();
            $('.hire_id').val(id);
        });

        $('.your_project').on('click', '.payment_request', function() {
            var that = $(this);
            var parent = that.closest('.request_payment_btn');
            var hire_id = that.attr('hire_id');
            $.ajax({
                url: "payment/payment_request",
                type: 'POST',
                data: {hire_id: hire_id},
                success: function(data) {
                    if(data == 'success'){
                        that.html('Payment Requested');
                        that.removeClass('payment_request');
                    }
                }
            });
        });




    });
</script>