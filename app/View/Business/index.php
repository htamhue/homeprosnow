
<div class="your_business">
    <div class="top_banner pro_banner">

        <div class="banner_img">
            <img alt="" src="img/pro_banner.jpg" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content">
            <div class="banner_heading">
                Get leads for your business
            </div>
            <div class="banner_sub_heading">
                Thousands of clients are looking for talented pros. Get hired today!
            </div>
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro')); ?>" class="listing_business common_button">LIST YOUR BUSINESS FOR FREE</a>
        </div>
    </div>
</div>


<div class="category_area">
    <div class="heading">
        Listen to what other pros are saying
    </div>
    <div class="pro_video">
        <iframe width="524" height="320" src="https://www.youtube.com/embed/<?php echo $landing_video['HomeprosVideo']['video_code']; ?>" frameborder="0" allowfullscreen></iframe>
    </div>
</div>
<div class="how_it_work_area pro_page">
    <div class="heading">
        How it works?
    </div>
    <div class="how_it_work_type_area">
        <div class="how_it_work_type col-md-4">
            <div class="type_image share_details_icon">
                <img alt="" src="img/share_details_icon.png" />
            </div>
            <div class="type_heading">
                Receive new leads
            </div>
            <div class="type_text">
                Get instant notifications about new 
                projects near you
            </div>
        </div>
        <div class="how_it_work_type col-md-4">
            <div class="type_image responses_icon">
                <img alt="" src="img/responses_icon.png" />
            </div>
            <div class="type_heading">
                Connect with clients
            </div>
            <div class="type_text">
                Contact clients to discuss project
                requirements, budgets & other details.
            </div>
        </div>
        <div class="how_it_work_type col-md-4">
            <div class="type_image hire_icon">
                <img alt="" src="img/hire_icon.png" />
            </div>
            <div class="type_heading">
                Get hired
            </div>
            <div class="type_text">
                Grow your network and get hired to
                do the work you love
            </div>
        </div>
        <div class="clear"></div>

    </div>
</div>


<div class="business_adding_area pro_bottom">
    <div class="heading">
        Want to add your business?
    </div>
    <div class="sub_heading">
        Join thousands of pros in the United States who are growing with us
    </div>
    <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro')); ?>" class="common_button business_list_btn">GET STARTED</a> 
</div>