<div class="dashboad_content">
    <div class="your_business_area col-md-6">
        <a href="<?php echo $this->Html->Url(array('controller' => 'business', 'action' => 'your_business_details')); ?>">
            <div class="your_project_main">
                <div class="col-md-4">
                    <div class="project_user_pic">
                        <?php if ($pro_user['ProUser']['business_logo']) { ?>
                            <img alt="" src="uploaded_files/pro_user_files/business_logo/<?php echo $pro_user['ProUser']['business_logo']; ?>" />
                        <?php } else { ?>
                            <img alt="" src="img/company_logo.jpg" />
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="project_user_info">
                        <div class="project_user_name">
                            <?php echo htmlspecialchars($pro_user['ProUser']['company_name']); ?>
                        </div>
                        <div class="project_user_rating">
                            <?php
                            $reviewPercent = ($pro_user['rating_sum']['Hire']['proRating'] / $pro_user['reviewCount']['Hire']['reviewCount']) * 20;
                            ?>
                            <div class="rating_area">
                                <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                            </div>
                            <div class="review_text">
                                <?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> Reviews
                            </div>

                        </div>
                        <div class="experience">
                            <?php echo $pro_user['ProUser']['company_description']; ?>
                        </div>
                        <div class="project_user_email">
                            <img alt="" src="img/location_icon.png" />
                            <span><?php echo $pro_user['ProUser']['city']; ?></span>
                        </div>
                        <div class="project_user_phone">
                            <img alt="" src="img/verified_icon.png" />
                            <span>
                                <?php if ($pro_user['project_count'] > 1) { ?>
                                    <?php echo $pro_user['project_count']; ?> Projects
                                <?php } else { ?>
                                    <?php echo $pro_user['project_count']; ?> Project
                                <?php } ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php if ($pro_user['ProUser']['is_verified']) { ?>
                        <div class="request_payment_btn">
                            <img alt="" src="img/pro_verified_icon.png"/>
                        </div>
                    <?php } ?>
                </div>
                <div class="clear"></div>
            </div>
        </a>

    </div>
</div>