<div class="col-sm-6 paymen_box">
    <div class="added_payment">
        <div class="added_payment_heading">
            <input type="checkbox" <?php if ($single_payment_method['PaymentAccount']['default']) { ?> checked <?php } ?> method="<?php echo $single_payment_method['PaymentAccount']['method'] ?>" class="default_account" rel="<?php echo $single_payment_method['PaymentAccount']['id'] ?>" />
            Default
        </div>
        <div class="added_payment_body">
            <a rel="<?php echo $single_payment_method['PaymentAccount']['id']; ?>" href="javascript:" class="remove_payment">
                <img alt="" src="img/remove_icon.png" />
                Remove
            </a>
            <?php
            if ($single_payment_method['PaymentAccount']['payment_account_type'] == 'paypal') {
                ?>
                <div class="card_icon">
                    <img alt="" src="img/paypal_icon.jpg" />
                </div>
                <div class="card_holder_email">
                    <?php echo $single_payment_method['PaymentAccount']['paypal_email']; ?>
                </div>

            <?php } else { ?>
                <div class="card_icon">
                    <img alt="" src="img/visa_icon.png" />
                </div>
                <div class="card_holder_name">
                    <?php echo $single_payment_method['PaymentAccount']['name_on_the_card']; ?>
                </div>
                <div class="card_number">
                    <?php echo $single_payment_method['PaymentAccount']['card_number']; ?>
                </div>
                <div class="card_date">
                    <?php
                    if ($single_payment_method['PaymentAccount']['expiry_month'] < 10) {
                        $expiry_month = '0' . $single_payment_method['PaymentAccount']['expiry_month'];
                    } else {
                        $expiry_month =  $single_payment_method['PaymentAccount']['expiry_month'];
                    }
                    echo $expiry_month . '-' . $single_payment_method['PaymentAccount']['expiry_year']
                    ?>
                </div>
<?php } ?>

        </div>
    </div>

</div>