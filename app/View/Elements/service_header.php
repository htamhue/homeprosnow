<section class="banner_top">
    <div class="banner_container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="banner_text_area">
                    <div class="top_banner_head">
                        <!--                    Get a fully managed, custom website for just $59-->
                        <?php echo $header_data['header_heading']; ?>
                    </div>
                    <div class="top_banner_text">
                        <!--                    We build, host and manage your website so you can focus on what matters most - growing your business!-->
                        <?php echo $header_data['header_sub_heading']; ?>
                    </div>
                    <div class="top_banner_start_btn">
                        <a href="https://www.bond10.com/" target="_blank" class="btn btn-warning start_btn"><?php echo @$header_data['header_button_text']? $header_data['header_button_text']:'START HERE'; ?></a>
                    </div>
                </div>

                <!--                <div class="top_banner_ratting">
                                    <div class="star">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                    <div class="text">4.8-star rated by 376 contractors</div>
                                </div>-->
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="top_banner_img">
                    <img class="img img-responsive" src="images/<?php echo $header_data['header_img']; ?>"/>
                </div>
            </div>
        </div>
    </div>
</section>

