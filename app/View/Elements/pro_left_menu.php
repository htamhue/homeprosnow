<div class="col-md-3 get_quotes_link">
    <div class="get_quotes_link_heading">
        How it works?
    </div>
    <div class="how_it_work_type">
        <div class="type_image share_details_icon">
            <img alt="" src="img/share_details_icon.png" />
        </div>
        <div class="type_heading">
            Receive new leads
        </div>
        <div class="type_text">
            Get instant notifications about new 
            projects near you
        </div>
    </div>
    <div class="how_it_work_type ">
        <div class="type_image responses_icon">
            <img alt="" src="img/responses_icon.png" />
        </div>
        <div class="type_heading">
            Connect with clients
        </div>
        <div class="type_text">
            Contact clients to discuss project
            requirements, budgets & other details.
        </div>
    </div>
    <div class="how_it_work_type ">
        <div class="type_image hire_icon">
            <img alt="" src="img/hire_icon.png" />
        </div>
        <div class="type_heading">
            Get hired
        </div>
        <div class="type_text">
            Grow your network and get hired to
            do the work you love
        </div>
    </div>
</div>