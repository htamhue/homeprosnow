<section class="succeed">
    <div class="container">
        <div class="succeed_heading">
            We love to see you succeed
        </div>
        <div class="succeed_quats">
            "I close more leads with my website managed by Homeprosnow. They have really turned my business around."
        </div>
        <div class="succeed_star">
            <div class="star">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
            <div class="text">Jack C.</div>
            <div class="text">(allgreatpainting.com)</div>
        </div>
        <div class="succeed_quats">
            "Not only did Homeprosnow build a great website for me but they also managed every aspect of it. This was a
            huge relief as I did not have to hire a programmer to maintain my site. Once the leads started pouring in, all I
            had to do was take care of business while they took care of everything else."
        </div>
        <div class="succeed_star">
            <div class="star">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
            <div class="text">Christian H.</div>
            <div class="text">(statelineflooring.com)</div>
        </div>
    </div>
</section>
<section class="feature">
    <div class="container">
        <div class="feature_heading">Powerful Features</div>
        <div class="feature_heading_text">Every fully managed website comes with a host of fantastic features to help you scale your business.</div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 com-md-4 col-lg-4">
                <div class="feature_img">
                    <img class="img img-responsive" src="images/computer.png"/>
                </div>
                <div class="feature_name">Fully Managed</div>
                <div class="feature_description">
                    We manage your site so you can focus on your business
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 com-md-4 col-lg-4">
                <div class="feature_img">
                    <img class="img img-responsive" src="images/server.png"/>
                </div>
                <div class="feature_name">Free Hosting</div>
                <div class="feature_description">
                    Free web hosting that is lightning-fast and secure
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 com-md-4 col-lg-4">
                <div class="feature_img">
                    <img class="img img-responsive" src="images/promotion.png"/>
                </div>
                <div class="feature_name">Marketing Tools</div>
                <div class="feature_description">
                    Fully integrated marketing tools to reach your audience
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 com-md-4 col-lg-4">
                <div class="feature_img">
                    <img class="img img-responsive" src="images/mobile.png"/>
                </div>
                <div class="feature_name">Responsive Design</div>
                <div class="feature_description">
                    Our templates are designed to work flawlessly across all devices
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 com-md-4 col-lg-4">
                <div class="feature_img">
                    <img class="img img-responsive" src="images/code.png"/>
                </div>
                <div class="feature_name">Professional Email</div>
                <div class="feature_description">
                    Reply to leads from your new business email
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 com-md-4 col-lg-4">
                <div class="feature_img">
                    <img class="img img-responsive" src="images/http.png"/>
                </div>
                <div class="feature_name">Custom Domains</div>
                <div class="feature_description">
                    Help customers find you easily with a custom domain
                </div>
            </div>
        </div>
    </div>
</section>

<section class="grow">
    <div class="container">
        <div class="grow_heading">How do we help you grow?</div>
        <div class="grow_heading_text">
            Stay ahead of the competition by building your presence online so potential customers can find you easily. Our services drive more leads to your website so you can scale your business
            without having to worry about how to manage it.
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="grow_content">
                    <div class="grow_img">
                        <img class="img img-responsive" src="images/graphic.png"/>
                    </div>
                    <div class="grow_name">Design & Print</div>
                    <div class="grow_text">
                        Create branded flyersbrochures, coupons, etc.with the help of our talented designers.
                    </div>
                    <div class="grow_button">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'graphic_design')); ?>" class="btn btn-warning start_btn">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="grow_content">
                    <div class="grow_img">
                        <img class="img img-responsive" src="images/layer2.png"/>
                    </div>
                    <div class="grow_name">Search Marketing</div>
                    <div class="grow_text">
                        Get listed on all major search engines - Google, Bing, etc. so customers can find you easily.
                    </div>
                    <div class="grow_button">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'seo')); ?>" class="btn btn-warning start_btn">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="grow_content">
                    <div class="grow_img">
                        <img class="img img-responsive" src="images/social_media.png"/>
                    </div>
                    <div class="grow_name">Social Media</div>
                    <div class="grow_text">
                        Connect with customers on popular social media platforms and engage with them directly.
                    </div>
                    <div class="grow_button">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'social_media_marketing')); ?>" class="btn btn-warning start_btn">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="grow_content">
                    <div class="grow_img">
                        <img class="img img-responsive" src="images/mail.png"/>
                    </div>
                    <div class="grow_name">Email Marketing</div>
                    <div class="grow_text">
                        Keep your customers updated with new offers & promotions via email marketing.
                    </div>
                    <div class="grow_button">
                        <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'email_marketing')); ?>" class="btn btn-warning start_btn">LEARN MORE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bottom">
    <div class="container">
        <div class="footer_text">Are you ready to drive more leads with a fully managed website?</div>
        <div class="footer_button">
            <!--<a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'web_service')); ?>" class="btn btn-warning start_btn">GET ME STARTED</a>-->
            <a href="https://www.bond10.com/" target="_blank" class="btn btn-warning start_btn">GET ME STARTED</a>
        </div>
    </div>
</section> 