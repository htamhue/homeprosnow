<?php
if ( ($this->params['controller'] == 'businesspro' && $this->params['action'] == 'become_a_pro' ) || ($this->params['controller'] == 'businesspro' && $this->params['action'] == 'add_pro' ) ) {
    ?>
<?php
}elseif ($this->params['controller'] == 'pro' && $this->params['action'] == 'index') {
?>
    <div class="top_banner pro_banner">

        <div class="banner_img pro_banner_image">
            <img alt="" src="img/pro_banner.jpg" />
            <!--            <div class="banner_overlay">
            
                        </div>-->
        </div>
        <div class="banner_content">
            <div class="banner_heading">
                Get leads for your business
            </div>
            <div class="banner_sub_heading">
                Thousands of clients are looking for talented pros. Get hired today!
            </div>
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro')); ?>" class="listing_business common_button">LIST YOUR BUSINESS FOR FREE</a>
        </div>
    </div>

<?php } else if ($this->params['controller'] == 'pro' && $this->params['action'] == 'find_more_pro') {
?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/banner_one.jpg" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content">
            <div class="banner_heading">
                Find top pros near you
            </div>

            <div class="search_area">
                <input type="text" placeholder="zip code" class="zip_code_search" value="<?php echo $this->Session->read('user_zipcode'); ?>" />
                <input type="text" disabled placeholder="I need a" class="search_content" />
                <select class="search_type">
                    <option value="" selected hidden>Painter or Electrician, etc</option>
                    <?php
                    foreach ($categories as $category) {
                    ?>
                        <option value="<?php echo $category['Categorie']['id'] ?>"><?php echo $category['Categorie']['category'] ?></option>
                    <?php
                    }
                    ?>
                </select>
                <a href="javascript:" class="search_button common_button">Search</a>
            </div>
        </div>
    </div>
<?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'about_us') {
?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/about_us.jpg" width="100%" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content">
            <div class="banner_heading">
                About Us
            </div>
            <div class="banner_sub_heading">
                We built Homeprosnow to help you with everything you need.
            </div>

        </div>
    </div>
<?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'career') {
?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/carrer_bg.jpg" width="100%" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content">
            <div class="banner_heading">
                Join Our Team
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
<?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'email_marketing') {
?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/email_marketing_banner.jpg" width="100%" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Email Marketing
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
<?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'lead_generation') {
?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/lead_generation_banner.jpg" width="100%" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Lead Generation
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
<?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'rewarding_partnerships') {
?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/rewarding_partnerships_banner.jpg" width="100%" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Rewarding Partnerships
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
<?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'seo') {
?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/seo_banner.jpg" width="100%" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Search Engine Optimization
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
<?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'social_media_marketing') {
?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/social_media_marketing_banner.jpg" width="100%" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Social Media Marketing
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
<?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'web_development') {
?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/web_development_banner.jpg" width="100%" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Website Development & Hosting
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
<?php
} else {
?></div>
    </div>
    <main>
        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron banner">
            <div class="container">
                <div class="b-wrapper-text1">
                    <h2 class="b-title">Find reliable & verified home service providers.</h2>
                    <form action="<?php echo $this->Html->Url(array('controller' => 'businesspro', 'action' => 'become_a_pro')); ?>">
                        <div class="input-container">
                            <i class="fa fa-home fa-search" aria-hidden="true"></i>
                            <input type="hidden" id="category_id" value="" name="category_id">
                            <input class="search-inp" id="search" type="text" placeholder="What service do you need" name="">
                        </div>
                        <button type="submit" id="searchItem" class="btn btn-search">CREATE A FREE ACOUNT</button>
                    </form>
                </div>
            </div>
			    <script>
        $(function() {
            var data = [{
                category_id: "14",
                label: "Architecture and Building Design",
            }, {
                category_id: "55",
                label: "Carpentry & Woodworking",
            }, {
                category_id: "45",
                label: "Carpet and Upholstery",
            }, {
                category_id: "15",
                label: "Deck and Patio",
            }, {
                category_id: "23",
                label: "Design-Build Firms",
            }, {
                category_id: "29",
                label: "Door Sales and Installation",
            }, {
                category_id: "30",
                label: "Driveway Installation and Maintenance",
            }, {
                category_id: "7",
                label: "Electrical & Computer Services",
            }, {
                category_id: "39",
                label: "Environmental Services and Restoration",
            }, {
                category_id: "47",
                label: "Exterior Cleaning",
            }, {
                category_id: "31",
                label: "Fence Installation",
            }, {
                category_id: "32",
                label: "Fireplace Manufacturers and Showrooms",
            }, {
                category_id: "21",
                label: "Furniture and Accessories",
            }, {
                category_id: "40",
                label: "Furniture Repair and Upholstery",
            }, {
                category_id: "41",
                label: "Garage Door Services",
            }, {
                category_id: "42",
                label: "Gardeners, Lawn Care and Sprinklers",
            }, {
                category_id: "5",
                label: "General Contracting",
            }, {
                category_id: "33",
                label: "Architecture and Building Design",
            }, {
                category_id: "4",
                label: "Glass, Mirror and Shower Door",
            }, {
                category_id: "1",
                label: "ardwood Floors",
            }, {
                category_id: "54",
                label: "Heating, Ventilation &Air-Conditioning(HVAC)",
            }, {
                category_id: "49",
                label: "Home Staging",
            }, {
                category_id: "43",
                label: "Home Theater and Home Automation",
            }, {
                category_id: "34",
                label: "Hot Tub and Spa Sales and Showroom",
            }, {
                category_id: "16",
                label: "House Cleaning Services",
            }, {
                category_id: "59",
                label: "Insurance Services",
            }, {
                category_id: "6",
                label: "Interior Design & Decoration",
            }, {
                category_id: "48",
                label: "Junk Removal",
            }, {
                category_id: "19",
                label: "Kitchen and Bath Design",
            }, {
                category_id: "51",
                label: "Kitchen and Bath Remodeling",
            }, {
                category_id: "25",
                label: "Landscape Architecture and Design",
            }, {
                category_id: "3",
                label: "Landscaping",
            }, {
                category_id: "35",
                label: "Lighting Design and Supply",
            }, {
                category_id: "60",
                label: "Mortgage Lending Services",
            }, {
                category_id: "18",
                label: "Home Staging",
            }, {
                category_id: "11",
                label: "Painting Services",
            }, {
                category_id: "8",
                label: "Pest Control",
            }, {
                category_id: "52",
                label: "Photography",
            }, {
                category_id: "9",
                label: "Plumbing Services",
            }, {
                category_id: "61",
                label: "Real Estate Legal Services",
            }, {
                category_id: "20",
                label: "Real Estate Services",
            }, {
                category_id: "12",
                label: "Roofing and Gutter Sales & Services",
            }, {
                category_id: "13",
                label: "Septic Tank Services",
            }, {
                category_id: "44",
                label: "Siding and Exterior",
            }, {
                category_id: "36",
                label: "Staircase and Railing",
            }, {
                category_id: "26",
                label: "Stone, Pavement and Concrete",
            }, {
                category_id: "17",
                label: "Swimming Pool & Spa Installation & Maintenance",
            }, {
                category_id: "27",
                label: "Tile, Stone and Countertop",
            }, {
                category_id: "22",
                label: "Tree Services",
            }, {
                category_id: "28",
                label: "Vinyl, Tile & Linoleum Floors",
            }, {
                category_id: "37",
                label: "Window Sales, Repair and Installation",
            }, {
                category_id: "38",
                label: "Window Treatments",
            }];
            $("#search").autocomplete({
                minLength: 0,
                source: data,
                foucs: function(event, ui) {
                    $("#search").val(ui.item.label);
                    return false;
                },
                select: function(event, ui) {
                    $("#search").val(ui.item.label);
                    $('#category_id').val(ui.item.category_id);
                    // console.log(category_id);
                    return false;
                }
            })

            .data("ui-autocomplete")._renderItem = function(ul, item) {
                return $("<li>")
                    .append("<a>" + item.label + "<br>" + "</a>")
                    .appendTo(ul);
            };
            $('#searchItem').on('click', function() {
                window.location.href = 'https://homeprosnow.com/pro/pro_finder_lists/' + "category_id:" + $(category_id).val();
            })
        });
    </script>
        </div>
    <?php } ?>