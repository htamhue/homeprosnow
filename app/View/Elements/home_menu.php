<nav class="navbar navbar-expand-md navbar-light">
    
        <a href="<?php echo $this->Html->Url(array('controller' => 'home','action' => 'index')); ?>">
            <img class="img-responsive" alt="" src="assets/img/logo.png" />
        </a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="true" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <?php
                        if($this->params['controller'] == 'businesspro' && $this->params['action'] == 'add_pro'):
                            $add_pro = '<li class="nav-item active">
                                <a class="nav-link" href="'.$this->Html->Url(array('controller' => 'businesspro','action' => 'add_pro')).'">Start a project</a></li>';
                        else:
                            $add_pro = '<li class="nav-item">
                            <a class="nav-link" href="'.$this->Html->Url(array('controller' => 'businesspro','action' => 'add_pro')).'">Start a project</a></li>';
                        endif;
                        echo $add_pro;
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $this->Html->Url(array('controller' => 'pro','action' => 'profinder')); ?>">Profinder</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://homeprosnow.com/help">Help</a>
                    </li>
                    <?php
                        if($this->params['controller'] == 'businesspro' && $this->params['action'] == 'become_a_pro'):
                            $become_a_pro = '<li class="nav-item active">
                                <a class="nav-link" href="'.$this->Html->Url(array('controller' => 'businesspro','action' => 'become_a_pro')).'">Are you a business pro?</a></li>';
                        else:
                            $become_a_pro = '<li class="nav-item">
                            <a class="nav-link" href="'.$this->Html->Url(array('controller' => 'businesspro','action' => 'become_a_pro')).'">Are you a business pro?</a></li>';
                        endif;
                        echo $become_a_pro;
                    ?>
                </ul>
                <div class="align">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link loginModal" href="#" data-toggle="modal" data-target="#loginModal">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link signUpModal" href="#" data-toggle="modal" data-target="#signUpModal">Sign up</a>
                        </li>
                    </ul>
                </div>
	</div>
    <div class="clear"></div>
</nav>
<style>
.navbar{margin-bottom: 0px;}
</style>