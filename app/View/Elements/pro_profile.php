<?php
$blur_class = '';
if (!$this->Session->read('Auth.User.id')) {
    $blur_class = 'blurMode';
}
?>
<div class="dashboad_content dashboad_content_pro_new <?php echo $blur_class; ?>">
    <div class="view_profile_area">
        <div class="row new-pro-design">
            <div class="col-sm-9 pro-lb">
                <div class="row">
                    <div class="col-sm-4 pro-lb1">
                        <div class="col-sm-12 whitebox sidebar-l1">
                            <div class="pro-logo">
                                <a href="">
<!--                                    <img  src="img/profile_design/logo.png">-->
                                    <?php
                                    $pro_file = "uploaded_files/profile_pic/" . $pro_user['User']['id'] . "/" . $pro_user['User']['profile_pic'];

                                    if (file_exists($pro_file)) {
                                        ?>
                                        <img alt="" src="uploaded_files/profile_pic/<?php echo $pro_user['User']['id']; ?>/<?php echo $pro_user['User']['profile_pic']; ?>" />
                                    <?php } else { ?>
<!--                                        <img alt="" src="img/company_logo.jpg" />-->
                                        <img alt="" src="uploaded_files/profile_pic/<?php echo $pro_user['User']['id']; ?>/<?php echo $pro_user['User']['profile_pic']; ?>" />
                                    <?php } ?>





                                    <h2><?php echo $pro_user['ProUser']['company_name'] ?><!--Candoor Contracting LLC--></h2>
                                </a>

                                <?php if ($ownprofile) { ?>
                                    <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'edit_pro')); ?>">
                                        <img alt="" src="img/pencil_icon.png" width="12" />
                                    </a>
                                <?php } ?>
                            </div>

                            <?php if ($pro_user['reviewCount']['Hire']['reviewCount']) { ?>
                                <div class="pro-main-reviews">
                                    <?php $reviewPercent = ($pro_user['rating_sum']['Hire']['proRating'] / $pro_user['reviewCount']['Hire']['reviewCount']) * 20; ?>




                                    <div class="m-r">
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                        </div>
        <!--                                        <img  src="img/profile_design/sy.png">-->
                                        <p><span>(<?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> Reviews)</span></p>
                                    </div>

                                </div>
                            <?php } ?>
                            <?php
                            if ($pro_user['ProUser']['is_verified']) {
                                ?>
                                                                            <!--                                <img alt="" src="img/pro_verified_icon.png"/>-->
                                <div class="pro-verified">
    <!--                                    <img  src="img/profile_design/badge.png">-->
                                    <img  src="img/pro_verified_icon.png">
                                </div>
                                <?php
                            }
                            ?>

                            <div class="pro-social">
                                <ul>
                                    <li><a href=""><img  src="img/profile_design/f.png"></a></li>
                                    <li><a href=""><img  src="img/profile_design/t.png"></a></li>
                                    <li><a href=""><img  src="img/profile_design/l.png"></a></li>
                                    <li><a href=""><img  src="img/profile_design/g.png"></a></li>
                                    <li><a href=""><img  src="img/profile_design/y.png"></a></li>
                                </ul>
                            </div>
                            <div class="pro-basic-info">
                                <ul>
                                    <?php if ($pro_user['ProUser']['experience']) { ?>
                                        <li><span><i class="fa fa-calendar"></i></span> <em><?php echo $pro_user['ProUser']['experience']; ?></em></li>
                                    <?php } ?>
                                    <li><span><i class="fa fa-globe"></i></span> <em>
                                            <?php echo strlen($pro_user['ProUser']['website_url']) ? $pro_user['ProUser']['website_url'] : 'N/A' ?>
                                        </em></li>
                                    <li><span><i class="fa fa-phone"></i></span> <em><?php echo strlen($pro_user['ProUser']['business_phone']) ? $pro_user['ProUser']['business_phone'] : $pro_user['User']['phone']; ?></em></li>
                                    <li><span><i class="fa fa-envelope"></i></span> <em>xxx xxx xxxx</em></li>

                                    <?php $address = $pro_user['ProUser']['address'] . ", <br />" . $pro_user['ProUser']['city'] . ", " . $pro_user['ProUser']['state'] . ", " . $pro_user['ProUser']['zip_code']; ?>
                                    <li><span><i class="fa fa-map-marker"></i></span> <em><?= $address ?></em></li>
                                    <li><span><i class="fa fa-briefcase"></i></span> <em>
                                            <!--                                            67 Jobs-->
                                            <?php if ($pro_user['project_count'] > 1) { ?>
                                                <?php echo $pro_user['project_count']; ?> Projects
                                            <?php } else { ?>
                                                <?php echo $pro_user['project_count']; ?> Project
                                            <?php } ?>
                                        </em></li>
                                </ul>
                            </div>
                            <div class="pro-claim">

                                <!--                                <a href="">CLAIM BUSINESS</a>-->
                                <?php if (($pro_user['ProUser']['claim_status'] == 'not_claimed') && ($this->Session->read('Auth.User.user_type') == 'user')) { ?>
                                    <a href="javascript:" class="btn  claim_business_btn">Claim Business</a>
                                <?php } ?>



                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 pro-lb2">
                        <div class="col-sm-12 whitebox pro-content">
                            <h3>ABOUT <?php echo $pro_user['ProUser']['company_name']; ?></h3>

                            <p> <?php echo $pro_user['ProUser']['company_description']; ?> </p>
<!--                            <p>Lorem ipsum dolor sit amet, no has decore maluisset, es
                                sent iisque patrioque ea eos, pri in iuvaret interesset. 
                                Impetus nominati his in. Ne quo ludus nullam, harum de
                                core vim te, qui facete iisque referrentur ea. Eripuit conte
                                ntiones vix at, diam meis nusquam mei eu.</p>
                            <p>Est at sonet inimicus. Eu est esse conclusionemque. Sanc
                                tus deserunt ius ad, per modus saperet aliquando no. Ut 
                                vix dicunt phaedrum, vel no aperiri suscipiantur. Duis pos
                                tea interesset no est, sanctus menandri qui ei, hinc home
                                ro nec an.</p>
                            <p>Impetus nominati his in. Ne quo ludus nullam, harum def
                                core vim te, qui facete iisque referrentur ea. Eripuit conte
                                ntiones vix at, diam meis nusquam mei eu.modus saperet 
                                aliquando no. Ut vix dicunt phaedrum...</p>-->
                        </div>

                        <div class="row ">
                            <div class="col-sm-6 pro-two-block">
                                <div class="col-sm-12 whitebox sec-row pro-so">
                                    <h3>SERVICES OFFERED</h3>
                                    <ul>

                                        <?php foreach ($pro_user['ProUserCategorie'] as $key => $val) { ?>
                                            <li>
    <!--                                            <img  alt="" src="img/tick_mark.png"/>-->
                                                <?php echo $val['sub_category']['SubCategorie']['sub_category']; ?>
                                            </li>
                                        <?php } ?>

                                        <!--                                        <li>General Contracting</li>
                                                                                <li>Home Improvement</li>
                                                                                <li>Cleaning</li>
                                                                                <li>Kitchen Repair</li>
                                                                                <li>Bathroom Repair</li>
                                                                                <li>Floor Sanding</li>-->
                                        <!--                                        <li><a class="more-service"> and 6 more... </a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 pro-two-block">
                                <div class="col-sm-12 whitebox sec-row pro-bh">
                                    <h3>BUSINESS HOURS</h3>
                                    <ul>
                                        <li>
                                            <span class="bhd">Monday</span> 
                                            <?php
                                            if ($pro_user['ProOperatingHour'][0]['mon_start'] != '' && $pro_user['ProOperatingHour'][0]['mon_end'] != '') {
                                                echo $pro_user['ProOperatingHour'][0]['mon_start'] . " - " . $pro_user['ProOperatingHour'][0]['mon_end'];
                                            } else {
                                                echo "(Unavailable)";
                                            }
                                            ?>
                                        </li>
                                        <li><span class="bhd">Tuesday</span> 
                                            <?php
                                            if ($pro_user['ProOperatingHour'][0]['tue_start'] != '' && $pro_user['ProOperatingHour'][0]['tue_end'] != '') {
                                                echo $pro_user['ProOperatingHour'][0]['tue_start'] . " - " . $pro_user['ProOperatingHour'][0]['tue_end'];
                                            } else {
                                                echo "(Unavailable)";
                                            }
                                            ?>
                                        </li>
                                        <li><span class="bhd">Wednesday</span> 
                                            <?php
                                            if ($pro_user['ProOperatingHour'][0]['wed_start'] != '' && $pro_user['ProOperatingHour'][0]['wed_end'] != '') {
                                                echo $pro_user['ProOperatingHour'][0]['wed_start'] . " - " . $pro_user['ProOperatingHour'][0]['wed_end'];
                                            } else {
                                                echo "(Unavailable)";
                                            }
                                            ?>
                                        </li>
                                        <li><span class="bhd">Thursday</span> 
                                            <?php
                                            if ($pro_user['ProOperatingHour'][0]['thu_start'] != '' && $pro_user['ProOperatingHour'][0]['thu_end'] != '') {
                                                echo $pro_user['ProOperatingHour'][0]['thu_start'] . " - " . $pro_user['ProOperatingHour'][0]['thu_end'];
                                            } else {
                                                echo "(Unavailable)";
                                            }
                                            ?>
                                        </li>
                                        <li><span class="bhd">Friday</span> 
                                            <?php
                                            if ($pro_user['ProOperatingHour'][0]['fri_start'] != '' && $pro_user['ProOperatingHour'][0]['fri_end'] != '') {
                                                echo $pro_user['ProOperatingHour'][0]['fri_start'] . " - " . $pro_user['ProOperatingHour'][0]['fri_end'];
                                            } else {
                                                echo "(Unavailable)";
                                            }
                                            ?>
                                        </li>
                                        <li><span class="bhd">Saturday</span> 
                                            <?php
                                            if ($pro_user['ProOperatingHour'][0]['sat_start'] != '' && $pro_user['ProOperatingHour'][0]['sat_end'] != '') {
                                                echo $pro_user['ProOperatingHour'][0]['sat_start'] . " - " . $pro_user['ProOperatingHour'][0]['sat_end'];
                                            } else {
                                                echo "(Unavailable)";
                                            }
                                            ?>
                                        </li>
                                        <li><span class="bhd">Sunday</span> 
                                            <?php
                                            if ($pro_user['ProOperatingHour'][0]['sun_start'] != '' && $pro_user['ProOperatingHour'][0]['sun_end'] != '') {
                                                echo $pro_user['ProOperatingHour'][0]['sun_start'] . " - " . $pro_user['ProOperatingHour'][0]['sun_end'];
                                            } else {
                                                echo "(Unavailable)";
                                            }
                                            ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                $photocount = 0;
                $photo_list = '';
                $pcount = (count($pro_user['ProUserFile']) > 0) ? count($pro_user['ProUserFile']) : 0;
                ?>

                <?php if ($pcount > 0) { ?>

                    <div class="row sec-row">
                        <div class="col-sm-4 pro-full ">
                            <div class="col-sm-12 whitebox ctd sidebar-l2">
                                <div class="big-ct"><?php echo $pcount ?></div>
                                <h3>PHOTOS OF PAST WORK</h3>
                            </div>
                        </div>
                        <div class="col-sm-8 pro-full ">
                            <div class="col-sm-12 whitebox pro-gallery">
                                <?php //if ($photo_list) { ?>
                                <ul>
                                    <?php
                                    $pc = 1;
                                    foreach ($pro_user['ProUserFile'] as $proUserFile) {
                                        $hclass = ($pc > 12) ? 'hclass' : '';
                                        ?>
                                        <li class="<?php echo $hclass; ?>"><a href=""><img  src="<?php echo $this->Html->url(array('controller' => 'home', 'action' => 'photoResize', $proUserFile['pro_user_id'], $proUserFile['image'], '100', '100')); ?>"></a></li>
                                        <?php
                                        $pc++;
                                    }
                                    ?>

                                    <?php //echo $photo_list;    ?>
                        <!--                                <li><a href=""><img  src="img/profile_design/p1.png"></a></li>-->
                        <!--                                <li><a href=""><img  src="img/profile_design/p2.png"></a></li>
                        <li><a href=""><img  src="img/profile_design/p3.png"></a></li>
                        <li><a href=""><img  src="img/profile_design/p4.png"></a></li>
                        <li><a href=""><img  src="img/profile_design/p5.png"></a></li>
                        <li><a href=""><img  src="img/profile_design/p6.png"></a></li>
                        <li><a href=""><img  src="img/profile_design/p1.png"></a></li>
                        <li><a href=""><img  src="img/profile_design/p2.png"></a></li>
                        <li><a href=""><img  src="img/profile_design/p3.png"></a></li>
                        <li><a href=""><img  src="img/profile_design/p4.png"></a></li>
                        <li><a href=""><img  src="img/profile_design/p5.png"></a></li>
                        <li><a href=""><img  src="img/profile_design/p6.png"></a></li>-->
                                </ul>
                                <?php //} ?>
                                <?php if ($pcount > 12) { ?>
                                    <p class="pro-vm"><a href="">VIEW MORE</a></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>



                <?php
                $service_area_new = '';
                $service_area_new_list = '';
                $sv = 0;
                foreach ($pro_user['ProServiceArea'] as $key => $val) {
                    $service_area_new .= $val['service_area'] . ", ";
                    $service_area_new_list .= '<li><a href="">' . $val['service_area'] . '</a></li>';
                    $sv++;
                }

                // echo ($sa_li) ? '<ul>' . $service_area_new_list . '</ul>' : '';
                ?>

                <?php if ($sv > 0) { ?>
                    <div class="row sec-row">
                        <div class="col-sm-4 pro-full">
                            <div class="col-sm-12 whitebox sidebar-l2">
                                <div class="big-ct"><?= $sv ?></div>
                                <div class="s-areas">
                                    <h3>SERVICE AREAS</h3>

                                    <?php
                                    echo ($sa_li) ? '<ul>' . $service_area_new_list . '</ul>' : '';
                                    ?>

                                    <!--                                <ul>
                                                                        <li><a href="">Riverside</a></li>
                                                                        <li><a href="">Wellington</a></li>
                                                                        <li><a href="">North Cambridge</a></li>
                                                                        <li><a href="">East Cambridge</a></li>
                                                                        <li><a href="">South Cambridge</a></li>
                                                                        <li><a href="">West Cambridge</a></li>
                                                                    </ul>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 pro-full">
                            <div class="col-sm-12 whitebox pro-map">
                                <div id="service_areas_new" style="height: 300px; width: 100%;"><?= $service_area_new ?></div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if ($pro_user['User']['id'] == 639) { ?>
                    <div class="row sec-row review-desktop">
                        <div class="col-sm-4 pro-full">
                            <div class="col-sm-12 whitebox sidebar-l2">
                                <div class="big-ct">4.9</div>
                                <div class="rev-count-left">
                                    <p>
    <!--                                    <img class="right" src="img/profile_design/sl.png"> <br />-->
                                    <div class="rating_area">
                                        <div class="rated_area" style="width: 100%"></div>
                                    </div>
                                    <br />
                                    <span>5 REVIEWS</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 pro-full">
                            <div class="col-sm-12 whitebox pro-reviews">
                                <ul>
                                    <li>
                                        <a href="">
                                            <span>Aaron B.</span>
                                            <div class="rating_area">
                                                <div class="rated_area" style="width: 100%"></div>
                                            </div>
                                        </a>
                                        <p>"Like many we had severe damage from ice damage the past winter. In short the floor buckled and needed to be pulled in 2 places, sub floor had to be replaced, floor replaces, re-sanded and finished. Taozen did a great job and addressed all questions I had. They were on time and the workers were very nice."</p>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>Linda S.</span>
                                            <div class="rating_area">
                                                <div class="rated_area" style="width: 70%"></div>
                                            </div>
                                        </a>
                                        <p>"Taozen refinished my pine hardwood floors in the living room, dining room and stairs. I was very satisfied with the company's professionalism, timeliness, and the quality of the work. I would highly recommend Taozen for refinishing floors."</p>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>Mubing L.</span>
                                            <div class="rating_area">
                                                <div class="rated_area" style="width: 80%"></div>
                                            </div>
                                        </a>
                                        <p>"Taozen offered a very competitive price, and gave out a series of excellent suggestion. The team is very professional and the job was finished with top notch quality. A+ service! My project involved refinishing existing floors and installing unfinished hard wood floor."</p>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>Jessica B.</span>
                                            <div class="rating_area">
                                                <div class="rated_area" style="width: 100%"></div>
                                            </div>
                                        </a>
                                        <p>"We got our 900sq foot house refinished and Taozen did some patch work. it all came out beautiful and they were quick and inexpensive. They were extremely professional and easy to work with. The place looked brand new after they finished. I can't say enough great things about them. i would recommend them to anyone in a second and will be using them for my next house."</p>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>Suzanne N.</span>
                                            <div class="rating_area">
                                                <div class="rated_area" style="width: 100%"></div>
                                            </div>
                                        </a>
                                        <p>"Excellent job! Taozen were very responsive, came out and gave us a quote, were professional and friendly and helpful about some other flooring issues we had. They did high-quality work, cleaned up after themselves and finished on schedule. We would definitely use them again without hesitation."</p>
                                    </li>
                                </ul>
    <!--                            <p class="pro-vm"><a href="">VIEW MORE</a></p>-->
                            </div>
                        </div>
                    </div>
                <?php } ?>




                <?php if ($pro_user['reviewCount']['Hire']['reviewCount']) { ?>
                    <?php
                    $reviewPercent = ($pro_user['rating_sum']['Hire']['proRating'] / $pro_user['reviewCount']['Hire']['reviewCount']) * 20;
                    ?>
                    <div class="row sec-row review-desktop">
                        <div class="col-sm-4 pro-full">
                            <div class="col-sm-12 whitebox sidebar-l2">
                                <div class="big-ct"><?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> </div>
                                <div class="rev-count-left">
    <!--                                    <img class="right" src="img/profile_design/sl.png"> <br />-->
                                    <div class="rating_area">
                                        <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                    </div>
                                    <br />
                                    <span><?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> REVIEWS</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 pro-full">
                            <div class="col-sm-12 whitebox pro-reviews">
                                <ul>
                                    <?php
                                    foreach ($pro_user['reviews'] as $review) {
                                        ?>
                                        <li>
                                            <a href="">
                                                <span><?php echo $review['User']['name']; ?></span>
                                                <div class="rating_area">
                                                    <div class="rated_area" style="width: <?php echo 20 * $review['Hire']['user_review']; ?>%"></div>
                                                </div>
                                            </a>
                                            <p>
                                                "<?php echo $review['Hire']['user_review_comment']; ?>"
                                            </p>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <p class="pro-vm"><a href="">VIEW MORE</a></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
            <div class="col-sm-3 pro-rb">
                <?php
                $category_id = $pro_user['ProUser']['category_id'];
                if ($view_from == 'profinderlist') {
                    ?>
                    <div class="col-sm-12 whitebox sidebar-sb pro-in">
                        <h3>INTERESTED IN PRO?</h3>
                        <p>Get a quote for your next project.</p>
                        <p><a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'submit_project_details', 'category_id' => $category_id, 'specified_pro' => $pro_user['User']['id'])) ?>">REQUEST A QUOTE</a></p>
                    </div>
                    <?php
                }
                ?>


                <?php if ($pro_user['ProAward']) { ?>
                    <div class="col-sm-12 whitebox sidebar-sb pro-ac">
                        <h3>ACHIEVEMENTS</h3>
                        <ul>
                            <?php
                            foreach ($pro_user['ProAward'] as $key => $val) {
                                $award_exist = "uploaded_files/pro_user_files/awards/" . $val['pro_user_id'] . "/" . $val['award_image'];
                                ?>
                                <li>
                                    <?php if (file_exists($award_exist)) { ?> 
                                        <img class="img-responsive" src="uploaded_files/pro_user_files/awards/<?php echo $val['pro_user_id']; ?>/<?php echo $val['award_image']; ?>">
                                    <?php } else { ?>
                                        <img class="img-responsive" src="uploaded_files/pro_user_files/awards/d.png">
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>


                <?php if ($pro_user['CompanyLicense']) { ?>
                    <div class="col-sm-12 whitebox sidebar-sb pro-li">
                        <h3>LICENSE</h3>
                        <?php foreach ($pro_user['CompanyLicense'] as $license) { ?>
                            <p>
                                <span><em>Number:</em> <i><?php echo $license['license_number']; ?></i></span>
                                <span><em>Type:</em> <i><?php echo $license['license_type']; ?></i></span>
                                <span><em>Expiry:</em> <i><?php echo $license['expir_date']; ?></i></span>
                            </p>
                        <?php } ?>


                                                    <!--                    <p>
                                                        <span><em>Number:</em> <i>A490JH94</i></span>
                                                        <span><em>Type:</em> <i>GN</i></span>
                                                        <span><em>Expiry:</em> <i>31 Oct 2019</i></span>
                                                    </p>
                                                    <p>
                                                        <span><em>Number:</em><i>489HK92</i></span>
                                                        <span><em>Type:</em><i>AAA</i></span>
                                                        <span><em>Expiry:</em><i>26 Jan 2020</i></span>
                                                    </p>-->
                    </div>
                <?php } ?>


                <?php //print_r($pro_user)     ?>

                <?php if ($pro_user['user_packages']) { ?>
                    <div class="col-sm-12 whitebox sidebar-sb pro-pv">
                        <h3>PEOPLE ALSO VIEWED</h3>
                        <ul>
                            <?php foreach ($pro_user['user_packages'] as $packages) { ?>
                                <li>
                                    <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'profile', $packages['ProUser']['user_id'], 'view_from' => 'profinderlist')) ?>">
                                    <?php
                                    if ($packages['ProUser']['business_logo'] != '') {
                                        $b = "uploaded_files/pro_user_files/business_logo/" . $packages['ProUser']['business_logo'];
                                        if (file_exists($b)) {
                                            ?>
                                            <img  alt="" src="<?php echo $b; ?>" />
                                        <?php } else { ?>
                                            <img alt="" src="img/company_logo.jpg" />
                                        <?php
                                        }
                                    } else {
                                        ?>
                                        <img alt="" src="img/company_logo.jpg" />
                                    <?php }
                                    ?>     



                                                                                        <!--                                <img src="img/profile_design/l1.png">-->
                                    <!--                                <h4>RH Contractors</h4>-->
                                    <h4><?php echo $packages['ProUser']['company_name'] ?></h4>

                                    <!--        
                                    <img class="star" src="img/profile_design/sr.png">-->


                                    <?php $rPercent = ($packages['rating_sum']['Hire']['proRating'] / $packages['reviewCount']['Hire']['reviewCount']) * 20; ?>
        <?php if ($packages['reviewCount']['Hire']['reviewCount']) { ?>
                                        <div class="c-r">
                                            <div class="rating_area">
                                                <div class="rated_area" style="width: <?php echo $rPercent; ?>%"></div>
                                            </div>
                                        </div>
        <?php } ?>

                                    <p>"<?php echo substr($packages['ProUser']['company_description'], 0, 50); ?> .."</p>
                                    </a>
                                </li>
    <?php } ?>
                            <!--                        <li>
                                                        <img src="img/profile_design/l2.png">
                                                        <h4>Phoenix Solutions</h4>
                                                        <img class="star" src="img/profile_design/sr.png">
                                                        <p>"Simply fantastic! I love my new roof and I ..."</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/profile_design/l3.png">
                                                        <h4>JBS Roofing</h4>
                                                        <img class="star" src="img/profile_design/sr.png">
                                                        <p>"Amazing job & they were very professional..."</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/profile_design/l4.png">
                                                        <h4>Trininda Roofers</h4>
                                                        <img class="star" src="img/profile_design/sr.png">
                                                        <p>"Simply fantastic! I love my new roof and I ..."</p>
                                                    </li>-->
                        </ul>
                    </div>
                </div>
<?php } ?>




<?php if ($pro_user['User']['id'] == 639) { ?>
                <div class="row sec-row  review-mobile">
                    <div class="col-sm-4 pro-full">
                        <div class="col-sm-12 whitebox sidebar-l2">
                            <div class="big-ct">4.9</div>
                            <div class="rev-count-left">
                                <p>
    <!--                                    <img class="right" src="img/profile_design/sl.png"> <br />-->
                                <div class="rating_area">
                                    <div class="rated_area" style="width: 100%"></div>
                                </div>
                                <br />
                                <span>5 REVIEWS</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 pro-full">
                        <div class="col-sm-12 whitebox pro-reviews">
                            <ul>
                                <li>
                                    <a href="">
                                        <span>Aaron B.</span>
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: 100%"></div>
                                        </div>
                                    </a>
                                    <p>"Like many we had severe damage from ice damage the past winter. In short the floor buckled and needed to be pulled in 2 places, sub floor had to be replaced, floor replaces, re-sanded and finished. Taozen did a great job and addressed all questions I had. They were on time and the workers were very nice."</p>
                                </li>
                                <li>
                                    <a href="">
                                        <span>Linda S.</span>
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: 70%"></div>
                                        </div>
                                    </a>
                                    <p>"Taozen refinished my pine hardwood floors in the living room, dining room and stairs. I was very satisfied with the company's professionalism, timeliness, and the quality of the work. I would highly recommend Taozen for refinishing floors."</p>
                                </li>
                                <li>
                                    <a href="">
                                        <span>Mubing L.</span>
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: 80%"></div>
                                        </div>
                                    </a>
                                    <p>"Taozen offered a very competitive price, and gave out a series of excellent suggestion. The team is very professional and the job was finished with top notch quality. A+ service! My project involved refinishing existing floors and installing unfinished hard wood floor."</p>
                                </li>
                                <li>
                                    <a href="">
                                        <span>Jessica B.</span>
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: 100%"></div>
                                        </div>
                                    </a>
                                    <p>"We got our 900sq foot house refinished and Taozen did some patch work. it all came out beautiful and they were quick and inexpensive. They were extremely professional and easy to work with. The place looked brand new after they finished. I can't say enough great things about them. i would recommend them to anyone in a second and will be using them for my next house."</p>
                                </li>
                                <li>
                                    <a href="">
                                        <span>Suzanne N.</span>
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: 100%"></div>
                                        </div>
                                    </a>
                                    <p>"Excellent job! Taozen were very responsive, came out and gave us a quote, were professional and friendly and helpful about some other flooring issues we had. They did high-quality work, cleaned up after themselves and finished on schedule. We would definitely use them again without hesitation."</p>
                                </li>
                            </ul>
    <!--                            <p class="pro-vm"><a href="">VIEW MORE</a></p>-->
                        </div>
                    </div>
                </div>
<?php } ?>




            <?php if ($pro_user['reviewCount']['Hire']['reviewCount']) { ?>
                <?php
                $reviewPercent = ($pro_user['rating_sum']['Hire']['proRating'] / $pro_user['reviewCount']['Hire']['reviewCount']) * 20;
                ?>
                <div class="row sec-row  review-mobile">
                    <div class="col-sm-4 pro-full">
                        <div class="col-sm-12 whitebox sidebar-l2">
                            <div class="big-ct"><?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> </div>
                            <div class="rev-count-left">
    <!--                                    <img class="right" src="img/profile_design/sl.png"> <br />-->
                                <div class="rating_area">
                                    <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                </div>
                                <br />
                                <span><?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> REVIEWS</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 pro-full">
                        <div class="col-sm-12 whitebox pro-reviews">
                            <ul>
                                <?php
                                foreach ($pro_user['reviews'] as $review) {
                                    ?>
                                    <li>
                                        <a href="">
                                            <span><?php echo $review['User']['name']; ?></span>
                                            <div class="rating_area">
                                                <div class="rated_area" style="width: <?php echo 20 * $review['Hire']['pro_review']; ?>%"></div>
                                            </div>
                                        </a>
                                        <p>
                                            "<?php echo $review['Hire']['pro_review_comment']; ?>"
                                        </p>
                                    </li>
    <?php } ?>
                            </ul>
                            <p class="pro-vm"><a href="">VIEW MORE</a></p>
                        </div>
                    </div>
                </div>
<?php } ?>




            <!--            <div class="col-sm-9 pro-lb review-mobile">
                            <div class="row sec-row">
                                <div class="col-sm-4 pro-full">
                                    <div class="col-sm-12 whitebox sidebar-l2">
                                        <div class="big-ct">5.0</div>
                                        <div class="rev-count-left">
                                            <p>
                                                <img class="right" src="img/profile_design/sl.png"> <br />
                                                <span>21 REVIEWS</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8 pro-full">
                                    <div class="col-sm-12 whitebox pro-reviews">
                                        <ul>
                                            <li>
                                                <a href=""><span>Ryan Almeida</span><img class="img-responsive" src="img/profile_design/sy.png"></a>
                                                <p>
                                                    Lorem ipsum dolor sit amet, ad persius sanctus ius. Anmo
                                                    vet convenire per, sensibus referrentur ei usu. Qui soluta 
                                                    expetenda ad. Sit omnis oratio ne.
                                                </p>
                                            </li>
                                            <li>
                                                <a href=""><span>Ryan Almeida</span><img class="img-responsive" src="img/profile_design/sy.png"></a>
                                                <p>
                                                    Lorem ipsum dolor sit amet, ad persius sanctus ius. Anmo
                                                    vet convenire per, sensibus referrentur ei usu. Qui soluta 
                                                    expetenda ad. Sit omnis oratio ne.
                                                </p>
                                            </li>
                                            <li>
                                                <a href=""><span>Ryan Almeida</span><img class="img-responsive" src="img/profile_design/sy.png"></a>
                                                <p>
                                                    Lorem ipsum dolor sit amet, ad persius sanctus ius. Anmo
                                                    vet convenire per, sensibus referrentur ei usu. Qui soluta 
                                                    expetenda ad. Sit omnis oratio ne.
                                                </p>
                                            </li>
                                            <li>
                                                <a href=""><span>Ryan Almeida</span><img class="img-responsive" src="img/profile_design/sy.png"></a>
                                                <p>
                                                    Lorem ipsum dolor sit amet, ad persius sanctus ius. Anmo
                                                    vet convenire per, sensibus referrentur ei usu. Qui soluta 
                                                    expetenda ad. Sit omnis oratio ne.
                                                </p>
                                            </li>
                                        </ul>
                                        <p class="pro-vm"><a href="">VIEW MORE</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>-->


        </div>
    </div>


    <div style="display: none;" class="view_profile_area">
        <div class="row">
            <div class="col-sm-8">
                <div class="profile_header_area">
                    <div class="profile_header_top">
                        <div class="companyLogo">
                            
                            
                            
                               <?php
                                    if ($pro_user['ProUser']['business_logo'] != '') {
                                        $c = "uploaded_files/pro_user_files/business_logo/" . $packages['ProUser']['business_logo'];
                                        if (file_exists($b)) {
                                            ?>
                                            <img  alt="" src="<?php echo $c; ?>" />
                                        <?php } else { ?>
                                            <img alt="" src="img/company_logo.jpg" />
                                        <?php
                                        }
                                    } else {
                                        ?>
                                        <img alt="" src="img/company_logo.jpg" />
                                    <?php }
                                    ?>     

                            
                         
                        </div>
                        <div class="company_short_details">
                            <div class="company_short_details_top">
                                <div class="companyName">
                                    <?php echo $pro_user['ProUser']['company_name'] ?>
<?php if ($ownprofile) { ?>
                                        <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'edit_pro')); ?>">
                                            <img alt="" src="img/pencil_icon.png" width="12" />
                                        </a>
<?php } ?>
                                </div>
                                <div class="companySubName">
                                    <?php
                                    echo $pro_user['ProUser']['experience'];
                                    ?>
                                </div>
                            </div>
                            <div class="company_short_details_bottom">
                                <ul>
                                    <li>
                                        <img alt="" src="img/location_icon.png" />
<?php echo $pro_user['ProUser']['city'] ?>
                                    </li>
                                    <li >
                                        <img alt="" src="img/verified_icon.png" />
                                        <span>
                                            <?php if ($pro_user['project_count'] > 1) { ?>
                                                <?php echo $pro_user['project_count']; ?> Projects
                                            <?php } else { ?>
                                                <?php echo $pro_user['project_count']; ?> Project
<?php } ?>
                                        </span>
                                    </li>
                                        <?php if ($pro_user['reviewCount']['Hire']['reviewCount']) { ?>
                                        <li class="user_review_area">
                                            <?php
                                            $reviewPercent = ($pro_user['rating_sum']['Hire']['proRating'] / $pro_user['reviewCount']['Hire']['reviewCount']) * 20;
                                            ?>
                                            <div class="rating_area">
                                                <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                            </div>
                                            <div class="review_text">
    <?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> Reviews
                                            </div>
                                        </li>
<?php } ?>
                                </ul>
                            </div>

                        </div>
                        <div class="complay_verification">
                            <?php
                            if ($pro_user['ProUser']['is_verified']) {
                                ?>
                                <img alt="" src="img/pro_verified_icon.png"/>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="profile_header_bottom">
                        <ul>
                            <li>
                                <div class="heading">Website</div>
                                <div class="text"><?php echo strlen($pro_user['ProUser']['website_url']) ? $pro_user['ProUser']['website_url'] : 'N/A' ?></div>
                            </li>
                            <li>
                                <div class="heading">Phone Number</div>
                                <div class="text"><?php echo strlen($pro_user['ProUser']['business_phone']) ? $pro_user['ProUser']['business_phone'] : $pro_user['User']['phone']; ?></div>
                            </li>
                            <li>
                                <div class="heading">Email Address</div>
                                <div class="text"> XXX XXX XXX</div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="profile_details_area">
                    <div class="project_details">
                        <div class="project_details_heading">
                            About <?php echo $pro_user['ProUser']['company_name']; ?>
                        </div>
                        <div class="project_details_text">
<?php echo $pro_user['ProUser']['company_description']; ?>
                        </div>


                        <div class="project_details_heading">
                            PHOTOS OF PAST WORK SIMILAR TO THIS
                        </div>
                        <div class="project_details_photo">
                            <?php
                            foreach ($pro_user['ProUserFile'] as $proUserFile) {
                                ?>
                                <img src="<?php echo $this->Html->url(array('controller' => 'home', 'action' => 'photoResize', $proUserFile['pro_user_id'], $proUserFile['image'], '100', '100')); ?>" alt=""> 
                                <img alt="" width="100" src="uploaded_files/pro_user_files/<?php echo $proUserFile['pro_user_id'] ?>/<?php echo $proUserFile['image'] ?>"/>
<?php } ?>

                        </div>
                    </div>
                </div>
                <div class="profile_address_area">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="address_content_area">
                                <div class="address_content_header">
                                    <div class="heading">
                                        Address
                                    </div>
                                        <?php $address = $pro_user['ProUser']['address'] . ", " . $pro_user['ProUser']['city'] . ", " . $pro_user['ProUser']['state'] . ", " . $pro_user['ProUser']['zip_code']; ?>
                                    <div class="text" title="<?php echo $address; ?>">
<?php echo $address; ?>
                                    </div>
                                </div>
                                <div class="address_map">
                                    <div id="map_address" style="height: 300px; width: 100%;"></div>
                                    <iframe src="https://maps.google.com/maps?q=<?php echo $address; ?>&hl=en;z=14&output=embed" width="100%" height="300" sameorigin="false" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="address_content_area">
                                <div class="address_content_header">
                                    <div class="heading">
                                        Service areas
                                    </div>
                                    <?php
                                    $service_area = '';
                                    foreach ($pro_user['ProServiceArea'] as $key => $val) {
                                        $service_area .= $val['service_area'] . ", ";
                                    }
                                    ?>
                                    <div class="text" title="<?php echo $service_area; ?>">
<?php echo $service_area; ?>
                                    </div>
                                </div>
                                <div class="address_map">
                                    <div id="map_service_areas" style="height: 300px; width: 100%;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                    <?php if (!empty($pro_user['ProUserCategorie']) || !empty($pro_user['ProServiceArea'])) { ?>
                    <div class="profile_service_offer_area">
    <?php if (!empty($pro_user['ProUserCategorie'])) { ?>
                            <div class="service_offer_heading">
                                Services Offered
                            </div>
                            <div class="service_offer_content">
                                <ul>
        <?php foreach ($pro_user['ProUserCategorie'] as $key => $val) { ?>
                                        <li>
                                            <img  alt="" src="img/tick_mark.png"/>
                                        <?php echo $val['sub_category']['SubCategorie']['sub_category']; ?>
                                        </li>
        <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
    <?php if (!empty($pro_user['ProBrand'])) { ?>
                            <div class="service_offer_heading brand_used">
                                Brands Used
                            </div>
                            <div class="service_offer_content">
                                <ul>
        <?php foreach ($pro_user['ProBrand'] as $key => $val) { ?>
                                        <li>
                                            <img  alt="" src="img/tick_mark.png"/>
                                        <?php echo $val['brand_name']; ?>
                                        </li>
        <?php } ?>
                                </ul>
                            </div>
                    <?php } ?>
                    </div>
                    <?php } ?>
                <div class="profile_review_area">
<?php if ($pro_user['User']['id'] == 639) { ?>
                        <div class="reviews_area">
                            <div class="reviews_heading">

                                <div class="rating_area">
                                    <div class="rated_area" style="width: 90%"></div>
                                </div>
                                <div class="review_text">
                                    5 Reviews
                                </div>
                            </div>
                            <div class="reviews_list_area">
                                <ul>

                                    <li>
                                        <div class="username_rating">
                                            <div class="username">Aaron B.</div>
                                            <div class="rating">
                                                <div class="rating_area">
                                                    <div class="rated_area" style="width: 100%"></div>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="review_date">
                                            20-jan-2017
                                        </div>
                                        <div class="review_text">
                                            "Like many we had severe damage from ice damage the past winter. In short the floor buckled and needed to be pulled in 2 places, sub floor had to be replaced, floor replaces, re-sanded and finished. Taozen did a great job and addressed all questions I had. They were on time and the workers were very nice."
                                        </div>
                                    </li>
                                    <li>
                                        <div class="username_rating">
                                            <div class="username">Linda S.</div>
                                            <div class="rating">
                                                <div class="rating_area">
                                                    <div class="rated_area" style="width: 70%"></div>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="review_date">
                                            28-feb-2017
                                        </div>
                                        <div class="review_text">
                                            "Taozen refinished my pine hardwood floors in the living room, dining room and stairs. I was very satisfied with the company's professionalism, timeliness, and the quality of the work. I would highly recommend Taozen for refinishing floors."
                                        </div>
                                    </li>
                                    <li>
                                        <div class="username_rating">
                                            <div class="username">Mubing L.</div>
                                            <div class="rating">
                                                <div class="rating_area">
                                                    <div class="rated_area" style="width: 80%"></div>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="review_date">
                                            15-mar-2017
                                        </div>
                                        <div class="review_text">
                                            "Taozen offered a very competitive price, and gave out a series of excellent suggestion. The team is very professional and the job was finished with top notch quality. A+ service! My project involved refinishing existing floors and installing unfinished hard wood floor."
                                        </div>
                                    </li>
                                    <li>
                                        <div class="username_rating">
                                            <div class="username">Jessica B.</div>
                                            <div class="rating">
                                                <div class="rating_area">
                                                    <div class="rated_area" style="width: 100%"></div>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="review_date">
                                            25-jun-2017
                                        </div>
                                        <div class="review_text">
                                            "We got our 900sq foot house refinished and Taozen did some patch work. it all came out beautiful and they were quick and inexpensive. They were extremely professional and easy to work with. The place looked brand new after they finished. I can't say enough great things about them. i would recommend them to anyone in a second and will be using them for my next house."
                                        </div>
                                    </li>
                                    <li>
                                        <div class="username_rating">
                                            <div class="username">Suzanne N.</div>
                                            <div class="rating">
                                                <div class="rating_area">
                                                    <div class="rated_area" style="width: 100%"></div>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="review_date">
                                            23-jul-2017
                                        </div>
                                        <div class="review_text">
                                            "Excellent job! Taozen were very responsive, came out and gave us a quote, were professional and friendly and helpful about some other flooring issues we had. They did high-quality work, cleaned up after themselves and finished on schedule. We would definitely use them again without hesitation."
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
<?php } ?>


<?php if ($pro_user['reviewCount']['Hire']['reviewCount']) { ?>
                        <div class="reviews_area">
                            <div class="reviews_heading">
                                <?php
                                $reviewPercent = ($pro_user['rating_sum']['Hire']['proRating'] / $pro_user['reviewCount']['Hire']['reviewCount']) * 20;
                                ?>
                                <div class="rating_area">
                                    <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                </div>
                                <div class="review_text">
    <?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> Reviews
                                </div>
                            </div>
                            <div class="reviews_list_area">
                                <ul>
                                    <?php
                                    foreach ($pro_user['reviews'] as $review) {
                                        ?>
                                        <li>
                                            <div class="username_rating">
                                                <div class="username"><?php echo $review['User']['name']; ?></div>
                                                <div class="rating">
                                                    <div class="rating_area">
                                                        <div class="rated_area" style="width: <?php echo 20 * $review['Hire']['pro_review']; ?>%"></div>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="review_date">
        <?php echo $review['Hire']['pro_review_date']; ?>
                                            </div>
                                            <div class="review_text">
        <?php echo $review['Hire']['pro_review_comment']; ?>
                                            </div>
                                        </li>
    <?php } ?>


                                </ul>
                            </div>
                            <a href="javascript:" class="view_more_btn">
                                View More
                            </a>
                        </div>
<?php } ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="claim_business_area">
                    <?php if ($pro_user['ProUser']['claim_status'] == 'not_claimed') { ?>
                        <a href="javascript:" class="btn  claim_business_btn">Claim Business</a>
<?php } ?>
                    <div class="share_area">
                        <div class="heading">
                            Share
                        </div>
                        <div class="share_icons">

                        </div>
                    </div>
                </div>
                <div class="business_hour_area">
                    <div class="license_area">
                        <div class="license_heading">
                            Business Hours
                        </div>
                        <div class="business_hour_lists">
                            <ul>
                                <li>
                                    <div class="day_text">
                                        Monday
                                    </div>
                                    <div class="day_activity">
                                        <?php
                                        if ($pro_user['ProOperatingHour'][0]['mon_start'] != '' && $pro_user['ProOperatingHour'][0]['mon_end'] != '') {
                                            echo $pro_user['ProOperatingHour'][0]['mon_start'] . " - " . $pro_user['ProOperatingHour'][0]['mon_end'];
                                        } else {
                                            echo "(Unavailable)";
                                        }
                                        ?>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <div class="day_text">
                                        Tuesday
                                    </div>
                                    <div class="day_activity">
                                        <?php
                                        if ($pro_user['ProOperatingHour'][0]['tue_start'] != '' && $pro_user['ProOperatingHour'][0]['tue_end'] != '') {
                                            echo $pro_user['ProOperatingHour'][0]['tue_start'] . " - " . $pro_user['ProOperatingHour'][0]['tue_end'];
                                        } else {
                                            echo "(Unavailable)";
                                        }
                                        ?>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <div class="day_text">
                                        Wednesday
                                    </div>
                                    <div class="day_activity">
                                        <?php
                                        if ($pro_user['ProOperatingHour'][0]['wed_start'] != '' && $pro_user['ProOperatingHour'][0]['wed_end'] != '') {
                                            echo $pro_user['ProOperatingHour'][0]['wed_start'] . " - " . $pro_user['ProOperatingHour'][0]['wed_end'];
                                        } else {
                                            echo "(Unavailable)";
                                        }
                                        ?>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <div class="day_text">
                                        Thursday
                                    </div>
                                    <div class="day_activity">
                                        <?php
                                        if ($pro_user['ProOperatingHour'][0]['thu_start'] != '' && $pro_user['ProOperatingHour'][0]['thu_end'] != '') {
                                            echo $pro_user['ProOperatingHour'][0]['thu_start'] . " - " . $pro_user['ProOperatingHour'][0]['thu_end'];
                                        } else {
                                            echo "(Unavailable)";
                                        }
                                        ?>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <div class="day_text">
                                        Friday
                                    </div>
                                    <div class="day_activity">
                                        <?php
                                        if ($pro_user['ProOperatingHour'][0]['fri_start'] != '' && $pro_user['ProOperatingHour'][0]['fri_end'] != '') {
                                            echo $pro_user['ProOperatingHour'][0]['fri_start'] . " - " . $pro_user['ProOperatingHour'][0]['fri_end'];
                                        } else {
                                            echo "(Unavailable)";
                                        }
                                        ?>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <div class="day_text">
                                        Saturday
                                    </div>
                                    <div class="day_activity">
                                        <?php
                                        if ($pro_user['ProOperatingHour'][0]['sat_start'] != '' && $pro_user['ProOperatingHour'][0]['sat_end'] != '') {
                                            echo $pro_user['ProOperatingHour'][0]['sat_start'] . " - " . $pro_user['ProOperatingHour'][0]['sat_end'];
                                        } else {
                                            echo "(Unavailable)";
                                        }
                                        ?>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <div class="day_text">
                                        Sunday
                                    </div>
                                    <div class="day_activity">
                                        <?php
                                        if ($pro_user['ProOperatingHour'][0]['sun_start'] != '' && $pro_user['ProOperatingHour'][0]['sun_end'] != '') {
                                            echo $pro_user['ProOperatingHour'][0]['sun_start'] . " - " . $pro_user['ProOperatingHour'][0]['sun_end'];
                                        } else {
                                            echo "(Unavailable)";
                                        }
                                        ?>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                            </ul>
                        </div>

                        <?php
                        $category_id = $pro_user['ProUser']['category_id'];
                        if ($view_from == 'profinderlist') {
                            ?>
                            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'submit_project_details', 'category_id' => $category_id, 'specified_pro' => $pro_user['User']['id'])) ?>" class="btn request_a_quote_btn">
                                Request a quote
                            </a>
                            <?php
                        }
                        ?>


                    </div>
                </div>
                <div class="awards_achievements_area">
                    <div class="license_area">
                        <div class="license_heading">
                            awards & achievements
                        </div>
                        <div class="awards_achievements_lists">
                            <ul>
                                <?php
                                foreach ($pro_user['ProAward'] as $key => $val) {
                                    ?>
                                    <li>
                                        <img alt="" width="30" src="img/award_icon.jpg"/>
                                    <?php echo $val['award']; ?>
                                    </li>
<?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="profile_license_area">
                    <div class="license_area">
                        <div class="license_heading">
                            LICENSE
                        </div>
                        <div class="license_details">
<?php foreach ($pro_user['CompanyLicense'] as $license) { ?>
                                <div class="single_license">
                                    <span class="license_num">
                                        LICENSE NO: <?php echo $license['license_number']; ?>
                                    </span>
                                    <br/>
                                    <span>
    <?php echo $license['license_type']; ?>
                                    </span>
                                    <br/>
                                    <span>
                                        Active until: <?php echo $license['expir_date']; ?>
                                    </span>     
                                </div>
<?php } ?>


                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

</div>


<!-- Modal -->
<div id="claimBusinessModal" class="modal fade claim_business_modal" role="dialog">
    <div class="modal-dialog">
        <form autocomplete="off" action="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'claim_business')); ?>" id="claimBusiness" >
            <input type="hidden" name="user_id" value="<?php echo $pro_user['ProUser']['user_id']; ?>" />
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Claim Your Business</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name:</label>
                        <input type="text" name="name" class="form-control name_input" placeholder="Your name"/>
                        <div class="nameError error_msg"></div>
                    </div>
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="email" name="email" class="form-control email_input" placeholder="Your email"/>
                        <div class="emailError error_msg"></div>
                    </div>
                    <div class="form-group">
                        <label>Phone:</label>
                        <input type="text" name="phone" class="form-control phone_input" placeholder="Your phone"/>
                        <div class="phoneError error_msg"></div>
                    </div>
                    <div class="form-group">
                        <label>Message:</label>
                        <textarea name="message" class="form-control message_input" placeholder="Message"></textarea>
                        <div class="messageError error_msg"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" >Claim Business</button>
                </div>
            </div>
        </form>
    </div>
</div>


<div id="successModal" class="modal fade projectSubmitted " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="projectSubmittedArea">
                <img alt="" src="img/tick_mark_big.png"/>
                <div class="congratulations">
                    Congratulations!
                </div>
                <div class="details">
                    Your request has been submitted. 
                    Our verification expert will get in touch with you soon.
                </div>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('body').css('background', '#ededed');

        $('.claim_business_area, .pro-claim').on('click', '.claim_business_btn', function () {
            $('#claimBusinessModal').find('.name_input').val('');
            $('#claimBusinessModal').find('.email_input').val('');
            $('#claimBusinessModal').find('.phone_input').val('');
            $('#claimBusinessModal').find('.message_input').val('');
            $('#claimBusinessModal').modal();
        });




        var options = {
            target: 'pro/add_pro_action', // target element(s) to be updated with server response 
            beforeSubmit: showRequest, // pre-submit callback 
            success: showResponse, // post-submit callback 

            // other available options: 
            //url:       url         // override for form's 'action' attribute 
            type: 'post', // 'get' or 'post', override for form's 'method' attribute 
            dataType: 'json'        // 'xml', 'script', or 'json' (expected server response type) 
                    //clearForm: true        // clear all form fields after successful submit 
                    //resetForm: true        // reset the form after successful submit 

                    // $.ajax options can be used here too, for example: 
                    //timeout:   3000 
        };
        // bind to the form's submit event 
        $('#claimBusiness').submit(function () {
            // inside event callbacks 'this' is the DOM element so we first 
            // wrap it in a jQuery object and then invoke ajaxSubmit 
            $(this).ajaxSubmit(options);
            // !!! Important !!! 
            // always return false to prevent standard browser submit and page navigation 
            return false;
        });


    });

    // pre-submit callback 
    function showRequest(formData, jqForm, options) {
        $('#claimBusiness').find('.error_msg').hide();

        return true;
    }

// post-submit callback 
    function showResponse(responseText, statusText, xhr, $form) {

        if (responseText.type == 'error') {
//            alert_r(responseText);
            if (responseText.errorMsg.nameError) {
                $('#claimBusiness').find('.nameError').html(responseText.errorMsg.nameError).show();
            }
            if (responseText.errorMsg.emailError) {
                $('#claimBusiness').find('.emailError').html(responseText.errorMsg.emailError).show();
            }
            if (responseText.errorMsg.emailError) {
                $('#claimBusiness').find('.phoneError').html(responseText.errorMsg.phoneError).show();
            }
            if (responseText.errorMsg.messageError) {
                $('#claimBusiness').find('.messageError').html(responseText.errorMsg.messageError).show();
            }


        } else if (responseText.type == 'success') {
            $('#claimBusinessModal').modal('hide');
            $('#successModal').modal();
            $('.claim_business_btn').remove();

        }

//        $('#successModal').on('hidden.bs.modal', function () {
//            window.location.href = "<?php echo $this->Html->Url(array('controller' => 'home')); ?>";
//        });
    }
</script>
<script>
    $(".share_icons, .pro-social").jsSocials({
        showLabel: false,
        showCount: false,
        shares: ["twitter", "facebook", "googleplus", "linkedin", "pinterest"]
    });
</script>



<script src="//maps.google.com/maps/api/js?key=<?php echo Configure::read('settings.gmap_api_key'); ?>&sensor=false" type="text/javascript"></script>

<script type="text/javascript">

//    Setting address on map
    var address = '<?php echo trim(addslashes($address)); ?>';
    var geocoder = new google.maps.Geocoder();
    var defaultLatlng = new google.maps.LatLng(41.125370, -98.268082);
    var mapOptions = {
        zoom: 8,
        center: defaultLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAPF
    }
    var map_address = new google.maps.Map(document.getElementById('map_address'), mapOptions);
    geocoder.geocode({'address': address}, function (results, status) {
        if (status == 'OK') {
            map_address.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map_address,
                position: results[0].geometry.location
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });

    //Setting services areas on map
    var locations = [
<?php foreach ($pro_user['ProServiceArea'] as $key => $val) { ?>
            ['<?php echo $val['service_area']; ?>', <?php echo $val['latitude']; ?>, <?php echo $val['longitude']; ?>, <?php echo 1 + $key; ?>],
<?php } ?>
    ];

    var map_service_areas = new google.maps.Map(document.getElementById('map_service_areas'), {
        zoom: 10,
        center: defaultLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });


    var map_service_areas = new google.maps.Map(document.getElementById('service_areas_new'), {
        zoom: 10,
        center: defaultLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });


    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map_service_areas
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
        map_service_areas.setCenter(new google.maps.LatLng(locations[i][1], locations[i][2]));
    }
</script>
<script>
    $(document).ready(function () {
        //alert("Sa");
        var fbox1 = $('.pro-so').outerHeight();
        var fbox2 = $('.pro-bh').outerHeight();
        //alert(fbox1);

        var sw = $(document).width();
        if (sw > 768) {
            if (fbox2 > fbox1) {
                $('.pro-so').css({'height': fbox2 + 'px'});
            } else {
                $('.pro-bh').css({'height': fbox1 + 'px'});
            }
        }


        $(".pro-gallery p.pro-vm a").click(function (e) {
            e.preventDefault();
            var thisText = $(this).text();
            var nText = '';
            if (thisText == 'VIEW MORE') {
                nText = 'VIEW LESS';
                $('.hclass').css("display", "block");
            } else {
                nText = 'VIEW MORE';
                $('.hclass').css("display", "none");
            }

            $(this).text(nText);
        });
    })
</script>

<script type="text/javascript">
    $(function () {
<?php
if (!$this->Session->read('Auth.User.id')) {
    ?>
            $('.login_signin').modal({backdrop: 'static', keyboard: false});
            $('#SignInForm').hide();
            $('#SignUpForm').show();
<?php } ?>
    });
</script>

