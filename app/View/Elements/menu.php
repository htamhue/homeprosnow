<div class="col-sm-9 col-md-10 col-xs-3 menu">
    <?php
    if ($this->Session->read('Auth.User.id')) {
        if ($this->Session->read('Auth.User.profile_pic')) {
            $profile_pic = "uploaded_files/profile_pic/" . $this->Session->read('Auth.User.id') . '/' . $this->Session->read('Auth.User.profile_pic');
        } else {
            $profile_pic = "img/default_pro_pic.png";
        }
    } else {
        $profile_pic = "img/expand_menu.png";
    }
    ?>
    <div class="expand_menu">
        <img src="<?php echo $profile_pic; ?>" alt="Menu" />
    </div>
    <?php
    if ($this->Session->read('Auth.User.id')) {
        ?>
        <ul>
            <li>
                <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'profinder')); ?>">Profinder</a>
            </li>
            <li>
                <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'web_service')); ?>">Marketing</a>
            </li>
            <li class="business_center">
                <?php
                if ($this->Session->read('RealUserType') == 'pro' and $this->Session->read('Auth.User.user_type') == 'user') {
                    $class = 'alert_user_for_switch_profile';
                } else {
                    $class = '';
                }
                ?>
                <a class="<?php echo $class; ?>" href="<?php echo $this->Html->Url(array('controller' => 'business', 'action' => 'index')); ?>">
                    Business Center
                </a>
            </li>
            <li class="dashboard">
                <a href="<?php echo $this->Html->Url(array('controller' => 'dashboard', 'action' => 'manage_account')); ?>">
                    Dashboard
                </a>
            </li>
            <li class="messages">
                <a href="<?php echo $this->Html->Url(array('controller' => 'messages')); ?>">
                    Messages
                </a>
                <span class="unread_notification"></span>
            </li>
            <li class="notification">
                <a href="javascript:">
                    Notifications
                </a>
                <div class="notification_area" >
                    <div class="notification_arrow">
                        <img alt="" src="img/arrow_up.png"/>
                    </div>
                    <div class="clear"></div>
                    <div class="notification_block" >

                    </div>
                </div>
                <div class="notification_counter">
                    0
                </div>

            </li>
            <li class="user_icon">
                <a href="javascript:" class="user_name">
                    <?php
                    echo htmlspecialchars($this->Session->read('Auth.User.name'));
                    ?>
                    <div class="user_pic">
                        <img alt="" src="<?php echo $profile_pic; ?>" />
                    </div>
                </a>
                <ul>
                    <?php
                    if ($this->Session->read('RealUserType') == 'pro') {
                        ?>
                        <li>
                            <a href="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'login_as_user')); ?>">
                                User Profile
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'login_as_pro')); ?>">
                                Business Profile
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                    <li>
                        <a href="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'logout')); ?>">
                            Logout
                        </a>
                    </li>
                </ul>
            </li>
            <?php
            if ($this->Session->read('RealUserType') == 'pro') {
                ?>
                <li class="logout">
                    <a href="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'login_as_user')); ?>">
                        User Profile
                    </a>
                </li>
                <li class="logout">
                    <a href="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'login_as_pro')); ?>">
                        Business Profile
                    </a>
                </li>
                <?php
            }
            ?>
            <li class="logout">
                <a href="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'logout')); ?>">
                    Logout
                </a>
            </li>
        </ul>
    <?php } else { ?>
        <ul>
            <li>
                <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'index')); ?>" class="become_pro">Become a pro</a>
            </li>
            <li>
                <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'profinder')); ?>">Profinder</a>
            </li>
            <li>
                <a href="<?php echo $this->Html->Url(array('controller' => 'service', 'action' => 'web_service')); ?>">Marketing</a>
            </li>
            <li><a href="<?php echo $this->Html->Url(array('controller' => 'home', 'action' => 'contact')); ?>">Help</a></li>
            <li><a href="javascript:" class="signUpModal" >Sign up</a></li>
            <li><a href="javascript:" class="loginModal" >Login</a></li>
        </ul>
    <?php } ?>
</div>

<script type="text/javascript">
    $('.expand_menu').click(function() {
        $(this).next('ul').toggle();
    });

    $('.alert_user_for_switch_profile').click(function(ev) {
        if (!confirm('You are about to switch to your business profile. Confirm?')) {
            ev.preventDefault();
        }
    });
</script>