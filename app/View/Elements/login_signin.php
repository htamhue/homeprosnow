<script type="text/javascript">
    $(function() {

        $('.signUpModal').click(function() {
            $('.login_signin').modal();
            $('#SignInForm').hide();
            $('#SignUpForm').show();
        });

        $('.loginModal').click(function() {
            $('.login_signin').modal();
            $('#SignUpForm').hide();
            $('#SignInForm').show();
        });

        $('.open_sign_in').click(function() {
            $('#SignUpForm').hide();
            $('#SignInForm').show();
        })
        $('.open_sign_up').click(function() {
            $('#SignInForm').hide();
            $('#SignUpForm').show();
        })

        $('.sign_me_up').click(function() {
            $('#SignUpForm').submit();
        })


        $('.login').click(function() {
            $('#SignInForm').submit();
        })

        $('#SignUpForm').submit(function() {
            var form = this;

            $.ajax({
                url: "users/sign_up_action",
                type: 'POST',
                dataType: 'json',
                data: {User: {email: $(form).find('.userEmail').val(), password: $(form).find('.userPassword').val(), confirm_password: $(form).find('.userConfirmPassword').val(), name: $(form).find('.userName').val()}},
                success: function(data) {

                    if (data.type == 'error') {
                        if (data.errorMsg.nameError) {
                            $(form).find('.userNameError').html(data.errorMsg.nameError).show();
                        }

                        if (data.errorMsg.emailError) {
                            $(form).find('.userEmailError').html(data.errorMsg.emailError).show();
                        }

                        if (data.errorMsg.passwordError) {
                            $(form).find('.userPasswordError').html(data.errorMsg.passwordError).show();
                        }

                    } else {
                        document.location.href = $.trim($('#SignUpForm').attr('action'));
                    }
                }
            });
            return false;
        });


        $('#SignInForm').submit(function() {
            var form = $(this);
            form.find('.errorMsg').hide();
            var error = 0;
            if ($.trim(form.find('.userEmail').val()) == '') {
                $(from).find('.userEmailError').html('Email address should not be empty').show();
                error = 1;
            } else if ($.trim(form.find('.userPassword').val()) == '') {
                $from.find('.userPasswordError').html('Password should not be empty').show();
                error = 1;
            } else {
                if (error == 1) {
                    return false;
                } else {
                    $.ajax({
                        url: "users/sign_in_action",
                        type: 'POST',
                        dataType: 'json',
                        data: {email: form.find('.userEmail').val(), password: form.find('.userPassword').val()},
                        success: function(data) {
                                
                            if (data.type == 'error') {

                                if (data.errorMsg.emailError) {
                                    form.find('.userEmailError').html(data.errorMsg.emailError).show();
                                }

                                if (data.errorMsg.passwordError) {
                                    form.find('.userPasswordError').html(data.errorMsg.passwordError).show();
                                }
                                if (data.errorMsg.loginError) {
                                    form.find('.userPasswordError').html(data.errorMsg.loginError).show();
                                }


                            } else {
                                document.location.href = $.trim($('#SignInForm').attr('action'));
                            }

                        }
                    });
                }
            }
            return false;
        });
        $('.sign_up_area').on('click', '.forgot_password', function() {
            $('#SignInForm').hide();
            $('.under_logo_text').hide();
            $('.forgot_password_area').show();
        });
        $('.sign_up_area').on('click', '.cancel_btn', function() {
            $('#SignInForm').show();
            $('.under_logo_text').show();
            $('.forgot_password_area').hide();
        });
        $('.sign_up_area').on('click', '.forgot_password_btn', function() {
            var that = $(this);
            var parent = that.closest('.forgot_password_area');
            var secret_question = parent.find('.secret_question_id').val();
            var answer = parent.find('.secret_answer').val();
            var email = parent.find('.email').val();
            $.ajax({
                url: "users/forgot_password",
                type: 'POST',
                dataType: 'json',
                data: {secret_question: secret_question, answer: answer, email: email},
                success: function(data) {
                    if (data.status == 'error') {
                        parent.find('.userPasswordError').html(data.msg).show();
                    } else {
                        $('#successModal').find('.congratulations').html('Forgot Password');
                        $('#successModal').find('.details').html('You will get email from homeprosnow soon');
                        $('#successModal').modal();
                    }

                }
            });
        });
    });
</script>