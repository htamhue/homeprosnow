<!-- sign up Modal -->
<div id="signUpModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"> 
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body modal-signup">
                <div class="col-12 col-md-6">
                    <div class="modal-form">
                        <div class="title">
                            <a href="<?php echo $this->Html->Url(array('controller' => 'home')); ?>"><img src="img/dashboard_logo.png" alt="homeprosnow"></a>
                            <p class="text">Bring home the pros</p>
                        </div>
                        <form action="/" method="post" id="signUpFrm">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Name">
                                <div class="userError"></div>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email Address">
                                <div class="emailError"></div>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password">
                                <div class="passError"></div>
                            </div>
                            <div class="form-group d-flex">
                                <input type="password" class="form-control" placeholder="Confirm Password" id="confirm">
                                <a href="#" class="btn btn-signup"><img src="custom/assets//img/sign_up_tick_mark.png" alt="Sign Up"> Sign me up</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="have-account">
                                <span>Already have an account?</span>
                                <a href="javascript:" class="old">Sign In</a>
                            </div>
                            <input type="submit" value="Submit" style="display: none;">
                        </form>
                    </div>
                    <div class="modal-source signup-source">
                        <a href="<?php echo $this->Html->url('/');?>terms_of_service">Terms Of Service</a>
                        <a href="<?php echo $this->Html->url('/');?>copyright">Copyright Homeprosnow</a>
                        <a href="<?php echo $this->Html->url('/');?>privacy_policy">Privacy Policy</a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-right">
                    <img src="custom/assets/img/sign_up_image.jpg" alt="Example">
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Login Modal -->
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body modal-login">
                <div class="col-12 col-md-6 left">
                    <div class="modal-form">
                        <div class="title">
                            <a href="<?php echo $this->Html->Url(array('controller' => 'home')); ?>"><img src="custom/assets/img/logo.png" alt="homeprosnow"></a>
                            <p class="text">Bring home the pros</p>
                        </div>
                        <form action="/" method="post" id="loginFrm">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email Address">
                                <div class="emailError"></div>
                            </div>
                            <div class="form-group d-flex">
                                <input type="password" class="form-control" placeholder="Password">
                                <a href="#" class="btn btn-primary btn-login">Login <img src="custom/assets/img/login_entry_icon.png" alt="Login"></a>
                            </div>
                            <div class="clear">
                                <a href="javascript:" class="forgot-pass forgotModal text-primary">Forgot password?</a>
                                <div class="passError"></div>
                            </div>
                            <div class="have-account">
                                <span>Don't have an account?</span>
                                <a href="javascript:" class="new text-primary">Create one</a>
                            </div>
                            <input type="submit" value="Submit" style="display: none;">
                        </form>

                    </div>

                    <div class="modal-source">
                        <a href="<?php echo $this->Html->url('/');?>terms_of_service">Terms Of Service</a>
                        <a href="<?php echo $this->Html->url('/');?>copyright">Copyright Homeprosnow</a>
                        <a href="<?php echo $this->Html->url('/');?>privacy_policy">Privacy Policy</a>
                    </div>
                </div>
                <div class="col-12 col-md-6 right">
                    <img src="custom/assets/img/sign_up_image.jpg" alt="Example">
                </div>

            </div>
        </div>
    </div>
</div>

<!-- add process payout Modal -->
<div id="processPayoutStep_1" class="modal fade processPayout" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="process_payout_area">
                <div class="available_balance_text">
                    Available Balance
                </div>
                <div class="amount">
                    $0
                </div>

                <div class="withdraw_input_area">
                    <input type="text" disabled placeholder='I want to withdraw' />
                    <input type="text" class="withdraw_amount" placeholder='$' />
                    <div class="clear"></div>
                </div>

                <a href="javascript:" class="withdraw_btn common_button">
                    Withdraw
                </a>
                <a href="javascript:" class="learn_about">
                    Learn about our payout policies
                </a>

            </div>
        </div>

    </div>
</div>
<!-- add process payout Modal -->
<div id="processPayoutStep_2" class="modal fade processPayout" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="process_payout_area">
                <div class="enter_your_passwerd">
                    Enter Your Password
                </div>
                <input type="password" class="enter_pass"  placeholder='Password' />



                <a href="javascript:" class="confirm_withdraw_btn common_button">
                    Confirm Withdraw
                </a>

            </div>
        </div>

    </div>
</div>
<!-- add process payout Modal -->
<div id="processPayoutStep_3" class="modal fade processPayout " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="process_payout_area step_3">
                <img alt="" src="img/wallet_icon.png"/>
                <div class="enter_your_passwerd">
                    Congratulations!
                </div>
                <div class="confirm_msg">
                    Your withdrawal request is being processed. We will notify via email
                    once it is completed.
                </div>

            </div>
        </div>

    </div>
</div>

<!-- payment alert to view job Modal -->
<div id="paymentAlertToViewJob" class="modal fade paymentAlertToViewJob " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="paymentAlertToViewJobArea">
                <div class="payment_alert_text">
                    A payment of $<?php echo Configure::read('settings.payment.project_view_amount'); ?> will be applied to your account
                    to view this project.
                </div>
                <a href="javascript:" class="continue_btn">Continue</a>
                <a href="javascript:" class="no_thanks_btn">No thanks!</a>
                <br/>
                <br/>
                <br/>
                <a href="javascript:" class="learn_about">
                    Learn about our contractor policies
                </a>
            </div>
        </div>

    </div>
</div>
<div id="prePayment" class="modal fade prePayment" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="prePaymentContent">
                <div class="payment_alert_text">
                    You are about to mark the project <b class="project_name">PROJECT NAME</b> as complete and pay the contractor <b class="contractor_name">CONTRACTOR NAME</b> the agreed amount of <b class="quote_amount">$QUOTE AMOUNT</b>.
                    <br>
                    Click confirm to proceed
                </div>
                <a href="javascript:" class="payemnt_confirm_btn">Confirm</a>
                <a href="javascript:" class="payment_cancel">Cancel</a>

            </div>
        </div>

    </div>
</div>

<!-- project submitted Modal -->
<div id="projectSubmitted" class="modal fade projectSubmitted " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="projectSubmittedArea">
                <img alt="" src="img/tick_mark_big.png"/>
                <div class="congratulations">
                    Congratulations!
                </div>
                <div class="details">
                    Your project has been successfully submitted. 
                    Our project coordinator will get in touch with you soon.
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Become_a_pro Modal -->
<div id="successModal" class="modal fade projectSubmitted " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="projectSubmittedArea">
                <img alt="" src="img/tick_mark_big.png"/>
                <div class="congratulations">
                    Congratulations!
                </div>
                <div class="details">
                    Your request has been submitted. 
                    Our verification expert will get in touch with you soon.
                </div>
            </div>
        </div>

    </div>
</div>
<!-- Become_a_pro Modal -->
<div id="givenRating" class="modal fade givenRating " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="given_rating_area">
                <div class="given_rating_heading heading">
                    Review
                </div>
                <div class="given_rating_body">
                    <input type="hidden" class="hire_id" />
                    <div class="put_rating">

                        <input class="defaultCheckbox" id="" type="radio" checked name="rating" value="0"/>
                        <label class="defultLabel"></label>

                        <input id="rating-1" class="rating" type="radio" name="rating" value="1"/>
                        <label for="rating-1"></label>
                        <input id="rating-2" class="rating" type="radio" name="rating" value="2"/>
                        <label for="rating-2"></label>
                        <input id="rating-3" class="rating" type="radio" name="rating" value="3"/>
                        <label for="rating-3"></label>
                        <input id="rating-4" class="rating" type="radio" name="rating" value="4"/>
                        <label for="rating-4"></label>
                        <input id="rating-5" class="rating" type="radio" name="rating" value="5"/>
                        <label for="rating-5"></label>
                    </div>
                    <div class="put_comment">
                        <textarea class="review_comment" name="review_comment"></textarea>
                    </div>
                    <a href="javascript:" class="common_button put_comment_btn">
                        Done
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="showReview" class="modal fade show_review " role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="show_review_area">
            </div>
        </div>

    </div>
</div>
