<ul>
    <li>
        <a href="<?php echo $this->Html->Url(array('controller' => 'dashboard','action' => 'manage_account')); ?>" <?php if($this->request->params['controller'] == 'dashboard' && $this->request->params['action'] == 'manage_account'){ ?> class="selected"  <?php } ?>>
            Your Profile
            <img alt="" src="img/right_arrow.png"/>
        </a>
    </li>
    <li>
        <a href="<?php echo $this->Html->Url(array('controller' => 'dashboard','action' => 'transaction_history')); ?>" <?php if($this->request->params['controller'] == 'dashboard' && $this->request->params['action'] == 'transaction_history'){ ?> class="selected"  <?php } ?>>
            Transaction History
            <img alt="" src="img/right_arrow.png"/>
        </a>
    </li>
    <li>
        <a href="<?php echo $this->Html->Url(array('controller' => 'dashboard','action' => 'payment_method')); ?>" <?php if($this->request->params['controller'] == 'dashboard' && $this->request->params['action'] == 'payment_method'){ ?> class="selected"  <?php } ?>>
            Payment Method
            <img alt="" src="img/right_arrow.png"/>
        </a>
    </li>
    <li>
        <a href="<?php echo $this->Html->Url(array('controller' => 'dashboard','action' => 'payout_method')); ?>" <?php if($this->request->params['controller'] == 'dashboard' && $this->request->params['action'] == 'payout_method'){ ?> class="selected"  <?php } ?>>
            Payout Method
            <img alt="" src="img/right_arrow.png"/>
        </a>
    </li>
    <li>
        <a href="<?php echo $this->Html->Url(array('controller' => 'dashboard','action' => 'security')); ?>" <?php if($this->request->params['controller'] == 'dashboard' && $this->request->params['action'] == 'security'){ ?> class="selected"  <?php } ?>>
            Security
            <img alt="" src="img/right_arrow.png"/>
        </a>
    </li>
    <li>
        <a href="<?php echo $this->Html->Url(array('controller' => 'dashboard','action' => 'badges')); ?>" <?php if($this->request->params['controller'] == 'dashboard' && $this->request->params['action'] == 'badges'){ ?> class="selected"  <?php } ?>>
            Badges
            <img alt="" src="img/right_arrow.png"/>
        </a>
    </li>
</ul>