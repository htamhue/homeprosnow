<?php if($this->params['action'] !=="index"){?>
<div class="header_area ">
    <?php
}
    if (isset($view_header) && $view_header == 'landing') {
        echo $this->Element('home_menu');
        echo $this->Element('banner');
    } else {
        echo $this->Element('dashboard_menu');
    }
    ?>
    <?php // echo $this->Element('dashboard_menu'); ?>
	<?php if($this->params['action'] !=="index"){?>
</div>

<?php
	}
if (isset($view_header) && $view_header == 'dashboard') {
    if (isset($view_menu) && $view_menu == 'dashboard_sub_menu') {
        if ($this->request->params['controller'] == 'messages') {
            ?>
            <div class="dashboad_sub_menu">
                <ul>
                    <li>
                        <a href="javascript:">
                            Inbox
                        </a>
                    </li>

                </ul>
            </div>
            <?php
        } else if ($this->request->params['controller'] == 'business') {
            ?>
            <div class="dashboad_sub_menu">
                <?php
                if (in_array($this->Session->read('Auth.User.user_type'), array('pro', 'both'))) {
                    ?>
                    <ul>
                        <li <?php if ($this->request->params['controller'] == 'business' && $this->request->params['action'] == 'your_business') { ?> class="selected"  <?php } ?>>
                            <a href="<?php echo $this->Html->Url(array('controller' => 'business', 'action' => 'your_business')); ?>">
                                Your Business
                            </a>
                        </li>
                        <li <?php if ($this->request->params['controller'] == 'business' && ($this->request->params['action'] == 'your_project' || $this->request->params['action'] == 'your_business_details')) { ?> class="selected"  <?php } ?>>
                            <a href="<?php echo $this->Html->Url(array('controller' => 'business', 'action' => 'your_project')); ?>">
                                Your Project
                            </a>
                        </li>
                    </ul>
                    <?php
                }
                ?>
            </div>
            <?php
        } elseif (in_array($this->Session->read('Auth.User.user_type'), array('user'))) {
            ?>
            <div class="dashboad_sub_menu">
                <ul>
                    <li <?php if ($this->request->params['controller'] == 'dashboard' && $this->request->params['action'] == 'manage_account') { ?> class="selected"  <?php } ?>>
                        <a href="<?php echo $this->Html->Url(array('controller' => 'dashboard', 'action' => 'manage_account')); ?>">
                            Manage Account
                        </a>
                    </li>
                    <?php if ($this->Session->read('Auth.User.user_type') == 'user') { ?>
                        <li <?php if ($this->request->params['controller'] == 'dashboard' && $this->request->params['action'] == 'your_project') { ?> class="selected"  <?php } ?>>
                            <a href="<?php echo $this->Html->Url(array('controller' => 'dashboard', 'action' => 'your_project')); ?>">
                                Your Project
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } else {
            ?>
            <div class="dashboad_sub_menu">
            </div>
            <?php
        }
        ?>

        <?php
    } else if (isset($view_menu) && $view_menu == 'no_menu') {
        ?> 
        <div class="dashboad_sub_menu">

        </div>
        <?php
    } else {
        ?>

        <div class="dashboad_sub_menu">
            <div class="search_area">
                <?php
                $zip_code = '';

                $category_id = $this->request->params['named']['category_id'];

                if (($this->request->params['controller'] == 'pro') && (($this->request->params['action'] == 'profinder') || ($this->request->params['action'] == 'pro_finder_lists'))) {
                    $zip_code = $this->request->params['named']['zip_code'];
                    ?>
                    <input type="text" placeholder="zip code" value="<?php echo $zip_code; ?>" class="zip_code_search" />
                    <?php
                } else {
                    if ($this->Session->read('user_zipcode')) {
                        $zip_code = $this->Session->read('user_zipcode');
                    }
                    ?>
                    <input type="text" placeholder="zip code" value="<?php echo $zip_code; ?>" class="zip_code_search" />
                <?php } ?>
                <input type="text" disabled placeholder="I need a" class="search_content" />
                <select class="search_type">
                    <option value="" selected hidden>Painter or Electrician, etc</option>
                    <?php
                    foreach ($categories as $category) {
                        ?>
                        <option <?php if ($category['Categorie']['id'] == $category_id) { ?> selected <?php } ?> value="<?php echo $category['Categorie']['id'] ?>"><?php echo $category['Categorie']['category'] ?></option>
                        <?php
                    }
                    ?>
                </select>
                <a href="javascript:" class="search_button common_button" search_from ="<?php if (isset($serach_from)) echo $serach_from ?>"  >Search</a>
            </div>
        </div>
        <?php
    }
}
?>
<script type = "text/javascript">
//    $(function () {
//        $('.search_area').on('click', '.search_button', function () {
//            var parent = $(this).closest('.search_area');
//            var category_id = parent.find('.search_type').val();
//            var zipCode = parent.find('.zip_code_search').val();
//
//            if (zipCode != '') {
//                var url = "pro/add_project/category_id:" + category_id + "/zip_code:" + zipCode + "/from:search_box";
//            } else {
//                var url = "pro/find_pro/category_id:" + category_id;
//            }
//            window.location.href = url;
//
//
//        });
//    });
</script>