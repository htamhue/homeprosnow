<div class="container-fluid work-wrp">
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-3">
                <img class="img-logo" src="assets/img/logo.png" alt="" />
                <p>At Homeprosnow, we connect homeowners with verified, top rated contractors.</p>
                <p class="social">
                    <a href=""><i class="fa fa-facebook" aria-hidden="true" style="margin-right: 5%;"></i></a>
                    <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </p>
            </div>
            <div class="row col-md-9">
                <div class="col col-12 col-md-3">
                    <ul class="child_ul">
                        <li class="f-title">
                            Popular Categories
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/pro/find_pro/category_id:1">Hardwood Floors</a>
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/pro/find_pro/category_id:11">Painting</a>
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/pro/find_pro/category_id:4">Handyman</a>
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/pro/find_pro/category_id:20">Real Estate</a>
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/pro/find_pro/category_id:7">Electrician</a>
                        </li>
                    </ul>
                </div>
                <div class="col col-12 col-md-3">
                    <ul class="child_ul">
                        <li class="f-title">
                            Homeowner Services
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/pro/profinder">Find Verified Pros</a>
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/pro/add_project">Start a Project</a>
                        </li>
                        <li>
                            <a class="signUpModal " href="javascript:">My Account</a>
                        </li>
                        <li>
                            <a class="loginModal" href="javascript:">Login</a>
                        </li>
                        <li>
                            <a class="signUpModal " href="javascript:">Sign Up</a>
                        </li>
                    </ul>

                </div>
                <div class="col col-12 col-md-3">
                    <ul class="child_ul">
                        <li class="f-title">
                            Business Pro Services
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/become_a_pro">List Your Business for Free</a>
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/become_a_pro">Get a Pro Website</a>
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/service/web_service">Use Our Marketing Suite</a>
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/become_a_pro">Manage Your Projects</a>
                        </li>
                        <li>
                            <a class="loginModal" href="javascript:">Login</a>
                        </li>
                    </ul>
                </div>
                <div class="col col-12 col-md-3">
                    <ul class="child_ul">
                        <li class="f-title">
                            Our Company
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/aboutus">About Us</a>
                        </li>
                        <li>
                            <a href="https://homeprosnow.com/help">Contact Us</a>
                        </li>
                        <li>
                            <a href="/pro/find_pro/category_id:4">617 453 2800</a>
                        </li>
                        <li>
                            <a href="/pro/find_pro/category_id:20">1 Mifflin Place, Suite 400 Cambridge, MA 02138</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
    <div class="container">
    <div class="row">
        <div class="col col-12 col-md-6">
            <p class="text text1"><span>Terms and conditions</span><span>Privacy Policy</span></p>
        </div>

        <div class="col col-12 col-md-6 right">
            <p class="text text2"><span>&copy;2020 - homeprosnow | All right reserved</span></p>
        </div>
    </div>
</div>