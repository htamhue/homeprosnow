<div class="top_menu dash_menu">
    <div class="col-sm-3 col-md-2 col-xs-9 logo">
        <a href="<?php echo $this->Html->Url(array('controller' => 'home','action' => 'index')); ?>">
            <img class="img-responsive" alt="" src="img/dashboard_logo.png" />
        </a>
    </div>
    <?php echo $this->Element('menu');?>
    <div class="clear"></div>
</div>