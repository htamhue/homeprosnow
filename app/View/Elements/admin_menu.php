<ul id="main-nav">  <!-- Accordion Menu -->
    <?php
    if ($this->Session->read('Auth.User.type') == 'admin') {
        ?>
        <li>
            <a href="/admin/dashboard" class="nav-top-item <?php if ($this->params['action'] == 'admin_dashboard') echo "current"; ?> no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
                Dashboard
            </a>       
        </li>
<!--        <li>
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_verified_project')); ?>" class="nav-top-item no-submenu <?php if ($this->request->params['controller'] == 'pro' && ($this->request->params['action'] == 'admin_get_verified_project' || $this->request->params['action'] == 'admin_get_unverified_project' || $this->request->params['action'] == 'admin_project_view')) echo "current"; ?> ">
                Project
            </a>    
        </li>-->
        
        <li>
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_unverified_project')); ?>" class="nav-top-item no-submenu <?php if ($this->request->params['controller'] == 'pro' && ($this->request->params['action'] == 'admin_get_unverified_project' || $this->request->params['action'] == 'admin_get_verified_project' || $this->request->params['action'] == 'admin_project_view')) echo "current"; ?> ">
                Project
            </a>    
        </li>
        
        
        <li>
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_verified_pro_user')); ?>" class="nav-top-item no-submenu <?php if ($this->request->params['controller'] == 'pro' && ($this->request->params['action'] == 'admin_get_verified_pro_user' || $this->request->params['action'] == 'admin_get_unverified_pro_user' || $this->request->params['action'] == 'admin_pro_view')) echo "current"; ?> ">
                Pro User
            </a>    
        </li>
        <li>
            <a href="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'get_user_list')); ?>" class="nav-top-item no-submenu <?php if ($this->request->params['controller'] == 'users' && ($this->request->params['action'] == 'admin_get_user_list')) echo "current"; ?> ">
                User
            </a>    
        </li>
        <li>
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'get_claim_business_list')); ?>" class="nav-top-item no-submenu <?php if ($this->request->params['controller'] == 'pro' && $this->request->params['action'] == 'admin_get_claim_business_list') echo "current"; ?> ">
                Claim Business lists
            </a>    
        </li>
        <li>
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro_by_csv')); ?>" class="nav-top-item no-submenu <?php if ($this->request->params['controller'] == 'pro' && ($this->request->params['action'] == 'add_pro_by_excel' )) echo "current"; ?> ">
                Add Pro User By CSV
            </a>    
        </li>
        <li>
            <a href="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'homepros_videos')); ?>" class="nav-top-item no-submenu <?php if ($this->request->params['controller'] == 'admin' && ($this->request->params['action'] == 'admin_homepros_videos' || $this->request->params['action'] == 'admin_add_homepros_file')) echo "current"; ?> ">
                homeprosnow videos
            </a>    
        </li>

        <li>
            <a href="javascript:void(0);" class="nav-top-item <?php if ($menu_select == 'category') echo "current"; ?>"> Category</a>
            <ul>
                <li><a href="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_category_list')); ?>" class="<?php if ($menu_select == 'category' && $sub_menu_select == 'category_list') echo "current"; ?>">Category list</a></li>
                <li><a href="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'admin_sub_category_list')); ?>" class="<?php if ($menu_select == 'category' && $sub_menu_select == 'sub_category_list') echo "current"; ?>">Sub category list</a></li>
            </ul> 
        </li>
        <li>
            <a href="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'featured_category')); ?>" class="nav-top-item no-submenu <?php if ($menu_select == 'featured_category') echo "current"; ?> ">
                Featured category
            </a>    
        </li>
        <li>
            <a href="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'website_template_list')); ?>" class="nav-top-item no-submenu <?php if ($menu_select == 'website_template') echo "current"; ?> ">
                Website Template
            </a>
        </li>
        <li>
            <a href="javascript:void(0);" class="nav-top-item <?php if ($menu_select == 'bulk') echo "current"; ?>"> Bulk Upload</a>
            <ul>
                <li><a href="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'bulk_category_upload')); ?>" class="<?php if ($menu_select == 'current' && $sub_menu_select == 'bulk_category_upload') echo "current"; ?>">Category list upload</a></li>
            </ul> 
        </li>
        <li>
            <a target="_blank" href="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'download_contractor')); ?>"  class=" nav-top-item no-submenu"> Download contractors </a>
        </li>
    <?php } else if ($this->Session->read('Auth.User.type') == 'co_admin') { ?>
        <li>
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro_by_admin')); ?>" class="nav-top-item no-submenu "> Add pro</a>
        </li>
    <?php } ?>

</ul> <!-- End #main-nav -->