<div class="dashboad_content">
    <div class="view_project">
        <div class="col-md-9 porject_details_area view_quoute_area">
            <div class="company_details_area">
                <div class="col-sm-2 col-md-2">
                    <div class="company_logo">
                        <?php
                        $category_id = $pro_user['ProUser']['category_id'];
                        if ($pro_user['User']['profile_pic']) {
                            ?>
                            <img alt="" src="uploaded_files/profile_pic/<?php echo $pro_user['User']['id']; ?>/<?php echo $pro_user['User']['profile_pic']; ?>" />
                        <?php } else { ?>
                            <img alt="" src="img/company_logo.jpg" />
                        <?php } ?>
                    </div>
                </div>


                <div class="company_details col-sm-4 col-md-8">
                    <div class="company_name">
                        <?php echo $pro_user['ProUser']['company_name'] ?>
                        <?php if ($ownprofile) { ?>
                            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'edit_pro')); ?>">
                                <img alt="" src="img/pencil_icon.png" width="12" />
                            </a>
                        <?php } ?>
                    </div>
                    <div class="clear"></div>
                    <div class="company_moto">
                        &nbsp;
                    </div>
                    <div class="company_basic_info">
                        <ul>
                            <li>
                                <img alt="" src="img/location_icon.png" />
                                <?php echo $pro_user['ProUser']['city'] ?>
                            </li>
                            <li >
                                <img alt="" src="img/verified_icon.png" />
                                <span>
                                    <?php if ($pro_user['project_count'] > 1) { ?>
                                        <?php echo $pro_user['project_count']; ?> Projects
                                    <?php } else { ?>
                                        <?php echo $pro_user['project_count']; ?> Project
                                    <?php } ?>
                                </span>
                            </li>
                            <?php if ($pro_user['reviewCount']['Hire']['reviewCount']) { ?>
                                <li class="user_review_area">
                                    <?php
                                    $reviewPercent = ($pro_user['rating_sum']['Hire']['proRating'] / $pro_user['reviewCount']['Hire']['reviewCount']) * 20;
                                    ?>
                                    <div class="rating_area">
                                        <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                                    </div>
                                    <div class="review_text">
                                        <?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> Reviews
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>

                </div>

                <div class="verified_pro col-sm-2 col-md-2">
                    <?php
                    if ($pro_user['ProUser']['is_verified']) {
                        ?>
                        <img alt="" src="img/pro_verified_icon.png"/>
                        <?php
                    }
                    ?>

                </div>
                <div class="clear"></div>
            </div>
            <div class="company_address_area">
                <div class="col-md-4 address_part">
                    <div class="address_heading">
                        Address
                    </div>
                    <div class="address_text">
                        <?php echo $pro_user['ProUser']['address'] . ', ' . $pro_user['ProUser']['city'] . ', ' . $pro_user['ProUser']['zip_code']; ?>
                    </div>
                </div>
                <div class="col-md-4 address_part">
                    <div class="address_heading">
                        Phone Number
                    </div>
                    <div class="address_text">
                        <?php
                        echo $pro_user['User']['phone'];
//                        if ($view_from == false) {
//                            echo $pro_user['User']['phone'];
//                        } else {
//                            echo 'XXXX XXXX XXXX';
//                        }
                        ?>
                        
                    </div>
                </div>
                <div class="col-md-4 address_part">
                    <div class="address_heading">
                        Email Address
                    </div>
                    <div class="address_text">
                        <?php echo $pro_user['User']['email']; ?> 
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="project_details">
                <div class="project_details_heading">
                    About <?php echo $pro_user['ProUser']['company_name']; ?>
                </div>
                <div class="project_details_text">
                    <?php echo $pro_user['ProUser']['company_description']; ?>
                </div>


                <div class="project_details_heading">
                    PHOTOS OF PAST WORK SIMILAR TO THIS
                </div>
                <div class="project_details_photo">
                    <?php
                    foreach ($pro_user['ProUserFile'] as $proUserFile) {
                        ?>
                        <img alt="" width="100" src="uploaded_files/pro_user_files/<?php echo $proUserFile['pro_user_id'] ?>/<?php echo $proUserFile['image'] ?>"/>
                    <?php } ?>

                </div>
            </div>

            <?php  
                
            if ($pro_user['User']['id'] == 639) { ?>
                <div class="reviews_area">
                    <div class="reviews_heading">

                        <div class="rating_area">
                            <div class="rated_area" style="width: 90%"></div>
                        </div>
                        <div class="review_text">
                            5 Reviews
                        </div>
                    </div>
                    <div class="reviews_list_area">
                        <ul>

                            <li>
                                <div class="username_rating">
                                    <div class="username">Aaron B.</div>
                                    <div class="rating">
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: 100%"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="review_date">
                                    20-jan-2017
                                </div>
                                <div class="review_text">
                                    "Like many we had severe damage from ice damage the past winter. In short the floor buckled and needed to be pulled in 2 places, sub floor had to be replaced, floor replaces, re-sanded and finished. Taozen did a great job and addressed all questions I had. They were on time and the workers were very nice."
                                </div>
                            </li>
                            <li>
                                <div class="username_rating">
                                    <div class="username">Linda S.</div>
                                    <div class="rating">
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: 70%"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="review_date">
                                    28-feb-2017
                                </div>
                                <div class="review_text">
                                    "Taozen refinished my pine hardwood floors in the living room, dining room and stairs. I was very satisfied with the company's professionalism, timeliness, and the quality of the work. I would highly recommend Taozen for refinishing floors."
                                </div>
                            </li>
                            <li>
                                <div class="username_rating">
                                    <div class="username">Mubing L.</div>
                                    <div class="rating">
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: 80%"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="review_date">
                                    15-mar-2017
                                </div>
                                <div class="review_text">
                                    "Taozen offered a very competitive price, and gave out a series of excellent suggestion. The team is very professional and the job was finished with top notch quality. A+ service! My project involved refinishing existing floors and installing unfinished hard wood floor."
                                </div>
                            </li>
                            <li>
                                <div class="username_rating">
                                    <div class="username">Jessica B.</div>
                                    <div class="rating">
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: 100%"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="review_date">
                                    25-jun-2017
                                </div>
                                <div class="review_text">
                                    "We got our 900sq foot house refinished and Taozen did some patch work. it all came out beautiful and they were quick and inexpensive. They were extremely professional and easy to work with. The place looked brand new after they finished. I can't say enough great things about them. i would recommend them to anyone in a second and will be using them for my next house."
                                </div>
                            </li>
                            <li>
                                <div class="username_rating">
                                    <div class="username">Suzanne N.</div>
                                    <div class="rating">
                                        <div class="rating_area">
                                            <div class="rated_area" style="width: 100%"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="review_date">
                                    23-jul-2017
                                </div>
                                <div class="review_text">
                                    "Excellent job! Taozen were very responsive, came out and gave us a quote, were professional and friendly and helpful about some other flooring issues we had. They did high-quality work, cleaned up after themselves and finished on schedule. We would definitely use them again without hesitation."
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>


            <?php if ($pro_user['reviewCount']['Hire']['reviewCount']) { ?>
                <div class="reviews_area">
                    <div class="reviews_heading">
                        <?php
                        $reviewPercent = ($pro_user['rating_sum']['Hire']['proRating'] / $pro_user['reviewCount']['Hire']['reviewCount']) * 20;
                        ?>
                        <div class="rating_area">
                            <div class="rated_area" style="width: <?php echo $reviewPercent; ?>%"></div>
                        </div>
                        <div class="review_text">
                            <?php echo $pro_user['reviewCount']['Hire']['reviewCount'] ?> Reviews
                        </div>
                    </div>
                    <div class="reviews_list_area">
                        <ul>
                            <?php
                            foreach ($pro_user['reviews'] as $review) {
                                ?>
                                <li>
                                    <div class="username_rating">
                                        <div class="username"><?php echo $review['User']['name']; ?></div>
                                        <div class="rating">
                                            <div class="rating_area">
                                                <div class="rated_area" style="width: <?php echo 20 * $review['Hire']['pro_review']; ?>%"></div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="review_date">
                                        <?php echo $review['Hire']['pro_review_date']; ?>
                                    </div>
                                    <div class="review_text">
                                        <?php echo $review['Hire']['pro_review_comment']; ?>
                                    </div>
                                </li>
                            <?php } ?>


                        </ul>
                    </div>
                    <!--                <a href="javascript:" class="view_more_btn">
                                        View More
                                    </a>-->
                </div>
            <?php } ?>
        </div>
        <div class="col-md-3 user_details_area">

            <?php
            if ($view_from == 'profinderlist') {
                ?>
                <div class="request_a_quote_btn_area">
                    <a  href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'submit_project_details', 'category_id' => $category_id, 'specified_pro' => $pro_user['User']['id'])) ?>" class="request_quote common_button">REQUEST A QUOTE</a>
                </div>
                <?php
            }
            ?>





            <div class="license_area">
                <div class="license_heading">
                    LICENSE
                </div>
                <div class="license_details">
                    <?php foreach ($pro_user['CompanyLicense'] as $license) { ?>
                        <div class="single_license">
                            <span class="license_num">
                                LICENSE NO: <?php echo $license['license_number']; ?>
                            </span>
                            <br/>
                            <span>
                                <?php echo $license['license_type']; ?>
                            </span>
                            <br/>
                            <span>
                                Active until: <?php echo $license['expir_date']; ?>
                            </span>     
                        </div>
                    <?php } ?>


                </div>
            </div>






        </div>
        <div class="clear"></div>
    </div>

</div>