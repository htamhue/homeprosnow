<section class="banner_top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="top_banner_head">
                    Get a fully managed, custom website for just $59
                </div>
                <div class="top_banner_text">
                    We build, host and manage your website so you can focus on what matters most - growing your business!
                </div>
                <div class="top_banner_start_btn">
                    <a href="https://www.bond10.com/" target="_blank" class="btn btn-warning start_btn">START HERE</a>
                </div>
                <div class="top_banner_ratting">
                    <div class="star">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="text">4.8-star rated by 376 contractors</div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="top_banner_img">
                    <img class="img img-responsive" src="images/top_banner_right.png"/>
                </div>
            </div>
        </div>
    </div>
</section>

