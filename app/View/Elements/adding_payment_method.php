<!-- add PayPal Account Modal -->
<div id="addPayPalAccount" class="modal fade addPayPalAccount" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="addPayPalAccountArea">
                <?php
                if ($this->request->params['controller'] == 'dashboard' && $this->request->params['action'] == 'payment_method') {
                    $method = 'payment';
                } else {
                    $method = 'payout';
                }
                ?>
                <form id="addPayPalAccountForm" autocomplete="off" action="<?php echo $this->Html->Url(array('controller' => 'payment', 'action' => 'add_payment')); ?>" >
                    <input type="hidden" name="method" value="<?php echo $method; ?>" />
                    <div class="card_icon">
                        <input type="radio" checked id="credit_card" name="account_type" value="credit_card" />
                        <label for="credit_card">
                            <img alt=""   src="img/credit_card_icon.jpg"/>
                        </label>
                        <input type="radio" id="paypal" name="account_type" value="paypal" />
                        <label for="paypal">
                            <img alt="" src="img/paypal_icon.jpg"/>
                        </label>

                    </div>
                    <div class="paypal_area">
                        <div class="paypal_heading heading">
                            PayPal account
                        </div>
                        <input type="text" name="paypal_email" class="paypal_email" placeholder="PayPal Email" />
                        <div class="paypalEmailError error_msg"></div>
                    </div>

                    <div class="credit_area">
                        <div class="paypal_heading heading">
                            Credit card
                        </div>
                        <div class="input_list">
                            <input type="text" name="name_on_the_card" class="name_on_the_card" placeholder="Name on the card" />
                            <div class="cardNameError error_msg"></div>
                        </div>
                        <div class="input_list">
                            <input type="text" name="card_number" class="card_number" placeholder="Card number" />
                            <div class="cardNumberError error_msg"></div>
                        </div>


                        <div class="card_info_area">
                            <div class="expiry_date_aera">
                                <div class="expiry_date_label">
                                    Expiry Date
                                </div>
                                <div class="expiry_date_input input_list">
<!--                                    <input type="text" name="expiry_date" class="expiry_date" placeholder='MM-YY'/>-->
                                    <select name="expiry_month">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>

                                    <select name="expiry_year">
                                        <?php
                                        $current_year = date("Y");
                                        $year_limit = intval($current_year) + 10;
                                        for ($current_year = $current_year; $current_year <= $year_limit; $current_year++) {
                                            ?>
                                            <option value="<?php echo $current_year ?>"><?php echo $current_year ?></option>
                                            <?php
                                        }
                                        ?>

                                    </select>

                                    <div class="expiryDateError error_msg"></div>
                                </div>
                            </div>
                            <div class="cvv">
                                <div class="cvv_label">
                                    CVV
                                    <a href="javascript:" class="what_is_this">what's this?</a>
                                </div>
                                <div class="cvv_input input_list">
                                    <input name="ccv" type="text" />
                                    <div class="ccvError error_msg"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <a href="javascript:" class="addPaymentBtn common_button">
                        Add payment method
                    </a>
                    <input type="submit" value="submit"  style="display: none;"/>
                </form>
                <div class="paypla_footer">
                    <img alt="" src="img/lock_icon.png" />
                    Your payment information is encrypted
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    $(function() {
//        $(".expiry_date").datepicker({
//            dateFormat: "yy-mm-dd"
//        });
        $('.adding_payment').on('click', '.adding_payment_method', function() {
            $('.addPayPalAccount').modal();
        });


        $('.addPaymentBtn').click(function() {
            $('#addPayPalAccountForm').submit();
        });
        $('.card_icon').on('click', '#paypal', function() {
            if ($(this).prop("checked")) {
                $('.paypal_area').show();
                $('.credit_area').hide();
            }
        });
        $('.card_icon').on('click', '#credit_card', function() {
            if ($(this).is(":checked")) {
                $('.credit_area').show();
                $('.paypal_area').hide();
            }
        });

        var options = {
            success: showResponse,
            type: 'post',
            dataType: 'json'
        };
        // bind to the form's submit event 
        $('#addPayPalAccountForm').submit(function() {
            $(this).ajaxSubmit(options);
            return false;
        });

        $('.show_payment_method').on('click', '.remove_payment', function() {
            var that = $(this);
            var id = that.attr('rel');
            if (confirm("Are you sure to delete this payemnt method!")) {
                $.ajax({
                    url: "payment/remove_payment_method",
                    type: 'POST',
                    data: {id: id},
                    success: function(data) {
                        if (data == 'success') {
                            that.closest('.paymen_box').remove();
                        }
                    }
                });
            }

        });

        $('.show_payment_method').on('click', '.default_account', function() {
            var that = $(this);
            var id = that.attr('rel');
            var method = that.attr('method');
            var status;
            if ($(this).is(':checked')) {
                status = 'checked';
            } else {
                status = 'unchecked';
            }
            $.ajax({
                url: "payment/set_default_payment",
                type: 'POST',
                data: {id: id, method: method, status: status},
                success: function(data) {
                    if (data == 'selected') {
                        $('.default_account').removeAttr('checked');
//                        that.attr('checked','checked');
                        that.prop('checked', true);
                    }
                }
            });
        });

    });

// post-submit callback 
    function showResponse(responseText, statusText, xhr, $form) {

        if (responseText.type == 'error') {
//            alert_r(responseText);
            if (responseText.errorMsg.cardNameError) {
                $('.addPayPalAccountArea').find('.cardNameError').html(responseText.errorMsg.cardNameError).show();
            }
            if (responseText.errorMsg.cardNumberError) {
                $('.addPayPalAccountArea').find('.cardNumberError').html(responseText.errorMsg.cardNumberError).show();
            }
            if (responseText.errorMsg.expiryDateError) {
                $('.addPayPalAccountArea').find('.expiryDateError').html(responseText.errorMsg.expiryDateError).show();
            }
            if (responseText.errorMsg.ccvError) {
                $('.addPayPalAccountArea').find('.ccvError').html(responseText.errorMsg.ccvError).show();
            }
            if (responseText.errorMsg.paypalEmailError) {
                $('.addPayPalAccountArea').find('.paypalEmailError').html(responseText.errorMsg.paypalEmailError).show();
            }
        } else if (responseText.type == 'success') {
            $.ajax({
                url: "payment/single_payment_method",
                type: 'POST',
                async: false,
                data: {id: responseText.id},
                success: function(data) {
                    $('.show_payment_method').prepend(data);
                    $('.addPayPalAccount').modal('hide');
                }
            });
        }
    }

</script>
