<li message_id ='<?php echo $message['Message']['id'] ?>' project_quote_id='<?php echo $message['Message']['project_quote_id'] ?>'>
    <div class="inbox_user_img">
        <?php if ($message['pro_pic']) { ?>
            <img alt="" src="uploaded_files/profile_pic/<?php echo $message['user_id'] ?>/<?php echo $message['pro_pic'] ?>" />
        <?php } else { ?>
            <img alt="" src="img/default_pro_pic.png" />
        <?php } ?>
        
    </div>
    <div class="inbox_message_details">
        <div class="inbox_user_name">
            <?php echo $message['user_name']; ?>
        </div>
        <div class="inbox_message">
            <?php echo $message['Message']['message']; ?>
        </div>
        <?php
        if ($message['Message']['attachment']) {
            ?>
            <a class="download_attachment" href="uploaded_files/message_attachment/<?php echo $message['Message']['id']; ?>/<?php echo $message['Message']['attachment']; ?>" download>
                <?php echo $message['Message']['attachment']; ?>
            </a>
        <?php } ?>
        <div class="message_date">
            <?php echo $message['Message']['create_date']; ?>
        </div>
    </div>
    <div class="clear"></div>
</li>