<div class="message_area">
    <div class="col-md-2 message_user_area">
        <div class="user_search_area">
            <input type="text" placeholder="Search" />
            <img alt="" src="img/search_icon.png" />
        </div>
        <div class="online_user_list">
            <ul>
                <?php
                $project_quote_id = 0;
                if (isset($project_quote)) {
                    $project_quote_id = $project_quote['ProjectQuote']['id'];
                    ?>
                    <li class="message_user" id="quote_<?php echo $project_quote['ProjectQuote']['id'] ?>" project_quote_id="<?php echo $project_quote['ProjectQuote']['id'] ?>" project_id = "<?php echo $project_quote['Project']['id'] ?>">
                        <div class="online_user_img">
                            <?php
                            if ($this->Session->read('Auth.User.user_type') == 'user') {
                                $img = $project_quote['User']['profile_pic'];
                                $img_folder = $project_quote['User']['id'];
                            } else {
                                $img = $project_quote['Project']['user']['User']['profile_pic'];
                                $img_folder = $project_quote['Project']['user']['User']['id'];
                            }
                            ?>
                            <?php if ($img) { ?>
                                <img alt="" src="uploaded_files/profile_pic/<?php echo $img_folder; ?>/<?php echo $img; ?>" />
                            <?php } else { ?>
                                <img alt="" src="img/default_pro_pic.png" />
                            <?php } ?>
                            <div class="online_icon"></div>
                        </div>
                        <div class="online_user_name">
                            <?php
                            if ($this->Session->read('Auth.User.user_type') == 'user') {
                                echo $project_quote['User']['name'];
                            } else {
                                echo $project_quote['Project']['user']['User']['name'];
                            }
                            ?>
                        </div>
                        <?php if ($project_quote['unReadCount']) { ?>
                            <div class="online_user_message_counter" rel='<?php echo $project_quote['unReadCount']; ?>'>
                                <?php echo $project_quote['unReadCount']; ?>
                            </div>
                        <?php } ?>
                        <div class="clear"></div>
                    </li>
                    <?php
                }
                ?>
                <?php
                foreach ($messages as $message) {
                    if ($message['project_quote_id'] == $project_quote_id) {
                        continue;
                    }
                    ?>
                    <li id="quote_<?php echo $message['project_quote_id']; ?>" class="message_user" project_quote_id="<?php echo $message['project_quote_id']; ?>" project_id = "<?php echo $message['project']['Project']['id'] ?>">
                        <div class="online_user_img">
                            <?php if ($message['perticipent']['User']['profile_pic']) { ?>
                                <img alt="" src="uploaded_files/profile_pic/<?php echo $message['perticipent']['User']['id'] ?>/<?php echo $message['perticipent']['User']['profile_pic'] ?>" />
                            <?php } else { ?>
                                <img alt="" src="img/default_pro_pic.png" />
                            <?php } ?>
                            <div class="online_icon"></div>
                        </div>
                        <div class="online_user_name">
                            <?php echo $message['perticipent']['User']['name'] ?>
                        </div>
                        <?php if ($message['unReadCount']) { ?>
                            <div class="online_user_message_counter" rel='<?php echo $message['unReadCount']; ?>'>
                                <?php echo $message['unReadCount']; ?>
                            </div>
                        <?php } ?>
                        <div class="clear"></div>
                    </li>
                    <?php
                }
                ?>


            </ul>
        </div>
    </div>
    <div class="col-md-10 inbox_area">
        <div class="inbox_list">
            <ul>

            </ul>
        </div>

        <div class="type_message_area">
            <div class="type_message">
                <textarea class="message_input" placeholder="Write message"></textarea>
                <input type="hidden" class="attached_file"  />
                <form autocomplete="off"  action="<?php echo $this->Html->Url(array('controller' => 'messages', 'action' => 'file_attach')); ?>" id="fileAttach" >
                    <input type="hidden" id="project_id" value="<?php echo $project_id; ?>" />
                    <input type="hidden" id="project_quote_id" value="<?php echo $project_quote_id; ?>" />
                    <input type="file" name="message_attach" id="message_attach" style="display: none;" />
                    <label for="message_attach" class="attach_btn">
                        <img alt="" src="img/attach_icon.png" />
                    </label>
                </form>

            </div>
            <a href="javascript:" class="send_btn">
                <img alt="" src="img/send_btn.png" />
            </a>
            <div class="clear"></div>
        </div>


    </div>
    <div class="clear"></div>
</div>

<div class="file_attach_html" style="display: none">
    <li class="message_file_upload">
        <div class="inbox_user_img">
            <img alt="" src="img/user.jpg" />
        </div>
        <div class="inbox_message_details">
            <div class="inbox_user_name ">
                <?php echo $this->Session->read('Auth.User.name'); ?>
            </div>
            <div class="inbox_message">

            </div>
            <div class="sending_file">
                Sending file...
            </div>
            <div class="sending_file_progress_area">
                <div class="file_icon">
                    <img alt="" src="img/file_icon.png" />
                </div>
                <div class="progress_area">
                    <div class="file_name">
                        filename.zip
                    </div>
                    <div class="progress_bar_area">
                        <div class="progress_bar">
                            <div class="progress_complete_bar">

                            </div>
                        </div>
                        <a href="javascript:" class="file_upload_cancle">
                            <img alt="" src="img/cross_icon.png" />
                        </a>
                    </div>
                    <div class="file_upload_status">
                        <span class="percentage">72%</span> Complete <span class="completeProgress">576</span> of <span class="total">800</span> MB transfered
                    </div>
                </div>
            </div>
            <div class="message_date">
                14:25
            </div>
        </div>
        <div class="clear"></div>
    </li>
</div>

<script type="text/javascript">

    var requestProcessing = false;
    var manuallyScrolled = false;

    setInterval(function () {
        if (requestProcessing) {
            return;
        }
        var project_quote_id = $('#project_quote_id').val();
        var project_id = $('#project_id').val();
        var lastMessageId = $('.inbox_area').find('.inbox_list ul li').last().attr('message_id');
        if (lastMessageId) {
            requestProcessing = true;
            $.ajax({
                url: "messages/open_message",
                type: 'POST',
                data: {project_quote_id: project_quote_id, project_id: project_id},
                success: function (data) {
                    requestProcessing = false;
                    $('.inbox_area').find('.inbox_list ul').html(data);
                    if (!manuallyScrolled) {
                        $('.inbox_area').find('.inbox_list').animate({scrollTop: $('.inbox_area').find('.inbox_list ul').height() - $('.inbox_area').find('.inbox_list').height()}, 1000, function () {
                            manuallyScrolled = false;
                        });
                    }
                }
            });
        }
    }, 2000);

    $(function () {
        $('.type_message_area').on('click', '.send_btn', function () {
            var parent = $(this).closest('.type_message_area');
            var project_quote_id = $('#project_quote_id').val();
            var project_id = $('#project_id').val();
            var message = parent.find('.message_input').val();
            var attached_file = parent.find('.attached_file').val();
            requestProcessing = true;
            $.ajax({
                url: "messages/send_message",
                type: 'POST',
                data: {project_quote_id: project_quote_id, project_id: project_id, message: message, attached_file: attached_file},
                success: function (data) {
                    requestProcessing = false;
                    $('.inbox_area').find('.inbox_list ul').append(data);
                    parent.find('.message_input').val('');
                    parent.find('.attached_file').val('');
                    $('.inbox_area').find('.inbox_list ul').find('.message_file_upload').remove();
                    $('.inbox_area').find('.inbox_list').animate({scrollTop: $('.inbox_area').find('.inbox_list ul').height() - $('.inbox_area').find('.inbox_list').height()}, 1000, function () {
                        manuallyScrolled = false;
                    });
                }
            });
        });

        $('.online_user_list').on('click', '.message_user', function () {
            var that = $(this);
            var project_quote_id = $(this).attr('project_quote_id');
            var project_id = $(this).attr('project_id');
            requestProcessing = true;
            $.ajax({
                url: "messages/open_message",
                type: 'POST',
                data: {project_quote_id: project_quote_id, project_id: project_id},
                success: function (data) {
                    requestProcessing = false;
                    $('.inbox_area').find('.inbox_list ul').html(data);
                    $('.inbox_area').find('.inbox_list').animate({scrollTop: $('.inbox_area').find('.inbox_list ul').height() - $('.inbox_area').find('.inbox_list').height()}, 1000, function () {
                        manuallyScrolled = false;
                    });
                    that.find('.online_user_message_counter').attr('rel', '0').html('0').hide();
                }
            });
        });

        $('.inbox_area').find('.inbox_list').scroll(function (ev) {
            if (this.scrollTop + $(this).outerHeight() >= this.scrollHeight) {
                manuallyScrolled = false;
            } else {
                manuallyScrolled = true;
            }

        });

        /**
         * after user browse new picture automatic form will be submitted
         */
        $('#message_attach').change(function () {
            $('#fileAttach').submit();
        });

        var options = {
            type: 'post',
            dataType: 'json',
            beforeSubmit: function (arr, $form, options) {
                $('.inbox_list ul').prepend($('.file_attach_html').html());
            },
            uploadProgress: function (event, position, total, percentComplete) {

                var total = Math.floor((parseInt(event.total) / 1024) / 1024);
                var loaded = Math.floor((parseInt(event.loaded) / 1024) / 1024);
                var percentVal = percentComplete + '%';
                $('.inbox_list ul').find('.message_file_upload').find('.progress_complete_bar').css('width', percentVal);
                $('.inbox_list ul').find('.message_file_upload').find('.percentage').html(percentVal);
                $('.inbox_list ul').find('.message_file_upload').find('.total').html(total);
                $('.inbox_list ul').find('.message_file_upload').find('.completeProgress').html(loaded);

            },
            success: responsefunc
        };

        $('#fileAttach').submit(function () {
            $(this).ajaxSubmit(options);
            //$(".addpictureblock").modal('hide');
            return false;
        });

        $('.inbox_list').on('click', '.file_upload_cancle', function () {
            var file_name = $(this).attr('rel');
            var that = $(this);
            $.ajax({
                url: "messages/remove_attachment",
                type: 'POST',
                data: {file_name: file_name},
                success: function (data) {
                    if (data == 'success') {
                        that.closest('li').remove();
                        $('.attached_file').val('');
                    }
                }
            });
        });
        $(".online_user_list ul li").first().trigger("click");

        $('.inbox_list').on('click', 'ul li', function () {
            var that = $(this);
            var id = that.attr('message_id');
            var project_quote_id = parseInt(that.attr('project_quote_id'));

            $.ajax({
                url: "messages/read_message",
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    if (data == 'success') {
                        that.removeClass('unRead');
                        var unReadCount = parseInt($('#quote_' + project_quote_id).find('.online_user_message_counter').attr('rel')) - 1;
                        $('#quote_' + project_quote_id).find('.online_user_message_counter').attr('rel', unReadCount).html(unReadCount);
                    }
                }
            });
        });

    });

    function responsefunc(responseText, statusText, xhr, $form) {
//        alert_r(responseText);
        if (responseText.type == 'success') {
            $('.attached_file').val(responseText.file_name);
            $('.inbox_list ul').find('.message_file_upload').find('.file_upload_cancle').attr('rel', responseText.file_name)
        }
    }

</script>