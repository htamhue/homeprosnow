<div class="dashboad_content">
    <div class="col-md-3">
        <div class="dashboad_menu">
            <?php echo $this->Element('dashboard_side_menu'); ?>
        </div>
    </div>
    <div class="col-md-9">
        <div class="history_table">
            <table>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Project</th>
                        <?php
                        $user_type = $this->Session->read('Auth.User.user_type');
                        if ($user_type == 'pro') {
                            ?>
                            <th>Client</th>
                            <?php
                        } else {
                            ?>
                            <th>Pro Hired</th>
                            <?php
                        }
                        ?>

                        <th>Job Status</th>
                        <th>Amount</th>
                        <th>Reviewed</th> 
                    </tr>
                </thead>
<!--                <tbody>
                    <tr class="shade">
                        <td>01/06/2016</td>
                        <td>
                            Need the roof fixed and kitchen
                            redone in a month.
                        </td>
                        <td>Jason Smith</td>
                        <td>In Progress</td>
                        <td>$2,000</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        <td>01/06/2016</td>
                        <td>
                            Need the roof fixed and kitchen
                            redone in a month.
                        </td>
                        <td>Jason Smith</td>
                        <td>In Progress</td>
                        <td>$2,000</td>
                        <td>Yes</td>
                    </tr>
                    <tr class="shade">
                        <td>01/06/2016</td>
                        <td>
                            Need the roof fixed and kitchen
                            redone in a month.
                        </td>
                        <td>Jason Smith</td>
                        <td>In Progress</td>
                        <td>$2,000</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        <td>01/06/2016</td>
                        <td>
                            Need the roof fixed and kitchen
                            redone in a month.
                        </td>
                        <td>Jason Smith</td>
                        <td>In Progress</td>
                        <td>$2,000</td>
                        <td>Yes</td>
                    </tr>
                </tbody>-->

            </table>
        </div>
    </div>
    <div class="clear"></div>
</div>