<div class="dashboad_content">
    <div class="project_top">
        <div class="col-md-3 project_left">
            <div class="project_heading">
                Imagine what you
                can build
            </div>
            <div class="project_text">
                With Homeprosnow, the possibilities
                are endless. Find the best pro, right
                away!
            </div>
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_project')); ?>" class="common_button strat_project_btn">
                Start a project
            </a>
        </div>
        <div class="col-md-9 project_right">
            <img alt="" width="100%" src="img/project_image.jpg"/>
        </div>
        <div class="clear"></div>
    </div>
    <div class="project_history_table">
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Project</th>
                    <th>Pro Hired</th>
                    <!--<th>Project Quote</th>-->
                    <th>Job Status</th>
                    <th>Amount</th>
                    <th>Reviewed</th> 
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($projects as $project) {
                    ?>
                    <tr class="shade single_project_list">
                        <td><?php echo $project['Project']['create_date']; ?></td>
                        <td>
                            <a class="common_link" href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'view_project', 'project_id' => $project['Project']['id'])); ?>">
                                <?php echo $project['Project']['project_title']; ?>
                            </a>

                        </td>
                        <td>
                            <?php echo $project['pro_user']['User']['name'] ?>
                        </td>
<!--                        <td>
                            <?php
                            if ($project['project_quote']['ProjectQuote']['id']) {
                                ?>
                                <a class="common_link" href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'view_quote', 'quote_id' => $project['project_quote']['ProjectQuote']['id'])); ?>">
                                    View Quote
                                </a>
                            <?php } ?>
                        </td>-->
                        <td>
                            <?php echo $project['hire']['Hire']['hire_status']; ?>
                        </td>
                        <td>$<?php echo $project['project_quote']['ProjectQuote']['amount']; ?></td>
                        <td>
                            <?php
                            if ($project['hire']['Hire']['hire_status'] == 'completed') {
                                $review_text = '';
                                $class = '';
                                if ($project['hire']['Hire']['user_review']) {
                                    $review_text = 'Yes';
                                    $class = 'show_review';
                                } else {
                                    $review_text = 'No';
                                    $class = 'user_review';
                                }
                                ?>
                                <a  href="javascript:" id="hire_<?php echo $project['hire']['Hire']['id'] ?>" rel="<?php echo $review_text; ?>" class="<?php echo $class; ?> common_link" hire_id="<?php echo $project['hire']['Hire']['id'] ?>">
                                    <?php
                                    echo $review_text;
                                    ?>
                                </a>

                                <?php
                            } else {
                                ?>
                                Working
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                <?php } ?>

            </tbody>

        </table>
    </div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    $(function() {
        $('.single_project_list').on('click', '.user_review', function() {
            var hire_id = $(this).attr('hire_id');
            var that = $(this);
            var parent = that.closest('.single_project_list');
            var status = that.attr('rel');
            if (status == 'No') {
                $('#givenRating').modal();
                $('.hire_id').val(hire_id);
            }

        });



    });
</script>