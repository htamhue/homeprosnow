<div class="dashboad_content">
    <div class="col-md-3">
        <div class="dashboad_menu">
            <?php echo $this->Element('dashboard_side_menu'); ?>
        </div>
    </div>
    <div class="show_payment_method row col-md-6">
        <?php
        foreach ($payment_accounts as $payment_account) {
            ?>
            <div class="col-sm-6 paymen_box">
                <div class="added_payment">
                    <div class="added_payment_heading">
                        <input type="checkbox" <?php if ($payment_account['PaymentAccount']['default']) { ?> checked <?php } ?> method="<?php echo $payment_account['PaymentAccount']['method'] ?>" class="default_account" rel="<?php echo $payment_account['PaymentAccount']['id'] ?>" />
                        Default
                    </div>
                    <div class="added_payment_body">
                        <a rel="<?php echo $payment_account['PaymentAccount']['id'] ?>" href="javascript:" class="remove_payment">
                            <img alt="" src="img/remove_icon.png" />
                            Remove
                        </a>
                        <?php
                        if ($payment_account['PaymentAccount']['payment_account_type'] == 'paypal') {
                            ?>
                            <div class="card_icon">
                                <img alt="" src="img/paypal_icon.jpg" />
                            </div>
                            <div class="card_holder_email">
                                <?php echo $payment_account['PaymentAccount']['paypal_email']; ?>
                            </div>
                        <?php } else { ?>
                            <div class="card_icon">
                                <img alt="" src="img/visa_icon.png" />
                            </div>
                            <div class="card_holder_name">
                                <?php echo $payment_account['PaymentAccount']['name_on_the_card']; ?>
                            </div>
                            <div class="card_number">
                                <?php echo $payment_account['PaymentAccount']['card_number']; ?>
                            </div>
                            <div class="card_date">
                                <?php
                                if ($payment_account['PaymentAccount']['expiry_month'] < 10) {
                                    $expiry_month = '0' . $payment_account['PaymentAccount']['expiry_month'];
                                } else {
                                    $expiry_month = $payment_account['PaymentAccount']['expiry_month'];
                                }
                                echo $expiry_month . '-' . $payment_account['PaymentAccount']['expiry_year']
                                ?>
                            </div>
    <?php } ?>

                    </div>
                </div>

            </div>
<?php } ?>
        <div class="col-sm-6">
            <div class="adding_payment">
                <a href="javascript:" class="adding_payment_method">
                    <img alt="" src="img/plus_icon.png" />
                    <br/>
                    Add another payment method
                </a>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="col-md-3">
        <div class="withdraw_area">
            <div class="available_heading">
                Available Balance
            </div>
            <div class="available_balance">
                $0
                <input type="hidden" value="4795" class="available_balance_input" />
            </div>
            <a href="javascript:" class="withdraw_btn common_button">Withdraw</a>
            <div class="available_heading">
                Pending Balance
            </div>
            <div class="available_balance">
                $0
                <input type="hidden" value="6802" class="pending_balance_input" />
            </div>
            <div class="available_text">
                will be available to withdraw
                on 16/06/2016
            </div>

            <a href="javascript:" class="payout_polici_btn">
                learn about our payout policies
            </a>

        </div>
    </div>
    <div class="clear"></div>
</div>

<?php echo $this->Element('adding_payment_method'); ?>

<script type="text/javascript">
    $(function() {
        $('.withdraw_area').on('click', '.withdraw_btn', function() {
            var that = $(this);
            var available_balance = that.closest('.withdraw_area').find('.available_balance_input').val();
            $('#processPayoutStep_1').find('.amount').html(available_balance);
            $('#processPayoutStep_1').modal();
        });

        $('#processPayoutStep_1').on('click', '.withdraw_btn', function() {
            $('#processPayoutStep_1').modal('hide');
            $('#processPayoutStep_2').modal();
        });
    });
</script>