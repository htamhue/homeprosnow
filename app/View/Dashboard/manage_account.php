<div class="dashboad_content">
    <div class="col-md-3">
        <div class="dashboad_menu">
            <?php echo $this->Element('dashboard_side_menu'); ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="profile_info">
            <?php if ($this->Session->read('Auth.User.id')) { ?>
                <a href="javascript:" class="profile_update_btn">
                    <img alt="" src="img/pencil_icon.png" />
                </a>
            <?php } ?>
            <form autocomplete="off"  action="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'update_profile')); ?>" id="updateProfile" >
                <div class="profile_pic">
                    <?php
                    if ($user['User']['profile_pic']) {
                        ?>
                        <img class="profile_image" alt="" src="uploaded_files/profile_pic/<?php echo $user['User']['id'] ?>/<?php echo $user['User']['profile_pic'] ?>"/>
                    <?php } else { ?>
                        <img class="profile_image" alt="" src="img/default_pro_pic.png"/>
                    <?php } ?>
                    <div class="profile_pic_upload">
                        <input type="file" name="profile_pic" id="profile_pic" />
                        <label for="profile_pic">
                            <img alt="" src="img/pencil_icon.png" />
                        </label>
                    </div>

                </div>
                <div class="error_msg imageError"></div>
                <div class="clear"></div>

                <div class="user_name">
                    <img alt="" src="img/username_icon.png" />
                    <span class="text">
                        <?php echo $user['User']['name']; ?>
                    </span>
                    <input type="text" name="name" value="<?php echo $user['User']['name']; ?>" class="userNameInput" />

                </div>
                <div class="user_email">
                    <img alt="" src="img/usermail_icon.png" />
                    <span class="text">
                        <?php echo $user['User']['email']; ?>
                    </span>
                    <input type="text" value="<?php echo $user['User']['email']; ?>" name="email" class="emalInput" />
                    <div class="error_msg emailError"></div>
                </div>
                <div class="user_phone">
                    <img alt="" src="img/userphone_icon.png" />
                    <span class="text">
                        <?php echo $user['User']['phone']; ?>
                    </span>
                    <input type="text" value="<?php echo $user['User']['phone']; ?>" name="phone" class="phoneInput" />
                </div>
                <div class="user_address">
                    <img alt="" src="img/userphone_icon.png" />
                    <span class="text">
                        <?php
                        if ($user['User']['address']) {
                            echo $user['User']['address'];
                        } else {
                            echo 'Please set your address';
                        }
                        ?>
                    </span>
                    <input type="text" placeholder="Address" value="<?php echo $user['User']['address']; ?>" name="address" class="addressInput" />
                </div>
                <div class="user_city">
                    <img alt="" src="img/userphone_icon.png" />
                    <span class="text">
                        <?php
                        if ($user['User']['city']) {
                            echo $user['User']['city'];
                        } else {
                            echo 'Please set your city';
                        }
                        ?>
                    </span>
                    <input type="text" placeholder="City" value="<?php echo $user['User']['city']; ?>" name="city" class="cityInput" />
                </div>
                <div class="user_state">
                    <img alt="" src="img/userphone_icon.png" />
                    <span class="text">
                        <?php
                        if ($user['User']['state']) {
                            echo $user['User']['state'];
                        } else {
                            echo 'Please set your state';
                        }
                        ?>
                    </span>
                    <select name="state" class="stateInput" >
                        <option value="">Select State</option>
                        <?php
                        foreach ($state_lists as $key => $val) {
                            ?>
                            <option <?php echo ($val == $user['User']['state'] ? 'selected' : ''); ?> value="<?php echo $val; ?>">
                                <?php echo $val; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="user_country">
                    <img alt="" src="img/userphone_icon.png" />
                    <span class="text">
                        <?php
                        if ($user['User']['country']) {
                            echo $user['User']['country'];
                        } else {
                            echo 'Please set your country';
                        }
                        ?>
                    </span>
                    <select name="country" class="countryInput" style="width: 168px;">
                        <option value="">Select Country</option>
                        <?php
                        foreach ($countries as $key => $val) {
                            ?>
                            <option <?php echo ($val['Country']['nicename'] == $user['User']['country'] ? 'selected' : ''); ?> value="<?php echo $val['Country']['nicename']; ?>">
                                <?php echo $val['Country']['nicename']; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="user_zip_code">
                    <img alt="" src="img/userphone_icon.png" />
                    <span class="text">
                        <?php
                        if ($user['User']['zip_code']) {
                            echo $user['User']['zip_code'];
                        } else {
                            echo 'Please set your zip code';
                        }
                        ?>
                    </span>
                    <input type="text" placeholder="Zip Code" value="<?php echo $user['User']['zip_code']; ?>" name="zip_code" class="zipCodeInput" />
                </div>
                <input type="submit" class="update_info common_button" value="Update Info" />
            </form>
        </div>
    </div>
    <div class="col-md-5">
        <div class="recommended_pro_area">
            <div class="recommended_img">
                <img alt="" src="img/recommended_img.jpg" />
                <div class="recommended_overlay">

                </div>
                <div class="recommended_action">
                    <div class="recommended_heading">
                        Get quotes from recommended pros
                    </div>
                    <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_project')); ?>" class="common_button strat_project_btn">
                        Start a project
                    </a>
                </div>
            </div>


        </div>
    </div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    $(function() {
        $('.profile_info').on('click', '.profile_update_btn', function() {
            var parent = $(this).closest('.profile_info');
            parent.find('.text').hide();
            parent.find('.user_name,.user_email,.user_phone,.user_address,.user_city,.user_state,.user_country,.user_zip_code').find('input,select').show();
            parent.find('.profile_pic_upload').show();
        });

        var options = {
            type: 'post',
            dataType: 'json',
            beforeSubmit: function(arr, $form, options) {
                $('.error_msg').hide();
            },
            success: responsefunc
        };

        $('#updateProfile').submit(function() {
            $(this).ajaxSubmit(options);
            //$(".addpictureblock").modal('hide');
            return false;
        });


        $('.profile_info').on('change', '#profile_pic', function() {
            var that = $(this);
            $('.imageError').hide();
            var file_extension_array = ["jpg", "png", "gif"];
            var file_extension = that.val().split('.').pop();
            if ($.inArray(file_extension.toLowerCase(), file_extension_array) !== -1) {
                readURL(this);
            } else {
                $('.imageError').html('jpg, png or gif file are allowed!!').show();
            }

        });

    });

    function responsefunc(responseText, statusText, xhr, $form) {
//        alert_r(responseText);
        if (responseText.type == 'error') {
            $('.emailError').html(responseText.error.emailError).show();
            $('.imageError').html(responseText.error.imageError).show();
        } else {
            $('.profile_info').find('.user_name .text').html(responseText.user.User.name).show();
            $('.profile_info').find('.user_email .text').html(responseText.user.User.email).show();
            $('.profile_info').find('.user_phone .text').html(responseText.user.User.phone).show();
            $('.profile_info').find('.user_address .text').html(responseText.user.User.address).show();
            $('.profile_info').find('.user_city .text').html(responseText.user.User.city).show();
            $('.profile_info').find('.user_state .text').html(responseText.user.User.state).show();
            $('.profile_info').find('.user_country .text').html(responseText.user.User.country).show();
            $('.profile_info').find('.user_zip_code .text').html(responseText.user.User.zip_code).show();
            $('.profile_info').find('.profile_image').attr('src', 'uploaded_files/profile_pic/' + responseText.user.User.id + '/' + responseText.user.User.profile_pic);
            $('.profile_info').find('.user_name,.user_email,.user_phone,.user_address,.user_city,.user_state,.user_country,.user_zip_code').find('input,select').hide();
            $('.profile_info').find('.profile_pic_upload').hide();
        }

    }

    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();
            reader.onload = function(e) {

                $('.profile_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }

    }
</script>