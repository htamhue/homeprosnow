<div class="dashboad_content">
    <div class="col-md-3">
        <div class="dashboad_menu">
            <?php echo $this->Element('dashboard_side_menu'); ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="added_payment">
            <div class="added_payment_heading">
                Manage Password
            </div>
            <form autocomplete="off"  action="<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'update_password')); ?>" id="changePassword" >
                <div class="added_payment_body manage_pass">
                    <label>Old Password</label>
                    <input name="oldPassword" type="password" />
                    <div class="clear"></div>
                    <label>New Password</label>
                    <input name="newPassword" type="password" />
                    <div class="clear"></div>
                    <label>Confirm New Password</label>
                    <input name="confirmNewPassword" type="password" />
                    <div class="clear"></div>
                    <div class="error_msg passwordError">
                    </div>
                    <a href="javascript:" class="change_pass common_button">Change Password</a>
                    <input type="submit" value="submit" style="display: none;" />
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-4">
        <div class="added_payment">
            <div class="added_payment_heading">
                Secret Question
            </div>
            <div class="added_payment_body secret_question">
                <label>Question</label>
                <select class="secret_question_id"  name="secret_question">
                    <option>Select question</option>
                    <?php
                    foreach ($secret_questions as $secret_question) {
                        ?>
                        <option value="<?php echo $secret_question['SecretQuestion']['id']; ?>"><?php echo $secret_question['SecretQuestion']['question']; ?></option>
                        <?php
                    }
                    ?>
                </select>
                <div class="clear"></div>
                <label>Answer</label>
                <input type="text" name="secret_answer" class="secret_answer" />
                <div class="clear"></div>
                <a href="javascript:" class="confirm_btn common_button">Confirm</a>
            </div>
        </div>

    </div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    $(function() {
        $('.change_pass').click(function() {
            $('#changePassword').submit();
        });


        var options = {
            type: 'post',
            dataType: 'json',
            beforeSubmit: function(arr, $form, options) {

            },
            success: responsefunc
        };

        $('#changePassword').submit(function() {
            $(this).ajaxSubmit(options);
            //$(".addpictureblock").modal('hide');
            return false;
        });

        $('.secret_question').on('click', '.confirm_btn', function() {
            var that = $(this);
            var parent = that.closest('.secret_question');
            var question = parent.find('.secret_question_id').val();
            var answer = parent.find('.secret_answer').val();
            $.ajax({
                url: "users/update_secret_question",
                type: 'POST',
                data: {question: question, answer: answer},
                success: function(data) {
                    if(data == 'success'){
                        $('#successModal').find('.congratulations').html('Secret Question Updated');
                        $('#successModal').find('.details').html('');
                        $('#successModal').modal();
                    }

                }
            });
        });

    });
    function responsefunc(responseText, statusText, xhr, $form) {
//        alert_r(responseText);
        if (responseText.type == 'success') {
            
            $('#successModal').find('.details').html('Your password has been successfully updated');
            $('.passwordError').hide();
            $('.manage_pass input').val('');
            $('#successModal').modal();


        } else if (responseText.type == 'error') {
            $('.passwordError').html(responseText.errorMsg.passwordError);
        }
    }
</script>