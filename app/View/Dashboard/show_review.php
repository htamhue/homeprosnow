<?php
$user_id = $this->Session->read('Auth.User.id');
$user_type = $this->Session->read('Auth.User.user_type');
?>
<div class="review_part pro_user_review">
    <?php if ($hire['Hire']['pro_user_id'] == $user_id) { ?>
        <div class="heading">
            Your review
        </div>
    <?php } else { ?>
        <div class="heading">
            You have received a review from <?php echo $hire['pro_user']['User']['name'] ?>
        </div>
    <?php } ?>
    <div class="user_rating_area">
        <label>Rating : </label>
        <div class="rating_area">
            <div class="rated_area" style="width: <?php echo $hire['Hire']['pro_review'] * 20 ?>%;">

            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="review_comment_text">
        <label>Comment : </label><?php echo $hire['Hire']['pro_review_comment']; ?>
    </div>
</div>
<div class="review_part project_user_review">
    <?php if ($hire['Hire']['user_id'] == $user_id) { ?>
        <div class="heading">
            Your review
        </div>
    <?php } else { ?>
        <div class="heading">
            You have received a review from <?php echo $hire['pro_user']['User']['name'] ?>
        </div>
    <?php } ?>
    
    <div class="user_rating_area">
        <label>Rating : </label>
        <div class="rating_area">
            <div class="rated_area" style="width: <?php echo $hire['Hire']['user_review'] * 20 ?>%;">

            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="review_comment_text">
        <label>Comment : </label><?php echo $hire['Hire']['user_review_comment']; ?>
    </div>
</div>