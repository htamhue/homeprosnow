<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <?php echo $this->Element('head'); ?>
    <?php 
        if(($this->params['controller'] == 'businesspro' && $this->params['action'] == 'become_a_pro') || $this->params['controller'] == 'businesspro' && $this->params['action'] == 'add_pro'){
            $body = 'become-a-pro1';
        }else{
            $body = 'become-a-pro-wrapper';
        }
    ?>
    <body class="<?php echo $body; ?>">
        <?php
        
            $class = '';
            if (isset($view_header) && $view_header == 'dashboard') {
                $class = 'dashboard_area';
            }
        ?>
        <?php if($this->params['controller'] == 'businesspro'): ?>
            <div class="container">
                <?php echo $this->Element('header'); ?>
                <?php echo $this->Element('login_signin'); ?>
            </div>
            <?php echo $this->fetch('content'); ?>
            <?php echo $this->Element('footer'); ?>
            <?php echo $this->Element('modal'); ?>
        <?php else: ?>
            <div class="container <?php echo $class; ?>">
                <?php echo $this->Element('login_signin'); ?>
                <?php echo $this->Element('header'); ?>
                <div class="content">
                    <?php echo $this->fetch('content'); ?>
                </div>
                <?php echo $this->Element('footer'); ?>
            </div>
            <?php echo $this->Element('modal'); ?>
        <?php endif; ?>
    </body>
</html>
