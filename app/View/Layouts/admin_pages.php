<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <base href="<?php echo $this->Html->url('/'); ?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>homeprosnow</title>
        <link type="image/x-icon" href="favicon.ico" rel="shortcut icon" />
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="css/admin/reset.css" />
        <link rel="stylesheet" type="text/css" href="css/admin/style.css" />
        <link rel="stylesheet" type="text/css" href="css/admin/invalid.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui-1.8.1.custom.css" />
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <!--<script type="text/javascript" src="js/admin/jquery-1.3.2.min.js"></script>-->
        <script type="text/javascript" src="js/jquery.form.min.js"></script>
        <script type="text/javascript" src="js/admin/simpla.jquery.configuration.js"></script>
        <script type="text/javascript" src="js/admin/jquery.wysiwyg.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    </head>
    <body>
        <div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

            <div id="sidebar"><div id="sidebar-wrapper"> 
                    <a href="<?php echo $this->Html->Url(array('controller' => 'home', 'admin' => false)); ?>" class="logo" style="float:right; margin-right:10px;">
                        <img id="logo" src="img/logo.png" alt="" width="199" />
                    </a>
                    <div id="profile-links">
                        Hello, <a href="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'change')); ?>" title="Edit your profile">Welcome Admin</a><br />
                        <!--                        Hello, <a href="/admin/change" title="Edit your profile">Welcome Admin</a><br />-->
                        <br />
                        <a href="<?php echo $this->Html->Url(array('controller' => 'home', 'admin' => false)); ?>" title="View the Site">View the Site</a> | <a href="<?php echo $this->Html->Url(array('controller' => 'admin', 'action' => 'logout')); ?>" title="Sign Out">Sign Out</a>
                    </div>
                    <?php echo $this->Element('admin_menu'); ?>
                </div></div> <!-- End #sidebar -->

            <div id="main-content"> <!-- Main Content Section with everything -->

                <!-- Start Notifications -->
                <?php echo $this->Session->flash(); ?>
                <!-- End Notifications -->

                <?php echo $content_for_layout; ?>

                <div id="footer">
<!--                    <small>  Remove this notice or replace it with whatever you want 
                        &#169; Copyright <?php echo date('Y'); ?> NYC Smart | Powered by <a href="http://numberonecreations.com" target="_blank">numberONE Creations</a> | <a href="#">Top</a>
                    </small>-->
                </div><!-- End #footer -->

            </div> <!-- End #main-content -->

        </div>
        <input type="hidden" name="base_path" id="base_path" value="" />
    </body>
</html>