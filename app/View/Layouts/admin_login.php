<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <base href="<?php echo $this->Html->url('/'); ?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>homeprosnow</title>
        <link type="image/x-icon" href="favicon.ico" rel="shortcut icon" />
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="css/admin/reset.css" />
        <link rel="stylesheet" type="text/css" href="css/admin/style.css" />
        <link rel="stylesheet" type="text/css" href="css/admin/invalid.css" />
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
   
        <!-- Javascripts -->
        <?php // echo $html->script(array("admin/jquery-1.3.2.min.js", "admin/simpla.jquery.configuration.js", "admin/facebox.js", "admin/jquery.wysiwyg.js", "jquery.validate.js", "admin_login.js", "errormessages.js")); ?>

      
    </head>
    <body id="login">
        <input type="hidden" name="base_path" id="base_path" value="" />
        <?php echo $content_for_layout; ?>
    </body>
</html>