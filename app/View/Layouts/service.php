<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <?php echo $this->Element('head'); ?>
    <body>
        <?php
        $class = '';
        if (isset($view_header) && $view_header == 'dashboard') {
            $class = 'dashboard_area';
        }
        ?>
        <?php echo $this->Element('login_signin'); ?>
        <div class="header_area">
            <?php
            if (isset($view_header) && $view_header == 'landing') {
                echo $this->Element('home_menu');
            } else {
                echo $this->Element('dashboard_menu');
            }
            ?>
        </div>


        <?php echo $this->Element('service_header'); ?>

        <?php echo $this->fetch('content'); ?>
        <?php echo $this->Element('service_footer'); ?>
        <?php echo $this->Element('footer'); ?>

        <?php echo $this->Element('modal'); ?>

    </body>
</html>
