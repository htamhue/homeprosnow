
function str_repeat(input, multiplier) {
    return new Array(multiplier + 1).join(input);
}

function typeOf(obj) {
    if (!obj)
        return "null";
    if (typeof (obj) == 'object') {
        if (obj.length)
            return 'array';
        else
            return 'object';
    } else
        return typeof (obj);
}

function dump(arr, level) {

    if (!level)
        level = 0;

    // The padding given at the beginning of the line.
    var level_padding = str_repeat("  ", level);
    var dumped_text = level_padding + typeOf(arr) + "\n";
    for (var j = 0; j < level + 1; j++)
        level_padding += "    ";

    if (typeof (arr) == 'object') { // Array/Hashes/Objects
        dumped_text += level_padding + "{\n";
        for (var item in arr) {
            var value = arr[item];

            if (typeof (value) == 'object') { // If it is an array,
                dumped_text += level_padding + "     [" + item + "] =>";
                dumped_text += dump(value, level + 1);
            } else {
                dumped_text += level_padding + "     [" + item + "] => "
                        + value + "\n";
            }
        }
        dumped_text += level_padding + "}\n";
    } else { // Stings/Chars/Numbers etc.
        dumped_text = "==>" + arr + "<==(" + typeof (arr) + ")";
    }
    return dumped_text;
}

function alert_r(arr) {
    alert(dump(arr));
}

function isValidUsaZip(zipCode) {
    //@Todo, make sure of the valid zip format
    if (zipCode.length >= 4) {
        return true;
    }
    return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode);
}














//setInterval(function(){
//    $.get('messages/get_unread_count',function(data){
//        if(data == 0){
//            $('.unread_notification').hide();
//        }else{
//            $('.unread_notification').html(data).show();
//        }
//    });
//},5000);