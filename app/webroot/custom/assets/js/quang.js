$(document).ready(function () {
    $.validator.addMethod("valueNotEquals", function(value, element, arg){
        return arg !== value;
    }, "Value must not equal arg.");

    $("#form-businesspro").validate({
        rules: {
            works: { 
                valueNotEquals: "" 
            },
            'services[]': {
                required: true,
                maxlength: 2
            },
            fname: {
                required: true,
            },
            lname: {
                required: true,
            },
            bname: {
                required: true,
            },
            phone: {
                required: true,
            },
            zip: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            pass: {
                required: true,
                minlength: 8
            },
            confirm: {
                equalTo : "#pass",
                minlength: 8
            },
            check: {
                required: true,
            }
        },
        messages: {
            works: { 
                valueNotEquals: "Please select an item!" 
            },
            'services[]': {
                required: "You must check at least 1 box",
                maxlength: "Check no more than {0} boxes"
            },
            check: { 
                required: "You must check agree our term",
            },
        },
        errorPlacement: function(error, element) {
            if($(element).is(':checkbox')){
                //$(element).parents('.frm-row').addClass('error');
                element.parents('.frm-row').append(error);
            }else{
                element.parent().append(error);
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit(function(e){
                var data_form = $(form).serialize();
                $.ajax({
                    url: "businesspro/register",
                    type: 'POST',
                    dataType: 'json',
                    data: data_form,
                    success: function(response) {
                        if(response.status == 'success'){
                            $('.step-3 .mx-3').append('<div class="alert alert-success" style="margin-top: 1rem;">'+ response.messages +'</div>');
                        }else{
                            $('.step-3 .mx-3').append('<div class="alert alert-danger" style="margin-top: 1rem;">'+ response.messages +'</div>');
                        }
                        setTimeout(function() {
                            $('.alert').remove();
                        }, 5000);
                    }
                });
            });
        }
    });

    $("#form-add-project").validate({
        rules: {
            describe: { 
                required: true,
            },
            budget: { 
                required: true,
            },
            fname: {
                required: true,
            },
            lname: {
                required: true,
            },
            phone: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            question1: {
                valueNotEquals: "select"
            },
            question2: {
                valueNotEquals: "select"
            },
            question3: {
                required: true,
            },
            street: {
                required: true,
            },
            state: {
                valueNotEquals: "select"
            },
            country: {
                valueNotEquals: "select"
            },
            zipcode: {
                required: true,
            },
            password: {
                required: true,
                minlength: 8
            },
            repassword: {
                equalTo : "#password",
                minlength: 8
            },
            check: {
                required: true,
            },
        },
        messages: {
            budget: { 
                required: "Please enter your project budget",
            },
            check: { 
                required: "You must check agree our term",
            },
            question1: {
                valueNotEquals: "Please select an item!" 
            },
            question2: {
                valueNotEquals: "Please select an item!" 
            },
            state: {
                valueNotEquals: "Please select state"
            },
            country: {
                valueNotEquals: "Please select country"
            },
        },
        errorPlacement: function(error, element) {
            if($(element).is(':checkbox')){
                //$(element).parents('.frm-row').addClass('error');
                element.parents('.frm-row').append(error);
            }else{
                element.parent().append(error);
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit(function(e){
                var data_form = $(form).serialize();
                $.ajax({
                    url: "businesspro/add_project",
                    type: 'POST',
                    dataType: 'json',
                    data: data_form,
                    success: function(response) {
                        if(response.status == 'success'){
                            $('.step-4 .mx-3').append('<div class="alert alert-success" style="margin-top: 1rem;">'+ response.messages +'</div>');
                        }else{
                            $('.step-4 .mx-3').append('<div class="alert alert-danger" style="margin-top: 1rem;">'+ response.messages +'</div>');
                        }
                        setTimeout(function() {
                            $('.alert').remove();
                        }, 5000);
                    }
                });
            });
        }
    });

    $('.btn-step').on('click', function (e) {
        e.preventDefault();
        var step = $(this).data('step');
       if($(this).hasClass('btn-back')){
            $('.step-'+step).addClass('active');
            $('.step-'+step).siblings().removeClass('active');
       }else{
            if($("#form-businesspro").length){
                if($("#form-businesspro").valid()){
                    $('.step-'+step).addClass('active');
                    $('.step-'+step).siblings().removeClass('active');
                }
            }else{
                if($("#form-add-project").valid()){
                    $('.step-'+step).addClass('active');
                    $('.step-'+step).siblings().removeClass('active');
                }
            }
       }
    });

    $('#box').change(function (e) { 
        //e.preventDefault();
        if($(this).is(':checked')){
            $('#website').prop('disabled', true);
        }else{
            $('#website').prop('disabled', false);
        }
    });

    $('#works').change(function (e) { 
        e.preventDefault();
        var sub_categories = $(this).val();
        if(sub_categories == 'all'){
            $(this).parent().next().addClass('show');
            $(this).parent().next().find('.output').addClass('show');
        }else if(sub_categories == ''){
            $(this).parent().next().removeClass('show');
        }else{
            $(this).parent().next().addClass('show');
            $(this).parent().next().find('#'+sub_categories).addClass('show');
            $(this).parent().next().find('#'+sub_categories).siblings().removeClass('show');
        }
    });
});