<?php

App::uses('AuthComponent', 'Controller/Component');

class Notification extends AppModel {

    public $name = 'Notification';
   
    
//    var $hasMany = array(
//        'ProUser' => array(
//            'className' => 'ProUser',
//            'dependent' => true
//        )
//    );
    
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'dependent' => true
        )
    );

}