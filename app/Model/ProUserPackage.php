<?php

App::uses('AuthComponent', 'Controller/Component');

class ProUserPackage extends AppModel {

    public $name = 'ProUserPackage';
//    var $hasMany = array(
//        'Service' => array(
//            'className' => 'Service',
//            'dependent' => true
//        )
//    );
    
    var $belongsTo = array(
        'ProUser' => array(
            'className' => 'ProUser',
            'dependent' => true
        )
    );

}
