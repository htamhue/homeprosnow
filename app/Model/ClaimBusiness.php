<?php

App::uses('AuthComponent', 'Controller/Component');

class ClaimBusiness extends AppModel {

    public $name = 'ClaimBusiness';
    var $belongsTo = array(
        'ProUser' => array(
            'className' => 'ProUser',
            'foreignKey' => 'pro_user_id',
            'dependent' => true
        )
    );

}
