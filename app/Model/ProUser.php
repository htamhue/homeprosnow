<?php

App::uses('AuthComponent', 'Controller/Component');

class ProUser extends AppModel {

    public $name = 'ProUser';
//    var $hasOne = array(
//        'ProUserPackage' => array(
//            'className' => 'ProUserPackage',
//            'dependent' => true
//        )
//    );
    
//     public $hasOne = array(
//        'ProUserPackage' => array(
//            'className' => 'ProUserPackage',
//            'dependent' => true
//        )
//    );
//    
    
    var $hasMany = array(
        'CompanyLicense' => array(
            'className' => 'CompanyLicense',
            'dependent' => true
        ),
        'ProUserFile' => array(
            'className' => 'ProUserFile',
            'dependent' => true
        ),
        'ProUserCategorie' => array(
            'className' => 'ProUserCategorie',
            'dependent' => true
        ),
        'ProAward' => array(
            'className' => 'ProAward',
            'dependent' => true
        ),
        'ProOperatingHour' => array(
            'className' => 'ProOperatingHour',
            'dependent' => true
        ),
        'ProServiceArea' => array(
            'className' => 'ProServiceArea',
            'dependent' => true
        ),
//        'ProUserPackage' => array(
//            'className' => 'ProUserPackage',
//            'dependent' => true
//        ),
//        
    );
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'dependent' => true
        ),
        'Category' => array(
            'className' => 'Category',
            'dependent' => true
        )
    );

    function hasProAccount($user_id) {
        $result = $this->find('all', array('conditions' => array('ProUser.user_id' => $user_id)));
        if (count($result)) {
            return true;
        } else {
            return false;
        }
    }

}
