<?php

App::uses('AuthComponent', 'Controller/Component');

class Message extends AppModel {

    public $name = 'Message';

//    var $hasMany = array(
//        'ProUser' => array(
//            'className' => 'ProUser',
//            'dependent' => true
//        )
//    );
//    var $belongsTo = array(
//        'Categorie' => array(
//            'className' => 'Categorie',
//            'dependent' => true
//        )
//    );
    function get_message_threads($user_id, $user_type) {
        $this->loadModel('User');
        $this->loadModel('Project');
        if ($user_type == 'user') {
            $sql = 'SELECT * FROM messages WHERE user_id = ' . $user_id . ' GROUP BY project_quote_id';
        } else {
            $sql = 'SELECT * FROM messages WHERE pro_user_id = ' . $user_id . ' GROUP BY project_quote_id';
        }
        $return = array();
        $results = $this->query($sql);
        foreach ($results as $result) {
            $temp = array();
            $temp['unReadCount'] = $this->find('count', array('conditions' => array('project_quote_id' => $result['messages']['project_quote_id'], 'read_at'=> null)));
            $temp['project_quote_id'] = $result['messages']['project_quote_id'];
            if ($user_type == 'user') {
                $this->User->recursive = -1;
                $temp['perticipent'] = $this->User->findById($result['messages']['pro_user_id']);
            }else {
                $this->User->recursive = -1;
                $temp['perticipent'] = $this->User->findById($result['messages']['user_id']);
            }
            $temp['project'] = $this->Project->findById($result['messages']['project_id']);
            $return[] = $temp;
        }
        return $return;
    }

}
