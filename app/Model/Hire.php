<?php

App::uses('AuthComponent', 'Controller/Component');

class Hire extends AppModel {

    public $name = 'Hire';
   
    
//    var $hasMany = array(
//        'ProUser' => array(
//            'className' => 'ProUser',
//            'dependent' => true
//        )
//    );
    
    var $belongsTo = array(
        'Project' => array(
            'className' => 'Project',
            'dependent' => true
        ),
        'User' => array(
            'className' => 'User',
            'dependent' => true
        )
    );

}