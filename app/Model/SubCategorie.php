<?php

App::uses('AuthComponent', 'Controller/Component');

class SubCategorie extends AppModel {

    public $name = 'SubCategorie';
    var $hasMany = array(
        
    );
    
    var $belongsTo = array(
        'Categorie' => array(
            'className' => 'Categorie',
            'foreignKey' => 'category_id',
            'dependent' => true
        )
    );

}
