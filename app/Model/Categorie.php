<?php

App::uses('AuthComponent', 'Controller/Component');

class Categorie extends AppModel {

    public $name = 'Categorie';
    var $hasMany = array(
        'SubCategorie' => array(
            'className' => 'SubCategorie',
            'dependent' => true
        )
    );

}
