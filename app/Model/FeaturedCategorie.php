<?php

App::uses('AuthComponent', 'Controller/Component');

class FeaturedCategorie extends AppModel {

    public $name = 'FeaturedCategorie';
   
    
//    var $hasMany = array(
//        'ProUser' => array(
//            'className' => 'ProUser',
//            'dependent' => true
//        )
//    );
    
    var $belongsTo = array(
        'Categorie' => array(
            'className' => 'Categorie',
            'foreignKey' => 'category_id',
            'dependent' => true
        )
    );

}