<?php

App::uses('AuthComponent', 'Controller/Component');

class ProjectQuote extends AppModel {

    public $name = 'ProjectQuote';
    var $hasMany = array(
        'ProjectQuoteFile' => array(
            'className' => 'ProjectQuoteFile',
            'dependent' => true
        )
    );
    
    var $belongsTo = array(
        'Project' => array(
            'className' => 'Project',
            'dependent' => true
        ),
        'User' => array(
            'className' => 'User',
            'dependent' => true
        )
    );

}
