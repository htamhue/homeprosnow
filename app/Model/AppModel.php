<?php

/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    function getLastQuery($count = 1) {
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        if ($count == 1) {
            return $lastLog['query'];
        } else {
            $return = array();
            $return[] = $lastLog['query'];
            for ($i = 1; $i < $count; $i++) {
                if ($prevLog = prev($logs['log'])) {
                    $return[] = $prevLog['query'];
                } else {
                    break;
                }
            }
            return implode('<br><br>', $return);
        }
    }

    //This function will return the array off all other fields in the model 
    //except for the fileld provided in $blackList array
    public function blacklist($blackList = array()) {
        return array_diff(array_keys($this->schema()), $blackList);
    }

    function unbindModelAll() {
        $unbind = array();
        foreach ($this->belongsTo as $model => $info) {
            $unbind['belongsTo'][] = $model;
        }
        foreach ($this->hasOne as $model => $info) {
            $unbind['hasOne'][] = $model;
        }
        foreach ($this->hasMany as $model => $info) {
            $unbind['hasMany'][] = $model;
        }
        foreach ($this->hasAndBelongsToMany as $model => $info) {
            $unbind['hasAndBelongsToMany'][] = $model;
        }
        parent::unbindModel($unbind);
    }

    function loadModel($model) {
        if ($this->$model) {
            return $this->$model;
        } else {
            $this->$model = ClassRegistry::init($model);
        }
    }

}
