<?php

App::uses('AuthComponent', 'Controller/Component');

class Project extends AppModel {

    public $name = 'Project';
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'dependent' => true
        ),
        'Categorie' => array(
            'className' => 'Categorie',
            'foreignKey' => 'category_id',
            'dependent' => true
        ),
        'SubCategorie' => array(
            'className' => 'SubCategorie',
            'foreignKey' => 'sub_category_id',
            'dependent' => true
        )
    );
    var $hasMany = array(
        'ProjectFile' => array(
            'className' => 'ProjectFile',
            'dependent' => true
        ),
        'Hire' => array(
            'className' => 'Hire',
            'dependent' => true
        ),
        'SpecifiedPro' => array(
            'className' => 'SpecifiedPro',
            'dependent' => true
        )
    );

}
