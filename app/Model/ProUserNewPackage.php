<?php

App::uses('AuthComponent', 'Controller/Component');

class ProUserNewPackage extends AppModel {

    public $name = 'ProUserNewPackage';
//    var $hasMany = array(
//        'Service' => array(
//            'className' => 'Service',
//            'dependent' => true
//        )
//    );
    
    var $belongsTo = array(
        'ProUser' => array(
            'className' => 'ProUser',
            'dependent' => true
        )
    );

}
