<?php
if ($this->params['controller'] == 'pro' && $this->params['action'] == 'index') {
    ?>
    <div class="top_banner pro_banner">

        <div class="banner_img pro_banner_image">
            <img alt="" src="img/pro_banner.jpg" />
            <!--            <div class="banner_overlay">
            
                        </div>-->
        </div>
        <div class="banner_content">
            <div class="banner_heading">
                Get leads for your business
            </div>
            <div class="banner_sub_heading">
                Thousands of clients are looking for talented pros. Get hired today!
            </div>
            <a href="<?php echo $this->Html->Url(array('controller' => 'pro', 'action' => 'add_pro')); ?>" class="listing_business common_button">LIST YOUR BUSINESS FOR FREE</a>
        </div>
    </div>

<?php } else if ($this->params['controller'] == 'pro' && $this->params['action'] == 'find_more_pro') {
    ?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/banner_one.jpg" />
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content">
            <div class="banner_heading">
                Find top pros near you
            </div>

            <div class="search_area">
                <input type="text" placeholder="zip code" class="zip_code_search" value="<?php echo $this->Session->read('user_zipcode'); ?>" />
                <input type="text" disabled placeholder="I need a" class="search_content" />
                <select class="search_type">
                    <option value=""  selected hidden>Painter or Electrician, etc</option>
                    <?php
                    foreach ($categories as $category) {
                        ?>
                        <option value="<?php echo $category['Categorie']['id'] ?>"><?php echo $category['Categorie']['category'] ?></option>
                        <?php
                    }
                    ?>
                </select>
                <a href="javascript:" class="search_button common_button">Search</a>
            </div>
        </div>
    </div>
    <?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'about_us') {
    ?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/about_us.jpg"  width="100%"/>
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content">
            <div class="banner_heading">
                About Us
            </div>
            <div class="banner_sub_heading">
                We built Homeprosnow to help you with everything you need.
            </div>

        </div>
    </div>
    <?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'career') {
    ?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/carrer_bg.jpg"  width="100%"/>
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content">
            <div class="banner_heading">
                Join Our Team
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
    <?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'email_marketing') {
    ?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/email_marketing_banner.jpg"  width="100%"/>
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Email Marketing
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
    <?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'lead_generation') {
    ?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/lead_generation_banner.jpg"  width="100%"/>
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Lead Generation
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
    <?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'rewarding_partnerships') {
    ?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/rewarding_partnerships_banner.jpg"  width="100%"/>
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Rewarding Partnerships
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
    <?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'seo') {
    ?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/seo_banner.jpg"  width="100%"/>
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Search Engine Optimization
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
    <?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'social_media_marketing') {
    ?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/social_media_marketing_banner.jpg"  width="100%"/>
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Social Media Marketing
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
    <?php
} elseif ($this->params['controller'] == 'home' && $this->params['action'] == 'web_development') {
    ?>
    <div class="top_banner">

        <div class="banner_img">
            <img alt="" src="img/web_development_banner.jpg"  width="100%"/>
            <div class="banner_overlay">

            </div>
        </div>
        <div class="banner_content become_pro_page">
            <div class="banner_heading">
                Website Development & Hosting
            </div>
            <!--            <div class="banner_sub_heading">
                            We built Homeprosnow to help you with everything you need.
                        </div>-->

        </div>
    </div>
    <?php
} else {
    ?></div></div><main>
    <!-- Main jumbotron for a primary marketing message or call to action -->        <div class="jumbotron banner">            <div class="container">                <div class="b-wrapper-text">                    <h2 class="b-title">Find reliable & verified home service providers.</h2>                    <form action="/action_page.php" style="width: 90%;margin-top: 5%;margin-bottom: 4%;">                        <div class="input-container">                            <i class="fa fa-home fa-search" aria-hidden="true"></i>                            <input class="search-inp" type="text" placeholder="What service do you need" name="search">                            <button type="submit" class="btn btn-search">SEARCH</button>                        </div>                    </form>                    <p class="width">                        <button type="button" class="btn btn-outline-secondary bd-rdu"><a href="https://homeprosnow.com/pro/find_pro/category_id:1">Hardwood Floors</a></button>                        <button type="button" class="btn btn-outline-secondary bd-rdu"><a href="https://homeprosnow.com/pro/find_pro/category_id:11">Painting</a></button>                        <button type="button" class="btn btn-outline-secondary bd-rdu"><a href="https://homeprosnow.com/pro/find_pro/category_id:4">Handyman</a></button>                        <button type="button" class="btn btn-outline-secondary bd-rdu"><a href="https://homeprosnow.com/pro/find_pro/category_id:20">Real Estate</a></button>                        <button type="button" class="btn btn-outline-secondary bd-rdu"><a href="https://homeprosnow.com/pro/profinder">...</a></button>                    </p>                </div>            </div>        </div>
<?php } ?>



