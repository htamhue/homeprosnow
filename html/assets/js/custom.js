        function str_repeat(input, multiplier) {
            return new Array(multiplier + 1).join(input);
        }

        function typeOf(obj) {
            if (!obj)
                return "null";
            if (typeof(obj) == 'object') {
                if (obj.length)
                    return 'array';
                else
                    return 'object';
            } else
                return typeof(obj);
        }

        function dump(arr, level) {

            if (!level)
                level = 0;

            // The padding given at the beginning of the line.
            var level_padding = str_repeat("  ", level);
            var dumped_text = level_padding + typeOf(arr) + "\n";
            for (var j = 0; j < level + 1; j++)
                level_padding += "    ";

            if (typeof(arr) == 'object') { // Array/Hashes/Objects
                dumped_text += level_padding + "{\n";
                for (var item in arr) {
                    var value = arr[item];

                    if (typeof(value) == 'object') { // If it is an array,
                        dumped_text += level_padding + "     [" + item + "] =>";
                        dumped_text += dump(value, level + 1);
                    } else {
                        dumped_text += level_padding + "     [" + item + "] => " +
                            value + "\n";
                    }
                }
                dumped_text += level_padding + "}\n";
            } else { // Stings/Chars/Numbers etc.
                dumped_text = "==>" + arr + "<==(" + typeof(arr) + ")";
            }
            return dumped_text;
        }

        function alert_r(arr) {
            alert(dump(arr));
        }

        $(function() {
            $(".notification").hover(
                function() {
                    var that = $(this);
                    $.ajax({
                        url: "notifications/get_notifications",
                        success: function(data) {

                            that.find('.notification_block').html(data);
                        }
                    });
                },
                function() {

                });

            $('.notification').on('click', 'li', function() {
                //                alert($(this).attr('rel'));
                var that = $(this);
                var id = that.attr('rel');
                $.ajax({
                    type: 'POST',
                    async: false,
                    data: { id: id },
                    url: "notifications/read_notifications",
                    success: function(data) {
                        if (data == 'success') {
                            that.removeClass('unRead');
                            var notification_count = parseInt($('.notification_counter').attr('rel'));
                            $('.notification_counter').attr('rel', notification_count - 1).html(notification_count - 1);
                        }
                    }
                });

            });

            $('.notification').on('click', '.view_new_project', function(event) {
                event.preventDefault();
                var url = $(this).attr('href');
                var project_id = $(this).attr('project_id');
                $('#paymentAlertToViewJob').find('.continue_btn').attr('href', url);
                $('#paymentAlertToViewJob').find('.continue_btn').attr('project_id', project_id);
                $('#paymentAlertToViewJob').modal();
            });
            $('.notification').on('click', '.discard_btn', function(event) {
                var that = $(this);
                var id = that.attr('rel');
                $.ajax({
                    type: 'POST',
                    async: false,
                    data: { id: id },
                    url: "notifications/delete_notifications",
                    success: function(data) {
                        if (data == 'success') {
                            that.closest('li').remove();
                        }
                    }
                });
            });
            $('#paymentAlertToViewJob').on('click', '.continue_btn', function(event) {
                event.preventDefault();
                var url = $(this).attr('href');
                var project_id = $(this).attr('project_id');
                $.ajax({
                    type: 'POST',
                    async: false,
                    data: { project_id: project_id },
                    url: "payment/initial_payment_of_project_view",
                    success: function(data) {
                        if (data == 'success') {
                            window.location.href = url;
                        }
                    }
                });

            });
            $('#paymentAlertToViewJob').on('click', '.no_thanks_btn', function() {
                $('#paymentAlertToViewJob').modal('hide');

            });


            $('#givenRating').on('click', '.put_comment_btn', function() {
                var that = $(this);
                var parent = that.closest('.given_rating_area');
                var hireId = parent.find('.hire_id').val();
                var reviewComment = $('.review_comment').val();
                var rating;
                $(".rating").each(function() {
                    if ($(this).is(':checked')) {
                        rating = $(this).val();
                    }
                });

                $.ajax({
                    url: "pro/put_review",
                    type: 'POST',
                    dataType: 'json',
                    data: { id: hireId, reviewComment: reviewComment, rating: rating },
                    success: function(data) {
                        if (data.status == 'success') {
                            $('#givenRating').modal('hide');
                            $('#successModal').find('.details').html('Your review is successfully posted.');
                            $('#successModal').modal();
                            if (data.user_type == 'pro') {
                                $('#hire_' + hireId).closest('.review_button').remove();
                            } else {
                                $('#hire_' + hireId).attr('rel', 'Yes').html('Yes');
                                $('#hire_' + hireId).removeClass('user_review');
                                $('#hire_' + hireId).addClass('show_review');

                            }
                        }

                    }
                });

            });

            $('.single_project_list,.your_project').on('click', '.show_review', function() {
                var hire_id = $(this).attr('hire_id');
                var that = $(this);
                $.ajax({
                    url: "dashboard/show_review",
                    type: 'POST',
                    data: { id: hire_id },
                    success: function(data) {
                        $('#showReview').modal();
                        $('#showReview').find('.show_review_area').html(data);
                    }
                });
            });
            setInterval(function() {
                $.ajax({
                    url: "notifications/get_notification_count",
                    success: function(data) {
                        //                        alert(data);
                        if (data != 0) {
                            $('.notification_counter').attr('rel', data).html(data).show();
                        }
                    }
                });
            }, 30000);

        });


        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-119902182-4');



        $(function() {
            $('.search_area').on('click', '.search_button', function() {
                //alert('s');
                var parent = $(this).closest('.search_area');
                var category_id = parent.find('.search_type').val();
                var zipCode = parent.find('.zip_code_search').val();
                var search_from = $(this).attr('search_from');

                var error;

                if (category_id == 0) {
                    parent.find('.search_content').css('background-color', '#fe8b8b');
                    parent.find('.search_type').css('background-color', '#fe8b8b');
                    error = 1;
                } else {
                    parent.find('.search_content').css('background-color', '#fff');
                    parent.find('.search_type').css('background-color', '#fff');
                }
                //                if (!isValidUsaZip(zipCode)) {
                //                    parent.find('.zip_code_search').css('background-color', '#fe8b8b');
                //                    error = 1;
                //                } else {
                //                    parent.find('.zip_code_search').css('background-color', '#fff');
                //                }

                if (error == 1) {
                    return;
                } else if (search_from == 'profinder') {
                    if (zipCode) {
                        var url = "pro/pro_finder_lists/category_id:" + category_id + "/zip_code:" + zipCode;
                    } else {
                        var url = "pro/pro_finder_lists/category_id:" + category_id;
                    }
                    window.location.href = url;
                } else {
                    var url = "pro/add_project/category_id:" + category_id + "/zip_code:" + zipCode + "/from:search_box";
                    window.location.href = url;
                }
            });
        });

        $(function() {
            $('#works').change(function() {
                $('.output').hide();
                $('#' + $(this).val()).show();
            });
        });